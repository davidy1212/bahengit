﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc
{
    public class ResourceListing
    {
        public Version ApiVersion { get; set; }
        public string BasePath { get; set; }
        public List<ApiSummary> Apis { get; set; }
        public string SwaggerVersion { get; set; }
    }

    public class ApiSummary
    {
        public string Path { get; set; }
        public string Description { get; set; }
    }

    public class ApiPage : ResourceListing
    {
        public string ResourcePath { get; set; }
        public string[] Produces { get; set; }
        public List<object> Models { get; set; }
    }

    public class Api : ApiSummary
    {
        public string Path { get; set; }
        public string Description { get; set; }
        public List<ApiOperation> Operations { get; set; }
    }

    public class ApiOperation
    {
        public HttpMethod HttpMethod { get; set; }
        public string NickName { get; set; }
        public string ResponseClass { get; set; }
        public string Summary { get; set; }
        public string Notes { get; set; }
        public string[] Produces { get; set; }
        public List<ApiParameter> Parameters { get; set; }
    }

    public class ApiParameter
    {
        public ParameterType ParamType { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataType { get; set; }
        public bool Required { get; set; }
        public bool AllowMultiple { get; set; }
    }

    public class Model
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class ModelProperty
    {
        public string Type { get; set; }
        public bool Required { get; set; }
        public string Description { get; set; }
        public AllowableValues AllowableValues { get; set; }
    }

    public class ContainerProperty : ModelProperty
    {
        public ContainerItem Items { get; set; }
    }

    public class ContainerItem
    {
        [JsonProperty("$ref")]
        public string Reference { get; set; }
    }

    public class AllowableValues
    {
        public ValueType ValueType { get; set; }
        public List<object> Values { get; set; }
        public long? Min { get; set; }
        public long? Max { get; set; }
    }

    public enum HttpMethod
    {
        GET, PUT, POST, DELETE
    }

    public enum ParameterType
    {
        Path, Query, Body
    }

    public enum ValueType
    {
        LIST, RANGE
    }

    public enum ContainerType
    {
        List, Set, Array
    }
}
