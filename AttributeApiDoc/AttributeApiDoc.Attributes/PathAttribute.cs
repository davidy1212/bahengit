﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class PathAttribute : Attribute
    {
        // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        readonly string path;

        // This is a positional argument
        public PathAttribute(string path)
        {
            this.path = path;

            // TODO: Implement code here
            throw new NotImplementedException();
        }

        public string Path
        {
            get { return path; }
        }

        // This is a named argument
        public string Description { get; set; }
    }
}
