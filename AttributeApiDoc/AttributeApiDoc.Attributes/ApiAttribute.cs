﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class ApiAttribute : Attribute
    {
        // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236

        // This is a positional argument
        public ApiAttribute()
        {
            NickName = String.Empty;
            Summary = String.Empty;
            Note = String.Empty;
        }

        public string NickName { get; set; }

        public string Summary { get; set; }

        public string Note { get; set; }
    }
}
