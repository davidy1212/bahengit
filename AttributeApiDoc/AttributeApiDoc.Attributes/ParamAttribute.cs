﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc
{
    [AttributeUsage(AttributeTargets.Parameter|AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public sealed class ParamAttribute : Attribute
    {
        public ParamAttribute()
        {
            this.Description = String.Empty;
            this.Required = true;
        }

        // This is a named argument
        public string Description { get; set; }

        public bool Required { get; set; }

        public ParameterType Type { get; set; }
    }
}
