﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc
{
    internal class ProgramOptions
    {
        internal string[] Assemblies { get; set; }
        internal string BasePath { get; set; }
        internal string OutputFolder { get; set; }
    }
}
