﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApiDoc
{
    /// <summary>
    /// This tool will generate Swagger compatible JSON files for .NET Web API documentation.
    /// Arguments:
    /// 1. /assembly: or /a: Assemblies where the ApiController classes locates, use comma as delimiters
    /// 2. /url: or /u: Url of the API endpoint
    /// 3. /out: or /o: Output folder of the JSON documents (by default current executable folder)
    /// 4. /help or /? to get help
    /// </summary>
    internal class Program
    {
        internal const string HelpDoc =
@"AttributeApiDoc - Generates Swagger compliant JSON API documentations.

Summary:

The program will read the input assemblies and scan for
ApiController derived classes and look for Attributes,
generate static JSON documents in the output folder specified, 
for Swagger - UI to display.

Arguments:

1. /assembly: or /a: Assemblies where the ApiController classes locates, use comma as delimiters (required)
2. /url: or /u: Base url of the API endpoint (required)
3. /out: or /o: Output folder of API documentations (optional, executable path by default)
4. /help or /? Print help documentation of this tool

Examples:

AttributeApiDoc.exe /a:test.dll /u:http://localhost > Scan test.dll with base API path to be localhost, 
and generate JSON docs in local folder.

AttributeApiDoc.exe /a:test.dll /u:http://api.example.com /o:../bin/Debug > Scan test.dll with api.example.com
to be base path of API, and generates JSON docs in ../bin/Debug";

        internal const string AssemblyPrefix = "/assembly:";
        internal const string AssemblyPrefixShort = "/a:";
        internal const string UrlPrefix = "/url:";
        internal const string UrlPrefixShort = "/u:";
        internal const string OutputPrefix = "/output:";
        internal const string OutputPrefixShort = "/o:";
        internal const string HelpPrefix = "/help";
        internal const string HelpPrefixShort = "/?";

        internal static void Main(string[] args)
        {
            try
            {
                ProgramOptions options = ParseArguments(args);
                if (options == null)
                {
                    return;
                }
                ResourceListing listing = GenerateResourceListing(options.Assemblies.Select(
                    x => Assembly.LoadFile(x)));
                SerializeResourceListing(listing);
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0} occured because of {1}", ex.GetType().Name, ex.Message);
            }
        }

        internal static ProgramOptions ParseArguments(string[] args)
        {
            if (args.Length < 2)
            {
                throw new ArgumentException("Invalid argument size");
            }

            ProgramOptions options = new ProgramOptions();
            foreach (string arg in args)
            {
                if (arg.StartsWith(HelpPrefix) || arg.StartsWith(HelpPrefixShort))
                {
                    Console.WriteLine(HelpDoc);
                    return null;
                }
                else if (arg.StartsWith(AssemblyPrefix) || arg.StartsWith(AssemblyPrefixShort))
                {
                    options.Assemblies = GetValueFromArgument(arg).Split(',');
                }
                else if (arg.StartsWith(UrlPrefix) || arg.StartsWith(UrlPrefixShort))
                {
                    options.BasePath = GetValueFromArgument(arg);
                }
                else if (arg.StartsWith(OutputPrefix) || arg.StartsWith(OutputPrefixShort))
                {
                    options.OutputFolder = GetValueFromArgument(arg);
                }
                else
                {
                    throw new ArgumentException(String.Format("Unrecognized argument: {0}", arg));
                }
            }

            if (options.Assemblies == null)
            {
                throw new ArgumentException("No assembly specified");
            }
            else if (options.BasePath == null)
            {
                throw new ArgumentException("No path specified");
            }
            else if (options.OutputFolder == null)
            {
                options.OutputFolder = ".";
            }

            return options;
        }

        internal static string GetValueFromArgument(string arg)
        {
            int colonIndex = arg.IndexOf(':');
            if (colonIndex == arg.Length - 1 || colonIndex == -1)
            {
                throw new ArgumentException(String.Format("Invalid Argument: {0}. Cannot separate value from the argument.", arg));
            }
            return arg.Substring(colonIndex + 1, arg.Length - colonIndex - 1);
        }

        internal static ResourceListing GenerateResourceListing(IEnumerable<Assembly> assemblies)
        {
            throw new NotImplementedException();
        }

        internal static void SerializeResourceListing(ResourceListing listing)
        {
            throw new NotImplementedException();
        }
    }
}
