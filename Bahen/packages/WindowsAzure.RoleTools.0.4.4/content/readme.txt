﻿README
Please note, you MUST add a reference path to "c:\windows\system32\inetsrv\" 
in order for the Microsoft.Web.Administration.dll to resolve properly.

Otherwise, it will try to use the one from the GAC, which will throw a
COM Exception and none of the configuration options will be applied.