﻿using Bahen.Api.Models;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bahen.Api.Common
{
    public class LikeProvider
    {
        public LikeProvider(ListApiOptions options)
        {
            this.Options = options;
        }

        public ListApiOptions Options { get; set; }

        public CountedListResponse<Like> GetLikes(string targetString, EntityTypeCode targetTypeCode)
        {
            CountedListResponse<Like> response = new CountedListResponse<Like>();
            response.Data = this.GetLikesPagedData(targetString, targetTypeCode);
            var countOp= new TableLikeCountOperator(ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            int count = countOp.GetCount(targetString, targetTypeCode);
            response.Summary = new CountSummary(count);

            if (response.Data.Count == 0)
            {
                response.Paging = null;
            }
            else
            {
                response.Paging = new PagingLinks(
                    this.Options.RequestUri,
                    null,
                    response.Data.Last().StringId);
            }
            return response;
        }

        protected IList<Like> GetLikesPagedData(string targetString, EntityTypeCode targetTypeCode)
        {
            TableLikeOperator op = new TableLikeOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            SqlUserOperator uop = new SqlUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            var raw = op.Truncate(
                op.GetByEntityID(targetString, targetTypeCode), this.Options).ToList();

            List<Like> result = raw
                .Select<TableLikeEntity, Like>
                (x =>
                {
                    var user = uop.Get(x.UserID);
                    var userLike =
                    new Like()
                    {
                        UserID = x.UserID,
                        UserName = user.PreferredName,
                        //ProfilePic = user.ProfilePicture,
                        StringId = x.RowKey
                    };
                    return userLike;
                }).ToList();

            return result;
        }
    }
}
