﻿using Bahen.Api.Models;
using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Api.Common
{
    public class EventProvider
    {
        public EventDetail GetDetail(EventEntity entity)
        {
            return this.GetDetail(entity, new FieldsFilter());
        }

        public EventDetail GetDetail(EventEntity entity, FieldsFilter filter)
        {
            return new EventDetail()
            {
                ID = entity.ID,
                Name = entity.Name,
                Location = entity.Address.ToString(),
                StartTime = entity.StartTime,
                EndTime = entity.EndTime,
                BriefDescription = entity.DescriptionAbstract,
                Picture = filter.Contains("picture") ? entity.Picture : null,
                IsRecurring = entity.IsRecurring,
                RecurringType = entity.IsRecurring ? new EventRecurringType?(entity.RecurringFrequency) : null,
                RecurringTimes = entity.IsRecurring ? new int?(entity.RecurringTimes) : null,
                RecurringExceptions = entity.IsRecurring ? entity.RecurringExceptions : null,
                Alias = entity.Alias,
                Categories = entity.Categories.ToArray(),
                Coordinate = filter.Contains("coordinate") ? entity.Coordinate : null,
                Publisher = filter.Contains("publisher") ? new AttendanceProvider().GetAttendant(entity.ID, entity.PublisherID) : null,
                CoPublishers = filter.Contains("copublishers") ? new AttendanceProvider().GetAttendants(entity.ID, AttendanceStatus.Copublisher).Data.ToArray() : null,
                Keywords = filter.Contains("keywords") ? entity.SearchKeywords.ToArray() : null,
                Address = filter.Contains("address") ? entity.Address : null
            };
        }
    }
}
