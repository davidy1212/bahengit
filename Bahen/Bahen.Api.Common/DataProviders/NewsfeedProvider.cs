﻿using Bahen.Api.Models;
using Bahen.Common;
using Bahen.Data.Operation;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Bahen.Api.Common
{
    /// <summary>
    /// Provide newsfeed data (readonly) directly to web api
    /// </summary>
    public class NewsfeedProvider
    {
        /// <summary>
        /// Construct a newsfeed provider with paging options
        /// </summary>
        /// <param name="options"></param>
        public NewsfeedProvider(ListApiOptions options)
        {
            this.Options = options;
        }

        /// <summary>
        /// Paging options
        /// </summary>
        public ListApiOptions Options { get; set; }

        /// <summary>
        /// Get the newsfeeds
        /// </summary>
        /// <param name="userId">User ID of the recipient</param>
        /// <returns></returns>
        public ListResponse<Newsfeed> GetNewsfeeds(long userId)
        {
            ListResponse<Newsfeed> response = new ListResponse<Newsfeed>();
            response.Data = this.GetNewsfeedsPagedData(userId);

            if (response.Data.Count == 0)
            {
                response.Paging = null;
            }
            else
            {
                response.Paging = new PagingLinks(
                    this.Options.RequestUri,
                    null,
                    response.Data.Last().StringId);
            }
            return response;
        }

        /// <summary>
        /// Get list data result from table storage
        /// </summary>
        /// <param name="userId">User ID of the recipient</param>
        /// <returns></returns>
        protected IList<Newsfeed> GetNewsfeedsPagedData(long userId)
        {
            TableNewsfeedOperator op = new TableNewsfeedOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            SqlUserOperator uop = new SqlUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            var raw = op.Truncate(
                op.GetAllItems(userId), this.Options).ToList();

            List<Newsfeed> result = raw
                .Join(
                uop.Set,
                x => x.SenderID,
                y => y.ID,
                (x, y) => new Newsfeed()
                {
                    Content = x.Content,
                    From = new NewsfeedSender()
                    {
                        ID = y.ID,
                        Name = y.PreferredName
                    },
                    StringId = x.RowKey,
                    TimeSent = x.TimeSent.ToUnixTime()
                }).ToList();

            return result;
        }
    }
}