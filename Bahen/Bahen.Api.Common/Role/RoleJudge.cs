﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Api.Common
{
    public static class RoleJudge
    {
        public static UserRoleLevel GetRole(this IPrincipal applicant)
        {
            LoginPrincipal principal = applicant as LoginPrincipal;
            if (principal == null)
            {
                return UserRoleLevel.User;
            }
            else
            {
                return principal.Role;
            }
        }

        public static bool HasRole(this IPrincipal applicant, UserRoleLevel role)
        {
            return applicant.IsInRole(role.ToString());
        }
    }
}
