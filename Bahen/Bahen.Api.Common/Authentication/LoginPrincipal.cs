﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Bahen.Api.Common
{
    public class LoginPrincipal : IPrincipal
    {
        public LoginIdentity Identity { get; set; }

        public bool IsInRole(string role)
        {
            // Need to add more stuff here.
            if (role.ToLowerInvariant() == Role.ToString().ToLowerInvariant())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public UserRoleLevel Role { get; set; }

        IIdentity IPrincipal.Identity
        {
            get { return this.Identity; }
        }
    }

    public class LoginIdentity : IIdentity
    {
        public string AuthenticationType
        {
            get
            {
                return Type.ToString().ToLowerInvariant() + ((Type == AuthType.OAuth) ? (":" + Scopes.ToString()) : "");
            }
        }

        public AuthType Type { get; set; }

        public OAuthScopes Scopes { get; set; }

        public bool IsAuthenticated { get; set; }

        public string Name
        {
            get
            {
                return ID.ToString();
            }
        }

        public long ID { get; set; }
    }

    public enum AuthType
    {
        Forms, OAuth
    }
}