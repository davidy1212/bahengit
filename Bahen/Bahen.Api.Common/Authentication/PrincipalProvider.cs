﻿using Bahen.Api.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Configuration;
using System.Security.Principal;

namespace Bahen.Api.Common
{
    public static class PrincipalProvider
    {
        /// <summary>
        /// Check if an authorization token is valid, if yes then return the logged in user principal
        /// </summary>
        /// <param name="token">Authorization token</param>
        /// <returns>Logged in user principal</returns>
        public static LoginPrincipal GetPrincipal(string token)
        {
            AccessToken atoken;
            if (!TokenFactory.ValidateToken<AccessToken>(token, out atoken))
            {
                throw new OAuthException(OAuthError.AccessDenied, "invalid access token");
            }
            if (atoken.Expires < DateTime.UtcNow)
            {
                throw new OAuthException(OAuthError.AccessDenied, "access token expired");
            }
            return new LoginPrincipal()
            {
                Identity = new LoginIdentity()
                {
                    Type = AuthType.OAuth,
                    Scopes = (OAuthScopes)atoken.Scope,
                    IsAuthenticated = true,
                    ID = atoken.UserId
                },
                Role = (UserRoleLevel)atoken.Role
            };
        }

        /// <summary>
        /// Get the current logged in user ID
        /// </summary>
        /// <param name="principal">User logged in principal object saved in context</param>
        /// <returns>User ID</returns>
        public static long GetID(this IPrincipal principal)
        {
            long result;
            try
            {
                Int64.TryParse(principal.Identity.Name, out result);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot parse ID from authentication data.", ex);
            }
            return result;
        }


        /// <summary>
        /// Get the user data reference from the database using the user ID
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static UserEntity GetUser(this IPrincipal principal)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            UserEntity user = userOperator.Get(principal.GetID());
            return user;
        }
    }
}
