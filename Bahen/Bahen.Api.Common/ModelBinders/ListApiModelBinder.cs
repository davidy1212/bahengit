﻿using Bahen.Api.Models;
using Bahen.Common;
using Bahen.Data.Operation;
using System;
using System.Web;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;

namespace Bahen.Api.Common
{
    /// <summary>
    /// Helps list api to bind the parameters to ListApiOptions model
    /// </summary>
    public class ListApiModelBinder : IModelBinder
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var name = bindingContext.ModelName;

            ValueProviderResult after = bindingContext.ValueProvider.GetValue("after");
            ValueProviderResult before = bindingContext.ValueProvider.GetValue("before");
            ValueProviderResult offset = bindingContext.ValueProvider.GetValue("offset");
            ValueProviderResult until = bindingContext.ValueProvider.GetValue("until");
            ValueProviderResult limit = bindingContext.ValueProvider.GetValue("limit");
            ValueProviderResult summary = bindingContext.ValueProvider.GetValue("summary");
            ListApiOptions options = new ListApiOptions();

            if (limit == null)
            {
                options.Limit = Constants.ListDefaultLimit;
            }
            else
            {
                int value;
                if (Int32.TryParse(limit.AttemptedValue, out value))
                {
                    options.Limit = value;
                }
                else
                {
                    return false;
                }
            }
            if (summary == null)
            {
                options.Summary = false;
            }
            else
            {
                bool value;
                if (Boolean.TryParse(summary.AttemptedValue, out value))
                {
                    options.Summary = value;
                }
                else
                {
                    return false;
                }
            }
            if (name == "Int64")
            {
                if (after == null && offset == null && before == null)
                {
                    options.Mode = PagingMode.Offset;
                    options.Offset = 0;
                }
                else if (after != null && offset == null && before == null)
                {
                    options.Mode = PagingMode.CursorAfterInt64;
                    options.AfterInt64 = HttpServerUtility.UrlTokenDecode(after.AttemptedValue).ToInt64();
                }
                else if (after == null && offset != null && before == null)
                {
                    options.Mode = PagingMode.Offset;
                    int value;
                    if (Int32.TryParse(offset.AttemptedValue, out value))
                    {
                        options.Offset = value;
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (after == null && offset == null && before != null)
                {
                    options.Mode = PagingMode.CursorBeforeInt64;
                    options.BeforeInt64 = HttpServerUtility.UrlTokenDecode(before.AttemptedValue).ToInt64();
                }
                else
                {
                    return false;
                }
            }
            else if (name == "String")
            {
                if (after == null && until == null)
                {
                    options.Mode = PagingMode.CursorAfterString;
                    options.AfterString = String.Empty;
                }
                else if (after != null && until == null)
                {
                    options.Mode = PagingMode.CursorAfterString;
                    options.AfterString = after.AttemptedValue;
                }
                else if (after == null && until != null)
                {
                    options.Mode = PagingMode.TimeUntil;
                    long value;
                    if (Int64.TryParse(until.AttemptedValue, out value))
                    {
                        options.Until = value;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            options.RequestUri = actionContext.Request.RequestUri;
            bindingContext.Model = options;
            return true;
        }
    }

    public class ListApiBinderProvider : ModelBinderProvider
    {
        public override IModelBinder GetBinder(System.Web.Http.HttpConfiguration configuration, Type modelType)
        {
            return new ListApiModelBinder();
        }
    }
}
