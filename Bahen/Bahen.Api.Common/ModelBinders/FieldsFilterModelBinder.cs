﻿using Bahen.Api.Models;
using System;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;

namespace Bahen.Api.Common
{
    public class FieldsFilterModelBinder : IModelBinder
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult fields = bindingContext.ValueProvider.GetValue("fields");
            if (fields != null)
            {
                bindingContext.Model = new FieldsFilter(fields.AttemptedValue);
                return true;
            }
            else
            {
                if (bindingContext.ModelName == "Optional")
                {
                    bindingContext.Model = new FieldsFilter();
                    return true;
                }
                return false;
            }
        }
    }

    public class FieldsFilterModelBinderProvider : ModelBinderProvider
    {
        public override IModelBinder GetBinder(System.Web.Http.HttpConfiguration configuration, Type modelType)
        {
            return new FieldsFilterModelBinder();
        }
    }
}
