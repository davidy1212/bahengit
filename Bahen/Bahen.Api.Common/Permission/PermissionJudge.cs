﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Api.Common
{
    public static class PermissionJudge
    {
        public static bool HasPermission(this IPrincipal applicant, OAuthScopes scope)
        {
            if (applicant.Identity.IsAuthenticated)
            {
                LoginPrincipal principal = applicant as LoginPrincipal;
                if (principal == null){
                    return false;
                }else{
                    return principal.Identity.Scopes.HasFlag(scope);
                }
            }
            else
            {
                return false;
            }
        }
    }
}
