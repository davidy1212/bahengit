﻿using System;
using System.Web;

namespace Bahen.Api.Common
{
    public static class Cache
    {
        public static bool IsClientCached(HttpContext context, DateTime contentModified)
        {
            string header = context.Request.Headers["If-Modified-Since"];

            if (header != null)
            {
                DateTime isModifiedSince;
                if (DateTime.TryParse(header, out isModifiedSince))
                {
                    return isModifiedSince > contentModified;
                }
            }

            return false;
        }
    }
}