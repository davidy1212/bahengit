﻿using Bahen.Data.Models;
using System;
using System.Drawing;

namespace Bahen.Api.Common
{
    /// <summary>
    /// Utility class that resizes 
    /// </summary>
    public class PictureResizer
    {
        /// <summary>
        /// Resize the image by constrain the maximum width or height
        /// </summary>
        /// <param name="original">Original image</param>
        /// <param name="maxWidthHeight">Maximum width or height</param>
        /// <returns>Resized image</returns>
        public static Image ResizeImage(Image original, int maxWidthHeight)
        {
            // Compute the new size for a large image
            Size newSize = new Size();
            if (original.Size.Width > original.Size.Height)
            {
                if (original.Size.Width > maxWidthHeight)
                {
                    newSize.Width = maxWidthHeight;
                    newSize.Height = maxWidthHeight * original.Size.Height / original.Size.Width;
                }
                else
                {
                    newSize = original.Size;
                }
            }
            else
            {
                if (original.Size.Height > maxWidthHeight)
                {
                    newSize.Height = maxWidthHeight;
                    newSize.Width = maxWidthHeight * original.Size.Width / original.Size.Height;
                }
                else
                {
                    newSize = original.Size;
                }
            }
            return new Bitmap(original, newSize);
        }

        /// <summary>
        /// Resize the image by specifying the picture size
        /// </summary>
        /// <param name="original">Original image</param>
        /// <param name="size">Picture size</param>
        /// <returns>Resized image</returns>
        public static Image ResizeImage(Image original, PictureSize size)
        {
            switch (size)
            {
                case (PictureSize.Thumb):
                    {
                        int square = Math.Min(original.Width, original.Height);
                        Rectangle cropRect = new Rectangle(
                            original.Width < original.Height ? 0 : (original.Height - original.Width) / 2,
                            original.Height < original.Width ? (original.Width - original.Height) / 2 : 0,
                        square, square);
                        Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);
                        using (Graphics g = Graphics.FromImage(target))
                        {
                            g.DrawImage(original, new Rectangle(0, 0, target.Width, target.Height),
                                cropRect,
                                GraphicsUnit.Pixel);
                        }
                        return new Bitmap(target, new Size(50, 50));
                    }
                case (PictureSize.Medium):
                    {
                        return ResizeImage(original, 300);
                    }
                case (PictureSize.Large):
                    {
                        return ResizeImage(original, 600);
                    }
                default:
                    {
                        return ResizeImage(original, 300);
                    }
            }
        }
    }
}