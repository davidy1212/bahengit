﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System.Configuration;
using System.Linq;

namespace Bahen.Api.Common
{
    public class CommentUtilities
    {
        static TableCommentOperator CommentOperator = new TableCommentOperator(
                                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        public static void PostComment(string commentContent, string targetString, EntityTypeCode targetTypeCode, long userID)
        {
            TableCommentEntity commentEntity = new TableCommentEntity(targetString, targetTypeCode, userID);
            commentEntity.Content = commentContent;
            CommentOperator.Insert(commentEntity);
        }

        public static void DeleteComment(string targetString, EntityTypeCode targetTypeCode, string rowkey)
        {
            CommentOperator.Remove(TableCommentEntity.ComputePartitionKey(targetString, targetTypeCode), rowkey);
        }


        /// <summary>
        /// Determine if an user has commented an entity
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="targetID"></param>
        /// <param name="targetTypeCode"></param>
        /// <returns></returns>
        public static bool UserComments(long userID, string targetString, EntityTypeCode targetTypeCode)
        {
            var ListOfComments = CommentOperator.GetByEntityID(targetString, targetTypeCode);
            var result = ListOfComments.Where(x => x.UserID == userID).ToList();
            if (result.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


}
