﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahen.Api.Common
{
    /// <summary>
    /// Json serialization contract resolver that only serializes selected property names
    /// </summary>
    public class OptionalPropertiesContractResolver : CamelCasePropertyNamesContractResolver
    {
        IEnumerable<string> IncludedProperties { get; set; }
        public OptionalPropertiesContractResolver(IEnumerable<string> includedProperties)
            : base()
        {
            this.IncludedProperties = includedProperties;
        }

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(type, memberSerialization).Where(
                prop => IncludedProperties.Contains(prop.PropertyName)).ToList();
        }
    }
}