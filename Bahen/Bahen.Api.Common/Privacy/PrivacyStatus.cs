﻿
namespace Bahen.Api.Common
{
    public enum PrivacyStatus
    {
        Blocked, UponRequest, Allowed
    }
}