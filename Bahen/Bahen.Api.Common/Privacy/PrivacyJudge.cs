﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;

namespace Bahen.Api.Common
{
    /// <summary>
    /// A judge to examine whether the current logged in user has the permission to view the data from the other user
    /// </summary>
    public static class PrivacyJudge
    {
        /// <summary>
        /// Examine the permission of an user based on the privacy policy settings
        /// </summary>
        /// <param name="applicantId">Current logged in user</param>
        /// <param name="code">Target type</param>
        /// <param name="targetUserId">User of the resource owner</param>
        /// <param name="policy">Resource owner's privacy settings</param>
        /// <returns>Whether the permission is granted</returns>
        public static PrivacyStatus GetPrivacyStatus(long applicantId, EntityTypeCode code, long targetUserId, PrivacyPolicy policy)
        {
            if (applicantId == targetUserId)
            {
                return PrivacyStatus.Allowed;
            }
            if (IsInPrivacyGroup(applicantId, code, targetUserId, policy.Blocked))
            {
                return PrivacyStatus.Blocked;
            }
            if (IsInPrivacyGroup(applicantId, code, targetUserId, policy.UponRequest))
            {
                return PrivacyStatus.UponRequest;
            }
            if (IsInPrivacyGroup(applicantId, code, targetUserId, policy.Allowed))
            {
                return PrivacyStatus.Allowed;
            }
            return PrivacyStatus.Blocked;
        }

        /// <summary>
        /// Check whether the applicant is allowed to access the target resource
        /// </summary>
        /// <param name="applicantId">Applicant user ID</param>
        /// <param name="code">Type of the requested target</param>
        /// <param name="targetId">Target resource ID</param>
        /// <param name="policy">Privacy policy from target user</param>
        /// <returns></returns>
        public static bool CheckPrivacy(long applicantId, EntityTypeCode code, long targetId, PrivacyPolicy policy)
        {
            switch (PrivacyJudge.GetPrivacyStatus(applicantId,code, targetId, policy))
            {
                case (PrivacyStatus.Allowed):
                    {
                        return true;
                    }
                case (PrivacyStatus.UponRequest):
                    {
                        throw new PrivacyPolicyException("Need to request premission");
                    }
                case (PrivacyStatus.Blocked):
                    {
                        throw new PrivacyPolicyException("No premission");
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        /// <summary>
        /// Check whether the applicant is allowed to access the target resource
        /// </summary>
        /// <param name="applicant"></param>
        /// <param name="code"></param>
        /// <param name="targetId"></param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static bool CheckPrivacy(IPrincipal applicant, EntityTypeCode code, long targetId, PrivacyPolicy policy)
        {
            return CheckPrivacy(applicant.Identity.IsAuthenticated ? applicant.GetID() : 0, code, targetId, policy);
        }

        /// <summary>
        /// Check whether a user is defined in the privacy group
        /// </summary>
        /// <param name="applicantId">Applicant user ID</param>
        /// <param name="targetId">Target user ID</param>
        /// <param name="group">Privacy group definition</param>
        /// <returns></returns>
        private static bool IsInPrivacyGroup(long applicantId, EntityTypeCode code, long targetId, PrivacyGroup group)
        {
            if (group.Relations.HasFlag(Relation.Public))
            {
                return true;
            }

            if (code == EntityTypeCode.User)
            {
                if (group.Relations.HasFlag(Relation.Followers))
                {
                    SqlSubscriptionOperator subOp = new SqlSubscriptionOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                    return subOp.GetSubscriberIDsByHost(targetId).Contains(applicantId);
                }
            }

            if (code == EntityTypeCode.Event)
            {
                if (group.Relations.HasFlag(Relation.Invited))
                {
                    SqlAttendanceOperator atOp = new SqlAttendanceOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                    return atOp.GetUserIDsByEvent(applicantId).Contains(applicantId);
                }
            }

            if (group.Users.Contains(applicantId))
            {
                return true;
            }

            SqlCycleUserOperator cycleOp = new SqlCycleUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            IEnumerable<long> cycles = cycleOp.GetCycleIDsByUser(applicantId);
            foreach (long cycleId in group.Cycles)
            {
                if (cycles.Contains(cycleId))
                {
                    return true;
                }
            }

            return false;
        }
    }
}