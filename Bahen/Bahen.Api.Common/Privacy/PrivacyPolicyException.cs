﻿using System;

namespace Bahen.Api.Common
{
    [Serializable]
    public class PrivacyPolicyException : Exception
    {
        public PrivacyPolicyException() { }
        public PrivacyPolicyException(string message) : base(message) { }
        public PrivacyPolicyException(string message, Exception inner) : base(message, inner) { }
        protected PrivacyPolicyException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}