﻿using System;

namespace Bahen.Api.Common
{
    [Serializable]
    public class InsufficientRoleException : Exception
    {
        public InsufficientRoleException() { }
        public InsufficientRoleException(string message) : base(message) { }
        public InsufficientRoleException(string message, Exception inner) : base(message, inner) { }
        protected InsufficientRoleException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}