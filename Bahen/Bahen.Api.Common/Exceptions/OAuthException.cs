﻿using Bahen.Common;
using System;

namespace Bahen.Api.Common
{
    [Serializable]
    public class OAuthException : Exception
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }

        public OAuthException(OAuthError error, string errorDescription)
            : base(error.ToDescriptionString() + ": " + errorDescription)
        {
            this.Error = error.ToDescriptionString();
            this.ErrorDescription = errorDescription;
        }
    }
}