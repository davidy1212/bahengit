﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Bahen.Api.Common
{
    public class OAuthExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is AuthenticationException)
            {
                context.Response = FormatMessage(HttpStatusCode.Unauthorized, context.Exception.Message);
            }
            else if (context.Exception is OAuthException)
            {
                string content = "{\"error\":" + (context.Exception as OAuthException).Error + ",\"error_description\":\"" + (context.Exception as OAuthException).ErrorDescription + "\"}";
                context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    ReasonPhrase = "An exception occurred",
                    Content = new StringContent(content)
                };
            }
            else
            {
                context.Response = FormatMessage(HttpStatusCode.NotFound, context.Exception.Message);
            }
        }

        private static HttpResponseMessage FormatMessage(HttpStatusCode code, string message)
        {
            return new HttpResponseMessage(code)
            {
                ReasonPhrase = "An exception occurred",
                Content = new StringContent("{" + String.Format("\"message\":\"{0}\"", message) + "}")
            };
        }
    }
}
