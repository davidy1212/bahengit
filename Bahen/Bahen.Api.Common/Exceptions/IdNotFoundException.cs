﻿using System;

namespace Bahen.Api.Common
{
    [Serializable]
    public class IdNotFoundException : Exception
    {
        public IdNotFoundException() { }
        public IdNotFoundException(string message) : base(message) { }
        public IdNotFoundException(string message, Exception inner) : base(message, inner) { }

        public IdNotFoundException(long id) : this(String.Format("invalid id: {0}", id)) { }

        protected IdNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}