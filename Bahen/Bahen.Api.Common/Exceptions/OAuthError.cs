﻿using System.ComponentModel;

namespace Bahen.Api.Common
{
    public enum OAuthError
    {
        [Description("invalid_request")]
        InvalidRequest,
        [Description("unauthorized_request")]
        UnauthorizedRequest,
        [Description("access_denied")]
        AccessDenied,
        [Description("unsupported_response_type")]
        UnsupportedResponseType,
        [Description("invalid_scope")]
        InvalidScope,
        [Description("server_error")]
        ServerError,
        [Description("temporarily_unavailable")]
        TemporarilyUnavailable
    }
}
