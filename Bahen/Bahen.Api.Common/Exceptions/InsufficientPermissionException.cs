﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Api.Common
{
    [Serializable]
    public class InsufficientPermissionException : Exception
    {
        public InsufficientPermissionException() : this("no permission") { }
        public InsufficientPermissionException(string message) : base(message) { }
        public InsufficientPermissionException(string message, Exception inner) : base(message, inner) { }
        protected InsufficientPermissionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
