﻿using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bahen.Api.Common
{
    /// <summary>
    /// Extension for server requests and responses
    /// </summary>
    public static class ServerExtensions
    {
        /// <summary>
        /// Context key saved in HttpRequestMessage
        /// </summary>
        private const string HttpContextKey = "MS_HttpContext";

        /// <summary>
        /// Try get the http context from HttpRequestMessage, useful in controller filters
        /// </summary>
        /// <param name="requestMessage">HttpRequestMessage</param>
        /// <param name="httpContext">Result of HttpContext</param>
        /// <returns>Whether successful in retrieving the context</returns>
        public static bool TryGetHttpContext(this HttpRequestMessage requestMessage, out HttpContextBase httpContext)
        {
            httpContext = null;
            object obj;
            if (requestMessage.Properties.TryGetValue(HttpContextKey, out obj))
            {
                httpContext = (HttpContextBase)obj;
            }

            return httpContext != null;
        }
        
        /// <summary>
        /// Read the stream data from form upload data in HttpRequestMessage
        /// </summary>
        /// <param name="message">Request message</param>
        /// <returns>Task of reading stream</returns>
        public static Task<Stream> ReadFromFormData(this HttpRequestMessage message)
        {
            if (!message.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            MultipartMemoryStreamProvider provider = new MultipartMemoryStreamProvider();

            return message.Content.ReadAsMultipartAsync(provider).ContinueWith(
                readTask =>
                {
                    if (readTask.IsFaulted || readTask.IsCanceled)
                    {
                        throw new HttpResponseException(HttpStatusCode.InternalServerError);
                    }
                    HttpContent fileContent = provider.Contents.FirstOrDefault();
                    return fileContent.ReadAsStreamAsync().Result;
                });
        }
    }
}