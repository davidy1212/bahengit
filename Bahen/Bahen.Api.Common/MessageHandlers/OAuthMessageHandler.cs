﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Bahen.Api.Common
{
    public class OAuthMessageHandler : DelegatingHandler
    {
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            try
            {
                if (request.Headers.Authorization != null)
                {
                    string scheme = request.Headers.Authorization.Scheme;
                    string token = request.Headers.Authorization.Parameter;
                    if (scheme == "Bearer")
                    {
                        this.Authorize(token);
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, string> pair in request.GetQueryNameValuePairs())
                    {
                        if (pair.Key == "access_token")
                        {
                            this.Authorize(pair.Value);
                            break;
                        }
                    }
                }
            }
            catch (OAuthException ex)
            {
                var err_response = request.CreateResponse();
                string content = "{\"error\":\"" + ex.Error + "\",\"error_description\":\"" + ex.ErrorDescription + "\"}";
                err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                return err_response;
            }
            var response = await base.SendAsync(request, cancellationToken);
            return response;
        }

        private void Authorize(string token)
        {
            LoginPrincipal user = PrincipalProvider.GetPrincipal(token);
            Thread.CurrentPrincipal = user;
        }
    }
}