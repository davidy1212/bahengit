﻿using Bahen.Common;
using Bahen.Data.Models;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Filters;

namespace Bahen.Api.Common
{
    public class RequirePermissionAttribute : AuthorizationFilterAttribute
    {
        private OAuthScopes[] ScopesRequired { get; set; }

        public RequirePermissionAttribute(params OAuthScopes[] scopes)
        {
            this.ScopesRequired = scopes;
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            IPrincipal user = Thread.CurrentPrincipal;
            foreach (OAuthScopes scope in ScopesRequired)
            {
                if (!user.HasPermission(scope))
                {
                    var err_response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    string content = "{\"error\":\"unauthorized request\",\"error_description\":\"require scope" + scope.ToDescriptionString() + "\"}";
                    err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                    actionContext.Response = err_response;
                    return;
                }
            }
        }
    }
}