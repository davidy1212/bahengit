﻿using Bahen.Data.Models;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;

namespace Bahen.Api.Common
{
    public class RequireRoleAttribute : AuthorizationFilterAttribute
    {
        private UserRoleLevel RoleRequired { get; set; }

        public RequireRoleAttribute(UserRoleLevel role)
        {
            this.RoleRequired = role;
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (Thread.CurrentPrincipal.GetRole() < this.RoleRequired)
            {
                var err_response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                string content = "{\"error\":\"insufficient role\",\"error_description\":\"your role is insufficient for this operation\"}";
                err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                actionContext.Response = err_response;
            }
        }
    }
}