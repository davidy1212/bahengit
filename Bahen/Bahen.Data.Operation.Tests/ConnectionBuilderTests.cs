﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests
{
    [TestClass]
    public class ConnectionBuilderTests
    {
        [TestMethod]
        public void ConnectionBuilderBasics()
        {
            ReflectionConstructor.ConstructOperator<SqlUserOperator>(
                new SqlConnectionBuilder().Build(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString));
        }
    }
}
