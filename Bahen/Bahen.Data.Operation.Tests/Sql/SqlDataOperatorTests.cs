﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System.Configuration;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlDataOperatorTests
    {
        public class SqlTestOperator : SqlDataOperator<SqlUserEntity>
        {
            public SqlTestOperator()
                : base(
                new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString), "Cycles")
            {
            }
        }

        public class SqlTestOperator2 : SqlDataOperator<SqlUserEntity>
        {
            public SqlTestOperator2()
                : base(
                new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString), "Haha")
            {
            }
        }

        public class SqlTestOperator3 : SqlDataOperator<SqlUserEntity>
        {
            public SqlTestOperator3()
                : base(
                new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString), "Users")
            {
            }
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void DbSetNameNotExist()
        {
            SqlTestOperator2 op = new SqlTestOperator2();
        }

        [TestMethod, ExpectedException(typeof(ArgumentException))]
        public void DbSetTypeNotMatch()
        {
            SqlTestOperator op = new SqlTestOperator();
        }

        [TestMethod,ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateDataIncompleteField()
        {
            SqlUserEntity user = new SqlUserEntity();
            SqlTestOperator3 op = new SqlTestOperator3();
            op.Insert(user);
        }
    }
}
