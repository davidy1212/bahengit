﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlSubscriptionOperatorTests
    {
        private SqlUserOperator userOperator;
        private SqlSubscriptionOperator subscriptionOperator;

        private SqlUserEntity userHost;
        private SqlUserEntity userSubscriber;

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connectionString));
            this.subscriptionOperator = new SqlSubscriptionOperator(new SqlConnectionBuilder().Build(connectionString));

            this.userHost = new SqlUserEntity()
            {
                LoginEmail = "steven.he@outlook.com",
                LoginPassword = "hehehe".ToSha512(),
                PreferredName = "Steven"
            };
            this.userSubscriber = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "MengYe"
            };

            this.userOperator.Insert(userHost);
            this.userOperator.Insert(userSubscriber);
        }

        [TestMethod]
        public void CreateSubscription()
        {
            SqlSubscriptionRelation sub = new SqlSubscriptionRelation()
            {
                HostID = userHost.ID,
                SubscriberID = userSubscriber.ID
            };

            this.subscriptionOperator.Insert(sub);
            Assert.AreNotEqual(0, sub.ID);
            Assert.AreEqual(true, this.subscriptionOperator.Contains(sub.ID));

            this.subscriptionOperator.Remove(sub);
            Assert.AreEqual(false, this.subscriptionOperator.Contains(sub.ID));
        }

        [TestMethod,ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateSubscriptionInvalideUserID()
        {
            SqlUserEntity userDeleted = new SqlUserEntity()
            {
                LoginEmail = "Steven.He.Wenhao@gmail.com",
                LoginPassword = "kakakkaka".ToSha512(),
                PreferredName = "Steven"
            };
            this.userOperator.Insert(userDeleted);

            SqlSubscriptionRelation sub = new SqlSubscriptionRelation()
            {
                HostID = userDeleted.ID,
                SubscriberID = userSubscriber.ID
            };
            this.userOperator.Remove(userDeleted);

            try
            {
                //Throw ForeignKeyNotMappedException
                this.subscriptionOperator.Insert(sub);
            }
            catch (Exception Ex)
            {
                Assert.AreEqual(0, sub.ID);
                throw Ex;
            }
        }

        [TestMethod]
        public void GetListOfSubscribers()
        {
            var subscriberList = new List<SqlUserEntity>();
            var subscriptionList = new List<SqlSubscriptionRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    SqlUserEntity userSubscriber1 = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("randomEngsc{0}@gmail.com", i),
                        LoginPassword = "kakakkaka".ToSha512(),
                        PreferredName = String.Format("{0} little Engsci", i)
                    };
                    this.userOperator.Insert(userSubscriber1);
                    subscriberList.Add(userSubscriber1);

                    SqlSubscriptionRelation sub = new SqlSubscriptionRelation()
                    {
                        HostID = userHost.ID,
                        SubscriberID = userSubscriber1.ID
                    };

                    this.subscriptionOperator.Insert(sub);
                    Assert.AreNotEqual(0, sub.ID);
                    subscriptionList.Add(sub);
                }

                var result = this.subscriptionOperator.GetSubscriberIDsByHost(this.userHost.ID);

                foreach (var item in subscriberList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in subscriptionList)
                {
                    this.subscriptionOperator.Remove(item);
                    Assert.AreEqual(false, this.subscriptionOperator.Contains(item.ID));
                }
                foreach (var item in subscriberList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.userOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetListOfHosts()
        {
            var hostList = new List<SqlUserEntity>();
            var subscriptionList = new List<SqlSubscriptionRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    SqlUserEntity userHost1 = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("randomEngsc{0}@gmail.com", i),
                        LoginPassword = "kakakkaka".ToSha512(),
                        PreferredName = String.Format("{0} little Engsci", i)
                    };
                    this.userOperator.Insert(userHost1);
                    hostList.Add(userHost1);

                    SqlSubscriptionRelation sub = new SqlSubscriptionRelation()
                    {
                        HostID = userHost1.ID,
                        SubscriberID = userSubscriber.ID
                    };

                    this.subscriptionOperator.Insert(sub);
                    Assert.AreNotEqual(0, sub.ID);
                    subscriptionList.Add(sub);
                }

                var result = this.subscriptionOperator.GetHostIDsBySubscriber(this.userSubscriber.ID);

                foreach (var item in hostList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in subscriptionList)
                {
                    this.subscriptionOperator.Remove(item);
                    Assert.AreEqual(false, this.subscriptionOperator.Contains(item.ID));
                }
                foreach (var item in hostList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.userOperator.Contains(item.ID));
                }
            }
        }


        [TestMethod]
        public void GetSubscriptionByIDs()
        {
            SqlSubscriptionRelation sub = new SqlSubscriptionRelation()
            {
                HostID = userHost.ID,
                SubscriberID = userSubscriber.ID
            };
            this.subscriptionOperator.Insert(sub);
            Assert.AreEqual(sub, this.subscriptionOperator.Get(sub.HostID, sub.SubscriberID));
            this.subscriptionOperator.Remove(sub);
            Assert.AreEqual(false, this.subscriptionOperator.Contains(sub.SubscriberID));
        }

        // Need to add tests: Get a empty set that matches the search
        // Add try-catch block in the operator to handle unknown exception
        // Test unknown exception

        [TestCleanup]
        public void CleanUp()
        {
            this.userOperator.Remove(userHost);
            this.userOperator.Remove(userSubscriber);
        }
    }
}
