﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlContextConcurrencyTests
    {
        [TestMethod]
        public void Basics()
        {


            SqlContext context1 = new SqlContext(ConfigurationManager.ConnectionStrings["SqlTests"].ConnectionString);
            SqlUserOperator userOperator1 = new SqlUserOperator(context1);

            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "xxx@xxx.com",
                LoginPassword = "xxx".ToSha512(),
                PreferredName = "xxx"
            };

            userOperator1.Insert(user);

            SqlContext context2 = new SqlContext(ConfigurationManager.ConnectionStrings["SqlTests"].ConnectionString);
            SqlUserOperator userOperator2 = new SqlUserOperator(context2);

            
        }
    }
}
