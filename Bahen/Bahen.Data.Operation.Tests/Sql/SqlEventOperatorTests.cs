﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlEventOperatorTests
    {
        private SqlEventOperator eventOperator;
        private SqlUserOperator userOperator;
        private SqlUserEntity publisher;

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.eventOperator = new SqlEventOperator(new SqlConnectionBuilder().Build(connectionString));
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connectionString));

            // Create a publisher for test purpose
            this.publisher = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            this.userOperator.Insert(publisher);
        }

        [TestMethod]
        public void CreateEventBasics()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                Name = "Birthday party",
                PublisherID = this.publisher.ID
            };

            this.eventOperator.Insert(myEvent);

            // Confirm that event ID has been assigned
            Assert.AreNotEqual(0, myEvent.ID);

            // Confirm that table contains event id
            Assert.IsTrue(this.eventOperator.Contains(myEvent.ID));
            Assert.IsTrue(this.eventOperator.TryGet(myEvent.ID, out myEvent));

            // Confirm that event has been removed
            this.eventOperator.Remove(myEvent);
            Assert.IsFalse(this.eventOperator.TryGet(myEvent.ID, out myEvent));
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateEventIncompleteFieldPublisher()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                Name = "Birthday party"
            };

            // Confirm that PublisherID cannot be missed
            this.eventOperator.Insert(myEvent);

        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateEventIncompleteFieldName()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                PublisherID = this.publisher.ID
            };

            // Confirm that Name cannot be missed
            this.eventOperator.Insert(myEvent);

        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateEventWithPublisherNotExisted()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                Name = "Birthday party",
                PublisherID = -1
            };

            // Confirm that publisher id not existed in user table
            this.eventOperator.Insert(myEvent);
        }

        [TestMethod]
        public void UpdateEventBasics()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                Name = "Birthday party",
                PublisherID = this.publisher.ID
            };

            // Confirm that successfully inserted
            this.eventOperator.Insert(myEvent);
            myEvent.Name = "Dinner";

            // Confirm that successfully updated
            this.eventOperator.Update(myEvent);

            // Query again to confirm dinner
            SqlEventEntity myEventQuery = this.eventOperator.Get(myEvent.ID);
            Assert.AreEqual("Dinner", myEventQuery.Name);

            // Remove the event
            this.eventOperator.Remove(myEventQuery);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdatePublisherIDNotExisted()
        {
            SqlEventEntity myEvent = new SqlEventEntity()
            {
                Name = "Birthday party",
                PublisherID = this.publisher.ID
            };

            // Confirm that successfully inserted
            this.eventOperator.Insert(myEvent);

            // Confirm that cannot change PublisherID to a not existed ID
            myEvent.PublisherID = -1;

            try
            {
                this.eventOperator.Update(myEvent);
            }
            finally
            {
                this.eventOperator.TryGet(myEvent.ID, out myEvent);
                this.eventOperator.Remove(myEvent);
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            // Confirm that publisher has been removed
            this.userOperator.Remove(this.publisher);
            this.userOperator.Dispose();
            this.eventOperator.Dispose();
        }
    }
}
