﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Operation;
using Bahen.Data.Models;
using Bahen.Common;
using Bahen.Data.Operation;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlCycleEventOperatorTests
    {
        private SqlCycleEventOperator cycleEventOperator;
        private SqlCycleOperator cycleOperator;
        private SqlEventOperator eventOperator;
        private SqlUserOperator userOperator;

        private SqlEventEntity eventTest;
        private SqlCycleEntity cycleTest;
        private SqlUserEntity userPublisher;

        private string connStr;

        [TestInitialize]
        public void Init()
        {
            this.connStr = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.cycleEventOperator = new SqlCycleEventOperator(new SqlConnectionBuilder().Build(connStr));
            this.cycleOperator = new SqlCycleOperator(new SqlConnectionBuilder().Build(connStr));
            this.eventOperator = new SqlEventOperator(new SqlConnectionBuilder().Build(connStr));
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connStr));

            this.userPublisher = new SqlUserEntity()
            {
                LoginEmail = "steven.he@outlook.com",
                LoginPassword = "hehehe".ToSha512(),
                PreferredName = "Steven"
            };

            this.userOperator.Insert(userPublisher);

            this.eventTest = new SqlEventEntity()
            {
                PublisherID = userPublisher.ID,
                Name = "PHY Midterm"
            };

            this.eventOperator.Insert(eventTest);
            Assert.IsTrue(this.eventOperator.Contains(eventTest.ID));

            this.cycleTest = new SqlCycleEntity()
            {
                Name = "Fake Commerce Group"
            };

            this.cycleOperator.Insert(cycleTest);
            Assert.IsTrue(this.cycleOperator.Contains(cycleTest.ID));
        }

        [TestMethod]
        public void CreateCycleEvent()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };

            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.AreNotEqual(0, cycleEventTest.ID);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));
            this.cycleEventOperator.Remove(cycleEventTest);
        }

        [TestMethod, ExpectedException(typeof(IdSpecifiedException))]
        public void CreateCycleEventCycleEventIDExist()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            this.cycleEventOperator.Remove(cycleEventTest);
            Assert.AreNotEqual(0, cycleEventTest.ID);
            this.cycleEventOperator.Insert(cycleEventTest);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateCycleEventMissingCycleID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateCycleEventMissingEventID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateCycleEventInvalidCycleID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = -1,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateCycleEventInvalidEventID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = -1
            };
            this.cycleEventOperator.Insert(cycleEventTest);
        }

        [TestMethod]
        public void UpdateCycleEvent()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            SqlCycleEntity cycleTestNew;
            cycleTestNew = new SqlCycleEntity()
            {
                Name = "Real Commerce Group"
            };

            this.cycleOperator.Insert(cycleTestNew);
            Assert.IsTrue(this.cycleOperator.Contains(cycleTestNew.ID));

            cycleEventTest.CycleID = cycleTestNew.ID;
            this.cycleEventOperator.Update(cycleEventTest);

            Assert.AreEqual(this.cycleEventOperator.Get(cycleEventTest.ID).CycleID, cycleTestNew.ID);
            this.cycleEventOperator.Remove(cycleEventTest);
            this.cycleOperator.Remove(cycleTestNew);

            Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
            Assert.IsFalse(this.cycleOperator.Contains(cycleTestNew.ID));
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdateCycleEventInvalidEventID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            cycleEventTest.EventID = -1;

            try
            {
                this.cycleEventOperator.Update(cycleEventTest);
            }
            catch (ForeignKeyNotMappedException ex)
            {
                this.cycleEventOperator.Remove(cycleEventTest);
                Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
                throw ex;
            }
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdateCycleEventInvalidCycleID()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            cycleEventTest.CycleID = -1;

            try
            {
                this.cycleEventOperator.Update(cycleEventTest);
            }
            catch (ForeignKeyNotMappedException ex)
            {
                this.cycleEventOperator.Remove(cycleEventTest);
                Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
                throw ex;
            }
        }

        //[TestMethod]
        //public void UpdateCycleEventCycleEventIDNotExist()
        //{
        //}

        [TestMethod]
        public void GetCycleEventByProperties()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = cycleTest.ID,
                EventID = eventTest.ID
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            Assert.AreEqual(this.cycleEventOperator.Get(cycleEventTest.CycleID, cycleEventTest.EventID).ID, cycleEventTest.ID);

            this.cycleEventOperator.Remove(cycleEventTest);
            Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
        }

        [TestMethod]
        public void GetCycleEventsByCycle()
        {
            var eventList = new List<SqlEventEntity>();
            var cycleEventList = new List<SqlCycleEventRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var events = new SqlEventEntity()
                    {
                        Name = "Watch Commerce Girls",
                        PublisherID = this.userPublisher.ID,
                    };
                    this.eventOperator.Insert(events);
                    eventList.Add(events);

                    SqlCycleEventRelation cycleEvents = new SqlCycleEventRelation()
                    {
                        CycleID = this.cycleTest.ID,
                        EventID = events.ID
                    };

                    this.cycleEventOperator.Insert(cycleEvents);
                    Assert.AreNotEqual(0, cycleEvents.ID);
                    cycleEventList.Add(cycleEvents);
                }


                var result = this.cycleEventOperator.GetEventIDsByCycle(this.cycleTest.ID);

                Assert.AreEqual(eventList.Count(), result.Count());
                foreach (var item in eventList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in cycleEventList)
                {
                    this.cycleEventOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleEventOperator.Contains(item.ID));
                }
                foreach (var item in eventList)
                {
                    this.eventOperator.Remove(item);
                    Assert.AreEqual(false, this.eventOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetCycleEventsByCycleNotExist()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = this.cycleTest.ID,
                EventID = this.eventTest.ID,
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            var result = this.cycleEventOperator.GetEventIDsByCycle(-1);
            Assert.AreEqual(0, result.Count());

            this.cycleEventOperator.Remove(cycleEventTest);
            Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
        }

        [TestMethod]
        public void GetCycleEventsByEvent()
        {
            var cycleList = new List<SqlCycleEntity>();
            var cycleEventList = new List<SqlCycleEventRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var cycles = new SqlCycleEntity()
                    {
                        Name = "Watch Commerce Girls"
                    };
                    this.cycleOperator.Insert(cycles);
                    cycleList.Add(cycles);

                    SqlCycleEventRelation cycleEvents = new SqlCycleEventRelation()
                    {
                        CycleID = cycles.ID,
                        EventID = this.eventTest.ID
                    };

                    this.cycleEventOperator.Insert(cycleEvents);
                    Assert.AreNotEqual(0, cycleEvents.ID);
                    cycleEventList.Add(cycleEvents);
                }


                var result = this.cycleEventOperator.GetCyclesByEvent(this.eventTest.ID);

                Assert.AreEqual(cycleList.Count(), result.Count());
                foreach (var item in cycleList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in cycleEventList)
                {
                    this.cycleEventOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleEventOperator.Contains(item.ID));
                }
                foreach (var item in cycleList)
                {
                    this.cycleOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetCycleEventsByEventNotExist()
        {
            SqlCycleEventRelation cycleEventTest = new SqlCycleEventRelation()
            {
                CycleID = this.cycleTest.ID,
                EventID = this.eventTest.ID,
            };
            this.cycleEventOperator.Insert(cycleEventTest);
            Assert.IsTrue(this.cycleEventOperator.Contains(cycleEventTest.ID));

            var result = this.cycleEventOperator.GetCyclesByEvent(-1);
            Assert.AreEqual(0, result.Count());

            this.cycleEventOperator.Remove(cycleEventTest);
            Assert.IsFalse(this.cycleEventOperator.Contains(cycleEventTest.ID));
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.cycleOperator.Remove(this.cycleTest);
            this.eventOperator.Remove(this.eventTest);
            this.userOperator.Remove(this.userPublisher);
        }
    }
}
