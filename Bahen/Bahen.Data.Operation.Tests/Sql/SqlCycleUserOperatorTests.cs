﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Operation;
using Bahen.Data.Models;
using Bahen.Common;
using Bahen.Data.Models;
using System.Diagnostics;
using Bahen.Data.Operation;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlCycleUserOperatorTests
    {
        private SqlCycleUserOperator cycleUserOperator;
        private SqlUserOperator userOperator;
        private SqlCycleOperator cycleOperator;

        private SqlUserEntity userTest;
        private SqlCycleEntity cycleTest;

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.cycleUserOperator = new SqlCycleUserOperator(new SqlConnectionBuilder().Build(connectionString));
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connectionString));
            this.cycleOperator = new SqlCycleOperator(new SqlConnectionBuilder().Build(connectionString));

            this.userTest = new SqlUserEntity()
            {
                LoginEmail = "steven.he@outlook.com",
                LoginPassword = "hehehe".ToSha512(),
                PreferredName = "Steven"
            };
            this.userOperator.Insert(userTest);
            Assert.IsTrue(this.userOperator.Contains(userTest.ID));

            this.cycleTest = new SqlCycleEntity()
            {
                Name = "No water fountain in this Building!"
            };
            this.cycleOperator.Insert(cycleTest);
            Assert.IsTrue(this.cycleOperator.Contains(cycleTest.ID));
        }

        [TestMethod]
        public void CreateCycleUser()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);

            Assert.AreNotEqual(cycleUserTest.ID, 0);
            Assert.IsTrue(this.cycleUserOperator.Contains(cycleUserTest.ID));
            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));
        }

        [TestMethod, ExpectedException(typeof(IdSpecifiedException))]
        public void CreateCycleUserCycleUserIDExist()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.AreNotEqual(0, cycleUserTest.ID);
            this.cycleUserOperator.Insert(cycleUserTest);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateCycleUserMissingCycleID()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateCycleUserMissingUserID()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateCycleUserInvalidCycleID()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = -1,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void CreateCycleUserInvalidUserID()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = -1,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
        }

        [TestMethod]
        public void UpdateCycleUser()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            Assert.IsTrue(this.cycleUserOperator.Contains(cycleUserTest.ID));
            cycleUserTest.MembershipType = Convert.ToInt32(CycleMembershipType.Member);
            this.cycleUserOperator.Update(cycleUserTest);
            Assert.AreEqual(this.cycleUserOperator.Get(cycleUserTest.ID).MembershipType, Convert.ToInt32(CycleMembershipType.Member));

            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void UpdateCycleUserCycleUserIDNotExist()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));

            cycleUserTest.MembershipType = Convert.ToInt32(CycleMembershipType.Administrator);

            this.cycleUserOperator.Update(cycleUserTest);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdateCycleUserInvalidCycleID()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            Assert.IsTrue(this.cycleUserOperator.Contains(cycleUserTest.ID));
            cycleUserTest.CycleID = -1;
            try
            {
                this.cycleUserOperator.Update(cycleUserTest);
            }
            catch (ForeignKeyNotMappedException ex)
            {
                this.cycleUserOperator.Remove(cycleUserTest);
                Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));
                throw ex;
            }
        }

        //[TestMethod]
        //public void UpdateCycleUserInvalidUserID()
        //{
        //}

        [TestMethod]
        public void GetUsersByCycle()
        {
            var userList = new List<SqlUserEntity>();
            var cycleUserList = new List<SqlCycleUserRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var users = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("steven.he{0}@outlook.com", i),
                        PreferredName = "Fake ID",
                        LoginPassword = "IAMSteven".ToSha512()
                    };
                    this.userOperator.Insert(users);
                    userList.Add(users);

                    SqlCycleUserRelation cycleUsers = new SqlCycleUserRelation()
                    {
                        CycleID = this.cycleTest.ID,
                        UserID = users.ID
                    };

                    this.cycleUserOperator.Insert(cycleUsers);
                    Assert.AreNotEqual(0, cycleUsers.ID);
                    cycleUserList.Add(cycleUsers);
                }


                var result = this.cycleUserOperator.GetUserIDsByCycle(this.cycleTest.ID);

                Assert.AreEqual(userList.Count(), result.Count());
                foreach (var item in userList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in cycleUserList)
                {
                    this.cycleUserOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleUserOperator.Contains(item.ID));
                }
                foreach (var item in userList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.userOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetUsersByCycleNotExist()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            Assert.IsTrue(this.cycleUserOperator.Contains(cycleUserTest.ID));

            var result = this.cycleUserOperator.GetUserIDsByCycle(-1);
            Assert.AreEqual(0, result.Count());

            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));
        }

        [TestMethod]
        public void GetUsersByCycleAndMembershipType()
        {
            var userList = new List<SqlUserEntity>();
            var cycleUserList = new List<SqlCycleUserRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var users = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("steven.he{0}@outlook.com", i),
                        PreferredName = "Fake ID",
                        LoginPassword = "IAMSteven".ToSha512()
                    };
                    this.userOperator.Insert(users);
                    userList.Add(users);

                    SqlCycleUserRelation cycleUsers = new SqlCycleUserRelation()
                    {
                        CycleID = this.cycleTest.ID,
                        UserID = users.ID,
                        MembershipType = Convert.ToInt32(CycleMembershipType.Member)
                    };

                    this.cycleUserOperator.Insert(cycleUsers);
                    Assert.AreNotEqual(0, cycleUsers.ID);
                    cycleUserList.Add(cycleUsers);
                }


                SqlCycleUserRelation cycleUser2 = new SqlCycleUserRelation()
                {
                    CycleID = this.cycleTest.ID,
                    UserID = this.userTest.ID,
                    MembershipType = Convert.ToInt32(CycleMembershipType.Administrator)
                };
                this.cycleUserOperator.Insert(cycleUser2);


                var result = this.cycleUserOperator.GetUserIDsByCycle(this.cycleTest.ID, Convert.ToInt32(CycleMembershipType.Member));

                Assert.AreEqual(userList.Count(), result.Count());
                foreach (var item in userList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                this.cycleUserOperator.Remove(cycleUser2);

                foreach (var item in cycleUserList)
                {
                    this.cycleUserOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleUserOperator.Contains(item.ID));
                }
                foreach (var item in userList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.userOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetCyclesByUser()
        {
            var cycleList = new List<SqlCycleEntity>();
            var cycleUserList = new List<SqlCycleUserRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var cycles = new SqlCycleEntity()
                    {
                        Name = String.Format("Common Room Stinks +{0}", i)
                    };
                    this.cycleOperator.Insert(cycles);
                    cycleList.Add(cycles);

                    SqlCycleUserRelation cycleUsers = new SqlCycleUserRelation()
                    {
                        CycleID = cycles.ID,
                        UserID = this.userTest.ID
                    };

                    this.cycleUserOperator.Insert(cycleUsers);
                    Assert.AreNotEqual(0, cycleUsers.ID);
                    cycleUserList.Add(cycleUsers);
                }


                var result = this.cycleUserOperator.GetCycleIDsByUser(this.userTest.ID);

                Assert.AreEqual(cycleList.Count(), result.Count());
                foreach (var item in cycleList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in cycleUserList)
                {
                    this.cycleUserOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleUserOperator.Contains(item.ID));
                }
                foreach (var item in cycleList)
                {
                    this.cycleOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetCyclesByUserNotExist()
        {
            SqlCycleUserRelation cycleUserTest = new SqlCycleUserRelation()
            {
                CycleID = this.cycleTest.ID,
                UserID = this.userTest.ID,
                MembershipType = Convert.ToInt32(CycleMembershipType.Publisher)
            };
            this.cycleUserOperator.Insert(cycleUserTest);
            Assert.IsTrue(this.cycleUserOperator.Contains(cycleUserTest.ID));

            var result = this.cycleUserOperator.GetCycleIDsByUser(-1);
            Assert.AreEqual(0, result.Count());

            this.cycleUserOperator.Remove(cycleUserTest);
            Assert.IsFalse(this.cycleUserOperator.Contains(cycleUserTest.ID));
        }

        [TestMethod]
        public void GetCyclesByUserAndMembershipType()
        {
            var cycleList = new List<SqlCycleEntity>();
            var cycleUserList = new List<SqlCycleUserRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var cycles = new SqlCycleEntity()
                    {
                        Name = String.Format("Common Room Stinks +{0}", i)
                    };
                    this.cycleOperator.Insert(cycles);
                    cycleList.Add(cycles);

                    SqlCycleUserRelation cycleUsers = new SqlCycleUserRelation()
                    {
                        CycleID = cycles.ID,
                        UserID = this.userTest.ID,
                        MembershipType = Convert.ToInt32(CycleMembershipType.Member)
                    };

                    this.cycleUserOperator.Insert(cycleUsers);
                    Assert.AreNotEqual(0, cycleUsers.ID);
                    cycleUserList.Add(cycleUsers);
                }

                SqlCycleUserRelation cycleUser2 = new SqlCycleUserRelation()
                {
                    CycleID = this.cycleTest.ID,
                    UserID = this.userTest.ID,
                    MembershipType = Convert.ToInt32(CycleMembershipType.Administrator)
                };
                this.cycleUserOperator.Insert(cycleUser2);

                var result = this.cycleUserOperator.GetCycleIDsByUser(this.userTest.ID, Convert.ToInt32(CycleMembershipType.Member));

                Assert.AreEqual(cycleList.Count(), result.Count());
                foreach (var item in cycleList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                this.cycleUserOperator.Remove(cycleUser2);

                foreach (var item in cycleUserList)
                {
                    this.cycleUserOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleUserOperator.Contains(item.ID));
                }
                foreach (var item in cycleList)
                {
                    this.cycleOperator.Remove(item);
                    Assert.AreEqual(false, this.cycleOperator.Contains(item.ID));
                }
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.userOperator.Remove(userTest);
            this.cycleOperator.Remove(cycleTest);
            Assert.IsFalse(this.cycleOperator.Contains(cycleTest.ID));
            Assert.IsFalse(this.userOperator.Contains(userTest.ID));
        }

    }
}
