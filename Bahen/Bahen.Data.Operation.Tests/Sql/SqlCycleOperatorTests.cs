﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlCycleOperatorTests
    {
        private SqlCycleOperator cycleOperator;

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.cycleOperator = new SqlCycleOperator(
                new SqlConnectionBuilder().Build(connectionString));
        }

        [TestMethod]
        public void CreateCycleBasics()
        {
            SqlCycleEntity cycle = new SqlCycleEntity()
            {
                Name = "University of Toronto"
            };

            this.cycleOperator.Insert(cycle);

            // Confirm that cycle ID has been assigned
            Assert.AreNotEqual(0, cycle.ID);

            // Confirm that table contains cycle id
            Assert.IsTrue(this.cycleOperator.Contains(cycle.ID));
            Assert.IsTrue(this.cycleOperator.TryGet(cycle.ID, out cycle));

            // Confirm that cycle has been removed
            this.cycleOperator.Remove(cycle);
            Assert.IsFalse(this.cycleOperator.TryGet(cycle.ID, out cycle));
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateCycleIncompleteField()
        {
            SqlCycleEntity cycle = new SqlCycleEntity();

            // Confirm that Name cannot be missed
            this.cycleOperator.Insert(cycle);
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.cycleOperator.Dispose();
        }
    }
}
