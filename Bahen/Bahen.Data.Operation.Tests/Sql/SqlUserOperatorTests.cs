﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlUserOperatorTests
    {
        private SqlUserOperator userOperator;

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connectionString));
        }

        #region CreateUserTests
        [TestMethod]
        public void CreateUserBasics()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Confirm that insertion successful
            this.userOperator.Insert(user);

            // Confirm that user ID has been assigned
            Assert.AreNotEqual(0, user.ID);

            // Confirm that table contains user id
            Assert.IsTrue(this.userOperator.Contains(user.ID));

            // Confirm can query based on email
            Assert.IsTrue(this.userOperator.TryGet("renmengye@gmail.com", out user));

            // Confirm remove
            this.userOperator.Remove(user);
            Assert.IsFalse(this.userOperator.TryGet(user.ID, out user));
        }

        [TestMethod, ExpectedException(typeof(IdSpecifiedException))]
        public void CreateUserSpecifiedID()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Get a user with pre non-zero ID
            this.userOperator.Insert(user);
            this.userOperator.Remove(user);

            // Confirm the error code that user with id -1 cannot be inserted
            this.userOperator.Insert(user);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateUserIncompleteFieldEmail()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "MY"
            };

            // Confirm that Login Email cannot be missed
            this.userOperator.Insert(user);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateUserIncompleteFieldName()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512()
            };

            // Confirm that Preferred Name cannot be missed
            this.userOperator.Insert(user);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void CreateUserIncompleteFieldPassword()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                PreferredName = "Mengye"
            };

            // Confirm that password cannot be missed
            this.userOperator.Insert(user);
        }

        [TestMethod, ExpectedException(typeof(UniqueKeyCollisionException))]
        public void CreateUserEmailCollision()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Confirm that user has been inserted
            this.userOperator.Insert(user);

            SqlUserEntity collisionUser = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            try
            {
                // Confirm the error code that collisionUser cannot be inserted
                this.userOperator.Insert(collisionUser);
            }
            finally
            {
                // Confirm that user has been removed
                this.userOperator.Remove(user);
            }
        }

        [TestMethod, ExpectedException(typeof(NullReferenceException))]
        public void CreateNullUser()
        {
            this.userOperator.Insert(null);
        }
        #endregion

        #region UpdateUserTests
        [TestMethod]
        public void UpdateUserBasics()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Confirm that insertion successful
            this.userOperator.Insert(user);

            user.PreferredName = "Mengye Ren";
            user.LoginPassword = "hehehe".ToSha512();

            // Confirm the change successful
            this.userOperator.Update(user);

            // Confirm the change are made
            SqlUserEntity user2 = this.userOperator.Get(user.ID);
            Assert.AreEqual("Mengye Ren", user2.PreferredName);
            Assert.IsTrue(user2.LoginPassword.SequenceEqual("hehehe".ToSha512()));

            this.userOperator.Remove(user2);
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void UpdateUserNotExist()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginPassword = "haha".ToSha512(),
                LoginEmail = "hah",
                PreferredName = "h"
            };
            this.userOperator.Insert(user);
            this.userOperator.Remove(user);
            this.userOperator.Update(user);
        }

        [TestMethod, ExpectedException(typeof(IncompleteNotNullFieldException))]
        public void UpdateUserNullFields()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Confirm that insertion successful
            this.userOperator.Insert(user);

            // Confirm that PreferredName can't be changed to null
            user.PreferredName = null;

            try
            {
                this.userOperator.Update(user);
            }
            finally
            {
                this.userOperator.TryGet(user.ID, out user);
                this.userOperator.Remove(user);
            }

        }

        [TestMethod, ExpectedException(typeof(UniqueKeyCollisionException))]
        public void UpdateUserLoginEmail()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };
            this.userOperator.Insert(user);

            SqlUserEntity user2 = new SqlUserEntity()
            {
                LoginEmail = "renmengye@yahoo.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };
            this.userOperator.Insert(user2);
            user2.LoginEmail = "renmengye@gmail.com";

            // Confirm can't change email to an existing one
            try
            {
                this.userOperator.Update(user2);
            }
            finally
            {
                this.userOperator.Remove(user);
                this.userOperator.Remove(user2);
            }

        }

        [TestMethod]
        public void UpdateUserAfterRefreshContext()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "haha".ToSha512(),
                PreferredName = "Mengye"
            };

            this.userOperator.Insert(user);
            user.LoginEmail = null;

            try
            {
                this.userOperator.Update(user);
            }
            catch
            {
                // Now, after refreshing context, user entity is detached,
                user.LoginEmail = "renmengye@yahoo.com";
                this.userOperator.Update(user);

                this.userOperator.TryGet(user.ID, out user);
                Assert.AreEqual("renmengye@yahoo.com", user.LoginEmail);

                this.userOperator.Remove(user);
            }
        }

        [TestMethod, ExpectedException(typeof(NullReferenceException))]
        public void UpdateUserNull()
        {
            this.userOperator.Update(null);
        }

        [TestMethod]
        public void UpdateUserByDeepCopy()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "mengye"
            };
            this.userOperator.Insert(user);

            // Make a deep copy
            SqlUserEntity user2 = new SqlUserEntity();

            foreach (PropertyInfo property in typeof(SqlUserEntity).GetProperties())
            {
                if (property.Name != "ID")
                {
                    property.SetValue(user2, property.GetValue(user, null), null);
                }
            }

            user2.LoginEmail = "renmengye@yahoo.com";
            this.userOperator.Update(user2);

            user = this.userOperator.Get(user2.ID);
            Assert.AreEqual("renmengye@yahoo.com", user.LoginEmail);

            this.userOperator.Remove(user);
        }
        #endregion

        #region RemoveUserTests
        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void RemoveUserNotFound()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            this.userOperator.Insert(user);
            this.userOperator.Remove(user);
            this.userOperator.Remove(user);
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void RemoveUserNotExist()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "xxx"
            };
            this.userOperator.Remove(user);
        }

        [TestMethod, ExpectedException(typeof(NullReferenceException))]
        public void RemoveUserNull()
        {
            this.userOperator.Remove(null);
        }

        [TestMethod]
        public void RemoveUserByDeepCopy()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "mengye"
            };
            this.userOperator.Insert(user);

            // Make a deep copy
            SqlUserEntity user2 = new SqlUserEntity();

            foreach (PropertyInfo property in typeof(SqlUserEntity).GetProperties())
            {
                if (property.Name != "ID")
                {
                    property.SetValue(user2, property.GetValue(user, null), null);
                }
            }

            this.userOperator.Remove(user2);
            Assert.IsFalse(this.userOperator.TryGet(user.ID, out user));
        }
        #endregion

        #region GetUserTests
        [TestMethod]
        public void GetUserNotInContext()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            // Confirm that insertion successful
            this.userOperator.Insert(user);

            // Confirm can get the user initially
            Assert.IsTrue(this.userOperator.TryGet(user.ID, out user));

            // Refresh the context
            this.userOperator.Dispose();
            this.userOperator = new SqlUserOperator(
                new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString));

            // Confirm that can still get the user without being in the context
            Assert.IsTrue(this.userOperator.TryGet(user.ID, out user));
            Assert.AreEqual("renmengye@gmail.com", user.LoginEmail);
            this.userOperator.Remove(user);
        }
        #endregion

        #region UnknownExceptionTests
        /***************************************************************
         * Note: the tests below are highly expensive to run
         * Only run them to get test coverage results
         * ************************************************************/
        [TestMethod, ExpectedException(typeof(BackendUnknownException))]
        public void CreateUserUnknownException()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };

            this.userOperator.Dispose();

            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build("Data Source=Something"));
            this.userOperator.Insert(user);
        }

        [TestMethod, ExpectedException(typeof(BackendUnknownException))]
        public void UpdateUserUnknownException()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };
            this.userOperator.Insert(user);
            this.userOperator.Dispose();

            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build("Data Source=Something"));

            try
            {
                this.userOperator.Update(user);
            }
            finally
            {
                this.userOperator = new SqlUserOperator(
                    new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString));
                this.userOperator.TryGet(user.ID, out user);
                this.userOperator.Remove(user);
            }
        }

        [TestMethod, ExpectedException(typeof(BackendUnknownException))]
        public void RemoveUserUnknownException()
        {
            SqlUserEntity user = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "Mengye"
            };
            this.userOperator.Insert(user);
            this.userOperator.Dispose();

            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build("Data Source=Something"));

            try
            {
                this.userOperator.Remove(user);
            }
            finally
            {
                this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString));
                this.userOperator.TryGet(user.ID, out user);
                this.userOperator.Remove(user);
            }
        }

        [TestMethod, ExpectedException(typeof(BackendUnknownException))]
        public void GetUserUnknownException()
        {
            this.userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build("Data Source=Something"));

            SqlUserEntity user;
            this.userOperator.TryGet(0, out user);
            this.userOperator.Contains(0);
            this.userOperator.Get(0);
        }
        #endregion

        [TestCleanup]
        public void CleanUp()
        {
            var settings = ConfigurationManager.ConnectionStrings[1];
            var fi = typeof(ConfigurationElement).GetField("_bReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = "Data Source=.;Initial Catalog=Griddy;Integrated Security=True;MultipleActiveResultSets=True";
            this.userOperator.Dispose();
        }
    }
}
