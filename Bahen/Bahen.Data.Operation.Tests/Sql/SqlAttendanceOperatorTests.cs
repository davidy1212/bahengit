﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation.Tests.Sql
{
    [TestClass]
    public class SqlAttendanceOperatorTest
    {
        private SqlUserOperator userOperator;
        private SqlEventOperator eventOperator;
        private SqlAttendanceOperator attendanceOperator;

        private SqlUserEntity userPublisher;
        private SqlUserEntity userAttendant;

        private SqlEventEntity eventAttend;

        [TestInitialize]
        public void Init() 
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            userOperator = new SqlUserOperator(new SqlConnectionBuilder().Build(connectionString));
            eventOperator = new SqlEventOperator(new SqlConnectionBuilder().Build(connectionString));
            attendanceOperator = new SqlAttendanceOperator(new SqlConnectionBuilder().Build(connectionString));

            this.userPublisher = new SqlUserEntity()
            {
                LoginEmail = "steven.he@outlook.com",
                LoginPassword = "hehehe".ToSha512(),
                PreferredName = "Steven"
            };

            this.userAttendant = new SqlUserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "hahaha".ToSha512(),
                PreferredName = "MengYe"
            };

            this.userOperator.Insert(userPublisher);
            this.userOperator.Insert(userAttendant);

            this.eventAttend = new SqlEventEntity()
            {
                PublisherID = userPublisher.ID,
                Name = "MAT Midterm"
            };

            this.eventOperator.Insert(eventAttend);
        }

        [TestMethod]
        public void CreateAttendance()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID
            };

            this.attendanceOperator.Insert(att);
            Assert.AreNotEqual(0, att.ID);
            Assert.AreEqual(0, att.AttendanceStatus);
            Assert.AreEqual(true, attendanceOperator.Contains(att.ID));

            this.attendanceOperator.Remove(att);
            Assert.AreEqual(false, this.attendanceOperator.Contains(att.ID));
        }

        [TestMethod]
        public void CreateAttendanceWithSpecifiedStatus()
        {
            int attStatus = 2;

            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID,
                AttendanceStatus = attStatus
            };

            this.attendanceOperator.Insert(att);
            Assert.AreNotEqual(0, att.ID);
            Assert.AreEqual(attStatus, att.AttendanceStatus);
            Assert.AreEqual(true, attendanceOperator.Contains(att.ID));

            this.attendanceOperator.Remove(att);
            Assert.AreEqual(false, this.attendanceOperator.Contains(att.ID));
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void RemoveAttendanceNotFound()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID
            };
            // Confirm that insertion successful
            this.attendanceOperator.Insert(att);
            this.attendanceOperator.Remove(att);

            // Confirm that cannot be removed twice. Throw EntityNotFoundException
            this.attendanceOperator.Remove(att);
        }

        [TestMethod]
        public void UpdateAttendanceStatus()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID
            };
            // Confirm that insertion successful
            this.attendanceOperator.Insert(att);

            att.AttendanceStatus = 1;
            this.attendanceOperator.Update(att);

            this.attendanceOperator.Remove(att);
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdateAttendanceInvalidEventID()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID
            };
            // Confirm that insertion successful
            this.attendanceOperator.Insert(att);

            att.EventID = -1;
            try
            {
                //Throw ForeignKeyNotMappedException
                this.attendanceOperator.Update(att);
            }
            catch(Exception Ex)
            {
                //confirm nothing is updated because att is now untracked
                SqlAttendanceRelation att2;
                Assert.AreEqual(true, this.attendanceOperator.TryGet(att.ID, out att2));

                Assert.AreNotEqual(-1, att2.EventID);

                this.attendanceOperator.Remove(att2);

                throw Ex;
            }
        }

        [TestMethod, ExpectedException(typeof(ForeignKeyNotMappedException))]
        public void UpdateAttendanceInvalidUserID()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                UserID = userAttendant.ID,
                EventID = eventAttend.ID
            };
            // Confirm that insertion successful
            this.attendanceOperator.Insert(att);

            att.UserID = -1;
            try
            {
                //Throw ForeignKeyNotMappedException
                this.attendanceOperator.Update(att);
            }
            catch (Exception Ex)
            {
                //confirm nothing is updated because att is now untracked
                SqlAttendanceRelation att2;
                Assert.AreEqual(true, this.attendanceOperator.TryGet(att.ID, out att2));

                Assert.AreNotEqual(-1, att2.UserID);

                this.attendanceOperator.Remove(att2);

                throw Ex;
            }
        }

        [TestMethod]
        public void GetAttendanceByIds()
        {
            SqlAttendanceRelation att = new SqlAttendanceRelation()
            {
                EventID = this.eventAttend.ID,
                UserID = this.userAttendant.ID
            };
            this.attendanceOperator.Insert(att);
            Assert.AreEqual(att, this.attendanceOperator.Get(att.UserID, att.EventID));
            this.attendanceOperator.Remove(att);
            Assert.AreEqual(false, this.attendanceOperator.Contains(att.ID));
        }

        [TestMethod]
        public void GetListOfEventsByAttendant()
        {
            var eventList = new List<SqlEventEntity>();
            var attendanceList = new List<SqlAttendanceRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var events = new SqlEventEntity()
                    {
                        PublisherID = userPublisher.ID,
                        Name = String.Format("AER Midterm {0}", i)
                    };
                    this.eventOperator.Insert(events);
                    eventList.Add(events);

                    SqlAttendanceRelation att = new SqlAttendanceRelation()
                    {
                        UserID = userAttendant.ID,
                        EventID = events.ID
                    };

                    this.attendanceOperator.Insert(att);
                    Assert.AreNotEqual(0, att.ID);
                    attendanceList.Add(att);
                }

                var result = this.attendanceOperator.GetEventIDsByUser(this.userAttendant.ID);

                Assert.AreEqual(eventList.Count(), result.Count());
                foreach (var item in eventList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                foreach (var item in attendanceList)
                {
                    this.attendanceOperator.Remove(item);
                    Assert.AreEqual(false, this.attendanceOperator.Contains(item.ID));
                }
                foreach (var item in eventList)
                {
                    this.eventOperator.Remove(item);
                    Assert.AreEqual(false, this.eventOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetListOfEventsByAttendantWithStatus()
        {
            var eventList = new List<SqlEventEntity>();
            var attendanceList = new List<SqlAttendanceRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var events = new SqlEventEntity()
                    {
                        PublisherID = userPublisher.ID,
                        Name = String.Format("AER Midterm {0}", i)
                    };
                    this.eventOperator.Insert(events);
                    eventList.Add(events);

                    SqlAttendanceRelation att = new SqlAttendanceRelation()
                    {
                        UserID = userAttendant.ID,
                        EventID = events.ID,
                        AttendanceStatus = 2
                    };

                    this.attendanceOperator.Insert(att);
                    Assert.AreNotEqual(0, att.ID);
                    attendanceList.Add(att);
                }

                SqlAttendanceRelation att2 = new SqlAttendanceRelation()
                {
                    UserID = this.userAttendant.ID,
                    EventID = this.eventAttend.ID,
                    AttendanceStatus = 1
                };
                this.attendanceOperator.Insert(att2);

                var result = this.attendanceOperator.GetEventIDsByUser(this.userAttendant.ID, 2);

                Assert.AreEqual(eventList.Count(), result.Count());
                foreach (var item in eventList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                this.attendanceOperator.Remove(att2);

                foreach (var item in attendanceList)
                {
                    this.attendanceOperator.Remove(item);
                    Assert.AreEqual(false, this.attendanceOperator.Contains(item.ID));
                }
                foreach (var item in eventList)
                {
                    this.eventOperator.Remove(item);
                    Assert.AreEqual(false, this.eventOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetListOfAttendantsByEvent()
        {
            var userList = new List<SqlUserEntity>();
            var attendanceList = new List<SqlAttendanceRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var users = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("randomEngsci{0}@outlook.com", i),
                        LoginPassword = "hehehe".ToSha512(),
                        PreferredName = String.Format("{0} little Artsci", i)
                    };
                    this.userOperator.Insert(users);
                    userList.Add(users);

                    SqlAttendanceRelation att = new SqlAttendanceRelation()
                    {
                        UserID = users.ID,
                        EventID = this.eventAttend.ID
                    };

                    this.attendanceOperator.Insert(att);
                    Assert.AreNotEqual(0, att.ID);
                    attendanceList.Add(att);
                }

                var result = this.attendanceOperator.GetUserIDsByEvent(this.eventAttend.ID);

                foreach (var item in userList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                Assert.AreEqual(userList.Count(), result.Count());

                foreach (var item in attendanceList)
                {
                    this.attendanceOperator.Remove(item);
                    Assert.AreEqual(false, this.attendanceOperator.Contains(item.ID));
                }
                foreach (var item in userList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.eventOperator.Contains(item.ID));
                }
            }
        }

        [TestMethod]
        public void GetListOfAttendantsByEventWithStatus()
        {
            var userList = new List<SqlUserEntity>();
            var attendanceList = new List<SqlAttendanceRelation>();

            // Test the 0 entry case and 5 entries case
            for (int j = 0; j < 6; j = j + 5)
            {
                int length = j;
                for (int i = 0; i < length; i++)
                {
                    var users = new SqlUserEntity()
                    {
                        LoginEmail = String.Format("randomEngsci{0}@outlook.com", i),
                        LoginPassword = "hehehe".ToSha512(),
                        PreferredName = String.Format("{0} little Artsci", i)
                    };
                    this.userOperator.Insert(users);
                    userList.Add(users);

                    SqlAttendanceRelation att = new SqlAttendanceRelation()
                    {
                        UserID = users.ID,
                        EventID = this.eventAttend.ID,
                        AttendanceStatus = 2
                    };

                    this.attendanceOperator.Insert(att);
                    Assert.AreNotEqual(0, att.ID);
                    attendanceList.Add(att);
                }

                SqlAttendanceRelation att2 = new SqlAttendanceRelation()
                {
                    UserID = userAttendant.ID,
                    EventID = this.eventAttend.ID,
                    AttendanceStatus = 1
                };
                this.attendanceOperator.Insert(att2);

                var result = this.attendanceOperator.GetUserIDsByEvent(this.eventAttend.ID, 2);

                foreach (var item in userList)
                {
                    Assert.AreEqual(true, result.Contains(item.ID));
                }

                Assert.AreEqual(userList.Count(), result.Count());

                this.attendanceOperator.Remove(att2);
                foreach (var item in attendanceList)
                {
                    this.attendanceOperator.Remove(item);
                    Assert.AreEqual(false, this.attendanceOperator.Contains(item.ID));
                }
                foreach (var item in userList)
                {
                    this.userOperator.Remove(item);
                    Assert.AreEqual(false, this.eventOperator.Contains(item.ID));
                }
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.eventOperator.Remove(eventAttend);
            this.userOperator.Remove(userPublisher);
            this.userOperator.Remove(userAttendant);
        }
    }
}
