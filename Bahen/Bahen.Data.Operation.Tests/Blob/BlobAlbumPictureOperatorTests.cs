﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation.Tests.Blob
{
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClass]
    public class BlobAlbumPictureOperatorTests
    {
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethod]
        public void Basics()
        {
            TableAlbumOperator albumOperator = new TableAlbumOperator(ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            TableAlbumEntity album = new TableAlbumEntity()
            {
                AlbumPrefix = TableAlbumOperator.ComputeAlbumPrefix(AlbumType.EventPicture),
                OwnerID = 1
            };
            albumOperator.Insert(album);

            BlobAlbumPictureOperator pictureOperator = new BlobAlbumPictureOperator(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString, album);
            for (int i = 0; i < 4; i++)
            {
                using (Stream stream = new MemoryStream())
                {
                    BinaryWriter writer = new BinaryWriter(stream);
                    writer.Write(File.ReadAllBytes("D:\\test.png"));
                    writer.Flush();
                    stream.Seek(0, SeekOrigin.Begin);

                    pictureOperator.Upload(Guid.NewGuid(), PictureSize.Large, stream);
                }
            }

            var items = pictureOperator.GetAlbumPictureUrls(PictureSize.Large);

            foreach (var item in items)
            {
                pictureOperator.Delete(item);
            }

            albumOperator.Remove(album);
        }
    }
}
