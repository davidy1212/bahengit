﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Bahen.Data.Models;
using Bahen.Common;
using System.Linq;
using System.Collections.Generic;

namespace Bahen.Data.Operation.Tests
{
    [TestClass]
    public class UserOperatorTests
    {
        private static string sqlConnectionString;
        private static string tableConnectionString;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            sqlConnectionString = ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString;
            tableConnectionString = ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString;
        }

        [TestMethod]
        public void GetFilter()
        {
            UserOperator op = new UserOperator(sqlConnectionString, tableConnectionString);
            UserEntity user = new UserEntity()
            {
                LoginEmail = "renmengye@gmail.com",
                LoginPassword = "Hahaha".ToSha512(),
                FirstName = "Mengye",
                PreferredName = "Mengye",
                LastName = "Ren"
            };
            op.Insert(user);
            Assert.IsTrue(op.Contains(user.ID));

            op.Remove(user);
        }

        [TestMethod]
        public void EmptyYield()
        {
            List<long> list = new List<long>();
            var a = TestYield(list);
        }

        IEnumerable<long> TestYield(IEnumerable<long> source)
        {
            foreach (long item in source)
            {
                yield return 0;
            }
        }
    }
}
