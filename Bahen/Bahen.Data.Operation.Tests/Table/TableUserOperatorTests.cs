﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Diagnostics;

namespace Bahen.Data.Operation.Tests.Table
{
    [TestClass]
    public class TableUserOperatorTests
    {
        private TableUserOperator userOperator;
        private const string csRunPath = @"C:\Program Files\Microsoft SDKs\Windows Azure\Emulator\csrun.exe";

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext context)
        {
            var count = Process.GetProcessesByName("DSService").Length;
            if (count == 0)
            {
                StartStorage();
            }
        }

        [TestInitialize]
        public void Init()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString;
            this.userOperator = new TableUserOperator(
                new TableConnectionBuilder().Build(connectionString));
        }

        [TestMethod]
        public void InsertUserBasics()
        {
            SqlUserOperator tempOp = new SqlUserOperator(
                new SqlConnectionBuilder().Build(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString));
            SqlUserEntity userSq;
            tempOp.Insert(userSq = new SqlUserEntity()
            {
                LoginEmail="renmengye@gmail.com",
                LoginPassword="haaha".ToSha512(),
                PreferredName="Mengye Ren"
            });

            TableUserEntity user = new TableUserEntity()
            {
                UserID = userSq.ID,
                LoginEmail = "renmengye@gmail.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };

            this.userOperator.Insert(user);
            Assert.IsTrue(userOperator.TryGet(user.UserID, out user));

            this.userOperator = new TableUserOperator(
                new TableConnectionBuilder().Build(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString));
            user = this.userOperator.Get(user.UserID);
            tempOp.Remove(userSq);
            this.userOperator.Remove(user);
        }

        [TestMethod, ExpectedException(typeof(UniqueKeyCollisionException))]
        public void InsertUserSameKey()
        {
            TableUserEntity user = new TableUserEntity()
            {
                UserID = 1,
                LoginEmail = "renmengye@gmail.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };
            this.userOperator.Insert(user);

            TableUserEntity user2 = new TableUserEntity()
            {
                UserID = 1,
                LoginEmail = "renmengye@yahoo.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };
            try
            {
                this.userOperator.Insert(user2);
            }
            finally
            {
                this.userOperator.Remove(user);
            }
        }

        [TestMethod]
        public void UpdateBasics()
        {
            TableUserEntity user = new TableUserEntity()
            {
                UserID = 1,
                LoginEmail = "renmengye@gmail.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };
            this.userOperator.Insert(user);
            this.userOperator = new TableUserOperator(
                new TableConnectionBuilder().Build(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString));
            //Assert.IsTrue(this.userOperator.Get(user.ID).Equals(user));
            user.FirstName = "Steven";
            try
            {
                this.userOperator.Update(user);
                Assert.AreEqual("Steven", this.userOperator.Get(user.ID).FirstName);
            }
            finally
            {
                user = this.userOperator.Get(user.ID);
                this.userOperator.Remove(user);
            }
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void UpdateUserNotExist()
        {
            TableUserEntity user = new TableUserEntity()
            {
                UserID = 1,
                LoginEmail = "renmengye@gmail.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };

            this.userOperator.Insert(user);
            this.userOperator.Remove(user);
            this.userOperator.Update(user);
        }

        [TestMethod, ExpectedException(typeof(EntityNotFoundException))]
        public void RemoveUserTwice()
        {
            TableUserEntity user = new TableUserEntity()
            {
                UserID = 1,
                LoginEmail = "renmengye@gmail.com",
                FirstName = "Mengye",
                LastName = "Ren"
            };

            this.userOperator.Insert(user);
            Assert.IsTrue(this.userOperator.Contains(user.UserID));
            //this.userOperator = new TableUserOperator(ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            this.userOperator.Remove(user);
            this.userOperator.Remove(user);
        }

        #region UnknownExceptions
        //[TestMethod, ExpectedException(typeof(BackendUnknownException))]
        //public void InsertUserUnknownException()
        //{
        //    ShutDownStorage();

        //    TableUserEntity user = new TableUserEntity()
        //    {
        //        UserID = 1,
        //        LoginEmail = "renmengye@gmail.com",
        //        FirstName = "Mengye",
        //        LastName = "Ren"
        //    };

        //    try
        //    {
        //        this.userOperator.Insert(user);
        //    }
        //    finally
        //    {
        //        StartStorage();
        //    }
        //}

        [TestMethod, ExpectedException(typeof(BackendUnknownException))]
        public void InvalidConnectionString()
        {
            try
            {
                this.userOperator = new TableUserOperator(
                new TableConnectionBuilder().Build(""));
            }
            finally
            {
                this.userOperator = new TableUserOperator(
                new TableConnectionBuilder().Build(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString));
            }
        }

        #endregion

        public static void StartStorage()
        {
            //Bahen.Common.CmdUtilities.RunCommand(csRunPath, "/devstore:start /devfabric:start");
        }

        public static void ShutDownStorage()
        {
            //Bahen.Common.CmdUtilities.RunCommand(csRunPath, "/devstore:shutdown /devfabric:shutdown");
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            //Bahen.Common.CmdUtilities.RunCommand(csRunPath, "/devstore:shutdown /devfabric:shutdown");
        }
    }
}
