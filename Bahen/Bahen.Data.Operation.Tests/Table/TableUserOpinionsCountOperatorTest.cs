﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using Bahen.Data.Operation;
using Bahen.Data.Models;
using System.Configuration;
using System.Diagnostics;

namespace Bahen.Data.Operation.Tests.Table
{
    [TestClass]
    public class TableUserOpinionsCountOperatorTest
    {
        [TestMethod]
        public void UserOpinionsConcurrencyTest()
        {
            Trace.WriteLine("start");
            Thread thread1 = new Thread(new ThreadStart(TestProcs.ThreadProc));
            Thread thread2 = new Thread(new ThreadStart(TestProcs.ThreadProc));
            thread1.Start();
            Thread.Sleep(1000);
            thread2.Start();
            thread1.Join();
            thread2.Join();
        }
    }
    class TestProcs
    {
        public static void ThreadProc()
        {
            var op = new TableCountOperator(ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString, "LikeCountTest");
            string targetString = "123456789";
            EntityTypeCode targetTypeCode = EntityTypeCode.Event;

            bool success = false;

            for (int i = 0; i < 50; i++)
            {
                Trace.Write("iteration" + i.ToString());
                TableEntityCount count;
                if (op.TryGet(TableEntityCount.ComputePartitionKey(targetString, targetTypeCode),
                    TableEntityCount.ComputeRowKey(targetString, targetTypeCode), out count))
                {
                    count.Count++;
                    Thread.Sleep(2000);
                    try
                    {
                        op.Update(count);
                        success = true;
                        break;
                    }
                    catch (ConcurrencyCollisionException)
                    {
                        Trace.Write("Collision");
                        continue;
                    }
                }
                // Count does not exist yet
                else
                {
                    try
                    {
                        op.Insert(new TableEntityCount(targetString, targetTypeCode)
                        {
                            Count = 1
                        });
                        success = true;
                        break;
                    }
                    catch (UniqueKeyCollisionException) // Someone else inserted first while we are trying to insert 
                    {
                        Trace.WriteLine("UniqueKeyCollision");
                        continue;   // Try increment again
                    }
                }
            }
            if (!success)
            {
                throw new BackendUnknownException("Concurrency Busy");
            }

        }
    }
}
