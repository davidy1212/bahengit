﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Bahen.Data.Operation;
using System.Configuration;
using System.Linq;

namespace Bahen.Data.Operation.Tests.Table
{
    [TestClass]
    public class TableIndexOperatorTests
    {
        [TestMethod]
        public void Basic()
        {
            string type = "user";
            string prefix = "haha";
            string key = "ahah";
            HashSet<long> set = new HashSet<long>();
            for (int i = 0; i < 3000; i++)
            {
                set.Add(i);
            }
            TableIndexOperator indexOperator = new TableIndexOperator(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);

            indexOperator.Insert(type,prefix, key, set);

            try
            {
                HashSet<long> data = indexOperator.GetByPrefixKey(type,prefix, key);
                Assert.IsTrue(data.SequenceEqual(set));
            }
            finally
            {
                indexOperator.Remove(type, prefix, key);
            }
        }
    }
}
