﻿using Bahen.DataModels.Constants;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.TestPerf
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(DateTime.Now);
            TableLikeOperator likeOp = new TableLikeOperator(
                "UseDevelopmentStorage=true");
            for (int i = 0; i < 10; i++)
            {
                TableLikeEntity likeEntity = new TableLikeEntity(i.ToString(), EntityTypeCode.Event, 03);
                likeOp.Insert(likeEntity);
            }
            Console.WriteLine(DateTime.Now);
            Console.ReadLine();

            //TableCycleOperator cycleOp = new TableCycleOperator(
            //    "UseDevelopmentStorage=true");
            //for (int i = 0; i < 10; i++)
            //{
            //    TableCycleEntity likeEntity = new TableCycleEntity();
            //    likeEntity.CycleID = i;
            //    cycleOp.Insert(likeEntity);
            //}
        }
    }
}
