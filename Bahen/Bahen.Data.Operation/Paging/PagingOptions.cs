﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Option class sending to paging
    /// </summary>
    public class PagingOptions
    {
        /// <summary>
        /// Paging mode
        /// </summary>
        public PagingMode Mode { get; set; }

        /// <summary>
        /// ID based cursor reference
        /// </summary>
        public long AfterInt64 { get; set; }

        /// <summary>
        /// ID based cursor reference
        /// </summary>
        public long BeforeInt64 { get; set; }

        /// <summary>
        /// StringId based cursor reference
        /// </summary>
        public string AfterString { get; set; }

        /// <summary>
        /// StringId based cursor reference
        /// </summary>
        public string BeforeString { get; set; }

        /// <summary>
        /// Offset based reference
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Max size of the list
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// Until (unix timestamp)
        /// </summary>
        public long Until { get; set; }
    }
}