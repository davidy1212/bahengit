﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Data.Operation
{
    public enum PagingMode
    {
        CursorAfterInt64, CursorBeforeInt64, CursorAfterString, CursorBeforeString, Offset, TimeSince, TimeUntil
    }
}