﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public class PagedResult<T>
    {
        public PagingOptions Options { get; set; }
        public IQueryable<T> Data { get; set; }
        public bool HasBefore { get; set; }
        public bool HasAfter { get; set; }
    }

    public class PagedInt64IdResult<T> : PagedResult<T> where T : IInt64Id
    {
        public long Before { get; set; }
        public long After { get; set; }

        public PagedInt64IdResult<TO> Clone<TO>() where TO : IInt64Id
        {
            PagedInt64IdResult<TO> result = new PagedInt64IdResult<TO>();

            result.After = this.After;
            result.Before = this.Before;
            result.HasAfter = this.HasAfter;
            result.HasBefore = this.HasBefore;
            result.Options = this.Options;

            return result;
        }

    }

    public class PagedStringIdResult<T> : PagedResult<T> where T : IStringId
    {
        public string Before { get; set; }
        public string After { get; set; }

        public PagedStringIdResult<TO> Clone<TO>() where TO : IStringId
        {
            PagedStringIdResult<TO> result = new PagedStringIdResult<TO>();

            result.After = this.After;
            result.Before = this.Before;
            result.HasAfter = this.HasAfter;
            result.HasBefore = this.HasBefore;
            result.Options = this.Options;

            return result;
        }
    }
}
