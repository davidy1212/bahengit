﻿using Bahen.Common;
using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of upload/download of the album pictures.
    /// Once constructed, this operator will be bound to one album.
    /// To get other album results, construct another album picture operator.
    /// </summary>
    public class BlobAlbumPictureOperator : BlobDataOperator
    {
        /// <summary>
        /// Construct an album picture operator
        /// </summary>
        /// <param name="connectionString">Connection to data storage</param>
        /// <param name="album">Album entity that the album belongs to</param>
        public BlobAlbumPictureOperator(string connectionString, TableAlbumEntity album)
            : base(connectionString, ComputeContainerName(album))
        {
            this.Album = album;
        }

        /// <summary>
        /// Album that the operator bound to
        /// </summary>
        public TableAlbumEntity Album { get; set; }

        /// <summary>
        /// Upload an album picture
        /// </summary>
        /// <param name="albumID">Album ID</param>
        /// <param name="guid">Picture guid</param>
        /// <param name="size">Size of the picture</param>
        /// <param name="content">Content to upload</param>
        public void Upload(Guid guid, PictureSize size, Stream content)
        {
            base.Upload(ComputeBlobName(this.Album.OwnerID, this.Album.ID, guid, size),
                content);
        }

        /// <summary>
        /// Get the last modified date of the picture
        /// </summary>
        /// <param name="guid">Picture guid</param>
        /// <param name="size">Size of the picture</param>
        /// <returns>Last modified date of the picture</returns>
        public DateTime GetLastModifiedDate(Guid guid, PictureSize size)
        {
            return base.GetLastModifiedTime(ComputeBlobName(this.Album.OwnerID, this.Album.ID, guid, size));
        }

        /// <summary>
        /// Download an album picture
        /// </summary>
        /// <param name="guid">Picture guid</param>
        /// <param name="size">Size of the picture</param>
        /// <param name="content">Stream to download the content</param>
        public void Download(Guid guid, PictureSize size, Stream content)
        {
            base.Download(ComputeBlobName(this.Album.OwnerID, this.Album.ID, guid, size),
                content);
        }

        /// <summary>
        /// Delete an album picture
        /// </summary>
        /// <param name="guid">Picture guid</param>
        /// <param name="size">Size of the picture</param>
        public void Delete(Guid guid, PictureSize size)
        {
            base.Delete(ComputeBlobName(this.Album.OwnerID, this.Album.ID, guid, size));
        }

        /// <summary>
        /// Get all the picture urls of inside an album
        /// </summary>
        /// <param name="size">Size of the picture</param>
        /// <returns>A collection of urls of the pictures in the album</returns>
        public IEnumerable<string> GetAlbumPictureUrls(PictureSize size)
        {
            // Get all users in the container
            CloudBlobDirectory album = base.Container.GetDirectoryReference(BaseConvert.ToHexString(this.Album.OwnerID).Substring(14) + "/" + this.Album.ID.ToString("N"));

            return album.ListBlobs()
                .ToList()
                .Where(x =>
                x.Uri.AbsolutePath
                .EndsWith(size.ToString().ToLowerInvariant() + ".png"))
            .Select(x =>
                    x.Uri.AbsolutePath.Substring(
                    x.Container.Uri.AbsolutePath.Length + 1)
                    );
        }

        /// <summary>
        /// Similar to calculate the partition key of the album
        /// </summary>
        /// <param name="album">Album entity in the table</param>
        private static string ComputeContainerName(TableAlbumEntity album)
        {
            return album.AlbumPrefix + BaseConvert.ToHexString(album.OwnerID).Substring(0, 14);
        }

        /// <summary>
        /// Compute the blob name of the album picture, simlilar to the row key
        /// </summary>
        /// <param name="ownerID">Owner ID of the album</param>
        /// <param name="albumID">Album ID</param>
        /// <param name="guid">Picture Guid</param>
        /// <param name="size">Size specification</param>
        /// <returns></returns>
        private static string ComputeBlobName(long ownerID, Guid albumID, Guid guid, PictureSize size)
        {
            return String.Format(
                "{0}/{1}/{2}_{3}.png",
                BaseConvert.ToHexString(ownerID),
                   albumID.ToString("N"),
                   guid.ToString("N"),
                   size.ToString().ToLowerInvariant());
        }
    }
}
