﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Blob operator in charge of backing up the index file
    /// </summary>
    public class BlobIndexOperator : BlobDataOperator
    {
        public const string Address= "index.bin";

        /// <summary>
        /// Construct a description operator
        /// </summary>
        /// <param name="connectionString">Connection to data storage</param>
        public BlobIndexOperator(string connectionString)
            : base(connectionString, "index")
        {
        }

        /// <summary>
        /// Upload the index file
        /// </summary>
        /// <param name="content"></param>
        public void Upload(Stream content)
        {
            base.Upload(Address, content);
        }

        /// <summary>
        /// Download the index file
        /// </summary>
        /// <param name="content"></param>
        public void Download(Stream content)
        {
            base.Download(Address, content);
        }

        public DateTime GetLastModifiedTime()
        {
            return base.GetLastModifiedTime(Address);
        }
    }
}
