﻿using Bahen.Common;
using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the upload/download of descriptions
    /// </summary>
    public class BlobDescriptionOperator : BlobDataOperator
    {
        /// <summary>
        /// Construct a description operator
        /// </summary>
        /// <param name="connectionString">Connection to data storage</param>
        public BlobDescriptionOperator(string connectionString)
            : base(connectionString, "description")
        {
        }

        /// <summary>
        /// Upload a description text
        /// </summary>
        /// <param name="prefix">Prefix of the blob upload address</param>
        /// <param name="ID">ID of the description text owner</param>
        /// <param name="uploadGuid">Unique identifier of the description text</param>
        /// <param name="content">The content of the description format (already resized)</param>
        public void Upload(string prefix, long ID, Guid uploadGuid, Stream content)
        {
            base.Upload(
                String.Format("{0}_{1}/{2}.txt",
                prefix,
                BaseConvert.ToHexString(ID),
                uploadGuid),
                content);
        }

        /// <summary>
        /// Download a description text
        /// </summary>
        /// <param name="prefix">Prefix of the blob upload address</param>
        /// <param name="ID">ID of the description text owner</param>
        /// <param name="uploadGuid">Unique identifier of the description text</param>
        /// <param name="output">The output stream ready to receive the download content</param>
        public void Download(string prefix, long ID, string uploadId, Stream output)
        {
            base.Download(
                String.Format("{0}_{1}/{2}.txt",
                prefix,
                BaseConvert.ToHexString(ID),
                uploadId),
                output);
        }

        /// <summary>
        /// Get the description text of an event
        /// </summary>
        /// <param name="entity">Event entity</param>
        /// <returns>Description text</returns>
        public string GetEventDescription(EventEntity entity)
        {
            using (MemoryStream downloadStream = new MemoryStream())
            {
                this.Download(
                    "e",
                    entity.ID,
                    entity.DescriptionBlobId,
                    downloadStream
                    );
                downloadStream.Seek(0, SeekOrigin.Begin);
                StreamReader reader = new StreamReader(downloadStream);
                return reader.ReadToEnd();
            }
        }
    }
}
