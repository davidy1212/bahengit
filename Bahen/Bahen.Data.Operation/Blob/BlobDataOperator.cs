﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Operator in change of one blob container in blob storage
    /// </summary>
    public abstract class BlobDataOperator
    {
        /// <summary>
        /// Construct a blob data operator with connection string and container name
        /// </summary>
        /// <param name="connectionString">Connection string to storage account</param>
        /// <param name="containerName">Name of the container to be in charge of</param>
        public BlobDataOperator(string connectionString, string containerName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();
            this.Container = blobContainer;
        }

        public BlobDataOperator(string connectionString, string containerName, bool publicRead)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(containerName);
            blobContainer.CreateIfNotExists();
            this.Container = blobContainer;

            // set access
            BlobContainerPublicAccessType access;
            if (publicRead)
            {
                access = BlobContainerPublicAccessType.Blob;
            }
            else
            {
                access = BlobContainerPublicAccessType.Off;
            }
            blobContainer.SetPermissions(new BlobContainerPermissions()
            {
                PublicAccess = access
            });
        }

        /// <summary>
        /// Container that this data operator in charge of
        /// </summary>
        public CloudBlobContainer Container { get; set; }

        /// <summary>
        /// Upload a stream to a new blob
        /// </summary>
        /// <param name="blobName">Name of the blob</param>
        /// <param name="content">Content stream to be uploaded</param>
        public virtual void Upload(string blobName, Stream content)
        {
            CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
            blockBlob.UploadFromStream(content);
        }

        public virtual IAsyncResult BeginUpload(string blobName, Stream content, AsyncCallback callback)
        {
            CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
            return blockBlob.BeginUploadFromStream(content, callback, blockBlob);
        }

        public virtual string GetBlobAbsoluteUrl(string blobName)
        {
            CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
            return blockBlob.Uri.AbsoluteUri.ToString();
        }

        /// <summary>
        /// Get last modified date
        /// </summary>
        /// <param name="blobName">Name of the blob</param>
        /// <returns>Last modified date</returns>
        public virtual DateTime GetLastModifiedTime(string blobName)
        {
            try
            {
                CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
                return blockBlob.Properties.LastModified.Value.DateTime;
            }
            catch
            {
                return new DateTime();
            }
        }

        /// <summary>
        /// Download a blob content to a stream
        /// </summary>
        /// <param name="blobName">Name of the blob</param>
        /// <param name="content">Stream to receive the download content</param>
        public virtual void Download(string blobName, Stream content)
        {
            CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
            blockBlob.DownloadToStream(content);
        }

        /// <summary>
        /// Delete a blob
        /// </summary>
        /// <param name="blobName">Name of the blob</param>
        public virtual void Delete(string blobName)
        {
            CloudBlockBlob blockBlob = this.Container.GetBlockBlobReference(blobName);
            blockBlob.Delete();
        }
    }
}
