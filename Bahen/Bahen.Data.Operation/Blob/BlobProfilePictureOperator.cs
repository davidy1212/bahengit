﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Bahen.Common;
using System.ComponentModel;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of upload/download of the profile picture
    /// </summary>
    public class BlobProfilePictureOperator : BlobDataOperator
    {
        /// <summary>
        /// Construct a profile picture operator
        /// </summary>
        /// <param name="connectionString">Connection to data storage</param>
        public BlobProfilePictureOperator(string connectionString)
            : base(connectionString, "ppic", true)
        {
        }

        /// <summary>
        /// Upload a profile picture
        /// </summary>
        /// <param name="prefix">Prefix of the blob upload address</param>
        /// <param name="id">ID of the picture owner</param>
        /// <param name="guid">Unique identifier of the picture</param>
        /// <param name="size">Size info of the picture</param>
        /// <param name="content">Content of the picture in png format (already resized)</param>
        public void Upload(string prefix, long id, Guid guid, PictureSize size, Stream content)
        {
            // Compute url first based on id

            base.Upload(
                ComputeProfilePictureUrl(prefix, id, guid, size),
                content);
        }


        public virtual IAsyncResult BeginUpload(string prefix, long id, Guid guid, PictureSize size, Stream content, AsyncCallback callback)
        {
            return base.BeginUpload(
                ComputeProfilePictureUrl(prefix, id, guid, size),
                content, callback);
        }

        /// <summary>
        /// Compute the picture url
        /// </summary>
        /// <param name="prefix">Category of profile picture (e.g. user, event, cycle)</param>
        /// <param name="id">ID of the entity</param>
        /// <param name="guid">Random string of the picture</param>
        /// <param name="size">Size of the picture</param>
        /// <returns></returns>
        public static string ComputeProfilePictureUrl(string prefix, long id, Guid guid, PictureSize size)
        {
            return String.Format("{0}/{1}/{2}_{3}.png",
                prefix,
                BaseConvert.ToHexShort(id),
                guid.ToString("N"),
                size.ToString().ToLowerInvariant());
        }

        ///// <summary>
        ///// Get last modified date of a picture
        ///// </summary>
        ///// <param name="prefix">Prefix of the blob upload address</param>
        ///// <param name="ID">ID of the picture owener</param>
        ///// <param name="guid">Unique identifier of the picture</param>
        ///// <param name="size">Size info of the picture</param>
        ///// <returns></returns>
        //public DateTime GetLastModifiedDate(string prefix, long ID, PictureSize size)
        //{
        //    return base.GetLastModifiedTime(
        //        String.Format("{0}_{1}/{2}_{3}.png",
        //        prefix,
        //        HashUtilities.ToHexString(ID),
        //        guid,
        //        size.ToString().ToLowerInvariant()));
        //}

        ///// <summary>
        ///// Download a profile picture
        ///// </summary>
        ///// <param name="prefix">Prefix of the blob upload address</param>
        ///// <param name="ID">ID of the picture owner</param>
        ///// <param name="guid">Unique identifier of the picture</param>
        ///// <param name="size">The size info of the picture</param>
        ///// <param name="output">The output stream ready to receive the download content</param>
        //public void Download(string prefix, long ID, Guid guid, PictureSize size, Stream output)
        //{
        //    // Retrieve url from sql first based on id

        //    base.Download(
        //        String.Format("{0}_{1}/{2}_{3}.png",
        //        prefix,
        //        HashUtilities.ToHexString(ID),
        //        guid,
        //        size.ToString().ToLowerInvariant()),
        //        output);
        //}

        //private string CreatePictureUrl(ProfilePictureType t, long id)
        //{

        //}
    }

}
