using Bahen.Data.Models;
using Bahen.Data.Models;
using System.Data.Entity;

namespace Bahen.Data.Operation
{
    public class SqlContext : DbContext
    {
        static SqlContext()
        {
            Database.SetInitializer<SqlContext>(null);
        }

        //public SqlContext() : base("Name=SqlContext") { }
        public SqlContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.ConnectionString = nameOrConnectionString;
        }

        public string ConnectionString { get; set; }
        public DbSet<SqlApiAuthRelation> ApiAuth { get; set; }
        public DbSet<SqlApiClientEntity> ApiClients { get; set; }
        public DbSet<SqlAttendanceRelation> Attendances { get; set; }
        public DbSet<SqlCookieEntity> Cookies { get; set; }
        public DbSet<SqlCycleEventRelation> CycleEvents { get; set; }
        public DbSet<SqlCycleEntity> Cycles { get; set; }
        public DbSet<SqlCycleUserRelation> CycleUsers { get; set; }
        public DbSet<SqlEventEntity> Events { get; set; }
        public DbSet<SqlSubscriptionRelation> Subscriptions { get; set; }
        public DbSet<SqlUserEntity> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApiAuthMap());
            modelBuilder.Configurations.Add(new ApiClientMap());
            modelBuilder.Configurations.Add(new AttendanceMap());
            modelBuilder.Configurations.Add(new CookieMap());
            modelBuilder.Configurations.Add(new CycleEventMap());
            modelBuilder.Configurations.Add(new CycleMap());
            modelBuilder.Configurations.Add(new CycleUserMap());
            modelBuilder.Configurations.Add(new EventMap());
            modelBuilder.Configurations.Add(new SubscriptionMap());
            modelBuilder.Configurations.Add(new UserMap());
        }
    }
}
