using Bahen.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bahen.Common;
using Microsoft.WindowsAzure.Storage.Table;

namespace Bahen.Data.Operation
{
    public class TableCommentOperator : TableUserOpinionOperator<TableCommentEntity>
    {
        public TableCommentOperator(string connectionString)
            : base(connectionString, "Comments")
        {
        }

        public TableCommentOperator(CloudTableClient client)
            : base(client, "Comments")
        {
        }
    }
}
