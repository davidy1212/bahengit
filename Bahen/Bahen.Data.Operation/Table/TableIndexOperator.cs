﻿using Bahen.Common;
using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the manual indice of the databases
    /// </summary>
    public class TableIndexOperator : TableDataOperator<TableIndexEntity>
    {
        /// <summary>
        /// Construct a new index operator
        /// </summary>
        /// <param name="connectionString">Connection used to connect table database</param>
        public TableIndexOperator(string connectionString) : base(connectionString, "Indices") { }

        /// <summary>
        /// Insert a key with a collection of IDs
        /// </summary>
        /// <param name="typeString">Type of the index entity</param>
        /// <param name="prefix">Prefix of the key</param>
        /// <param name="key">Key of the ID collection</param>
        /// <param name="value">ID collection</param>
        public void Insert(string typeString, string prefix, string key, HashSet<long> value)
        {
            // Partition the HashSet into pieces of 1024
            for (int i = 0; i <= value.Count / 1024; i++)
            {
                byte[] part = ByteConvert.GetBytes(value.Skip(i * 1024).Take(1024).ToArray());
                TableIndexEntity entity = new TableIndexEntity()
                {
                    IndexNumber = i,
                    Root = key,
                    Type = prefix,
                    Value = part
                };
                this.Insert(entity, false);
            }
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Update a key with a collection of IDs
        /// </summary>
        /// <param name="typeString">Type of the index entity</param>
        /// <param name="prefix">Prefix of the key</param>
        /// <param name="key">Key of the entry to be updated</param>
        /// <param name="value">Value to be updated</param>
        public void Update(string typeString, string prefix, string key, HashSet<long> value)
        {
            this.Remove(typeString, prefix, key);
            this.Insert(typeString, prefix, key, value);
        }

        /// <summary>
        /// Remove a key-value entry
        /// </summary>
        /// <param name="typeString">Type of the index entity</param>
        /// <param name="prefix">Prefix of the key</param>
        /// <param name="key">Key of the entry to be removed</param>
        public void Remove(string typeString, string prefix, string key)
        {
            // Find all entries starts with key (because appended by indexNumber)
            var entities = this.Context.CreateQuery<TableIndexEntity>(this.TableName)
                .Where(x => x.PartitionKey == typeString
                    && x.RowKey.CompareTo(String.Format("{0}/{1}{2}", prefix, key, "0")) > 0
                    && x.RowKey.CompareTo(String.Format("{0}/{1}{2}", prefix, key, "__")) < 0);
            foreach (var entity in entities)
            {
                this.Context.DeleteObject(entity);
            }
            this.Context.SaveChanges();
        }

        /// <summary>
        /// Get the key-id pair by prefix and key
        /// </summary>
        /// <param name="typeString">Type of the index entity</param>
        /// <param name="prefix">Prefix of the key</param>
        /// <param name="key">Key</param>
        /// <returns>Set of IDs matching the prefix and key</returns>
        public HashSet<long> GetByPrefixKey(string typeString, string prefix, string key)
        {
            // Find all entries starts with key (because appended by indexNumber)
            var entities = this.Context.CreateQuery<TableIndexEntity>(this.TableName)
                .Where(x => x.PartitionKey == typeString
                    && x.RowKey.CompareTo(String.Format("{0}/{1}{2}", prefix, key, "0")) > 0
                    && x.RowKey.CompareTo(String.Format("{0}/{1}{2}", prefix, key, "__")) < 0);
            HashSet<long> result = new HashSet<long>();
            foreach (var entity in entities)
            {
                result.UnionWith(ByteConvert.GetInt64(entity.Value));
            }
            return result;
        }
    }
}
