﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table;


namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the cycle container in Azure Table Service
    /// </summary>
    public class TableCycleOperator : TableDataOperator<TableCycleEntity>
    {
        /// <summary>
        /// Initiate the class by defining the name of the cycles table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        public TableCycleOperator(string connectionString) : base(connectionString, "Cycles") { }

        /// <summary>
        /// Initiate the class by defining the name of the cycles table
        /// </summary>
        /// <param name="client">Storage client that have data access</param>
        public TableCycleOperator(CloudTableClient client) : base(client, "Cycles") { }

        /// <summary>
        /// Get a cycle from the table based on cycle ID
        /// </summary>
        /// <param name="ID">Cycle ID</param>
        /// <returns>The cycle table entity, null if not found</returns>
        public TableCycleEntity Get(long ID)
        {
            return base.Get(TableCycleEntity.ComputePartitionKey(ID), TableCycleEntity.ComputeRowKey(ID));
        }

        /// <summary>
        /// Determine whether the cycle ID exists
        /// </summary>
        /// <param name="ID">Cycle ID</param>
        /// <returns>Whether the cycle ID exists in table</returns>
        public bool Contains(long ID)
        {
            return base.Contains(TableCycleEntity.ComputePartitionKey(ID), TableCycleEntity.ComputeRowKey(ID));
        }

        /// <summary>
        /// Try get a cycle based on ID
        /// </summary>
        /// <param name="ID">Cser ID</param>
        /// <param name="result">Search result, null if not found</param>
        /// <returns>Whether the cycle ID exists</returns>
        public bool TryGet(long ID, out TableCycleEntity result)
        {
            return base.TryGet(TableCycleEntity.ComputePartitionKey(ID), TableCycleEntity.ComputeRowKey(ID), out result);
        }
    }
}
