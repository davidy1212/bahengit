﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Bahen.Data.Models;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation
{
    public class TableLikeOperator : TableUserOpinionOperator<TableLikeEntity>
    {
        public TableLikeOperator(string connectionString)
            : base(connectionString, "Likes")
        {
            //countOp = new TableLikeCountOperator(connectionString);
        }

        public TableLikeOperator(CloudTableClient client)
            : base(client, "Likes")
        {
            //countOp = new TableLikeCountOperator(client);
        }

        //private TableLikeCountOperator countOp;

        //public void Insert(TableLikeEntity likeEntity)
        //{
        //    countOp.Increment(likeEntity.TargetString, ((EntityTypeCode)likeEntity.TargetTypeCode));
        //    try
        //    {
        //        base.Insert(likeEntity);
        //    }
        //    catch (Exception ex)
        //    {
        //        countOp.Decrement(likeEntity.TargetString, ((EntityTypeCode)likeEntity.TargetTypeCode));
        //    }
        //}

        //public void Remove(TableLikeEntity likeEntity)
        //{
        //    countOp.Decrement(likeEntity.TargetString, ((EntityTypeCode)likeEntity.TargetTypeCode));
        //    try
        //    {
        //        base.Remove(likeEntity);
        //    }
        //    catch (Exception ex)
        //    {
        //        countOp.Increment(likeEntity.TargetString, ((EntityTypeCode)likeEntity.TargetTypeCode));
        //    }
        //}
    }
}
