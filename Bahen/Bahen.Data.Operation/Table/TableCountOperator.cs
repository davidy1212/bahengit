﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bahen.Data.Models;
using Bahen.Data.Operation;

namespace Bahen.Data.Operation
{
    public class TableCountOperator : TableDataOperator<TableEntityCount>
    {
        public TableCountOperator(string connectionString, string tableName)
            : base(connectionString, tableName)
        {
        }

        public TableCountOperator(CloudTableClient client, string tableName)
            : base(client, tableName)
        {
        }

        public int GetCount(string targetString, EntityTypeCode targetTypeCode)
        {
            TableEntityCount count;

            if (this.TryGet(TableEntityCount.ComputePartitionKey(targetString, targetTypeCode),
                    TableEntityCount.ComputeRowKey(targetString, targetTypeCode), out count))
            {
                return count.Count;
            }
            else
            {
                return 0;
            }
        }

        public void Increment(String targetString, EntityTypeCode targetTypeCode)
        {
            bool success = false;

            for (int i = 0; i < 50; i++)
            {
                TableEntityCount count;
                if (this.TryGet(TableEntityCount.ComputePartitionKey(targetString, targetTypeCode),
                    TableEntityCount.ComputeRowKey(targetString, targetTypeCode), out count))
                {
                    count.Count++;
                    try
                    {
                        this.Update(count);
                        success = true;
                        break;
                    }
                    catch (ConcurrencyCollisionException)
                    {
                        continue;
                    }
                }
                // Count does not exist yet
                else
                {
                    try
                    {
                        this.Insert(new TableEntityCount(targetString, targetTypeCode)
                        {
                            Count = 1
                        });
                        success = true;
                        break;
                    }
                    catch (UniqueKeyCollisionException) // Someone else inserted first while we are trying to insert 
                    {
                        continue;   // Try increment again
                    }
                }
            }
            if (!success)
            {
                throw new BackendUnknownException("Concurrency Busy");
            }
        }
        public void Decrement(String targetString, EntityTypeCode targetTypeCode)
        {
            bool success = false;

            for (int i = 0; i < 50; i++)
            {
                TableEntityCount count;
                if (this.TryGet(TableEntityCount.ComputePartitionKey(targetString, targetTypeCode),
                    TableEntityCount.ComputeRowKey(targetString, targetTypeCode), out count))
                {
                    count.Count--;
                    try
                    {
                        this.Update(count);
                        success = true;
                        break;
                    }
                    catch (ConcurrencyCollisionException)
                    {
                        continue;
                    }
                }
                // Count does not exist yet
                else
                {
                    try
                    {
                        this.Insert(new TableEntityCount(targetString, targetTypeCode)
                        {
                            Count = -1
                        });
                        success = true;
                        break;
                    }
                    catch (UniqueKeyCollisionException) // Someone else inserted first while we are trying to insert 
                    {
                        continue;   // Try increment again
                    }
                }
            }
            if (!success)
            {
                throw new BackendUnknownException("Concurrency Busy");
            }
        }
    }
}
