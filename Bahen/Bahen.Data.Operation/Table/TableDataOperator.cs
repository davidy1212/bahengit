﻿using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// A base class for Azure Table operators
    /// </summary>
    /// <typeparam name="T">Table entity types defined in Bahen.Data.Models</typeparam>
    public abstract class TableDataOperator<T> : IDataOperator<T> where T : TableServiceEntity
    {
        /// <summary>
        /// Constructor by specifying the name fo the table.
        /// Create the table if not exists
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        /// <param name="tableName">Name of the table</param>
        /// <exception cref="BackendUnknownException">Cannot connect to table storage for some unknown reason</exception>
        protected TableDataOperator(string connectionString, string tableName)
            : this(new TableConnectionBuilder().Build(connectionString), tableName)
        { }

        /// <summary>
        /// Constructor by specifying the name fo the table.
        /// Create the table if not exists
        /// </summary>
        /// <param name="client">Storage client that have data access</param>
        /// <param name="tableName">Name of the table</param>
        protected TableDataOperator(CloudTableClient client, string tableName)
        {
            this.Client = client;
            this.TableName = tableName;
            CloudTable table = this.Client.GetTableReference(tableName);
            table.CreateIfNotExists();
            this.Context = client.GetTableServiceContext();
        }

        /// <summary>
        /// Cloud storage account (a connection string)
        /// </summary>
        protected CloudStorageAccount Account { get; set; }

        /// <summary>
        /// Client that is created every time connected to the table
        /// </summary>
        protected CloudTableClient Client { get; set; }

        /// <summary>
        /// Context used to track the state of the entities
        /// </summary>
        public TableServiceContext Context { get; set; }

        /// <summary>
        /// Name of the table that the operator is in charge of
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Insert a data entity into the table
        /// </summary>
        /// <param name="data">The data entity to be inserted</param>
        public virtual void Insert(T data)
        {
            this.Insert(data, true);
        }

        /// <summary>
        /// Insert a data entity into the table
        /// </summary>
        /// <param name="data">The data entity to be inserted</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="UniqueKeyCollisionException">The same partition key and row key already exist in the table</exception>
        /// <exception cref="BackendUnknownException">Cannot identify the exception type</exception>
        public virtual void Insert(T data, bool saveChanges)
        {
            if (String.IsNullOrEmpty(data.RowKey) || String.IsNullOrEmpty(data.PartitionKey))
            {
                throw new KeyUnspecifiedException(typeof(T));
            }

            this.Context.AddObject(this.TableName, data);

            try
            {
                if (saveChanges)
                {
                    this.Context.SaveChangesWithRetries();
                }
            }
            catch (System.Data.Services.Client.DataServiceRequestException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Context.DeleteObject(data);
                throw new UniqueKeyCollisionException(typeof(T), data.PartitionKey + data.RowKey, "ID", data.PartitionKey + data.RowKey, ex);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Context.DeleteObject(data);
                throw new BackendUnknownException(String.Format("Cannot insert {0} with partition key {1} and row key {2}", typeof(T).Name, data.PartitionKey, data.RowKey), ex);
            }
        }

        /// <summary>
        /// Insert the entity without save changes
        /// </summary>
        /// <param name="data">Data entity to be inserted</param>
        //public virtual void InsertNotSaveChanges(T data)
        //{
        //    if (String.IsNullOrEmpty(data.RowKey) || String.IsNullOrEmpty(data.PartitionKey))
        //    {
        //        throw new KeyUnspecifiedException(typeof(T));
        //    }

        //    this.Context.AddObject(this.TableName, data);
        //}

        /// <summary>
        /// Update a data entity to the tabnle
        /// </summary>
        /// <param name="data">The data entity to be updated</param>
        public virtual void Update(T data)
        {
            this.Update(data, true);
        }

        /// <summary>
        /// Update a data entity to the table
        /// </summary>
        /// <param name="data">The data entity to be updated</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="EntityNotFoundException">Cannot find the entity to update</exception>
        /// <exception cref="BackendUnknownException">Cannot identify the exception type</exception>
        public virtual void Update(T data, bool saveChanges)
        {
            try
            {
                this.Context.UpdateObject(data);
            }
            catch (ArgumentException)
            {
                this.Context.AttachTo(this.TableName, data, "*");
                this.Context.UpdateObject(data);
            }

            try
            {
                if (saveChanges)
                {
                    this.Context.SaveChangesWithRetries();
                }
            }
            catch (System.Data.Services.Client.DataServiceRequestException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Context.Detach(data);
                DataServiceClientException clientEx = ex.GetBaseException() as DataServiceClientException;
                throw new EntityNotFoundException(typeof(T), data.PartitionKey + data.RowKey, ex);

            }
            catch (Exception ex)
            {
                DataServiceClientException clientEx = ex.GetBaseException() as DataServiceClientException;
                if (clientEx != null && clientEx.StatusCode == 412)
                {
                    throw new ConcurrencyCollisionException();
                }
                else
                {
                    Trace.WriteLine(ex.ToString());
                    this.Context.Detach(data);
                    throw new BackendUnknownException(String.Format("Cannot update {0} with partition key {1} and row key {2}", typeof(T).Name, data.PartitionKey, data.RowKey), ex);
                }
            }
        }

        /// <summary>
        /// Remove a data entity form the table
        /// </summary>
        /// <param name="data">The data entity to be removed</param>
        public virtual void Remove(T data)
        {
            this.Remove(data, true);
        }

        /// <summary>
        /// Remove a data entity from the table
        /// </summary>
        /// <param name="data">The data entity to be removed</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="EntityNotFoundException">Cannot found the entity in table</exception>
        /// <exception cref="BackendUnknownException">Cannot identify the exception type</exception>
        public virtual void Remove(T data, bool saveChanges)
        {
            try
            {
                this.Context.DeleteObject(data);
            }
            catch (InvalidOperationException)
            {
                this.Context.AttachTo(this.TableName, data, "*");
                this.Context.DeleteObject(data);
            }

            try
            {
                if (saveChanges)
                {
                    this.Context.SaveChangesWithRetries();
                }
            }
            catch (System.Data.Services.Client.DataServiceRequestException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Context.Detach(data);
                throw new EntityNotFoundException(typeof(T), data.PartitionKey + data.RowKey, ex);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Context.Detach(data);
                throw new BackendUnknownException(String.Format("Cannot remove {0} with partition key {1} and row key {2}", typeof(T).Name, data.PartitionKey, data.RowKey), ex);
            }
        }

        /// <summary>
        /// Try get the data entity by querying the partition key and row key
        /// </summary>
        /// <param name="partitionKey">Partition key</param>
        /// <param name="rowKey">Row key</param>
        /// <param name="result">The search result, null if not found</param>
        /// <returns>Whether data is found</returns>
        public virtual bool TryGet(string partitionKey, string rowKey, out T result)
        {
            try
            {
                result = this.Get(partitionKey, rowKey);
                return result != null;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Check whether the data entity of the same partition key and row key exists in table
        /// </summary>
        /// <param name="ID">ID to compute </param>
        /// <returns>Whether the entity exists</returns>
        public virtual bool Contains(string partitionKey, string rowKey)
        {
            T result;
            return this.TryGet(partitionKey, rowKey, out result);
        }

        /// <summary>
        /// Batch save changes asynchronously
        /// </summary>
        public virtual void SaveChangesAsync()
        {
            this.Context.BeginSaveChangesWithRetries(SaveChangesOptions.Batch,
                (asyncResult => Context.EndSaveChangesWithRetries(asyncResult)),
                null);
        }

        /// <summary>
        /// Save changes synchronously
        /// </summary>
        public virtual void SaveChanges()
        {
            this.Context.SaveChangesWithRetries();
        }

        /// <summary>
        /// Get the data entity based on the partition key and row key
        /// </summary>
        /// <param name="partitionKey">Partition key</param>
        /// <param name="rowKey">Row key</param>
        /// <returns>The search result, null if not found</returns>
        public virtual T Get(string partitionKey, string rowKey)
        {
            var query =
                   from item in this.Context.CreateQuery<T>(this.TableName)
                   where item.PartitionKey == partitionKey && item.RowKey == rowKey
                   select item;
            return query.FirstOrDefault<T>();
        }

        /// <summary>
        /// Truncate data into pages. 
        /// Assume that the row key is already sorted and partition key is queried in raw data.
        /// </summary>
        /// <param name="rawData">Unpaged queryable data</param>
        /// <param name="options">Paging options</param>
        /// <returns>Paged data</returns>
        public virtual IQueryable<T> Truncate(IQueryable<T> rawData, PagingOptions options)
        {
            if (options == null)
            {
                return rawData;
            }
            if (options.Limit > 1000)
            {
                throw new NotSupportedException("Limit is too large. Please set limit < 1000.");
            }
            if (options.Mode == PagingMode.CursorAfterString)
            {
                return rawData
                    .Where(x => (x.RowKey.CompareTo(options.AfterString) > 0))
                    .Take(options.Limit);
            }
            else if (options.Mode == PagingMode.CursorBeforeString)
            {
                throw new NotSupportedException("Query before a cursor is currently not supported in table storage.");
            }
            else if (options.Mode == PagingMode.Offset)
            {
                return rawData
                    .Skip(options.Offset)
                    .Take(options.Limit);
            }
            return new List<T>().AsQueryable();
        }
    }
}
