﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the notification container in Azure Table Service
    /// </summary>
    public class TableNotificationOperator : TableMessageOperator<TableNotificationEntity>
    {
        /// <summary>
        /// Construct the eoperator by defining the name of the news feed table
        /// </summary>
        /// <param name="connectionString"></param>
        public TableNotificationOperator(string connectionString) :
            base(connectionString, "Notifications")
        {
        }
    }
}
