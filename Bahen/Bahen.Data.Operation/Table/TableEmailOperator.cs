﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the email container in Azure Table Service
    /// </summary>
    public class TableEmailOperator : TableMessageOperator<TableEmailEntity>
    {
        /// <summary>
        /// Construct the operator by defining the name of the email table
        /// </summary>
        /// <param name="connectionString">Connection strin gused to connect the table database</param>
        public TableEmailOperator(string connectionString)
            : base(connectionString, "Emails")
        {
        }
    }
}
