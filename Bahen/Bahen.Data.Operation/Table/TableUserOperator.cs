﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table;


namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the user container in Azure Table Service
    /// </summary>
    public class TableUserOperator : TableDataOperator<TableUserEntity>
    {
        /// <summary>
        /// Initiate the class by defining the name of the users table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        public TableUserOperator(string connectionString) : base(connectionString, "Users") { }

        /// <summary>
        /// Initiate the class by defining the name of the users table
        /// </summary>
        /// <param name="client">Storage client that have data access</param>
        public TableUserOperator(CloudTableClient client) : base(client, "Users") { }

        /// <summary>
        /// Get a user from the table based on user ID
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <returns>The user table entity, null if not found</returns>
        public TableUserEntity Get(long ID)
        {
            return base.Get(TableUserEntity.ComputePartitionKey(ID), TableUserEntity.ComputeRowKey(ID));
        }

        /// <summary>
        /// Determine whether the user ID exists
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <returns>Whether the user ID exists in table</returns>
        public bool Contains(long ID)
        {
            return base.Contains(TableUserEntity.ComputePartitionKey(ID), TableUserEntity.ComputeRowKey(ID));
        }

        /// <summary>
        /// Try get a user based on ID
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <param name="result">Search result, null if not found</param>
        /// <returns>Whether the user ID exists</returns>
        public bool TryGet(long ID, out TableUserEntity result)
        {
            return base.TryGet(TableUserEntity.ComputePartitionKey(ID), TableUserEntity.ComputeRowKey(ID), out result);
        }
    }
}
