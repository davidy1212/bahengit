﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the newsfeed and notification in Azure Table Service
    /// </summary>
    public class TableMessageOperator<T> : TableDataOperator<T> where T : TableMessageEntity
    {
        /// <summary>
        /// Construct the operator by defining the name of the table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        /// <param name="tableName">Name of the table</param>
        public TableMessageOperator(string connectionString, string tableName)
            : base(connectionString, tableName)
        {
        }

        /// <summary>
        /// Get a specific message
        /// </summary>
        /// <param name="itemID">Message ID</param>
        /// <param name="recipientID">Receiver user ID</param>
        /// <returns></returns>
        public T Get(Guid itemID, long recipientID, DateTime timeSent)
        {
            return base.Get(
                TableMessageEntity.ComputePartitionKey(recipientID),
                TableMessageEntity.ComputeRowKey(timeSent, itemID));
        }

        /// <summary>
        /// Get all the messages of a recipient
        /// </summary>
        /// <param name="recipientID">Receiver user ID</param>
        /// <returns></returns>
        public IQueryable<T> GetAllItems(long recipientID)
        {
            return this.Context.CreateQuery<T>(this.TableName)
                .Where(x => x.PartitionKey == TableMessageEntity.ComputePartitionKey(recipientID));
        }

        public IQueryable<T> GetItemsSince(long recipientID, DateTime since, int limit)
        {
            if (limit > 1000)
            {
                throw new ArgumentException("does not support limit larger than 1000");
            }
            return this.Context.CreateQuery<T>(this.TableName)
                .Where(x => x.PartitionKey == TableMessageEntity.ComputePartitionKey(recipientID)
                    && x.RowKey.CompareTo(TableMessageEntity.ComputeRowKey(since, Guid.Empty)) > 0)
                    .Reverse()
                    .Take(limit)
                    .Reverse();
        }

        public IQueryable<T> GetItemsUntil(long recipientID, DateTime until, int limit)
        {
            if (limit > 1000)
            {
                throw new ArgumentException("does not support limit larger than 1000");
            }
            return this.Context.CreateQuery<T>(this.TableName)
                .Where(x => x.PartitionKey == TableMessageEntity.ComputePartitionKey(recipientID)
                    && x.RowKey.CompareTo(TableMessageEntity.ComputeRowKey(until, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")) < 0)
                    .Take(limit);
        }

        public IQueryable<T> GetItemsBetween(long recipientID, DateTime since, DateTime until)
        {
            return this.Context.CreateQuery<T>(this.TableName)
                .Where(x => x.PartitionKey == TableMessageEntity.ComputePartitionKey(recipientID)
                    && x.RowKey.CompareTo(TableMessageEntity.ComputeRowKey(since, Guid.Empty)) > 0
                    && x.RowKey.CompareTo(TableMessageEntity.ComputeRowKey(until, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")) < 0);
        }
    }
}
