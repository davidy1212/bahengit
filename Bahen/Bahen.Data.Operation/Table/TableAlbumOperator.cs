﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the managing album information table
    /// </summary>
    public class TableAlbumOperator : TableDataOperator<TableAlbumEntity>
    {
        /// <summary>
        /// Construct the operator by defining the name of the email table
        /// </summary>
        /// <param name="connectionString">Connection strin gused to connect the table database</param>
        public TableAlbumOperator(string connectionString)
            : base(connectionString, "Albums")
        {
        }

        /// <summary>
        /// Insert an album info entry into the database
        /// </summary>
        /// <param name="data">Album info</param>
        public override void Insert(Bahen.Data.Models.TableAlbumEntity data)
        {
            data.ID = Guid.NewGuid();
            data.CreateTime = DateTime.UtcNow;
            base.Insert(data);
        }

        /// <summary>
        /// Get album info from the database
        /// </summary>
        /// <param name="albumID">ID of the album</param>
        /// <param name="ownerID">ID of the owner of the album</param>
        /// <param name="type">Type of the album</param>
        /// <returns>Album data that matches the query</returns>
        public TableAlbumEntity Get(Guid albumID, long ownerID, AlbumType type)
        {
            return base.Get(TableAlbumEntity.ComputePartitionKey(ComputeAlbumPrefix(type), ownerID), TableAlbumEntity.ComputeRowKey(ownerID, albumID));
        }

        /// <summary>
        /// Try get album info from the database
        /// </summary>
        /// <param name="albumID">ID of the album</param>
        /// <param name="ownerID">ID of the owner of the album</param>
        /// <param name="type">Type of the album</param>
        /// <param name="data">Album data that matches the query</param>
        /// <returns>Whether the album is found</returns>
        public bool TryGet(Guid albumID, long ownerID, AlbumType type, out TableAlbumEntity data)
        {
            try
            {
                data = this.Get(albumID, ownerID, type);
            }
            catch
            {
                data = null;
            }
            return data != null;
        }

        /// <summary>
        /// Get all the album info that belongs an owner
        /// </summary>
        /// <param name="ownerID">ID of the owner</param>
        /// <param name="type">Type of the album looking for</param>
        /// <returns>A collection of albums that matches the query</returns>
        public IQueryable<TableAlbumEntity> GetAllAlbums(long ownerID, AlbumType type)
        {
            return this.Context.CreateQuery<TableAlbumEntity>(this.TableName)
                .Where(
                x =>
                    x.PartitionKey == TableAlbumEntity.ComputePartitionKey(ComputeAlbumPrefix(type), ownerID)
                    && x.RowKey.CompareTo(TableAlbumEntity.ComputeRowKey(ownerID, Guid.Empty)) > 0
                    && x.RowKey.CompareTo(TableAlbumEntity.ComputeRowKey(ownerID, Guid.Parse("ffffffff-ffff-ffff-ffff-ffffffff"))) < 0);
        }

        /// <summary>
        /// Calculate the prefix of the album 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string ComputeAlbumPrefix(AlbumType type)
        {
            switch (type)
            {
                case (AlbumType.ProfilePicture):
                    {
                        return "up";
                    }
                case (AlbumType.EventPicture):
                    {
                        return "e";
                    }
                case (AlbumType.CyclePicture):
                    {
                        return "c";
                    }
                case (AlbumType.UserCustom):
                    {
                        return "u";
                    }
                default:
                    {
                        return "";
                    }
            }
        }
    }
}
