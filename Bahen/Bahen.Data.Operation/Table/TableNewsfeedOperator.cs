﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator in charge of the newsfeed container in Azure Table Service
    /// </summary>
    public class TableNewsfeedOperator : TableMessageOperator<TableNewsfeedEntity>
    {
        /// <summary>
        /// Construct the operator by defining the name of the newsfeed table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        public TableNewsfeedOperator(string connectionString) : base(connectionString, "Newsfeeds") { }
    }
}
