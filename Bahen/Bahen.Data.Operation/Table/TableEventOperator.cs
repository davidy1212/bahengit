﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the event container in Azure Table Service
    /// </summary>
    public class TableEventOperator : TableDataOperator<TableEventEntity>
    {
        /// <summary>
        /// Initiate the class by defining the name of the events table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the table database</param>
        public TableEventOperator(string connectionString) : base(connectionString, "Events") { }

        /// <summary>
        /// Initiate the class by defining the name of the events table
        /// </summary>
        /// <param name="client">Storage client that have data access</param>
        public TableEventOperator(CloudTableClient client) : base(client, "Events") { }

        /// <summary>
        /// Get an event from the table based on publisher ID and event ID
        /// </summary>
        /// <param name="publisherID">Publisher User ID</param>
        /// <param name="eventID">Event ID</param>
        /// <returns>The event table entity, null if not found</returns>
        public TableEventEntity Get(long publisherID, long eventID)
        {
            return base.Get(TableEventEntity.ComputePartitionKey(publisherID), TableEventEntity.ComputeRowKey(eventID));
        }

        /// <summary>
        /// Get a collection of events from the table based on publisher ID (Same partition key)
        /// </summary>
        /// <param name="publisherID">Publisher User ID</param>
        /// <returns>A collection of events published by the publisher ID</returns>
        public IQueryable<TableEventEntity> GetEventsByPublisher(long publisherID)
        {
            return from events in this.Context.CreateQuery<TableEventEntity>(this.TableName)
                   where events.PartitionKey == TableEventEntity.ComputePartitionKey(publisherID)
                   //where events.PublisherID == publisherID
                   select events;
        }

        /// <summary>
        /// Determine whether the event ID exists by certain publisher
        /// </summary>
        /// <param name="publisherID">Publisher User ID</param>
        /// <param name="eventID">Event ID</param>
        /// <returns>Whether the event ID exists by publisher</returns>
        public bool Contains(long publisherID, long eventID)
        {
            return base.Contains(TableEventEntity.ComputePartitionKey(publisherID), TableEventEntity.ComputeRowKey(eventID));
        }

        /// <summary>
        /// Try get an event by publisher ID and event ID
        /// </summary>
        /// <param name="publisherID">Publisher User ID</param>
        /// <param name="eventID">Event ID</param>
        /// <param name="result">Search result, null if not found</param>
        /// <returns>Whether the event ID exists by publisher</returns>
        public bool TryGet(long publisherID, long eventID, out TableEventEntity result)
        {
            return base.TryGet(TableEventEntity.ComputePartitionKey(publisherID), TableEventEntity.ComputeRowKey(eventID), out result);
        }
    }
}
