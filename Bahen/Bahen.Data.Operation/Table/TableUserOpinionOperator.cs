﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public abstract class TableUserOpinionOperator<T> : TableDataOperator<T> where T : TableUserOpinionEntity
    {
        public TableUserOpinionOperator(string connectionString, string tableName)
            : base(connectionString, tableName)
        {
            //iniStorage(tableName);   // fix a bug of the Storage Emulator
        }

        public TableUserOpinionOperator(CloudTableClient client, string tableName)
            : base(client, tableName)
        {
            //iniStorage(tableName);   // fix a bug of the Storage Emulator
        }

        public IQueryable<T> GetByEntityID(string targetString, EntityTypeCode typeCode)
        {
            this.Context.IgnoreResourceNotFoundException = true;
            string partitionKey = TableUserOpinionEntity.ComputePartitionKey(targetString, typeCode);
            var results =
                   from items in this.Context.CreateQuery<T>(this.TableName)
                   where items.PartitionKey == partitionKey
                   where items.TargetString == targetString
                   select items;
            return results;
        }

        public void Insert(T entity)
        {
            TableCountOperator countOp;
            string tableName = "Count";
            if (typeof(T) == typeof(TableLikeEntity))
            {
                tableName = "LikeCount";
            }
            else if (typeof(T) == typeof(TableCommentEntity))
            {
                tableName = "CommentCount";
            }
            countOp = new TableCountOperator(this.Client, tableName);
            countOp.Increment(entity.TargetString, ((EntityTypeCode)entity.TargetTypeCode));
            try
            {
                base.Insert(entity);
            }
            catch (Exception ex)
            {
                countOp.Decrement(entity.TargetString, ((EntityTypeCode)entity.TargetTypeCode));
            }
        }

        public void Remove(string partitionKey, string rowKey)
        {
            TableCountOperator countOp;
            string tableName = "Count";
            if (typeof(T) == typeof(TableLikeEntity))
            {
                tableName = "LikeCount";
            }
            else if (typeof(T) == typeof(TableCommentEntity))
            {
                tableName = "CommentCount";
            }
            countOp = new TableCountOperator(this.Client, tableName);

            T entity;
            if (this.TryGet(partitionKey, rowKey, out entity))
            {
                countOp.Decrement(entity.TargetString, ((EntityTypeCode)entity.TargetTypeCode));
                try
                {
                    this.Remove(entity);
                }
                catch (Exception ex)
                {
                    countOp.Increment(entity.TargetString, ((EntityTypeCode)entity.TargetTypeCode));
                }
            }
            else
            {
                throw new EntityNotFoundException(typeof(T), partitionKey + rowKey, null);
            }
        }
    }
}
