﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation.Table
{
    public class TableCommentCountOperator : TableCountOperator
    {
        public TableCommentCountOperator(string connectionString) : base(connectionString, "CommentCount") { }

        public TableCommentCountOperator(CloudTableClient client) : base(client, "CommentCount") { }
    }
}
