﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public class TableLikeCountOperator : TableCountOperator
    {
        public TableLikeCountOperator(string connectionString) : base(connectionString, "LikeCount") { }

        public TableLikeCountOperator(CloudTableClient client) : base(client, "LikeCount") { }
    }
}
