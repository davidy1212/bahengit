﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// A queue messager that sends messages about data operations to notify the indexer to index the changes
    /// </summary>
    public class QueueDataOperationMessenger : QueueMessenger<DataOperationMessage>
    {
        /// <summary>
        /// Construct a new data operation queue message by defining the table name
        /// </summary>
        /// <param name="connectionString">Connection string to access queue data source</param>
        public QueueDataOperationMessenger(string connectionString)
            : base(connectionString, "dataoperations")
        {
        }

        /// <summary>
        /// Construct a new data operation queue message by defining the table name
        /// </summary>
        /// <param name="connectionString">Connection string to access queue data source</param>
        /// <param name="backup">Whether the queue is designed for processed purpose (backup)</param>
        public QueueDataOperationMessenger(string connectionString, bool backup)
            : base(connectionString, backup ? "dataoperationsbackup" : "dataoperations")
        {
        }
    }
}
