﻿using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represent a messenger that can access azure queue service
    /// </summary>
    public class QueueMessenger<T> where T : ISerializable
    {
        /// <summary>
        /// Construct a new queue messager
        /// </summary>
        /// <param name="connectionString">Connection string to connect azure data storage</param>
        /// <param name="queueName">Name of the queue</param>
        public QueueMessenger(string connectionString, string queueName)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient client = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = client.GetQueueReference(queueName);
            queue.CreateIfNotExists();
            this.Queue = queue;
        }

        /// <summary>
        /// Queue data
        /// </summary>
        protected CloudQueue Queue { get; set; }

        /// <summary>
        /// Insert one message
        /// </summary>
        /// <param name="message">Content of the message</param>
        public void Insert(byte[] message)
        {
            CloudQueueMessage queueMessage = new CloudQueueMessage(message);
            this.Queue.AddMessage(queueMessage, TimeSpan.FromHours(5), TimeSpan.Zero);
        }

        /// <summary>
        /// Insert one serializable message
        /// </summary>
        /// <param name="message">Message data</param>
        public void Insert(T message)
        {
            this.Insert(this.Serialize(message));
        }

        /// <summary>
        /// Dequeue one message from the queue
        /// </summary>
        /// <returns>Message data</returns>
        public T Dequeue()
        {
            CloudQueueMessage message = this.Queue.GetMessage();
            this.Queue.Delete();
            return this.Deserialize(message.AsBytes);
        }

        /// <summary>
        /// Dequeue all the messages from the queue
        /// </summary>
        /// <returns>Collection of message data</returns>
        public IEnumerable<T> DequeueAll()
        {
            while (this.Queue.PeekMessage() != null)
            {
                foreach (CloudQueueMessage message in this.Queue.GetMessages(32))
                {
                    this.Queue.DeleteMessage(message);
                    yield return this.Deserialize(message.AsBytes);
                }
            }
        }

        /// <summary>
        /// Serialize a message
        /// </summary>
        /// <param name="message">Message class</param>
        /// <returns>Message data</returns>
        public byte[] Serialize(T message)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, message);
                stream.Seek(0, SeekOrigin.Begin);
                byte[] content = new byte[stream.Length];
                stream.Read(content, 0, (int)stream.Length);
                return content;
            }
        }

        /// <summary>
        /// Deserialize a message
        /// </summary>
        /// <param name="content">Message data</param>
        /// <returns>Message class</returns>
        public T Deserialize(byte[] content)
        {
            using (MemoryStream stream = new MemoryStream(content))
            {
                stream.Seek(0, SeekOrigin.Begin);
                return (T)new BinaryFormatter().Deserialize(stream);
            }
        }
    }
}
