﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// General data operator in charge of cycle data in both sql and no-sql databases
    /// </summary>
    public class CycleOperator : FullEntityOperator<CycleEntity, SqlCycleOperator, SqlCycleEntity, TableCycleOperator, TableCycleEntity>
    {
        /// <summary>
        /// Construct a cycle operator with sql connection string and table connection string
        /// </summary>
        /// <param name="sqlConnectionString">Connection string for sql database</param>
        /// <param name="tableConnectionString">Connection string for azure storage</param>
        public CycleOperator(string sqlConnectionString, string tableConnectionString) : base(sqlConnectionString, tableConnectionString) { }

        /// <summary>
        /// Using base insertion method with checking alias to be not null
        /// </summary>
        /// <param name="data">data to be inserted</param>
        /// <param name="saveChanges">Whether save changes</param>
        public override void Insert(CycleEntity data, bool saveChanges = true)
        {
            if (String.IsNullOrEmpty(data.Alias))
            {
                data.Alias = Guid.NewGuid().ToString("N");
            }
            base.Insert(data, saveChanges);
        }

        /// <summary>
        /// Get a cycle based on cycle ID
        /// </summary>
        /// <param name="ID">Cycle ID</param>
        /// <returns>Cycle entity that matches the ID</returns>
        public override CycleEntity Get(long ID)
        {
            SqlCycleEntity sqlEntity;
            if (!this.SqlOperator.TryGet(ID, out sqlEntity))
            {
                return null;
            }

            TableCycleEntity tableEntity;
            if (!this.TableOperator.TryGet(ID, out tableEntity))
            {
                throw new SqlTableNotSyncedException(String.Format("{0} ID:{1} is found in the sql database but not found in table storage", typeof(SqlUserEntity).Name, ID));
            }
            else
            {
                return new CycleEntity(sqlEntity, tableEntity);
            }
        }

        /// <summary>
        /// Get the membership type of a certain user in a cycle
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <param name="userID">User ID</param>
        /// <returns>Membership type</returns>
        public CycleMembershipType GetMembership(long cycleID, long userID)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            var result = cycleUserOperator.QueryByBoth(userID, cycleID);
            if (result.FirstOrDefault() != null)
            {
                return (CycleMembershipType)result.First().MembershipType;
            }
            else
            {
                return CycleMembershipType.None;
            }
        }

        /// <summary>
        /// Add a member to a cycle
        /// </summary>
        /// <param name="cycleID">Cycle id</param>
        /// <param name="userID">Member user id</param>
        /// <param name="membershipType">Membership type</param>
        public void AddMemebership(long cycleID, long userID, CycleMembershipType membershipType)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            cycleUserOperator.Insert(new SqlCycleUserRelation()
            {
                CycleID = cycleID,
                UserID = userID,
                MembershipType = (int)membershipType
            });
        }

        /// <summary>
        /// Update the membership of a certain user in a cycle.  Add the membership if not already exists
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <param name="userID">User ID</param>
        /// <param name="membershipType">Membership type</param>
        public void SetMembership(long cycleID, long userID, CycleMembershipType membershipType)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            var existingMembership = cycleUserOperator.QueryByBoth(userID, cycleID).FirstOrDefault();
            if (existingMembership != null)
            {
                existingMembership.MembershipType = (int)membershipType;
                cycleUserOperator.Update(existingMembership);
            }
            else
            {
                cycleUserOperator.Insert(new SqlCycleUserRelation()
                {
                    CycleID = cycleID,
                    UserID = userID,
                    MembershipType = (int)membershipType
                });
            }
        }

        /// <summary>
        /// Get a collection of cycle data
        /// </summary>
        /// <param name="start">Start index of the cycle data</param>
        /// <param name="number">Size of cycle data (does not guarantee to retrieve as many)</param>
        /// <returns>A collection of cycle data</returns>
        public override IEnumerable<CycleEntity> GetItems(int start, int number)
        {
            if (number > 1000)
            {
                throw new NotImplementedException("Cannot retrieve table data for more than 1000 rows at one transaction.");
            }
            else
            {
                List<SqlCycleEntity> sqlEntities = this.SqlOperator.Set.OrderBy(x => x.ID).Skip(start).Take(number).ToList();
                if (sqlEntities.FirstOrDefault() != null)
                {
                    long firstID = sqlEntities.First().ID;
                    long lastID = sqlEntities.Last().ID;

                    // Does not guarantee to retrieve number of data required (Retrieve everything in the lower partition key first)
                    string lowerPartitionKey = TableCycleEntity.ComputePartitionKey(firstID);

                    List<TableCycleEntity> tableEntities = this.TableOperator.Context
                        .CreateQuery<TableCycleEntity>(this.TableOperator.TableName)
                        .Where(x => x.PartitionKey == lowerPartitionKey
                            && x.RowKey.CompareTo(TableCycleEntity.ComputeRowKey(firstID)) >= 0
                            && x.RowKey.CompareTo(TableCycleEntity.ComputeRowKey(lastID)) <= 0)
                            .Take(number).ToList();

                    return tableEntities.Join(
                        sqlEntities,
                        (x => x.ID),
                        (x => x.ID),
                        ((x, y) => new CycleEntity(y, x)));
                }
                else
                {
                    return new List<CycleEntity>();
                }
            }
        }

        /// <summary>
        /// Get all the cycles that a user belongs to
        /// </summary>
        /// <param name="userID">Member User ID</param>
        /// <returns>All the cycles that a user joined</returns>
        public IQueryable<SqlCycleEntity> GetCyclesByMember(long userID)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            return cycleUserOperator.QueryByUser(userID)
                .Join(this.SqlOperator.Set,
                (x => x.CycleID),
                (x => x.ID),
                ((x, y) => y));
        }

        public IQueryable<SqlCycleEntity> GetCyclesByMember(long userID, CycleMembershipType type)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            return cycleUserOperator.QueryByUser(userID, type)
                .Join(this.SqlOperator.Set,
                (x => x.CycleID),
                (x => x.ID),
                ((x, y) => y));
        }

        public IQueryable<SqlCycleEntity> GetCyclesByMember(long userID, CycleMembershipType type, bool higherPermission)
        {
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.SqlOperator.Context);
            return cycleUserOperator.QueryByUser(userID, type, higherPermission)
                .Join(this.SqlOperator.Set,
                (x => x.CycleID),
                (x => x.ID),
                ((x, y) => y));
        }

        /// <summary>
        /// Get all the cycles that an event was published in
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <returns>All the cycles that an event was published in</returns>
        public IQueryable<SqlCycleEntity> GetCyclesByEvent(long eventID)
        {
            // Need to ask cycle user operator for membership information
            SqlCycleEventOperator cycleEventOperator = new SqlCycleEventOperator(this.SqlOperator.Context);
            return cycleEventOperator.QueryByEvent(eventID)
                .Join(this.SqlOperator.Set,
                (x => x.CycleID),
                (x => x.ID),
                ((x, y) => y));
        }
    }

}
