﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// A base class for all operators that manage the full entity
    /// </summary>
    /// <typeparam name="TEntity">Generic type for the entity.  Has to implement IFullEntity interface</typeparam>
    /// <typeparam name="TSqlOperator">Generic type for sql entity operator.  A child class of SqlDataOperator of TSqlEntity</typeparam>
    /// <typeparam name="TSqlEntity">Generic type for sql entity contained in the full entity class</typeparam>
    /// <typeparam name="TTableOperator">Generic type for table entity operator.  A child class of TableDataOperator of TTableEntity</typeparam>
    /// <typeparam name="TTableEntity">Generic type for table entity contained in the full entity class</typeparam>
    public abstract class FullEntityOperator<TEntity, TSqlOperator, TSqlEntity, TTableOperator, TTableEntity>
        : IEntityDataOperator<TEntity>, IDisposable
        where TEntity : class, IFullEntity<TSqlEntity, TTableEntity>
        where TSqlOperator : SqlDataOperator<TSqlEntity>
        where TSqlEntity : class, IInt64Id
        where TTableOperator : TableDataOperator<TTableEntity>
        where TTableEntity : TableServiceEntity, IInt64Id
    {
        /// <summary>
        /// Construct a FullEntityOperator with both sql and table connection string
        /// </summary>
        /// <param name="sqlConnectionString">Connection string used to connect sql database</param>
        /// <param name="tableConnectionString">Connection string used to connect table database</param>
        public FullEntityOperator(
            string sqlConnectionString,
            string tableConnectionString)
        {
            this.SqlOperator = ReflectionConstructor.ConstructOperator<TSqlOperator>(
                new SqlConnectionBuilder().Build(sqlConnectionString));
            this.TableOperator = ReflectionConstructor.ConstructOperator<TTableOperator>(
                new TableConnectionBuilder().Build(tableConnectionString));
            //this.Messenger = new QueueDataOperationMessenger(tableConnectionString);
        }

        /// <summary>
        /// Operator in charge of the sql database
        /// </summary>
        public TSqlOperator SqlOperator { get; set; }

        /// <summary>
        /// Operator in charge of the table database
        /// </summary>
        public TTableOperator TableOperator { get; set; }

        /// <summary>
        /// Messenger takes care of the change log of the data to notify the indexer to index the changes
        /// </summary>
        protected QueueDataOperationMessenger Messenger { get; set; }

        /// <summary>
        /// Insert an entity into data storage
        /// </summary>
        /// <param name="data">A full entity instance of type TEntity</param>
        public virtual void Insert(TEntity data)
        {
            this.Insert(data, true);
        }

        /// <summary>
        /// Insert an entity into data storage
        /// </summary>
        /// <param name="data">A full entity instance of type TEntity</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="DataOperationLayer.Exceptions.BackendUnknownException">Cannot insert entity due to unknown reason</exception>
        public virtual void Insert(TEntity data, bool saveChanges)
        {
            this.SqlOperator.Insert(data.SqlEntity, saveChanges);
            // Get ID to refresh Table User ID
            long ID = data.ID;
            this.TableOperator.Insert(data.TableEntity, saveChanges);
            //this.Messenger.Insert(new DataOperationMessage(data, DataOperation.Insert));
        }

        /// <summary>
        /// Update an entity to data storage.
        /// Assume that the entity already exists in the storage.
        /// </summary>
        /// <param name="data"></param>
        public virtual void Update(TEntity data)
        {
            this.Update(data, true);
        }

        /// <summary>
        /// Update an entity to data storage.
        /// Assume that the entity already exists in the storage.
        /// </summary>
        /// <param name="data">A full entity instance of type TEntity</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="DataOperationLayer.Exceptions.EntityNotFoundException">Cannot find the entity to be updated</exception>
        /// <exception cref="DataOperationLayer.Exceptions.BackendUnknownException">Cannot update entity due to unknown reason</exception>
        public virtual void Update(TEntity data, bool saveChanges)
        {
            this.SqlOperator.Update(data.SqlEntity, saveChanges);
            this.TableOperator.Update(data.TableEntity, saveChanges);
            //this.Messenger.Insert(new DataOperationMessage(data, DataOperation.Update));
        }

        /// <summary>
        /// Remove an entity in data storage.
        /// Assume that the entity already exists in the storage.
        /// </summary>
        /// <param name="data">A full entity instance of type TEntity</param>
        public virtual void Remove(TEntity data)
        {
            this.Remove(data, true);
        }

        /// <summary>
        /// Remove an entity in data storage.
        /// Assume that the entity already exists in the storage.
        /// </summary>
        /// <param name="data">A full entity instance of type TEntity</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="DataOperationLayer.Exceptions.EntityNotFoundException">Cannot find the entity to be removed</exception>
        /// <exception cref="DataOperationLayer.Exceptions.BackendUnknownException">Cannot remove entity due to unknown reason</exception>
        public virtual void Remove(TEntity data, bool saveChanges)
        {
            this.SqlOperator.Remove(data.SqlEntity, saveChanges);
            this.TableOperator.Remove(data.TableEntity, saveChanges);
            //this.Messenger.Insert(new DataOperationMessage(data, DataOperation.Remove));
        }

        /// <summary>
        /// Get an entity based on ID search
        /// </summary>
        /// <param name="ID">ID of the entity</param>
        /// <returns>The found entity, null if not found</returns>
        public abstract TEntity Get(long ID);

        /// <summary>
        /// Get a number of data
        /// </summary>
        /// <param name="start">Start index of the data</param>
        /// <param name="number">Size of the data</param>
        /// <returns>A collection of data</returns>
        public abstract IEnumerable<TEntity> GetItems(int start, int number);

        /// <summary>
        /// Try to get an entity based on ID search
        /// </summary>
        /// <param name="ID">ID of the entity</param>
        /// <param name="result">The found entity, null if not found</param>
        /// <returns>Whether the entity is found</returns>
        public virtual bool TryGet(long ID, out TEntity result)
        {
            try
            {
                result = this.Get(ID);
                return result != null;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Check if the storage contains an ID
        /// </summary>
        /// <param name="ID">ID of the entity to search</param>
        /// <returns>Whether the storage contains the entity</returns>
        public virtual bool Contains(long ID)
        {
            TEntity result;
            return this.TryGet(ID, out result);
        }

        /// <summary>
        /// Dispose the sql context
        /// </summary>
        public void Dispose()
        {
            this.SqlOperator.Dispose();
        }

        /// <summary>
        /// Save changes
        /// </summary>
        public void SaveChanges()
        {
            this.SqlOperator.SaveChanges();
            this.TableOperator.SaveChanges();
        }
    }
}
