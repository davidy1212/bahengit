﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Holds a static reference of the database connection for the entire site
    /// </summary>
    /// <typeparam name="TConnection">Type of the connection</typeparam>
    public interface IConnectionBuilder<TConnection>
    {
        /// <summary>
        /// Build new connection or return the built connection
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        /// <returns>Database context</returns>
        TConnection Build(string connectionString);
    }
}