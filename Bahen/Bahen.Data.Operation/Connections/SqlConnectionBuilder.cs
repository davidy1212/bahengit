﻿namespace Bahen.Data.Operation
{
    /// <summary>
    /// Holds a static reference of the sql connection for the entire site
    /// </summary>
    public class SqlConnectionBuilder : IConnectionBuilder<SqlContext>
    {
        /// <summary>
        /// Build new connection or return the built connection
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        /// <returns>Database context</returns>
        public SqlContext Build(string connectionString)
        {
            return new SqlContext(connectionString);
        }
    }
}