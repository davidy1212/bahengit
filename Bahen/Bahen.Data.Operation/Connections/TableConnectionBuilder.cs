﻿using Bahen.Data.Operation;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Holds a static reference of the table database connection for the entire site
    /// </summary>
    public class TableConnectionBuilder : IConnectionBuilder<CloudTableClient>
    {
        /// <summary>
        /// Build new connection or return the built connection
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        public CloudTableClient Build(string connectionString)
        {
            CloudStorageAccount account;

            try
            {
                account = CloudStorageAccount.Parse(connectionString);
            }
            catch (Exception ex)
            {
                throw new BackendUnknownException(String.Format("Cannot connect to table service with connection string: {0}", connectionString), ex);
            }

            return account.CreateCloudTableClient();
        }
    }
}
