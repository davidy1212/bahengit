﻿using Bahen.Data.Operation;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Build data connection object by connection string
    /// </summary>
    public static class ReflectionConstructor
    {
        public static Op ConstructOperator<Op>(SqlContext context) where Op : class
        {
            if (typeof(Op).GetConstructor(new Type[] { typeof(SqlContext) }) != null)
            {
                return typeof(Op).GetConstructor(new Type[] { typeof(SqlContext) })
                    .Invoke(new object[] { context }) as Op;
            }
            return null;
        }

        public static Op ConstructOperator<Op>(CloudTableClient client) where Op : class
        {
            if (typeof(Op).GetConstructor(new Type[] { typeof(CloudTableClient) }) != null)
            {
                return typeof(Op).GetConstructor(new Type[] { typeof(CloudTableClient) })
                    .Invoke(new object[] { client }) as Op;
            }
            return null;
        }
    }
}
