﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Search.Indexing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// General data operator in charge of user data in both sql and no-sql databases
    /// </summary>
    public class UserOperator : FullEntityOperator<UserEntity, SqlUserOperator, SqlUserEntity, TableUserOperator, TableUserEntity>
    {
        /// <summary>
        /// Construct a user operator with sql connection string and table connection string
        /// </summary>
        /// <param name="sqlConnectionString">Connection string for sql database</param>
        /// <param name="tableConnectionString">Connection string for azure storage</param>
        public UserOperator(string sqlConnectionString, string tableConnectionString) : base(sqlConnectionString, tableConnectionString) { }

        /// <summary>
        /// Using base insertion method with checking alias to be not null
        /// </summary>
        /// <param name="data">data to be inserted</param>
        /// <param name="saveChanges">Whether save changes</param>
        public override void Insert(UserEntity data, bool saveChanges = true)
        {
            if (String.IsNullOrEmpty(data.Alias))
            {
                data.Alias = Guid.NewGuid().ToString("N");
            }
            base.Insert(data, saveChanges);
        }
        /// <summary>
        /// Get a user based on user ID
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <returns>User entity that matches the ID</returns>
        public override UserEntity Get(long ID)
        {
            SqlUserEntity sqlEntity;
            if (!this.SqlOperator.TryGet(ID, out sqlEntity))
            {
                return null;
            }

            TableUserEntity tableEntity;
            if (!this.TableOperator.TryGet(ID, out tableEntity))
            {
                throw new SqlTableNotSyncedException(String.Format("{0} ID:{1} is found in the sql database but not found in table storage", typeof(SqlUserEntity).Name, ID));
            }
            else
            {
                return new UserEntity(sqlEntity, tableEntity);
            }
        }

        /// <summary>
        /// Get a user based on login email
        /// </summary>
        /// <param name="loginEmail">User login email</param>
        /// <returns>User entity that have the login email</returns>
        public UserEntity Get(string loginEmail)
        {
            SqlUserEntity sqlEntity;
            if (!this.SqlOperator.TryGet(loginEmail, out sqlEntity))
            {
                return null;
            }
            TableUserEntity tableEntity;
            if (!this.TableOperator.TryGet(sqlEntity.ID, out tableEntity))
            {
                throw new SqlTableNotSyncedException(String.Format("{0} ID:{1} is found in the sql database but not found in table storage", typeof(SqlUserEntity).Name, sqlEntity.ID));
            }
            else
            {
                return new UserEntity(sqlEntity, tableEntity);
            }
        }

        /// <summary>
        /// Get a collection of user data
        /// </summary>
        /// <param name="start">Start index of the data</param>
        /// <param name="number">Size of data (does not guarantee to retrieve as many)</param>
        /// <returns>A collection of data</returns>
        public override IEnumerable<UserEntity> GetItems(int start, int number)
        {
            if (number > 1000)
            {
                throw new NotImplementedException("Cannot retrieve table data for more than 1000 rows at one transaction.");
            }
            else
            {
                List<SqlUserEntity> sqlEntities = this.SqlOperator.Set.OrderBy(x => x.ID).Skip(start).Take(number).ToList();
                if (sqlEntities.FirstOrDefault() != null)
                {
                    long firstID = sqlEntities.First().ID;
                    long lastID = sqlEntities.Last().ID;

                    // Does not guarantee to retrieve number of data required (Retrieve everything in the lower partition key first)
                    string lowerPartitionKey = TableUserEntity.ComputePartitionKey(firstID);

                    List<TableUserEntity> tableEntities = this.TableOperator.Context
                        .CreateQuery<TableUserEntity>(this.TableOperator.TableName)
                        .Where(x => x.PartitionKey == lowerPartitionKey
                            && x.RowKey.CompareTo(TableUserEntity.ComputeRowKey(firstID)) >= 0
                            && x.RowKey.CompareTo(TableUserEntity.ComputeRowKey(lastID)) <= 0)
                            .Take(number).ToList();

                    return tableEntities.Join(
                        sqlEntities,
                        (x => x.ID),
                        (x => x.ID),
                        ((x, y) => new UserEntity(y, x)));
                }
                else
                {
                    return new List<UserEntity>();
                }
            }
        }

        /// <summary>
        /// Try to get a user based on login email
        /// </summary>
        /// <param name="loginEmail">User login email</param>
        /// <param name="result">User entity that have the login email</param>
        /// <returns>Whether login email is found</returns>
        public bool TryGet(string loginEmail, out UserEntity result)
        {
            try
            {
                result = this.Get(loginEmail);
                return result != null;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Get all the subscribers by the host ID
        /// Only doing sql database operation here
        /// </summary>
        /// <param name="hostID">Host User ID</param>
        /// <returns>A collection of TUserEntity</returns>
        public IQueryable<SqlUserEntity> GetSubscribersByHost(long hostID, PagingOptions options = null)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Get all the hosts by the subscriber ID
        /// Only doing sql database operation here
        /// </summary>
        /// <param name="subscriberID">Subscriber User ID</param>
        /// <returns>A collection of TUserEntity</returns>
        public IQueryable<SqlUserEntity> GetHostsBySubscriber(long subscriberID, PagingOptions options = null)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Get a collection of users that attends to an event
        /// </summary>
        /// <param name="eventID">Event ID</param> 
        /// <returns>All the attendants of the event</returns>
        public PagedInt64IdResult<SqlUserEntity> GetAttendantsByEvent(long eventID, PagingOptions options = null)
        {
            throw new NotSupportedException();
        //    SqlAttendanceOperator subOp = new SqlAttendanceOperator(this.SqlOperator.Context);
        //    SqlUserOperator uOp = new SqlUserOperator(this.SqlOperator.Context);

        //    PagedInt64IdResult<SqlAttendanceRelation> rawResult = subOp.Truncate(subOp.QueryByEvent(eventID), options);

        //    PagedInt64IdResult<SqlUserEntity> result = rawResult.Clone<SqlUserEntity>();
        //    result.Data = rawResult.Data.Join(
        //        uOp.Set,
        //        x => x.UserID,
        //        y => y.ID,
        //        (x, y) => y);

        //    return result;
        }

        /// <summary>
        /// Get a collection of users that attends to an event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="status">Attendance status</param>
        /// <returns>All the attendants of the evnet with the same attendance status</returns>
        public PagedInt64IdResult<SqlUserEntity> GetAttendantsByEvent(long eventID, AttendanceStatus status, PagingOptions options = null)
        {
            throw new NotSupportedException();
            //SqlAttendanceOperator subOp = new SqlAttendanceOperator(this.SqlOperator.Context);
            //SqlUserOperator uOp = new SqlUserOperator(this.SqlOperator.Context);

            //PagedInt64IdResult<SqlAttendanceRelation> rawResult = subOp.Truncate(subOp.QueryByEvent(eventID, status), options);

            //PagedInt64IdResult<SqlUserEntity> result = rawResult.Clone<SqlUserEntity>();
            //result.Data = rawResult.Data.Join(
            //    uOp.Set,
            //    x => x.UserID,
            //    y => y.ID,
            //    (x, y) => y);

            //return result;
        }

        /// <summary>
        /// Get a collection of users that attends to an event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="lowestStatus">Lowest level of attendance status</param>
        /// <param name="highestStatus">Highest level of attendance status</param>
        /// <returns>All the attendants of the event within the attendance status range</returns>
        public PagedInt64IdResult<SqlUserEntity> GetAttendantByEvent(long eventID, AttendanceStatus lowestStatus, AttendanceStatus highestStatus, PagingOptions options = null)
        {
            throw new NotSupportedException();
            //SqlAttendanceOperator subOp = new SqlAttendanceOperator(this.SqlOperator.Context);
            //SqlUserOperator uOp = new SqlUserOperator(this.SqlOperator.Context);

            //PagedInt64IdResult<SqlAttendanceRelation> rawResult = subOp.Truncate(subOp.QueryByEvent(eventID, lowestStatus, highestStatus), options);

            //PagedInt64IdResult<SqlUserEntity> result = rawResult.Clone<SqlUserEntity>();
            //result.Data = rawResult.Data.Join(
            //    uOp.Set,
            //    x => x.UserID,
            //    y => y.ID,
            //    (x, y) => y);

            //return result;
        }

        /// <summary>
        /// Get a collection of users that belongs to a specific cycle
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <returns>All the members of the cycle</returns>
        public PagedInt64IdResult<SqlUserEntity> GetMembersByCycle(long cycleID, PagingOptions options = null)
        {
            throw new NotSupportedException();
            //SqlCycleUserOperator subOp = new SqlCycleUserOperator(this.SqlOperator.Context);
            //SqlUserOperator uOp = new SqlUserOperator(this.SqlOperator.Context);

            //PagedInt64IdResult<SqlCycleUserRelation> rawResult = subOp.Truncate(subOp.QueryByCycle(cycleID), options);

            //PagedInt64IdResult<SqlUserEntity> result = rawResult.Clone<SqlUserEntity>();
            //result.Data = rawResult.Data.Join(
            //    uOp.Set,
            //    x => x.UserID,
            //    y => y.ID,
            //    (x, y) => y);

            //return result;
        }

        /// <summary>
        /// Add a new subscription between two users
        /// </summary>
        /// <param name="subscriberID">Subscriber user ID</param>
        /// <param name="hostID">Host user ID</param>
        public void AddSubscription(long subscriberID, long hostID)
        {
            SqlSubscriptionRelation subscription = new SqlSubscriptionRelation()
            {
                SubscriberID = subscriberID,
                HostID = hostID
            };
            new SqlSubscriptionOperator(this.SqlOperator.Context).Insert(subscription);
        }
    }
}
