﻿using System;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Each DataOperator will be in charge for one table in the sql
    /// </summary>
    /// <typeparam name="T">Any type in Bahen.Data.Models</typeparam>
    public interface IDataOperator<T> where T : class
    {
        /// <summary>
        /// Insert a row of data
        /// </summary>
        /// <param name="data">Data wrapped in the object to be inserted</param>
        void Insert(T data);

        /// <summary>
        /// Update a row of data
        /// </summary>
        /// <param name="data">Data wrapped in the object to be updated</param>
        void Update(T data);

        /// <summary>
        /// Remove a row of data
        /// </summary>
        /// <param name="data">Data wrapped in the object to be removed</param>
        void Remove(T data);
    }
}
