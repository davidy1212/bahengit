﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Search.Indexing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// A default indexer inheriting BasicIndexer
    /// </summary>
    public class DefaultIndexer : BasicIndexer
    {
        /// <summary>
        /// Construct a indexer
        /// </summary>
        /// <param name="sqlConnectionString">Connection string to access sql storage</param>
        /// <param name="tableConnectionString">Connection string to access table storage</param>
        /// <param name="blobConnectionString">Connection string to access blob storage</param>
        /// <param name="queueConnectionString">Connection string to access queue storage</param>
        public DefaultIndexer(
            string sqlConnectionString,
            string tableConnectionString,
            string blobConnectionString,
            string queueConnectionString)
            : base()
        {
            this.SqlConnectionString = sqlConnectionString;
            this.TableConnectionString = tableConnectionString;
            this.BlobConnectionString = blobConnectionString;
            this.QueueConnectionString = queueConnectionString;
        }

        /// <summary>
        /// Connection string to access sql storage
        /// </summary>
        protected string SqlConnectionString { get; set; }

        /// <summary>
        /// Connection string to access table storage
        /// </summary>
        protected string TableConnectionString { get; set; }

        /// <summary>
        /// Connection string to access blob storage
        /// </summary>
        protected string BlobConnectionString { get; set; }

        /// <summary>
        /// Connection string to access queue storage
        /// </summary>
        protected string QueueConnectionString { get; set; }

        /// <summary>
        /// Initialize the index from the blob storage and check if there is anything left on the backup queue service
        /// </summary>
        public virtual void Initialize(out InvertedWordIndex index)
        {
            this.Initialize(false, out index);
        }

        /// <summary>
        /// Initialize the index
        /// </summary>
        /// <param name="fullCrawl">Whether to start a full crawl</param>
        /// <param name="index">Reference to the index</param>
        public virtual void Initialize(bool fullCrawl, out InvertedWordIndex index)
        {
            if (fullCrawl)
            {
                index = new InvertedWordIndex();
                this.Serialize(index);
                this.ExecuteMessages(index, true);
                this.FullCrawl(index);
                new Thread(new ParameterizedThreadStart(FetchMessagesStart)).Start(index);
                new Thread(new ParameterizedThreadStart(BackupStart)).Start(index);
            }
            else
            {
                index = this.Deserialize();
                this.ExecuteMessages(index, true);
                this.Serialize(index);
                new Thread(new ParameterizedThreadStart(FetchMessagesStart)).Start(index);
                new Thread(new ParameterizedThreadStart(BackupStart)).Start(index);
            }
        }

        /// <summary>
        /// Full re-index
        /// </summary>
        /// <param name="index">Reference of the index object</param>
        /// <returns></returns>
        protected Task FullCrawl(InvertedWordIndex index)
        {
            return Task.Run(
                delegate
                {
                    UserOperator userOperator = new UserOperator(
                        this.SqlConnectionString,
                        this.TableConnectionString);

                    int startIndex = 0,
                        count = 0;

                    do
                    {
                        count = 0;
                        foreach (UserEntity userEntity in userOperator.GetItems(startIndex, 500))
                        {
                            this.Insert(index, userEntity);
                            count++;
                        }
                        startIndex += count;
                    } while (count > 0);

                    this.Serialize(index);

                    EventOperator eventOperator = new EventOperator(
                        this.SqlConnectionString,
                        this.TableConnectionString);

                    startIndex = 0;
                    count = 0;

                    do
                    {
                        count = 0;
                        foreach (EventEntity eventEntity in eventOperator.GetItems(startIndex, 500))
                        {
                            this.Insert(index, eventEntity);
                            count++;
                        }
                        startIndex += count;
                    } while (count > 0);

                    this.Serialize(index);

                    CycleOperator cycleOperator = new CycleOperator(
                        this.SqlConnectionString,
                        this.TableConnectionString);

                    startIndex = 0;
                    count = 0;

                    do
                    {
                        count = 0;
                        foreach (CycleEntity cycleEntity in cycleOperator.GetItems(startIndex, 500))
                        {
                            this.Insert(index, cycleEntity);
                            count++;
                        }
                        startIndex += count;
                    } while (count > 0);

                    this.Serialize(index);
                });
        }

        /// <summary>
        /// Constantly read the messages from the queue and execute
        /// </summary>
        /// <param name="index">Reference to the index to operate on</param>
        protected void FetchMessagesStart(object index)
        {
            while (true)
            {
                Thread.Sleep(60000);
                this.ExecuteMessages((InvertedWordIndex)index);
            }
        }

        /// <summary>
        /// Constantly back up index every 2 hours
        /// </summary>
        /// <param name="index">Reference to the index to operate on</param>
        protected void BackupStart(object index)
        {
            while (true)
            {
                Thread.Sleep(7200000);
                this.Serialize((InvertedWordIndex)index);
            }
        }

        /// <summary>
        /// Read the messages from the queue and execute
        /// </summary>
        /// <param name="index">Reference to the index to operate on</param>
        protected void ExecuteMessages(InvertedWordIndex index)
        {
            this.ExecuteMessages(index, false);
        }

        /// <summary>
        /// Read the messages from the queue and execute
        /// </summary>
        /// <param name="index">Reference to the index to operate on</param>
        /// <param name="backup">Whether to read the backup portion of the message</param>
        protected void ExecuteMessages(InvertedWordIndex index, bool backup)
        {
            QueueDataOperationMessenger messenger = new QueueDataOperationMessenger(this.BlobConnectionString, backup);
            QueueDataOperationMessenger backupMessenger = null;
            if (!backup)
            {
                backupMessenger = new QueueDataOperationMessenger(this.BlobConnectionString, true);
            }
            foreach (DataOperationMessage message in messenger.DequeueAll())
            {
                if (message.Timestamp > index.LastSaveTime || !backup)
                {
                    IIndexable entity = null;

                    if (message.EntityType == typeof(UserEntity).Name)
                    {
                        UserEntity userEntity;
                        if (!new UserOperator(
                            this.SqlConnectionString,
                            this.TableConnectionString)
                            .TryGet(message.EntityID, out userEntity))
                        {
                            continue;
                        }
                        entity = userEntity;
                    }
                    else if (message.EntityType == typeof(EventEntity).Name)
                    {
                        EventEntity eventEntity;
                        if (!new EventOperator(
                            this.SqlConnectionString,
                            this.TableConnectionString)
                            .TryGet(message.EntityID, out eventEntity))
                        {
                            continue;
                        }
                        entity = eventEntity;
                    }
                    else if (message.EntityType == typeof(CycleEntity).Name)
                    {
                        CycleEntity cycleEntity;
                        if (!new CycleOperator(
                            this.SqlConnectionString,
                            this.TableConnectionString)
                            .TryGet(message.EntityID, out cycleEntity))
                        {
                            continue;
                        }
                        entity = cycleEntity;
                    }

                    if (message.Operation == DataOperation.Insert || message.Operation == DataOperation.Update)
                    {
                        this.Insert(index, entity);
                    }
                    else if (message.Operation == DataOperation.Remove)
                    {
                        this.Remove(index, entity);
                    }
                }

                // Backup the message if not already in backup
                if (!backup)
                {
                    backupMessenger.Insert(message);
                }
            }
        }

        /// <summary>
        /// Serialize the index into blob storage
        /// </summary>
        /// <param name="index"></param>
        public virtual void Serialize(InvertedWordIndex index)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, index);
                stream.Seek(0, SeekOrigin.Begin);
                new BlobIndexOperator(this.BlobConnectionString).Upload(stream);
            }
            index.LastSaveTime = DateTime.Now;
        }

        /// <summary>
        /// Deserialize the index from blob storage
        /// </summary>
        /// <returns></returns>
        public virtual InvertedWordIndex Deserialize()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    BlobIndexOperator indexOperator = new BlobIndexOperator(this.BlobConnectionString);
                    indexOperator.Download(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    InvertedWordIndex index = (InvertedWordIndex)new BinaryFormatter().Deserialize(stream);
                    index.LastSaveTime = indexOperator.GetLastModifiedTime();
                    return index;
                }
                catch
                {
                    InvertedWordIndex index = new InvertedWordIndex();
                    index.LastSaveTime = DateTime.Now;
                    this.Serialize(index);
                    return index;
                }
            }
        }
    }
}
