﻿using Bahen.Search.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Query builder for cycle queries
    /// </summary>
    public class CycleQueryBuilder : IndexableQueryBuilder
    {
        public CycleQueryBuilder() : base("cycle") { }
    }
}
