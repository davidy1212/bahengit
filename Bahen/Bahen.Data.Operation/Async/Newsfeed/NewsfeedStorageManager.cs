﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Run task to store the news feed to each recipient at backend asynchronously.
    /// Recycle old feeds at backend.
    /// </summary>
    public class NewsfeedStorageManager : AsyncRecycleWorker<TableNewsfeedEntity>
    {
        /// <summary>
        /// Construct a newsfeed manager
        /// </summary>
        /// <param name="sqlConnectionString">Connection string to the sql database</param>
        /// <param name="tableConnectionString">Connection string to the table database</param>
        public NewsfeedStorageManager(string sqlConnectionString, string tableConnectionString)
            : base(sqlConnectionString, tableConnectionString) { }

        /// <summary>
        /// Asynchronously store one newsfeed to all the subscribers of the newsfeed publisher
        /// </summary>
        /// <param name="data">Newsfeed data</param>
        /// <returns></returns>
        public override Task ExecuteAsync(TableNewsfeedEntity data)
        {
            TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(this.TableConnectionString);
            return ExecuteAsync(newsfeedOperator, data, true, true);
        }

        /// <summary>
        /// Asynchronously store one newsfeed to all the subscribers of the newsfeed publisher
        /// </summary>
        /// <param name="newsfeedOperator">Newsfeed operator that have access to the table storage</param>
        /// <param name="data">Newsfeed data</param>
        /// <param name="saveChanges">Whether save changes at the end of the call</param>
        /// <returns></returns>
        public Task ExecuteAsync(TableNewsfeedOperator newsfeedOperator, TableNewsfeedEntity data, bool saveChanges)
        {
            return ExecuteAsync(newsfeedOperator, data, saveChanges, true);
        }

        /// <summary>
        /// Asynchronously store one newsfeed to all the subscribers of the newsfeed publisher
        /// </summary>
        /// <param name="newsfeedOperator">Newsfeed operator that have access to the table storage</param>
        /// <param name="data">Newsfeed data</param>
        /// <param name="saveChanges">Whether save change at the end of the call</param>
        /// <param name="broadcast">Whether to broadcast to all the subscribers of the host</param>
        /// <returns></returns>
        public Task ExecuteAsync(TableNewsfeedOperator newsfeedOperator, TableNewsfeedEntity data, bool saveChanges, bool broadcast)
        {
            return Task.Run(delegate
            {
                if (!broadcast)
                {
                    newsfeedOperator.Insert(data, false);
                }
                else
                {
                    UserOperator userOperator = new UserOperator(
                       this.SqlConnectionString, this.TableConnectionString);

                    // Save one copy to the publisher him/herself
                    data.RecipientID = data.SenderID;
                    data.IsOriginal = true;
                    newsfeedOperator.Insert(data, false);

                    foreach (long recipientID in userOperator.GetSubscribersByHost(data.SenderID).Select(x => x.ID))
                    {
                        TableNewsfeedEntity newsfeed = new TableNewsfeedEntity(data.SenderID, recipientID, false, data.TimeSent)
                        {
                            ReferenceType = data.ReferenceType,
                            Content = data.Content,
                            ReferencePartitioinKey = data.ReferencePartitioinKey,
                            ReferenceRowKey = data.ReferenceRowKey,
                            ReferenceLongID = data.ReferenceLongID,
                            OriginalNewsfeedRowKey = data.RowKey
                        };
                        newsfeedOperator.Insert(newsfeed, false);
                    }
                }
                if (saveChanges)
                {
                    newsfeedOperator.SaveChanges();
                    //newsfeedOperator.SaveChangesAsync();
                }
            });
        }

        /// <summary>
        /// Asynchronously store multiple newsfeeds to all the subscribers of all the newsfeed publisher
        /// </summary>
        /// <param name="data">A collection of newsfeeds</param>
        /// <returns></returns>
        public override Task ExecuteBatchAsync(IEnumerable<TableNewsfeedEntity> data)
        {
            return ExecuteBatchAsync(data, true);
        }

        /// <summary>
        /// Asynchronously store multiple newsfeeds to all the subscribers of all the newsfeed publisher
        /// </summary>
        /// <param name="data">A collection of newsfeeds</param>
        /// <param name="broadcast">Whether broadcast to all the subscribers of the host</param>
        /// <returns></returns>
        public Task ExecuteBatchAsync(IEnumerable<TableNewsfeedEntity> data, bool broadcast)
        {
            return Task.Run(
                async delegate
                {
                    TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(this.TableConnectionString);
                    foreach (var entity in data)
                    {
                        await ExecuteAsync(newsfeedOperator, entity, false, broadcast);
                    }
                    newsfeedOperator.Context.SaveChangesWithRetries();
                    //newsfeedOperator.SaveChangesAsync();
                });
        }

        /// <summary>
        /// Recycle routine for the recycle thread.
        /// Delete news feeds that are not from the user and that are more than 1000
        /// </summary>
        public override void Recycle()
        {
            while (true)
            {
                Thread.Sleep(7200000);
                //TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(this.TableConnectionString);
                //SqlUserOperator userOperator = new SqlUserOperator(this.SqlConnectionString);

                //// Iterate for all the users
                //foreach (long userID in userOperator.Set.Select(x => x.ID))
                //{
                //    //var allFeeds = newsfeedOperator.GetAllItems(userID).ToList()
                //    //    .Where(x => x.SenderID != userID)
                //    //    .OrderBy(x => x.TimeSent);
                //    int count = allFeeds.Count();
                //    if (count > 1000)
                //    {
                //        // Delete the older news feed that is bigger than 1000
                //        foreach (var newsFeed in allFeeds.Take(count - 1000))
                //        {
                //            newsfeedOperator.Remove(newsFeed);
                //        }
                //    }
                //}
            }
        }
    }
}
