﻿
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public abstract class AsyncRecycleWorker<T> : IAsyncWorker<T>, IRecycleWorker where T : TableServiceEntity
    {
        protected string SqlConnectionString { get; set; }
        protected string TableConnectionString { get; set; }
        protected Thread RecycleThread { get; set; }

        /// <summary>
        /// Construct an async worker with recycle functions
        /// </summary>
        /// <param name="sqlConnectionString">Connection string to the sql database</param>
        /// <param name="tableConnectionString">Connection string to the table database</param>
        public AsyncRecycleWorker(string sqlConnectionString, string tableConnectionString)
        {
            this.SqlConnectionString = sqlConnectionString;
            this.TableConnectionString = tableConnectionString;
            this.RecycleThread = new Thread(new ThreadStart(Recycle));
        }

        public abstract Task ExecuteAsync(T data);

        public abstract Task ExecuteBatchAsync(IEnumerable<T> data);

        public abstract void Recycle();
    }
}
