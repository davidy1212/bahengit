﻿
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public interface IAsyncWorker<TEntity> where TEntity:TableServiceEntity
    {
        Task ExecuteAsync(TEntity data);
        Task ExecuteBatchAsync(IEnumerable<TEntity> data);
    }
}
