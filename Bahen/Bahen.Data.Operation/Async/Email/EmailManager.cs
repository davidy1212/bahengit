﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Run task to send emails and store email entity at backend asynchronously
    /// Recycle old email entities at backend.
    /// </summary>
    public class EmailManager : AsyncRecycleWorker<TableEmailEntity>
    {
        private string SiteUrl { get; set; }
        private string SiteName { get; set; }

        /// <summary>
        /// Construct an email manager
        /// </summary>
        /// <param name="sqlConnectionString">Connection string to the sql database</param>
        /// <param name="tableConnectionString">Connection string to the table database</param>
        /// <param name="siteName">Name of the website</param>
        /// <param name="siteUrl">Root url of the website</param>
        public EmailManager(string sqlConnectionString, string tableConnectionString, string siteName, string siteUrl)
            : base(sqlConnectionString, tableConnectionString)
        {
            this.SiteName = siteName;
            this.SiteUrl = siteUrl;
        }

        /// <summary>
        /// Asychronously store send and store one email entity
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override Task ExecuteAsync(TableEmailEntity data)
        {
            return ExecuteAsync(new TableEmailOperator(this.TableConnectionString), data, true);
        }

        /// <summary>
        /// Asynchronously store and send one email entity
        /// </summary>
        /// <param name="emailOperator">Email operator that have access to the table storage</param>
        /// <param name="data">Email data</param>
        /// <param name="saveChanges">Whether call save changes of the email operator</param>
        /// <returns></returns>
        public Task ExecuteAsync(TableEmailOperator emailOperator, TableEmailEntity data, bool saveChanges)
        {
            return Task.Run(delegate
            {
                emailOperator.Insert(data, false);

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.To.Add(data.ReceiverEmail);
                message.Subject = this.SiteName + ": " + data.Content;
                message.From = new System.Net.Mail.MailAddress("no-reply@trovollor.com");
                message.Body =
                    String.Format(@"Hello,
{0}
Please click the following link to view the content:
{1}/emails/{2}/{3}", data.Content, this.SiteUrl, data.RecipientID, data.ID);
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("trovollor.com");
                smtp.Credentials = new System.Net.NetworkCredential("smtp", "smtp");
                smtp.Send(message);

                if (saveChanges)
                {
                    emailOperator.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Asynchronously send and store a list of email entities
        /// </summary>
        /// <param name="data">List of email entities</param>
        /// <returns></returns>
        public override Task ExecuteBatchAsync(IEnumerable<TableEmailEntity> data)
        {
            return Task.Run(async delegate
            {
                TableEmailOperator emailOperator = new TableEmailOperator(this.TableConnectionString);
                foreach (var item in data)
                {
                    await this.ExecuteAsync(emailOperator, item, false);
                }
                emailOperator.SaveChanges();
                //emailOperator.SaveChangesAsync();
            });
        }

        /// <summary>
        /// Asynchronously send and store a list of email entities based on one email template and a list of recipients
        /// </summary>
        /// <param name="data">Email template</param>
        /// <param name="recipients">List of recipients</param>
        /// <returns></returns>
        public Task ExecuteBatchAsync(TableEmailEntity data, IEnumerable<SqlUserEntity> recipients)
        {
            return Task.Run(
                async delegate
                {
                    List<TableEmailEntity> list = new List<TableEmailEntity>();
                    foreach (var user in recipients)
                    {
                        list.Add(new TableEmailEntity()
                        {

                            Url = data.Url,
                            Content = data.Content,
                            SenderID = data.SenderID,
                            RecipientID = user.ID,
                            ReceiverEmail = user.LoginEmail,
                            EmailAction = data.EmailAction
                        });
                    }
                    await ExecuteBatchAsync(list);
                });
        }

        /// <summary>
        /// Recycle routine for the recycle thread.
        /// Delete email entities that are expired
        /// </summary>
        public override void Recycle()
        {
            while (true)
            {
                Thread.Sleep(7200000);
                TableEmailOperator emailOperator = new TableEmailOperator(this.TableConnectionString);
                SqlUserOperator userOperator = new SqlUserOperator(this.SqlConnectionString);

                // Iterate for all the users
                foreach (long userID in userOperator.Set.Select(x => x.ID))
                {
                    var allItems = emailOperator.GetAllItems(userID).ToList()
                        .Where(x => x.ValidUntil < DateTime.UtcNow);
                    // Delete expired links
                    foreach (var item in allItems)
                    {
                        emailOperator.Remove(item);
                    }
                }
            }
        }
    }
}
