﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Run task to store the notification to recipients at backend asynchronously.
    /// Recycle old read notifications at backend.
    /// </summary>
    public class NotificationStorageManager : AsyncRecycleWorker<TableNotificationEntity>
    {
        /// <summary>
        /// Construct a notification storage manager
        /// </summary>
        /// <param name="sqlConnectionString">Connection string to the sql database</param>
        /// <param name="tableConnectionString">Connection string to the table database</param>
        public NotificationStorageManager(string sqlConnectionString, string tableConnectionString)
            : base(sqlConnectionString, tableConnectionString) { }

        /// <summary>
        /// Asynchronously store one notification to one specified recipient
        /// </summary>
        /// <param name="data">Notification data</param>
        /// <returns></returns>
        public override Task ExecuteAsync(TableNotificationEntity data)
        {
            TableNotificationOperator notificationOperator = new TableNotificationOperator(this.TableConnectionString);
            return ExecuteAsync(notificationOperator, data, true);
        }

        /// <summary>
        /// Asynchronously store one notification to one specified recipient
        /// </summary>
        /// <param name="notificationOperator">Notification operator that have access to the table storage</param>
        /// <param name="data">Notification data</param>
        /// <param name="saveChanges">Whether save changes at the end of the call</param>
        /// <returns></returns>
        public Task ExecuteAsync(TableNotificationOperator notificationOperator, TableNotificationEntity data, bool saveChanges)
        {
            return Task.Run(delegate
            {
                notificationOperator.Insert(data, saveChanges);
            });
        }

        /// <summary>
        /// Asynchronously store a list of notifications to specified recipients
        /// </summary>
        /// <param name="data">List of notifications</param>
        /// <returns></returns>
        public override Task ExecuteBatchAsync(IEnumerable<TableNotificationEntity> data)
        {
            return Task.Run(
               async delegate
               {
                   TableNotificationOperator notificationOperator = new TableNotificationOperator(this.TableConnectionString);
                   foreach (var entity in data)
                   {
                       await ExecuteAsync(notificationOperator, entity, false);
                   }
                   notificationOperator.SaveChanges();
                   //notificationOperator.SaveChangesAsync();
               });
        }

        /// <summary>
        /// Asynchronously store same notification to a list of recipients
        /// </summary>
        /// <param name="data">Notification data</param>
        /// <param name="recipients">List of recipients</param>
        /// <returns></returns>
        public Task ExecuteBatchAsync(TableNotificationEntity data, IEnumerable<SqlUserEntity> recipients)
        {
            return Task.Run(
                async delegate
                {
                    List<TableNotificationEntity> list = new List<TableNotificationEntity>();
                    foreach (var user in recipients)
                    {
                        TableNotificationEntity entity = new TableNotificationEntity()
                        {
                            Content = data.Content,
                            PictureUrl = data.PictureUrl,
                            RecipientID = user.ID,
                            SenderID = data.SenderID,
                            TimeSent = data.TimeSent,
                            Url = data.Url,
                        };
                        list.Add(entity);
                    }
                    await ExecuteBatchAsync(list);
                });
        }

        /// <summary>
        /// Recycle routine for the recycle thread.
        /// Delete news feeds that are not from the user and that are more than 1000
        /// </summary>
        public override void Recycle()
        {
            while (true)
            {
                Thread.Sleep(7200000);
                //TableNotificationOperator newsfeedOperator = new TableNotificationOperator(this.TableConnectionString);
                //SqlUserOperator userOperator = new SqlUserOperator(this.SqlConnectionString);

                //// Iterate for all the users
                //foreach (long userID in userOperator.Set.Select(x => x.ID))
                //{
                //    var allFeeds = newsfeedOperator.GetAllItems(userID).ToList()
                //        .Where(x => x.Read)
                //        .OrderBy(x => x.TimeSent);
                //    int count = allFeeds.Count();
                //    if (count > 10)
                //    {
                //        // Delete the older read notifications that is bigger than 10
                //        foreach (var newsFeed in allFeeds.Take(count - 10))
                //        {
                //            newsfeedOperator.Remove(newsFeed);
                //        }
                //    }
                //}
            }
        }
    }
}
