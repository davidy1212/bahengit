﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the event table in the sql database
    /// </summary>
    public class SqlEventOperator : SqlDataOperator<SqlEventEntity>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the events table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlEventOperator(string connectionString) : base(connectionString, "Events") { }

        /// <summary>
        /// Construct a sql data operator with regards to the events table
        /// <param name="context">A used sql context</param>
        /// </summary>
        public SqlEventOperator(SqlContext context) : base(context, "Events") { }

        /// <summary>
        /// Insert an event entity into the sql database.
        /// ID is assigned after successful insertion.
        /// </summary>
        /// <param name="data">A new event entity to be inserted</param>
        public override void Insert(SqlEventEntity data, bool saveChanges = true)
        {
            // Imcomplete required field
            if (String.IsNullOrEmpty(data.Name) | data.PublisherID == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert an Event entity with");
                sb.Append(String.IsNullOrEmpty(data.Name) ? " null or empty event Name" : String.Empty);
                sb.Append(data.PublisherID == 0 ? " null or empty PublisherID" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Foreign key constriants not met
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(typeof(SqlEventEntity), data.ID, "PublisherID", data.PublisherID, ex);
            }
        }

        /// <summary>
        /// Update an event entity in the sql database
        /// </summary>
        /// <param name="data">The attached event entity to be updated</param>
        public override void Update(SqlEventEntity data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Foreign key constriants not met
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't update Event entity ID: {0} because foreign key PublisherID {1} is not found in Users table.", data.ID, data.PublisherID), ex);
            }
        }

        /// <summary>
        /// Delete an event entity in the sql database
        /// </summary>
        /// <param name="data">The attached event entity to be deleted</param>
        public override void Remove(SqlEventEntity data)
        {
            // Remove all attendance relations
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.Context);
            IList<SqlAttendanceRelation> attendances = new List<SqlAttendanceRelation>(attendanceOperator.QueryByEvent(data.ID));
            foreach (var item in attendances)
            {
                attendanceOperator.Remove(item);
            }
            // Remove all cycle-event relations
            SqlCycleEventOperator cycleEventOperator = new SqlCycleEventOperator(this.Context);
            IList<SqlCycleEventRelation> cycleEvents = new List<SqlCycleEventRelation>(cycleEventOperator.QueryByEvent(data.ID));
            foreach (var item in cycleEvents)
            {
                cycleEventOperator.Remove(item);
            }

            // Remove event itself
            base.Remove(data);
        }
    }
}
