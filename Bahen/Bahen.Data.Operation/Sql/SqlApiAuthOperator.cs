﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public class SqlApiAuthOperator : SqlDataOperator<SqlApiAuthRelation>
    {
        public SqlApiAuthOperator(string connectionString) : base(connectionString, "ApiAuth") { }

        public SqlApiAuthOperator(SqlContext context) : base(context, "ApiAuth") { }

        public IQueryable<SqlApiAuthRelation> QueryByClient(long clientID)
        {
            return this.Set.Where(x => x.ClientID == clientID);
        }

        public IQueryable<SqlApiAuthRelation> QueryByUser(long userID)
        {
            return this.Set.Where(x => x.UserID == userID);
        }
    }
}
