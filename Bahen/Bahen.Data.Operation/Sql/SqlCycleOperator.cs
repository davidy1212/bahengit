﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the cycle table in the sql database
    /// </summary>
    public class SqlCycleOperator : SqlDataOperator<SqlCycleEntity>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the cycles table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlCycleOperator(string connectionString) : base(connectionString, "Cycles") { }

        /// <summary>
        /// Construct a sql data operator with regards to the cycles table
        /// <param name="context">Database context to operate on</param>
        /// </summary>
        public SqlCycleOperator(SqlContext context) : base(context, "Cycles") { }

        /// <summary>
        /// Insert a cycle entity into the sql databse.
        /// ID is assigned after successful insertion.
        /// </summary>
        /// <param name="data">A new cycle entity to be inserted</param>
        public override void Insert(SqlCycleEntity data, bool saveChanges = true)
        {
            // Imcomplete required field
            if (String.IsNullOrEmpty(data.Name))
            {
                throw new IncompleteNotNullFieldException("Can't insert a Cycle entity with null or empty cycle Name.");
            }
            base.Insert(data, saveChanges);
        }

        /// <summary>
        /// Remove a cycle entity in the sql database.
        /// </summary>
        /// <param name="data">The cycle entity to be deleted</param>
        /// <param name="saveChanges">Whether save changes</param>
        public override void Remove(SqlCycleEntity data, bool saveChanges = true)
        {
            // Remove all the cycle-event relations
            SqlCycleEventOperator cycleEventOperator = new SqlCycleEventOperator(this.Context);
            IList<SqlCycleEventRelation> cycleEvents = new List<SqlCycleEventRelation>(cycleEventOperator.QueryByCycle(data.ID));
            foreach (var item in cycleEvents)
            {
                cycleEventOperator.Remove(item, saveChanges);
            }
            // Remove all the cycle-user relations
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.Context);
            IList<SqlCycleUserRelation> cycleUsers = new List<SqlCycleUserRelation>(cycleUserOperator.QueryByCycle(data.ID));
            foreach (var item in cycleUsers)
            {
                cycleUserOperator.Remove(item, saveChanges);
            }
            base.Remove(data, saveChanges);
        }
    }
}
