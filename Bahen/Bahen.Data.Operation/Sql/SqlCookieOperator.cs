﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator base class for any operator in charge of a sql database table
    /// </summary>
    /// <typeparam name="T">Any sql databse table datamodel types</typeparam>
    public class SqlCookieOperator
    {
        public SqlCookieOperator(string connectionString)
            : this(new SqlConnectionBuilder().Build(connectionString))
        {
        }

        /// <summary>
        /// Construct a sql data operator in charge of a single table in the database
        /// </summary>
        /// <param name="context">Database context to operate on</param>
        /// <param name="setName">Table name in the database</param>
        public SqlCookieOperator(SqlContext context)
        {
            this.Context = context;
            this.Set = this.Context.Cookies as DbSet<SqlCookieEntity>;

            // If fails to cast type using "as"
            if (this.Set == null)
            {
                throw new ArgumentException(String.Format("Set name {0} does not match type {1}", this.SetName, typeof(SqlCookieEntity).ToString()));
            }
        }

        /// <summary>
        /// A reference to the table dbset of the context
        /// </summary>
        public DbSet<SqlCookieEntity> Set { get; private set; }

        /// <summary>
        /// A reference to the database context (entity framework)
        /// </summary>
        public SqlContext Context { get; set; }

        /// <summary>
        /// Table name of the operator in charge of
        /// </summary>
        private string SetName { get; set; }

        /// <summary>
        /// Insert a sql data entity into sql database
        /// </summary>
        /// <param name="data">A new sql data entity wrapped in entity class (not attached)</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="IdSpecifiedException">Cannot insert with assigned id</exception>
        /// <exception cref="IncompleteNotNullFieldException">Incomplete required field</exception>
        /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateException">Exception that depends on the child operator class</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public void Insert(SqlCookieEntity data)
        {
            try
            {
                this.Set.Add(data);
                this.SaveChanges();
            }
            // Incomplete field
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw new IncompleteNotNullFieldException(typeof(SqlCookieEntity), data.ID, ex);
            }
            // Let the child classes to handle this type of exceptions
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw ex;
            }
            // Unknown exception
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw new BackendUnknownException(String.Format("Cannot insert {0}.", typeof(SqlCookieEntity).Name), ex);
            }
        }

        /// <summary>
        /// Update a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="IncompleteNotNullFieldException">Incomplete required field</exception>
        /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateException">Exception that depends on the child operator class</exception>
        /// <exception cref="EntityNotFoundException">The data entity is not currently in the data context (lost refresh or deleted or never inserted)</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public void Update(SqlCookieEntity data)
        {
            if (data == null) throw new NullReferenceException(String.Format("Cannot update null {0}", typeof(SqlCookieEntity).Name));

            // Change the state to modified no matter detached or attached
            // Catch exception if the entity is actually not found by framework
            try
            {
                this.GetEntityEntry(data).State = EntityState.Modified;
            }
            catch (InvalidOperationException ex1)
            {
                Trace.WriteLine(ex1.ToString());
                // Will not handle any more layers of exceptions for now
                this.GetEntityEntry(this.Get(data.ID)).State = EntityState.Detached;
                this.Set.Attach(data);
                this.GetEntityEntry(data).State = EntityState.Modified;
            }

            try
            {
                this.SaveChanges();
            }
            // Imcomplete required field
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new IncompleteNotNullFieldException(typeof(SqlCookieEntity), data.ID, ex);
            }
            // Cannot find row (entity already deleted)
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new EntityNotFoundException(typeof(SqlCookieEntity), data.ID, ex);
            }
            // Let the child classes to handle this type of exceptions
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw ex;
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new BackendUnknownException(String.Format("Cannot update {0} ID: {1}.", typeof(SqlCookieEntity).Name, data.ID), ex);
            }
        }

        /// <summary>
        /// Remove a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="EntityNotFoundException">The data entity is not currently in the data context (lost refresh or deleted or never inserted)</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public void Remove(SqlCookieEntity data)
        {
            if (data == null) throw new NullReferenceException(String.Format("Can't remove null {0}", typeof(SqlCookieEntity).Name));

            // Change the state to deleted no matter detached or attached
            // Catch exception if the entity is actually not found by framework
            try
            {
                this.GetEntityEntry(data).State = EntityState.Deleted;
            }
            catch (InvalidOperationException ex1)
            {
                Trace.WriteLine(ex1.ToString());
                // Will not handle any more layers of exceptions for now
                this.GetEntityEntry(this.Get(data.ID)).State = EntityState.Detached;
                this.Set.Attach(data);
                this.GetEntityEntry(data).State = EntityState.Deleted;
            }

            try
            {
                this.SaveChanges();
            }
            // Cannot find row (entity already deleted or never inserted)
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new EntityNotFoundException(typeof(SqlCookieEntity), data.ID, ex);
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new BackendUnknownException(String.Format("Cannot remove {0} entity ID: {1}", typeof(SqlCookieEntity).Name, data.ID), ex);
            }
        }

        /// <summary>
        /// Get an entity object wrapped in DbEntityEntry class, which can be used to track state
        /// </summary>
        /// <param name="data">The attached entity object to be wrapped</param>
        /// <returns>The entity object wrapped in DbEntityEntry</returns>
        protected System.Data.Entity.Infrastructure.DbEntityEntry<SqlCookieEntity> GetEntityEntry(SqlCookieEntity data)
        {
            return this.Context.Entry<SqlCookieEntity>(data);
        }

        /// <summary>
        /// Try to get a sql data entity searching by the primary key
        /// </summary>
        /// <param name="ID">Primary key of the entity</param>
        /// <param name="result">The query result</param>
        /// <returns>Whether the entity exists</returns>
        public bool TryGet(System.Guid ID, out SqlCookieEntity result)
        {
            try
            {
                result = this.Get(ID);
                return result != null;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Get a sql data entity searching by the primary key
        /// </summary>
        /// <param name="ID">Primary key of the entity</param>
        /// <returns>The query result</returns>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        public SqlCookieEntity Get(System.Guid ID)
        {
            try
            {
                return this.Set.Find(ID);
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                throw new BackendUnknownException(String.Format("Can't find User entity ID: {0}.", ID), ex);
            }
        }

        /// <summary>
        /// Check if the data context contains an entity of given primary key
        /// </summary>
        /// <param name="ID">The primary key of the entity</param>
        /// <returns>Whether the primary key exists</returns>
        public bool Contains(long ID)
        {
            try
            {
                return this.Set.Find(ID) != null;
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Dispose the database context
        /// </summary>
        public void Dispose()
        {
            this.Context.Dispose();
        }

        /// <summary>
        /// Equivalent of calling this.Context.SaveChanges()
        /// </summary>
        /// <returns>Whatever this.Context.SaveChanges() returns</returns>
        protected int SaveChanges()
        {
            return this.Context.SaveChanges();
        }
    }
}