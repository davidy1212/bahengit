﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the subscription table in the sql database
    /// </summary>
    public class SqlSubscriptionOperator : SqlDataOperator<SqlSubscriptionRelation>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the Subscriptions table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlSubscriptionOperator(string connectionString) : base(connectionString, "Subscriptions") { }

        /// <summary>
        /// Construct a sql data operator with regards to the Subscriptions table
        /// <param name="context">Database context to operate on</param>
        /// </summary>
        public SqlSubscriptionOperator(SqlContext context) : base(context, "Subscriptions") { }

        /// <summary>
        /// Insert a subscription entity into database. 
        /// </summary>
        /// <param name="data">A new subscription entity to be inserted</param>
        /// <returns>The attached and inserted subscription entity</returns>
        public override void Insert(SqlSubscriptionRelation data, bool saveChanges = true)
        {
            // Check required fields
            if (data.SubscriberID == 0 | data.HostID == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert a Subscription entity with");
                sb.Append(data.SubscriberID == 0 ? " unassigned SubscriberID" : String.Empty);
                sb.Append(data.HostID == 0 ? " unassigned HostID" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Foreign key can't match (User ID, Event ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't insert a Subscription entity because foreign keys SubscriberID {0} or HostID {1} is not found in Users table.", data.SubscriberID, data.HostID), ex);
            }
        }

        /// <summary>
        /// Update a subscription entity in the database.
        /// </summary>
        /// <param name="data">The attached subscription entity to be updated</param>
        public override void Update(SqlSubscriptionRelation data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Foreign key can't match (Publisher ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't update Subscription entity ID: {0} because foreign keys SubscriberID {1} or HostID {2} is not found in Users table.", data.ID, data.SubscriberID, data.HostID), ex);
            }
        }

        /// <summary>
        /// Get all the subscriber IDs that a specified host owned
        /// </summary>
        /// <param name="hostID">Specified host ID (UserID)</param>
        /// <returns>A collection of subscriber ID (UserID)</returns>
        public IQueryable<long> GetSubscriberIDsByHost(long hostID)
        {
            var result =
                from sub in this.Set
                where sub.HostID == hostID
                select sub.SubscriberID;
            return result;
        }

        /// <summary>
        /// Get an query of all subscriptions by a host ID
        /// </summary>
        /// <param name="hostID">User ID of the host</param>
        /// <returns></returns>
        public IQueryable<SqlSubscriptionRelation> QueryByHost(long hostID)
        {
            return this.Set.Where(x => x.HostID == hostID);
        }

        /// <summary>
        /// Get all the host IDs that a specified subscriber subscribes to
        /// </summary>
        /// <param name="subscriberID">Specified subscriber ID (UserID)</param>
        /// <returns>A collection of host ID (UserID)</returns>
        public IQueryable<long> GetHostIDsBySubscriber(long subscriberID)
        {
            var result =
                from sub in this.Set
                where sub.SubscriberID == subscriberID
                select sub.HostID;
            return result;
        }

        /// <summary>
        /// Get a query of all subscriptions by a subscriber ID
        /// </summary>
        /// <param name="subscriberID">User ID of the subscriber</param>
        /// <returns></returns>
        public IQueryable<SqlSubscriptionRelation> QueryBySubscriber(long subscriberID)
        {
            return this.Set.Where(x => x.SubscriberID == subscriberID);
        }

        /// <summary>
        /// Get a Subscription entity containing the specific HostID and SubscriberID from the Subscriptions table.
        /// </summary>
        /// <param name="hostID">The HostID</param>
        /// <param name="subscriberID">The SubscriberID</param>
        /// <returns>The entity found. If more than one such entities are found, return the first one</returns>
        public SqlSubscriptionRelation Get(long hostID, long subscriberID)
        {
            var result =
                from sub in this.Set
                where sub.HostID == hostID && sub.SubscriberID == subscriberID
                select sub;
            return result.FirstOrDefault();
        }
    }
}
