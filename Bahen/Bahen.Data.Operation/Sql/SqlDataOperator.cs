﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator base class for any operator in charge of a sql database table
    /// </summary>
    /// <typeparam name="T">Any sql databse table datamodel types</typeparam>
    public abstract class SqlDataOperator<T> : IEntityDataOperator<T>, IDisposable where T : class, IInt64Id
    {
        /// <summary>
        /// Construct a sql data operator in charge of a single table in the database
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// <param name="setName">Table name in the database</param>
        /// <remarks>This will initialize a new database connection using SqlConnectionBuilder.  
        /// Do not use if need to join with sql query results from other sql operator.</remarks>
        public SqlDataOperator(string connectionString, string setName)
            : this(new SqlConnectionBuilder().Build(connectionString), setName)
        {
            this.Context.ConnectionString = connectionString;
        }

        /// <summary>
        /// Construct a sql data operator in charge of a single table in the database
        /// </summary>
        /// <param name="context">Database context to operate on</param>
        /// <param name="setName">Table name in the database</param>
        public SqlDataOperator(SqlContext context, string setName)
        {
            this.Context = context;
            this.SetName = setName;

            switch (this.SetName)
            {
                case "Users":
                    {
                        this.Set = this.Context.Users as DbSet<T>;
                        break;
                    }
                case "Events":
                    {
                        this.Set = this.Context.Events as DbSet<T>;
                        break;
                    }
                case "Cycles":
                    {
                        this.Set = this.Context.Cycles as DbSet<T>;
                        break;
                    }
                case "Subscriptions":
                    {
                        this.Set = this.Context.Subscriptions as DbSet<T>;
                        break;
                    }
                case "Attendances":
                    {
                        this.Set = this.Context.Attendances as DbSet<T>;
                        break;
                    }
                case "CycleUsers":
                    {
                        this.Set = this.Context.CycleUsers as DbSet<T>;
                        break;
                    }
                case "CycleEvents":
                    {
                        this.Set = this.Context.CycleEvents as DbSet<T>;
                        break;
                    }
                case "Cookies":
                    {
                        this.Set = this.Context.Cookies as DbSet<T>;
                        break;
                    }
                case "ApiClients":
                    {
                        this.Set = this.Context.ApiClients as DbSet<T>;
                        break;
                    }
                case "ApiAuth":
                    {
                        this.Set = this.Context.ApiAuth as DbSet<T>;
                        break;
                    }
                default: throw new ArgumentException(String.Format("Cannot identify set name \"{0}\"", this.SetName));
            }

            // If fails to cast type using "as"
            if (this.Set == null)
            {
                throw new ArgumentException(String.Format("Set name {0} does not match type {1}", this.SetName, typeof(T).ToString()));
            }
        }

        /// <summary>
        /// A reference to the table dbset of the context
        /// </summary>
        public DbSet<T> Set { get; private set; }

        /// <summary>
        /// A reference to the database context (entity framework)
        /// </summary>
        public SqlContext Context { get; set; }

        /// <summary>
        /// Table name of the operator in charge of
        /// </summary>
        private string SetName { get; set; }

        /// <summary>
        /// Insert a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        public virtual void Insert(T data)
        {
            this.Insert(data, true);
        }

        /// <summary>
        /// Insert a sql data entity into sql database
        /// </summary>
        /// <param name="data">A new sql data entity wrapped in entity class (not attached)</param>
        /// <param name="saveChanges">Whether the operation save changes afterwards</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="IdSpecifiedException">Cannot insert with assigned id</exception>
        /// <exception cref="IncompleteNotNullFieldException">Incomplete required field</exception>
        /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateException">Exception that depends on the child operator class</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public virtual void Insert(T data, bool saveChanges)
        {
            // Can only insert with unspecified userID (i.e. new entity)
            if (data.ID != 0)
            {
                throw new IdSpecifiedException(typeof(T), data.ID);
            }
            try
            {
                this.Set.Add(data);

                if (saveChanges)
                {
                    this.SaveChanges();
                }
            }
            // Incomplete field
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw new IncompleteNotNullFieldException(typeof(T), data.ID, ex);
            }
            // Let the child classes to handle this type of exceptions
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw ex;
            }
            // Unknown exception
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.Set.Remove(data);
                throw new BackendUnknownException(String.Format("Cannot insert {0}.", typeof(T).Name), ex);
            }
        }

        /// <summary>
        /// Update a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        public virtual void Update(T data)
        {
            this.Update(data, true);
        }

        /// <summary>
        /// Update a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="IncompleteNotNullFieldException">Incomplete required field</exception>
        /// <exception cref="System.Data.Entity.Infrastructure.DbUpdateException">Exception that depends on the child operator class</exception>
        /// <exception cref="EntityNotFoundException">The data entity is not currently in the data context (lost refresh or deleted or never inserted)</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public virtual void Update(T data, bool saveChanges)
        {
            if (data == null) throw new NullReferenceException(String.Format("Cannot update null {0}", typeof(T).Name));

            // Change the state to modified no matter detached or attached
            // Catch exception if the entity is actually not found by framework
            try
            {
                this.GetEntityEntry(data).State = EntityState.Modified;
            }
            catch (InvalidOperationException ex1)
            {
                Trace.WriteLine(ex1.ToString());
                // Will not handle any more layers of exceptions for now
                this.GetEntityEntry(this.Get(data.ID)).State = EntityState.Detached;
                this.Set.Attach(data);
                this.GetEntityEntry(data).State = EntityState.Modified;
            }

            try
            {
                if (saveChanges)
                {
                    this.SaveChanges();
                }
            }
            // Imcomplete required field
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new IncompleteNotNullFieldException(typeof(T), data.ID, ex);
            }
            // Cannot find row (entity already deleted)
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new EntityNotFoundException(typeof(T), data.ID, ex);
            }
            // Let the child classes to handle this type of exceptions
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw ex;
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new BackendUnknownException(String.Format("Cannot update {0} ID: {1}.", typeof(T).Name, data.ID), ex);
            }
        }

        /// <summary>
        /// Remove a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        public virtual void Remove(T data)
        {
            this.Remove(data, true);
        }

        /// <summary>
        /// Remove a sql data entity
        /// </summary>
        /// <param name="data">An attached sql data entity</param>
        /// <param name="saveChanges">Whether save changes</param>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        /// <exception cref="EntityNotFoundException">The data entity is not currently in the data context (lost refresh or deleted or never inserted)</exception>
        /// <exception cref="NullReferenceException">The argument of data entity is null</exception>
        public virtual void Remove(T data, bool saveChanges)
        {
            if (data == null) throw new NullReferenceException(String.Format("Can't remove null {0}", typeof(T).Name));

            // Change the state to deleted no matter detached or attached
            // Catch exception if the entity is actually not found by framework
            try
            {
                this.GetEntityEntry(data).State = EntityState.Deleted;
            }
            catch (InvalidOperationException ex1)
            {
                Trace.WriteLine(ex1.ToString());
                // Will not handle any more layers of exceptions for now
                this.GetEntityEntry(this.Get(data.ID)).State = EntityState.Detached;
                this.Set.Attach(data);
                this.GetEntityEntry(data).State = EntityState.Deleted;
            }

            try
            {
                if (saveChanges)
                {
                    this.SaveChanges();
                }
            }
            // Cannot find row (entity already deleted or never inserted)
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new EntityNotFoundException(typeof(T), data.ID, ex);
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                this.GetEntityEntry(data).State = EntityState.Detached;
                throw new BackendUnknownException(String.Format("Cannot remove {0} entity ID: {1}", typeof(T).Name, data.ID), ex);
            }
        }

        /// <summary>
        /// Get an entity object wrapped in DbEntityEntry class, which can be used to track state
        /// </summary>
        /// <param name="data">The attached entity object to be wrapped</param>
        /// <returns>The entity object wrapped in DbEntityEntry</returns>
        protected System.Data.Entity.Infrastructure.DbEntityEntry<T> GetEntityEntry(T data)
        {
            return this.Context.Entry<T>(data);
        }

        /// <summary>
        /// Try to get a sql data entity searching by the primary key
        /// </summary>
        /// <param name="ID">Primary key of the entity</param>
        /// <param name="result">The query result</param>
        /// <returns>Whether the entity exists</returns>
        public virtual bool TryGet(long ID, out T result)
        {
            try
            {
                result = this.Get(ID);
                return result != null;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        /// <summary>
        /// Get a sql data entity searching by the primary key
        /// </summary>
        /// <param name="ID">Primary key of the entity</param>
        /// <returns>The query result</returns>
        /// <exception cref="BackendUnknownException">Backend cannot identify the exception type</exception>
        public virtual T Get(long ID)
        {
            try
            {
                return this.Set.Find(ID);
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                throw new BackendUnknownException(String.Format("Can't find User entity ID: {0}.", ID), ex);
            }
        }

        /// <summary>
        /// Check if the data context contains an entity of given primary key
        /// </summary>
        /// <param name="ID">The primary key of the entity</param>
        /// <returns>Whether the primary key exists</returns>
        public virtual bool Contains(long ID)
        {
            try
            {
                return this.Set.Find(ID) != null;
            }
            // Unknown exceptions
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                return false;
            }
        }

        /// <summary>
        /// Dispose the database context
        /// </summary>
        public void Dispose()
        {
            this.Context.Dispose();
        }

        /// <summary>
        /// Equivalent of calling this.Context.SaveChanges()
        /// </summary>
        /// <returns>Whatever this.Context.SaveChanges() returns</returns>
        public int SaveChanges()
        {
            return this.Context.SaveChanges();
        }

        /// <summary>
        /// Truncate data into pages
        /// </summary>
        /// <param name="rawData">Raw data</param>
        /// <param name="options">Paging options</param>
        /// <returns></returns>
        public virtual IQueryable<T> Truncate(IQueryable<T> rawData, PagingOptions options)
        {
            if (options == null)
            {
                return rawData;
            }
            if (options.Mode == PagingMode.CursorAfterInt64)
            {
                return rawData
                    .OrderBy(x => x.ID)
                    .Where(x => (x.ID > options.AfterInt64))
                    .Take(options.Limit);
            }
            else if (options.Mode == PagingMode.CursorBeforeInt64)
            {
                return rawData
                    .OrderByDescending(x => x.ID)
                    .Where(x => (x.ID < options.BeforeInt64))
                    .Take(options.Limit);
            }
            else if (options.Mode == PagingMode.Offset)
            {
                return rawData
                    .OrderBy(x => x.ID)
                    .Skip(options.Offset)
                    .Take(options.Limit);
            }
            return new List<T>().AsQueryable();
        }
    }
}
