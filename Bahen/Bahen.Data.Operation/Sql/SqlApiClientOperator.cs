﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    public class SqlApiClientOperator : SqlDataOperator<SqlApiClientEntity>
    {
        public SqlApiClientOperator(string connectionString) : base(connectionString, "ApiClients") { }

        public SqlApiClientOperator(SqlContext context) : base(context, "ApiClients") { }

        public override void Remove(SqlApiClientEntity data, bool saveChanges = true)
        {
            //Remove all api auth tokens
            SqlApiAuthOperator op = new SqlApiAuthOperator(this.Context);
            foreach (SqlApiAuthRelation auth in op.QueryByClient(data.ClientID))
            {
                op.Remove(auth, saveChanges);
            }
            base.Remove(data, saveChanges);
        }
    }
}
