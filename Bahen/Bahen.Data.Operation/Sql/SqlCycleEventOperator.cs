using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the cycle event table in the sql database
    /// </summary>
    public class SqlCycleEventOperator : SqlDataOperator<SqlCycleEventRelation>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the cycle events table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlCycleEventOperator(string connectionString) : base(connectionString, "CycleEvents") { }

        /// <summary>
        /// Construct a sql data operator with regards to the cycle events table
        /// <param name="connectionString">Database context to operate on</param>
        /// </summary>
        public SqlCycleEventOperator(SqlContext context) : base(context, "CycleEvents") { }

        /// <summary>
        /// Insert a cycle event entity into the sql database
        /// </summary>
        /// <param name="data">A new cycle event entity to be inserted</param>
        public override void Insert(SqlCycleEventRelation data, bool saveChanges = true)
        {
            // Check required fields
            if (data.CycleID == 0 | data.EventID == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert a CycleEvent entity with");
                sb.Append(data.CycleID == 0 ? " unassigned CycleID" : String.Empty);
                sb.Append(data.EventID == 0 ? " unassigned EventID" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Foreign key can't match (User ID, Event ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't insert a CycleEvent entity because foreign keys CycleID {0} or EventID {1} is not found in Cycles and Events table.", data.CycleID, data.EventID), ex);
            }
        }

        /// <summary>
        /// Update a cycle event entity
        /// </summary>
        /// <param name="data">The attached cycle event entity to be updated</param>
        public override void Update(SqlCycleEventRelation data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Foreign key can't match (Publisher ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't update CycleEvent entity ID: {0} because foreign keys CycleID {1} or EventID {2} is not found in Cycles and Events table.", data.ID, data.CycleID, data.EventID), ex);
            }
        }

        public SqlCycleEventRelation Get(long cycleID, long eventID)
        {
            var result =
                from att in this.Set
                where att.CycleID == cycleID && att.EventID == eventID
                select att;
            return result.FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cycleID"></param>
        /// <returns></returns>
        public IQueryable<long> GetEventIDsByCycle(long cycleID)
        {
            var result =
                from att in this.Set
                where att.CycleID == cycleID
                select att.EventID;
            return result;
        }

        public IQueryable<SqlCycleEventRelation> QueryByCycle(long cycleID)
        {
            return this.Set.Where(x => x.CycleID == cycleID);
        }

        public IQueryable<SqlCycleEventRelation> QueryByCycle(long cycleID, Func<SqlCycleEventRelation, bool> predicate)
        {
            return this.QueryByCycle(cycleID).ToList().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventID"></param>
        /// <returns></returns>
        public IQueryable<long> GetCyclesByEvent(long eventID)
        {
            var result =
                from att in this.Set
                where att.EventID == eventID
                select att.CycleID;
            return result;
        }

        public IQueryable<SqlCycleEventRelation> QueryByEvent(long eventID)
        {
            return this.Set.Where(x => x.EventID == eventID);
        }

        public IQueryable<SqlCycleEventRelation> QueryByEvent(long eventID, Func<SqlCycleEventRelation, bool> predicate)
        {
            return this.QueryByEvent(eventID).ToList().Where(predicate).AsQueryable();
        }
    }
}
