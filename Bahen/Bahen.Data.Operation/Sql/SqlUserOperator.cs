﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the Users table in the sql database
    /// </summary>
    public class SqlUserOperator : SqlDataOperator<SqlUserEntity>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the users table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlUserOperator(string connectionString) : base(connectionString, "Users") { }

        /// <summary>
        /// Construct a sql data operator with regards to the users table
        /// <param name="context">Database context to operate on</param>
        /// </summary>
        public SqlUserOperator(SqlContext context) : base(context, "Users") { }

        /// <summary>
        /// Insert a user entity into the sql database
        /// </summary>
        /// <param name="data">A new user entity to be inserted</param>
        public override void Insert(SqlUserEntity data, bool saveChanges = true)
        {
            // Check imcomplete required field
            if (String.IsNullOrEmpty(data.LoginEmail) | data.LoginPassword == null | String.IsNullOrEmpty(data.PreferredName))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert a user entity with");
                sb.Append(String.IsNullOrEmpty(data.LoginEmail) ? " null or empty LoginEmail" : String.Empty);
                sb.Append(data.LoginPassword == null ? " null or empty LoginPassword" : String.Empty);
                sb.Append(String.IsNullOrEmpty(data.PreferredName) ? " null or empty PreferredName" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Key collision
            catch (DbUpdateException ex)
            {
                throw new UniqueKeyCollisionException(typeof(SqlUserEntity), data.ID, "LoginEmail", data.LoginEmail, ex);
            }
        }

        /// <summary>
        /// Update a user entity in the sql database
        /// </summary>
        /// <param name="data">The attached user entity to be updated</param>
        public override void Update(SqlUserEntity data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Key collision
            catch (DbUpdateException ex)
            {
                throw new UniqueKeyCollisionException(typeof(SqlUserEntity), data.ID, "LoginEmail", data.LoginEmail, ex);
            }
        }

        /// <summary>
        /// Remove a user entity in the sql database
        /// </summary>
        /// <param name="data">The attached user entity to be removed</param>
        public override void Remove(SqlUserEntity data, bool saveChanges = true)
        {
            // Remove all the subscription relation
            SqlSubscriptionOperator subscriptionOperator = new SqlSubscriptionOperator(this.Context);
            IList<SqlSubscriptionRelation> subscriptions = new List<SqlSubscriptionRelation>(
                subscriptionOperator.QueryBySubscriber(data.ID).Concat(subscriptionOperator.QueryByHost(data.ID)));
            foreach (var item in subscriptions)
            {
                subscriptionOperator.Remove(item, saveChanges);
            }
            // Remove all the attendance relation
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.Context);
            IList<SqlAttendanceRelation> attendances = new List<SqlAttendanceRelation>(attendanceOperator.QueryByUser(data.ID));
            foreach (var item in attendances)
            {
                attendanceOperator.Remove(item, saveChanges);
            }
            // Remove all the cycle membership relation
            SqlCycleUserOperator cycleUserOperator = new SqlCycleUserOperator(this.Context);
            IList<SqlCycleUserRelation> cycleUsers = new List<SqlCycleUserRelation>(cycleUserOperator.QueryByUser(data.ID));
            foreach (var item in cycleUsers)
            {
                cycleUserOperator.Remove(item, saveChanges);
            }
            // Remove user itself
            base.Remove(data);
        }

        /// <summary>
        /// Try get a user entity provided the login email
        /// </summary>
        /// <param name="loginEmail">Login email of the user</param>
        /// <param name="user">The user entity with the login email, null if not found</param>
        /// <returns>Whether the user with the login email is found</returns>
        public bool TryGet(string loginEmail, out SqlUserEntity user)
        {
            user = this.Get(loginEmail);
            return user != null;
        }

        /// <summary>
        /// Get a user entity provide the login email
        /// </summary>
        /// <param name="loginEmail">Login email of the user</param>
        /// <returns>The user entity with the login email, null if not found</returns>
        public SqlUserEntity Get(string loginEmail)
        {
            var query = from u in this.Set where u.LoginEmail == loginEmail select u;
            return query.FirstOrDefault<SqlUserEntity>();
        }
    }
}
