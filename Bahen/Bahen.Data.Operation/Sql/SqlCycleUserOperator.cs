﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the cycle table in the sql database
    /// </summary>
    public class SqlCycleUserOperator : SqlDataOperator<SqlCycleUserRelation>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the cycle users table
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// </summary>
        /// <see cref="SqlDataOperator"/>
        public SqlCycleUserOperator(string connectionString) : base(connectionString, "CycleUsers") { }

        /// <summary>
        /// Construct a sql data operator with regards to the cycle users table
        /// <param name="connectionString">Database context to operate on</param>
        /// </summary>
        public SqlCycleUserOperator(SqlContext context) : base(context, "CycleUsers") { }

        /// <summary>
        /// Insert a cycle user entity into the sql database
        /// </summary>
        /// <param name="data">A new cycle user entity to be inserted</param>
        /// <param name="saveChanges">Whether to save changes</param>
        public override void Insert(SqlCycleUserRelation data, bool saveChanges = true)
        {
            // Check Imcomplete required field
            if (data.CycleID == 0 | data.UserID == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert a CycleUser entity with");
                sb.Append(data.CycleID == 0 ? " unassigned CycleID" : String.Empty);
                sb.Append(data.UserID == 0 ? " unassigned UserID" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Foreign key can't match (User ID, Cycle ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't insert a CycleUser entity because foreign keys CycleID {0} or UserID {1} is not found in Cycles and Users table.", data.CycleID, data.UserID), ex);
            }
        }

        /// <summary>
        /// Update a cycle user entity in the sql database
        /// </summary>
        /// <param name="data">The attached cycle user entity to be updated</param>
        /// <param name="saveChanges">Whether to save changes</param>
        public override void Update(SqlCycleUserRelation data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Foreign key can't match (Publisher ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't update CycleUser entity ID: {0} because foreign keys CycleID {1} or UserID {2} is not found in Cycles and Users table.", data.ID, data.CycleID, data.UserID), ex);
            }
        }

        /// <summary>
        /// Get all the members of a cycle
        /// </summary>
        /// <param name="cycleID">ID of the cycle</param>
        /// <returns>A collection of user IDs. Empty if cycleID not exist in the database.</returns>
        public IQueryable<long> GetUserIDsByCycle(long cycleID)
        {
            return from cycleUser in this.Set
                   where cycleUser.CycleID == cycleID
                   select cycleUser.UserID;
        }

        /// <summary>
        /// Get all the memebers of a cycle by specifying a memebership type
        /// </summary>
        /// <param name="cycleID">The ID of the cycle</param>
        /// <param name="membershipType">A certain membership type defined in CycleUserMembershipType enum</param>
        /// <returns>A collection of user IDs. Empty if cycleID not exist in the database</returns>
        public IQueryable<long> GetUserIDsByCycle(long cycleID, int membershipType)
        {
            return from cycleUser in this.Set
                   where cycleUser.CycleID == cycleID && cycleUser.MembershipType == membershipType
                   select cycleUser.UserID;
        }

        /// <summary>
        /// Get all the cycle IDs that a user joined
        /// </summary>
        /// <param name="userID">The ID of the user</param>
        /// <returns>A collection of cycle IDs. Empty if userID not exist in the database</returns>
        public IQueryable<long> GetCycleIDsByUser(long userID)
        {
            return from cycleUser in this.Set
                   where cycleUser.UserID == userID
                   select cycleUser.CycleID;
        }

        /// <summary>
        /// Get all the members of a cycle
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <returns>A collection of cycle user relation</returns>
        public IQueryable<SqlCycleUserRelation> QueryByCycle(long cycleID)
        {
            return this.Set.Where(x => x.CycleID == cycleID);
        }

        /// <summary>
        /// Get all the members of a cycle that matches given conditions
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <param name="predicate">Conditions on cycle user relation</param>
        /// <returns>A collection of cycle user relation</returns>
        public IQueryable<SqlCycleUserRelation> QueryByCycle(long cycleID, Func<SqlCycleUserRelation, bool> predicate)
        {
            return this.QueryByCycle(cycleID).ToList().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// Get the membership of the a cycle that matches the cycle ID and user ID
        /// </summary>
        /// <param name="userID">User ID of the member</param>
        /// <param name="cycleID">Cycle ID</param>
        /// <returns>Matched membership</returns>
        public IQueryable<SqlCycleUserRelation> QueryByBoth(long userID, long cycleID)
        {
            return this.Set.Where(x => x.UserID == userID && x.CycleID == cycleID);
        }

        /// <summary>
        /// Get all the cycle IDs that a user joined as a specified member type
        /// </summary>
        /// <param name="userID">The ID of the user</param>
        /// <param name="membershipType">A certain membership type defined in CycleUserMembershipType enum</param>
        /// <returns>A collection of cycle IDs. Empty if userID not exist in the database</returns>
        public IQueryable<long> GetCycleIDsByUser(long userID, int membershipType)
        {
            return from cycleUser in this.Set
                   where cycleUser.UserID == userID && cycleUser.MembershipType == membershipType
                   select cycleUser.CycleID;
        }

        /// <summary>
        /// Get all the cycles that a user joined
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns>A collection of cycle user relations</returns>
        public IQueryable<SqlCycleUserRelation> QueryByUser(long userID)
        {
            return this.Set.Where(x => x.UserID == userID);
        }

        /// <summary>
        /// Get all the cycles that a user joined with certain type of membership
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="type">Membership type</param>
        /// <returns>A collection of cycle user relations</returns>
        public IQueryable<SqlCycleUserRelation> QueryByUser(long userID, CycleMembershipType type)
        {
            return this.Set.Where(x => (x.UserID == userID && x.MembershipType == (int)type));
        }

        /// <summary>
        /// Get all the cycles that a user joined with certain type of membership or higher permission types
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="type">Membership type</param>
        /// <returns>A collection of cycle user relations</returns>
        public IQueryable<SqlCycleUserRelation> QueryByUser(long userID, CycleMembershipType type, bool samePermissionOrAbove)
        {
            if (!samePermissionOrAbove)
            {
                return this.QueryByUser(userID, type);
            }
            else
            {
                return this.Set.Where(x => (x.UserID == userID && x.MembershipType >= (int)type));
            }
        }

        /// <summary>
        /// Get all the cycles that a user joined with given conditions
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="predicate">Conditions on cycle user relation</param>
        /// <returns>A collection of cycle user relations</returns>
        public IQueryable<SqlCycleUserRelation> QueryByUser(long userID, Func<SqlCycleUserRelation, bool> predicate)
        {
            return this.QueryByUser(userID).ToList().Where(predicate).AsQueryable();
        }
    }
}
