﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// DataOperator in charge of the attendance table in the sql database
    /// </summary>
    public class SqlAttendanceOperator : SqlDataOperator<SqlAttendanceRelation>
    {
        /// <summary>
        /// Construct a sql data operator with regards to the attendances table
        /// </summary>
        /// <param name="connectionString">Connection string used to connect the sql database</param>
        /// <see cref="SqlDataOperator"/>
        public SqlAttendanceOperator(string connectionString) : base(connectionString, "Attendances") { }

        /// <summary>
        /// Construct a sql data operator with regards to the attendances table
        /// </summary>
        /// <param name="context">Database context to operate on</param>
        public SqlAttendanceOperator(SqlContext context) : base(context, "Attendances") { }

        /// <summary>
        /// Insert an attendance entity into the sql database.
        /// </summary>
        /// <param name="data">A new attendance entity tobe inserted</param>
        public override void Insert(SqlAttendanceRelation data, bool saveChanges = true)
        {
            // Check required fields
            if (data.UserID == 0 | data.EventID == 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Can't insert an Attendance entity with");
                sb.Append(data.UserID == 0 ? " unassigned UserID" : String.Empty);
                sb.Append(data.EventID == 0 ? " unassigned EventID" : String.Empty);
                sb.Append(".");

                throw new IncompleteNotNullFieldException(sb.ToString());
            }

            try
            {
                base.Insert(data, saveChanges);
            }
            // Foreign key can't match (User ID, Event ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't insert an Attendance entity because foreign keys EventID {0} or UserID {1} is not found in Events and Users table.", data.EventID, data.UserID), ex);
            }
        }

        /// <summary>
        /// Update an attendance entity in the sql databse
        /// </summary>
        /// <param name="data">The attached attendance entity to be updated</param>
        public override void Update(SqlAttendanceRelation data, bool saveChanges = true)
        {
            try
            {
                base.Update(data, saveChanges);
            }
            // Foreign key can't match (Publisher ID)
            catch (DbUpdateException ex)
            {
                throw new ForeignKeyNotMappedException(String.Format("Can't update Attendance entity ID: {0} because foreign keys EventID {1} or UserID {2} is not found in Events and Users table.", data.ID, data.EventID, data.UserID), ex);
            }
        }

        /// <summary>
        /// Get an Attendace entity containing the specific UserID and EventID from the Attendances table.
        /// </summary>
        /// <param name="userID">The UserID</param>
        /// <param name="eventID">The EventID</param>
        /// <returns>The entity found. If more than one such entities are found, return the first one</returns>
        public SqlAttendanceRelation Get(long userID, long eventID)
        {
            var result =
                from att in this.Set
                where att.UserID == userID && att.EventID == eventID
                select att;
            return result.FirstOrDefault();
        }

        /// <summary>
        /// Get all events related to the specific user from the Attendances table.
        /// </summary>
        /// <param name="userID">The ID of the specific user</param>
        /// <returns>an IQueryable of long representing EventIDs</returns>
        public IQueryable<long> GetEventIDsByUser(long userID)
        {
            var result =
                from att in this.Set
                where att.UserID == userID
                select att.EventID;
            return result;
        }

        /// <summary>
        /// Get all events related to the specific user from the Attendances table with the specific AttendanceStatus.
        /// </summary>
        /// <param name="userID">ID of the specific user</param>
        /// <param name="attendanceStatus">the attendanceStatus required for the Events</param>
        /// <returns>an IQueryable of long representing EventIDs</returns>
        public IQueryable<long> GetEventIDsByUser(long userID, int attendanceStatus)
        {
            var result =
                from att in this.Set
                where att.UserID == userID && att.AttendanceStatus == attendanceStatus
                select att.EventID;
            return result;
        }

        /// <summary>
        /// Get all attendance entity by specific user
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns>A collection attendance entity matching user ID</returns>
        public IQueryable<SqlAttendanceRelation> QueryByUser(long userID)
        {
            return this.Set.Where(x => x.UserID == userID);
        }

        /// <summary>
        /// Get all attendance entity by specific user matching the predicate
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="predicate">A function to filter further on the user ID matches</param>
        /// <returns>All attendance entity that matches both user ID and predicate</returns>
        public IQueryable<SqlAttendanceRelation> QueryByUser(long userID, Func<SqlAttendanceRelation, bool> predicate)
        {
            return this.QueryByUser(userID).ToList().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// Get all attendance entity by specific user with specific status
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="status">Attendance status</param>
        /// <returns></returns>
        public IQueryable<SqlAttendanceRelation> QueryByUser(long userID, AttendanceStatus status)
        {
            return this.Set.Where(x => x.UserID == userID && x.AttendanceStatus == (int)status);
        }

        /// <summary>
        /// Get all users related to the specific event from the Attendances table with the specific AttendanceStatus.
        /// </summary>
        /// <param name="eventID">ID of the specific Event</param>
        /// <param name="attendanceStatus">the attendanceStatus required for the Events</param>
        /// <returns>an IQueryable of long representing UserIDs</returns>
        public IQueryable<long> GetUserIDsByEvent(long eventID, int attendanceStatus)
        {
            var result =
                from att in this.Set
                where att.EventID == eventID && att.AttendanceStatus == attendanceStatus
                select att.UserID;
            return result;
        }

        /// <summary>
        /// Get all attendance entity by specific event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <returns>A collection of attendance entity matching event ID</returns>
        public IQueryable<SqlAttendanceRelation> QueryByEvent(long eventID)
        {
            return this.Set.Where(x => x.EventID == eventID);
        }

        /// <summary>
        /// Get all attendance entities by specific event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="predicate">A function to filter further on the event ID matches</param>
        /// <returns>All attendance entity that matches both event ID and predicate</returns>
        public IQueryable<SqlAttendanceRelation> QueryByEvent(long eventID, Func<SqlAttendanceRelation, bool> predicate)
        {
            return this.QueryByEvent(eventID).ToList().Where(predicate).AsQueryable();
        }

        /// <summary>
        /// Get all attendance entities by specific event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="status">Attendance status</param>
        /// <returns>All attendance entities matching the status</returns>
        public IQueryable<SqlAttendanceRelation> QueryByEvent(long eventID, AttendanceStatus status)
        {
            return this.Set.Where(x => x.EventID == eventID && x.AttendanceStatus == (int)status);
        }

        /// <summary>
        /// Get all attendance entities by specific event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="lowestStatus">Lowest attendance level</param>
        /// <param name="highestStatus">Highest attendance level</param>
        /// <returns>All attendance entities within the status range</returns>
        public IQueryable<SqlAttendanceRelation> QueryByEvent(long eventID, AttendanceStatus lowestStatus, AttendanceStatus highestStatus)
        {
            return this.Set.Where(x =>
                    x.EventID == eventID &&
                    x.AttendanceStatus >= (int)lowestStatus &&
                    x.AttendanceStatus >= (int)highestStatus);
        }

        /// <summary>
        /// Get all attendance entity by specific event and user
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="eventID">Event ID</param>
        /// <returns>All attendance entity matching the specific event and user</returns>
        public IQueryable<SqlAttendanceRelation> QueryByBoth(long userID, long eventID)
        {
            return this.Set.Where(x => x.EventID == eventID && x.UserID == userID);
        }

        /// <summary>
        /// Get all users related to the specific event from the Attendances table.
        /// </summary>
        /// <param name="eventID">ID of the specific event</param>
        /// <returns>an IQueryable of long representing UserIDs</returns>
        public IQueryable<long> GetUserIDsByEvent(long eventID)
        {
            var result =
                from att in this.Set
                where att.EventID == eventID
                select att.UserID;
            return result;
        }
    }
}
