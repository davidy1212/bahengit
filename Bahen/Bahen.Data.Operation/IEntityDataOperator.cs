﻿using Bahen.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Data operator interface for entities that have Int64 type ID
    /// </summary>
    /// <typeparam name="T">Type of the ientity</typeparam>
    public interface IEntityDataOperator<T> : IDataOperator<T> where T : class
    {
        /// <summary>
        /// Get the data based on ID
        /// </summary>
        /// <param name="ID">ID of the data entity</param>
        /// <returns></returns>
        T Get(long ID);

        /// <summary>
        /// Try to get the data based on ID
        /// </summary>
        /// <param name="ID">ID of the data entity</param>
        /// <param name="data">Result of the search</param>
        /// <returns>Whether ID exists</returns>
        bool TryGet(long ID, out T data);
    }
}
