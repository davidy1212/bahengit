﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that are not identified at back end
    /// </summary>
    [Serializable]
    public class BackendUnknownException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public BackendUnknownException() { }

        /// <summary>
        /// Constructs a BackEndUnknownException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public BackendUnknownException(string message) : base(message) { }

        /// <summary>
        /// Constructs a BackEndUnknownException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="inner">The inner exception</param>
        public BackendUnknownException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructs a BackEndUnknownException with serialized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected BackendUnknownException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
