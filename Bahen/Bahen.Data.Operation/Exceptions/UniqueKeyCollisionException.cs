﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that occur when unique key constraints are not met
    /// </summary>
    [Serializable]
    public class UniqueKeyCollisionException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public UniqueKeyCollisionException() { }

        /// <summary>
        /// Constructs a UniqueKeyCollisionException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public UniqueKeyCollisionException(string message) : base(message) { }

        /// <summary>
        /// Constructs a UniqueKeyCollisionException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="inner">The inner exception</param>
        public UniqueKeyCollisionException(string message, Exception inner) : base(message, inner) { }
        
        /// <summary>
        /// Constructs a UniqueKeyCollsionException
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="uniqueKeyName">The unique key field name</param>
        /// <param name="uniqueKey">The unique key that violates the constraint</param>
        public UniqueKeyCollisionException(Type type, object id, string uniqueKeyName, string uniqueKey) : this(type, id, uniqueKeyName, uniqueKey, null) { }
        
        /// <summary>
        /// Constructs a UniqueKeyCollisionException with an inner exception
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="uniqueKeyName">The unique key field name</param>
        /// <param name="uniqueKey">The unique key that violates the constraint</param>
        /// <param name="inner">The inner exception</param>
        public UniqueKeyCollisionException(Type type, object id, string uniqueKeyName, string uniqueKey, Exception inner) : base(String.Format("Can't insert {0} ID: {1} with unique key {2} {3} already existed.", type.Name, id, uniqueKeyName, uniqueKey), inner) { }
        
        /// <summary>
        /// Constructs a UniqueKeyCollisionException with serialized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected UniqueKeyCollisionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
