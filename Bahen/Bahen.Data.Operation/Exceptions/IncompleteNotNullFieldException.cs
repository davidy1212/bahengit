﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that occur when not null constraints are not met
    /// </summary>
    [Serializable]
    public class IncompleteNotNullFieldException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public IncompleteNotNullFieldException() { }

        /// <summary>
        /// Construct an IncompleteNotNullFieldException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public IncompleteNotNullFieldException(string message) : base(message) { }
        
        /// <summary>
        /// Construct an IncompleteNotNullFieldException with an error message and an inner exception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="inner"></param>
        public IncompleteNotNullFieldException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructs an IncompleteNotNullFieldException
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        public IncompleteNotNullFieldException(Type type, long id) : this(type, id, null) { }

        public IncompleteNotNullFieldException(Type type, System.Guid id) : this(type, id, null) { }
        
        /// <summary>
        /// Constructs an IncompleteNotNullFieldException with an inner exception
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="inner">The inner exception</param>
        public IncompleteNotNullFieldException(Type type, long id, Exception inner) : base(String.Format("Cannot operate on {0} ID: {1} with incomplete required field", type.Name, id), inner) { }

        public IncompleteNotNullFieldException(Type type, System.Guid id, Exception inner) : base(String.Format("Cannot operate on {0} ID: {1} with incomplete required field", type.Name, id), inner) { }
        
        /// <summary>
        /// Constructs an IncompleteNotNullFieldException with serilized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected IncompleteNotNullFieldException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
