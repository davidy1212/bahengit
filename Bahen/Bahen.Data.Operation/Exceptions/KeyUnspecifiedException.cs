﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    [Serializable]
    public class KeyUnspecifiedException : Exception
    {
        public KeyUnspecifiedException() { }
        public KeyUnspecifiedException(Type type) : this(type, null) { }
        public KeyUnspecifiedException(Type type, Exception inner) : this(String.Format("Cannot operate on {0} with key unspecified.", type.Name), inner) { }
        public KeyUnspecifiedException(string message) : base(message) { }
        public KeyUnspecifiedException(string message, Exception inner) : base(message, inner) { }
        protected KeyUnspecifiedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
