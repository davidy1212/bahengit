﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    [Serializable]
    public class SqlTableNotSyncedException : Exception
    {
        public SqlTableNotSyncedException() { }
        public SqlTableNotSyncedException(string message) : base(message) { }
        public SqlTableNotSyncedException(string message, Exception inner) : base(message, inner) { }
        protected SqlTableNotSyncedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
