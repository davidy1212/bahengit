﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that occur when trying to operate on an entity that is not found 
    /// </summary>
    [Serializable]
    public class EntityNotFoundException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public EntityNotFoundException() { }

        /// <summary>
        /// Constructs an EntityNotFoundException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public EntityNotFoundException(string message) : base(message) { }

        /// <summary>
        /// Constructs an EntityNotFoundException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="inner">The inner exception</param>
        public EntityNotFoundException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructs an EntityNotFoundException
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        public EntityNotFoundException(Type type, object id) : this(type, id, null) { }

        /// <summary>
        /// Constructs an EntityNotFoundException with an inner exception
        /// </summary>
        /// <param name="type">The type name of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="inner">The inner exception</param>
        public EntityNotFoundException(Type type, object id, Exception inner) : this(String.Format("Cannot operate on {0} ID: {1} since it is detached or deleted from the context.  Try re-get the entity by ID.", type.Name, id),inner) { }
        
        /// <summary>
        /// Constructs an EntityNotFoundException with serialized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected EntityNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
