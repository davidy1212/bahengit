﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that occur when trying to insert a row of data with primary key already defined
    /// </summary>
    [Serializable]
    public class IdSpecifiedException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public IdSpecifiedException() { }

        /// <summary>
        /// Constructs an IdSpecifiedException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public IdSpecifiedException(string message) : base(message) { }

        /// <summary>
        /// Constructs an IdSpecifiedException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="inner">The inner exception</param>
        public IdSpecifiedException(string message, Exception inner) : base(message, inner) { }
        
        /// <summary>
        ///Constructs an IdSpecifiedException
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        public IdSpecifiedException(Type type, long id) { }

        /// <summary>
        /// Constructs an IdSpecifiedException with an error message
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primaory key of the row</param>
        /// <param name="inner">The inner exception</param>
        public IdSpecifiedException(Type type, long id, Exception inner) : this(String.Format("Cannot operate on {0} with specified id {1}.", type.Name, id), inner) { }
       
        /// <summary>
        /// Constructs an IdSpecifiedException with serialized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected IdSpecifiedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
