﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// Represents errors that occurs when foreign key constraints are not met
    /// </summary>
    [Serializable]
    public class ForeignKeyNotMappedException : Exception
    {
        /// <summary>
        /// Empty constructor
        /// </summary>
        public ForeignKeyNotMappedException() { }

        /// <summary>
        /// Constructs a ForeignKeyNotMappedException with an error message
        /// </summary>
        /// <param name="message">The error message</param>
        public ForeignKeyNotMappedException(string message) : base(message) { }

        /// <summary>
        /// Constructs a ForeignKeyNotMappedException with an error message and an inner exception
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="inner">The inner exception</param>
        public ForeignKeyNotMappedException(string message, Exception inner) : base(message, inner) { }
        
        /// <summary>
        /// Constructs a ForeignKeyNotMappedException
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="foreignKeyName">The foreign key field name</param>
        /// <param name="foreignKey">The foreign key that violates the constraint</param>
        public ForeignKeyNotMappedException(Type type, long id, string foreignKeyName, long foreignKey) : this(type, id, foreignKeyName, foreignKey, null) { }
        
        /// <summary>
        /// Constructs a ForeignKeyNotMappedException with an inner exception
        /// </summary>
        /// <param name="type">The type of the entity</param>
        /// <param name="id">The primary key of the row</param>
        /// <param name="foreignKeyName">The foreign key field name</param>
        /// <param name="foreignKey">The foreign key that violates the contraint</param>
        /// <param name="inner">The inner exception</param>
        public ForeignKeyNotMappedException(Type type, long id, string foreignKeyName, long foreignKey, Exception inner) : base(String.Format("Can't operate on {0} ID: {1} because foreign key {2} {3} is not found.", type.Name, id, foreignKeyName, foreignKey), inner) { }
        
        /// <summary>
        /// Constructs a ForeignKeyNotMappedException with serialized info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected ForeignKeyNotMappedException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
