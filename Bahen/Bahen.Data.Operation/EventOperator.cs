﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahen.Data.Operation
{
    /// <summary>
    /// General data operator in charge of event data in both sql and no-sql databases
    /// </summary>
    public class EventOperator : FullEntityOperator<EventEntity, SqlEventOperator, SqlEventEntity, TableEventOperator, TableEventEntity>
    {
        /// <summary>
        /// Construct an event operator with sql connection string and table connection string
        /// </summary>
        /// <param name="sqlConnectionString">Connection string for sql database</param>
        /// <param name="tableConnectionString">Connection string for azure storage</param>
        public EventOperator(string sqlConnectionString, string tableConnectionString) : base(sqlConnectionString, tableConnectionString) { }

        /// <summary>
        /// Insert an event into data storage.  Register the publisher in attendance table afterwards.
        /// </summary>
        /// <param name="data">Event entity to insert</param>
        /// <param name="saveChanges">Whether save changes</param>
        public override void Insert(Bahen.Data.Models.EventEntity data, bool saveChanges = true)
        {
            if (String.IsNullOrEmpty(data.Alias))
            {
                data.Alias = Guid.NewGuid().ToString("N");
            }
            base.Insert(data, saveChanges);
            this.AddAttendance(data.EventID, data.PublisherID, AttendanceStatus.Publisher);
        }

        /// <summary>
        /// Get an event entity based on event ID
        /// </summary>
        /// <param name="ID">Event ID</param>
        /// <returns>The found event entity, null if not found</returns>
        /// <exception cref="BackendUnknownException">Multiple publishers</exception>
        /// <exception cref="SqlTableNotSyncedException">Found in sql, not found in table</exception>
        public override EventEntity Get(long ID)
        {
            long eventID = ID;
            long userID;
            IQueryable<long> results;

            // Need to ask attendance operator to get the publisher ID of this event
            // Since table uses publisher ID as partition key
            SqlAttendanceOperator attendacenOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            results = attendacenOperator.GetUserIDsByEvent(eventID, (int)AttendanceStatus.Publisher);

            // Check if we found any attendance match
            if (results.Count() > 1)
            {
                throw new BackendUnknownException(String.Format("Found more than one publishers/owners for single event ID: {0}", eventID));
            }
            else if (results.Count() == 0)
            {
                return null;
            }
            else
            {
                userID = results.FirstOrDefault();
            }

            SqlEventEntity sqlEntity = this.SqlOperator.Get(eventID);

            if (sqlEntity == null)
            {
                return null;
            }

            TableEventEntity tableEntity = this.TableOperator.Get(userID, eventID);

            if (tableEntity == null)
            {
                throw new SqlTableNotSyncedException(String.Format("{0} ID:{1} is found in the sql database but not found in table storage", typeof(SqlUserEntity).Name, ID));
            }

            return new EventEntity(sqlEntity, tableEntity);
        }

        /// <summary>
        /// Get a collection of user data
        /// </summary>
        /// <param name="start">Start index of the user data</param>
        /// <param name="number">Size of user data (does not guarantee to retrieve as many)</param>
        /// <returns>A collection of user data</returns>
        public override IEnumerable<EventEntity> GetItems(int start, int number)
        {
            if (number > 1000)
            {
                throw new NotImplementedException("Cannot retrieve table data for more than 1000 rows at one transaction.");
            }
            else
            {
                List<SqlEventEntity> sqlEntities = this.SqlOperator.Set.OrderBy(x => x.ID).Skip(start).Take(number).ToList();
                if (sqlEntities.FirstOrDefault() != null)
                {
                    long partID = sqlEntities.First().PublisherID;
                    long firstID = sqlEntities.First().ID;
                    long lastID = sqlEntities.Last(x => x.PublisherID == partID).ID;

                    // Does not guarantee to retrieve number of data required (Retrieve everything in the lower partition key first)
                    string partitionKey = TableEventEntity.ComputePartitionKey(partID);

                    List<TableEventEntity> tableEntities = this.TableOperator.Context
                        .CreateQuery<TableEventEntity>(this.TableOperator.TableName)
                        .Where(x => x.PartitionKey == partitionKey
                            && x.RowKey.CompareTo(TableEventEntity.ComputeRowKey(firstID)) >= 0
                            && x.RowKey.CompareTo(TableEventEntity.ComputeRowKey(lastID)) <= 0)
                            .Take(number).ToList();

                    return tableEntities.Join(
                        sqlEntities,
                        (x => x.ID),
                        (x => x.ID),
                        ((x, y) => new EventEntity(y, x)));
                }
                else
                {
                    return new List<EventEntity>();
                }
            }
        }

        /// <summary>
        /// Get the attendance status of a user to an event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="userID">User ID</param>
        /// <returns>The attendance status of the user</returns>
        public AttendanceStatus GetAttendance(long eventID, long userID)
        {
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            var result = attendanceOperator.QueryByBoth(userID, eventID);
            if (result.FirstOrDefault() != null)
            {
                return (AttendanceStatus)result.First().AttendanceStatus;
            }
            else
            {
                return AttendanceStatus.None;
            }
        }

        /// <summary>
        /// Register a user for an event
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="userID">UserID</param>
        /// <param name="status">Attendance status of the registration</param>
        public void AddAttendance(long eventID, long userID, AttendanceStatus status)
        {
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            attendanceOperator.Insert(new SqlAttendanceRelation()
            {
                AttendanceStatus = (int)status,
                EventID = eventID,
                UserID = userID
            });
        }

        /// <summary>
        /// Update the attedance of a user for an event.  Insert the attendance if not already exists
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="userID">User ID</param>
        /// <param name="status">Attendance status</param>
        public void SetAttendance(long eventID, long userID, AttendanceStatus status)
        {
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            var attendance = attendanceOperator.QueryByBoth(userID, eventID).FirstOrDefault();
            if (attendance != null)
            {
                attendance.AttendanceStatus = (int)status;
                attendanceOperator.Update(attendance);
            }
            else
            {
                attendanceOperator.Insert(new SqlAttendanceRelation()
                {
                    AttendanceStatus = (int)status,
                    EventID = eventID,
                    UserID = userID
                });
            }
        }

        /// <summary>
        /// Remove attendance record from database
        /// </summary>
        /// <param name="eventID">ID of the event</param>
        /// <param name="userID">ID of the attendant</param>
        public void RemoveAttendance(long eventID, long userID)
        {
            SqlAttendanceOperator attendanceOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            var attendance = attendanceOperator.QueryByBoth(userID, eventID).FirstOrDefault();
            if (attendance != null)
            {
                attendanceOperator.Remove(attendance);
            }
            else
            {
                throw new EntityNotFoundException(
                    String.Format(
                    "Cannot found corresponding attendance entity to update with userID:{0} and eventID:{1}",
                    userID, eventID));
            }
        }

        /// <summary>
        /// Add an event to a cycle
        /// </summary>
        /// <param name="eventID">Event ID</param>
        /// <param name="cycleID">Cycle ID</param>
        public void AddCycle(long eventID, long cycleID)
        {
            new SqlCycleEventOperator(this.SqlOperator.Context).Insert(
                new SqlCycleEventRelation()
                {
                    CycleID = cycleID,
                    EventID = eventID
                });
        }

        /// <summary>
        /// Get a collection of events by same publisher.
        /// Note: this method will join the table results,
        /// Although events are stored in the same partition key, but still cost more time.
        /// Use GetEventsByUser method if no table data is involved.
        /// </summary>
        /// <param name="publisherID">Publisher User ID</param>
        /// <returns>A collection of events by the publisher ID</returns>
        public IQueryable<EventEntity> GetEventsByPublisher(long publisherID)
        {
            IQueryable<SqlEventEntity> sqlResults;
            IQueryable<TableEventEntity> tableResults;

            // Ask the attendance operator for publisher status
            // Join the results with the event operator based on the event ID
            SqlAttendanceOperator attendacenOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            sqlResults = attendacenOperator
                .QueryByUser(publisherID,
                (x => x.AttendanceStatus == (int)AttendanceStatus.Publisher))
                .Join(this.SqlOperator.Set,
                (x => x.EventID),
                (x => x.ID),
                ((x, y) => y)
                );

            // Join sqlResults and tableResults to generate full entity
            tableResults = this.TableOperator.GetEventsByPublisher(publisherID);

            return sqlResults.Join(tableResults,
                (x => x.ID),
                (x => x.EventID),
                ((x, y) => new EventEntity(x, y)));
        }

        /// <summary>
        /// Get all the events that a specific user attends
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns>All the events that attendant go to</returns>
        public IQueryable<SqlEventEntity> GetEventsByAttendant(long userID)
        {
            // Ask the attendance operator for publisher status
            // Join the results with the event operator based on the event ID
            SqlAttendanceOperator attendacenOperator = new SqlAttendanceOperator(this.SqlOperator.Context);
            return attendacenOperator
                .QueryByUser(userID)
                .Where(x => x.AttendanceStatus != (int)AttendanceStatus.Declined)
                .Join(this.SqlOperator.Set,
                (x => x.EventID),
                (x => x.ID),
                ((x, y) => y)
                );
        }

        /// <summary>
        /// Get all the events belong to a specific cycle
        /// </summary>
        /// <param name="cycleID">Cycle ID</param>
        /// <returns>All the event belongs to the cycle</returns>
        public IQueryable<SqlEventEntity> GetEventsByCycle(long cycleID)
        {
            //Ask the cycle event operator
            SqlCycleEventOperator cycleEventOperator = new SqlCycleEventOperator(this.SqlOperator.Context);
            return cycleEventOperator
                .QueryByCycle(cycleID)
                .Join(this.SqlOperator.Set,
                (x => x.EventID),
                (x => x.ID),
                ((x, y) => y));
        }
    }
}
