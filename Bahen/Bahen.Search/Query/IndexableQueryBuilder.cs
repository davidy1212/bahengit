﻿using Bahen.Search.Correctors;
using Bahen.Search.Expressions;
using Bahen.Search.Indexing;
using Bahen.Search.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Query
{
    /// <summary>
    /// Query Builder for IEntity/IIndexable types
    /// </summary>
    public class IndexableQueryBuilder : BasicQueryBuilder
    {
        /// <summary>
        /// Construct a new IndexableQueryBuilder with type string
        /// </summary>
        /// <param name="type"></param>
        public IndexableQueryBuilder(string type)
            : base()
        {
            this.Type = type;
        }

        /// <summary>
        /// Type string
        /// </summary>
        protected string Type { get; set; }

        /// <summary>
        /// Build a string query into IExpression format
        /// </summary>
        /// <param name="query">String query</param>
        /// <param name="corrector">Term corrector</param>
        /// <returns>IExpression format of the query</returns>
        public new IExpression Build(string query, ITermCorrector corrector)
        {
            return base.Build(this.Type, null, query, corrector);
        }

        /// <summary>
        /// Build a string query into IExpression format
        /// </summary>
        /// <param name="prefix">Prefix of the query</param>
        /// <param name="query">String query</param>
        /// <param name="corrector">Term corrector</param>
        /// <returns>IExpression format of the query</returns>
        public IExpression Build(string prefix, string query, ITermCorrector corrector)
        {
            return base.Build(this.Type, prefix, query, corrector);
        }
    }
}
