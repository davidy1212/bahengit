﻿using Bahen.Search.Correctors;
using Bahen.Search.Expressions;
using Bahen.Search.Indexing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Query
{
    /// <summary>
    /// A basic implementation of a query builder interface
    /// </summary>
    public class BasicQueryBuilder : IQueryBuilder
    {
        /// <summary>
        /// Build a string query into IExpression format
        /// </summary>
        /// <param name="query">String query</param>
        /// <param name="corrector">Term corrector</param>
        /// <returns>IExpression of the query</returns>
        public virtual IExpression Build(string query, ITermCorrector corrector)
        {
            if (String.IsNullOrEmpty(query)) return new TermExpression(String.Empty);

            query = query.ToLowerInvariant();
            string[] pieces = query.Split(' ', '\t', '\n', '\r');

            IExpression result = null;

            foreach (string piece in pieces)
            {
                IExpression expr = null;

                // Scan the possibilities of AndNot and Or
                if (piece.Length > 1)
                {
                    if (piece.Contains('|'))
                    {
                        string[] orPieces = piece.Split('|');
                        foreach (string orPiece in orPieces)
                        {
                            expr = LogicOrExpression.Join(expr, this.Build(orPiece, corrector));
                        }
                        result = LogicAndExpression.Join(result, expr);
                        continue;
                    }
                    else if (piece[0] == '-')
                    {
                        expr = this.BuildTerm(piece.Substring(1), corrector);
                        result = LogicAndNotExpression.Join(result, expr);
                        continue;
                    }
                }

                // By default, use And to connect
                expr = this.BuildTerm(piece, corrector);
                result = LogicAndExpression.Join(result, expr);
            }
            return result;
        }

        /// <summary>
        /// Build a string query into IExpression format
        /// </summary>
        /// <param name="type">Type of the target result ID</param>
        /// <param name="prefix">Prefix of the query</param>
        /// <param name="query">String query</param>
        /// <param name="corrector">Term corrector</param>
        /// <returns>IExpression of the query</returns>
        protected IExpression Build(string type, string prefix, string query, ITermCorrector corrector)
        {
            IExpression typeExpression = null;
            IExpression prefixExpression = null;
            string[] pieces = query.Split(' ', '\t', '\n', '\r');
            foreach (string piece in pieces)
            {
                if (!piece.Contains('/'))
                {
                    if (piece.Length >= 2)
                    {
                        typeExpression = LogicAndExpression.Join(typeExpression, this.Build(new IndexString(type, null, piece).ToString(), corrector));
                        prefixExpression = LogicOrExpression.Join(prefixExpression, this.Build(new IndexString(type, prefix, piece).ToString(), corrector));
                    }
                }
                else
                {
                    if (piece.Length >= 2)
                    {
                        IndexString indexString = new IndexString(piece);
                        indexString.Type = type;
                        typeExpression = LogicAndExpression.Join(typeExpression, this.Build(indexString.ToString(), corrector));
                    }
                }
            }
            if (prefixExpression != null)
            {
                return LogicAndExpression.Join(typeExpression, prefixExpression);
            }
            else
            {
                return new TermExpression(String.Empty);
            }
        }

        /// <summary>
        /// Build a single term into IExpression
        /// </summary>
        /// <param name="term">String term</param>
        /// <param name="corrector">Term corrector</param>
        /// <returns>IExpression format of the term</returns>
        protected IExpression BuildTerm(string term, ITermCorrector corrector)
        {
            IExpression result = new TermExpression(term);
            if (corrector != null)
            {
                foreach (string correction in corrector.Correct(term))
                {
                    if (correction != term)
                    {
                        IExpression expr = new TermExpression(correction);
                        result = LogicOrExpression.Join(result, expr);
                    }
                }
            }
            return result;
        }
    }
}
