﻿using Bahen.Search.Correctors;
using Bahen.Search.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Query
{
    /// <summary>
    /// Build a string query into IExpression format
    /// </summary>
    interface IQueryBuilder
    {
        IExpression Build(string query, ITermCorrector corrector);
    }
}
