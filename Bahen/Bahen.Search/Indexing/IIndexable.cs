﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Indexing
{
    public interface IIndexable
    {
        string Type { get; }
        long ID { get; }
        IEnumerable<IndexString> IndexStrings { get; }
    }
}
