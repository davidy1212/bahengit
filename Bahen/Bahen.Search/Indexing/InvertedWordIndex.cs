﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bahen.Search.Indexing
{
    /// <summary>
    /// Memory data representation of a piece of search index
    /// </summary>
    [Serializable]
    public class InvertedWordIndex : IIdProvider, ISerializable
    {
        /// <summary>
        /// Construct a new inverted word index
        /// </summary>
        public InvertedWordIndex()
        {
            this.IndexEntries = new SortedDictionary<string, HashSet<long>>();
        }

        private SortedDictionary<string, HashSet<long>> IndexEntries { get; set; }

        public DateTime LastSaveTime { get; set; }

        /// <summary>
        /// All keys
        /// </summary>
        public string[] Keys
        {
            get
            {
                return IndexEntries.Keys.ToArray();
            }
        }

        /// <summary>
        /// Get matched IDs of the key
        /// </summary>
        /// <param name="key">Key of the object</param>
        /// <returns></returns>
        public HashSet<long> this[string key]
        {
            get
            {
                return this.Get(key);
            }
            set
            {
                this.Set(key, value);
            }
        }

        /// <summary>
        /// Get matched IDs of the key
        /// </summary>
        /// <param name="key">Key of the object</param>
        /// <returns></returns>
        public HashSet<long> Get(string key)
        {
            HashSet<long> results;
            if (IndexEntries.TryGetValue(key, out results))
            {
                return results;
            }
            else
            {
                return (results = new HashSet<long>());
            }
        }

        /// <summary>
        /// Add an index string to match with an ID
        /// </summary>
        /// <param name="indexString">String to index</param>
        /// <param name="ID">ID to match with the string</param>
        public void Add(string indexString, long ID)
        {
            HashSet<long> collection = new HashSet<long>();

            // Index only root
            if (!IndexEntries.TryGetValue(indexString, out collection))
            {
                collection = new HashSet<long>();
                IndexEntries.Add(indexString, collection);
            }
            collection.Add(ID);
        }

        /// <summary>
        /// Set an index string to match with a collection of IDs
        /// </summary>
        /// <param name="indexString">String to index</param>
        /// <param name="collection">Collection of IDs to match with the string</param>
        public void Set(string indexString, HashSet<long> collection)
        {
            if (this.IndexEntries.ContainsKey(indexString))
            {
                this.IndexEntries.Remove(indexString);
            }
            this.IndexEntries.Add(indexString, collection);
        }

        /// <summary>
        /// Remove the ID from the matching index string
        /// </summary>
        /// <param name="indexString">String to index</param>
        /// <param name="ID">ID previously matching the string</param>
        public void Remove(string indexString, long ID)
        {
            HashSet<long> collection = new HashSet<long>();

            if (IndexEntries.TryGetValue(indexString, out collection))
            {
                if (collection.Contains(ID))
                {
                    collection.Remove(ID);
                }
            }
        }

        #region IIdProvider implementation
        /// <summary>
        /// Get the value of a given key
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        /// <returns>Whether the key exists</returns>
        public bool TryGetValue(string key, out ICollection<long> value)
        {
            HashSet<long> tryValue;
            bool result = this.IndexEntries.TryGetValue(key, out tryValue);
            value = tryValue;
            return result;
        }
        #endregion

        // Implement this method to serialize data. The method is called  
        // on serialization. 
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // Use the AddValue method to specify serialized values.
            info.AddValue("IndexEntries", this.IndexEntries, typeof(SortedDictionary<string, HashSet<long>>));

        }

        // The special constructor is used to deserialize values. 
        public InvertedWordIndex(SerializationInfo info, StreamingContext context)
        {
            // Reset the property value using the GetValue method.
            this.IndexEntries = (SortedDictionary<string, HashSet<long>>)info.GetValue("IndexEntries", typeof(SortedDictionary<string, HashSet<long>>));
        }
    }
}
