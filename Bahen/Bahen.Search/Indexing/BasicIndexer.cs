﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Indexing
{
    /// <summary>
    /// A basic indexer inplementing the IIndexer interface
    /// </summary>
    public class BasicIndexer : IIndexer
    {
        /// <summary>
        /// Get a collection of indexable keywords of the indexable item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IEnumerable<string> Index(IIndexable item)
        {
            List<string> result = new List<string>();
            IndexString temp;
            foreach (IndexString indexString in item.IndexStrings)
            {
                foreach (IndexString part in indexString.Split())
                {
                    temp = part;
                    temp.Type = item.Type;
                    result.Add(temp.ToString());
                    result.Add(temp.ToString("N"));
                }
            }
            return result;
        }

        /// <summary>
        /// Insert an indexable item into the memory index
        /// </summary>
        /// <param name="index">memory index reference</param>
        /// <param name="item"></param>
        public virtual void Insert(InvertedWordIndex index, IIndexable item)
        {
            foreach (string keyword in this.Index(item))
            {
                index.Add(keyword, item.ID);
            }
        }

        /// <summary>
        /// Delete an indexable item from the memory index
        /// </summary>
        /// <param name="index">Memory index reference</param>
        /// <param name="item">Item to be removed</param>
        public virtual void Remove(InvertedWordIndex index, IIndexable item)
        {
            foreach (string keyword in this.Index(item))
            {
                index.Remove(keyword, item.ID);
            }
        }
    }
}
