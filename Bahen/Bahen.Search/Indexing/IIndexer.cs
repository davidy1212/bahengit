﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Search.Indexing
{
    /// <summary>
    /// Interface for a indexer managing the memory index
    /// </summary>
    public interface IIndexer
    {
        /// <summary>
        /// Get a collection of indexable keywords of the indexable item
        /// </summary>
        /// <param name="item">Indexable item</param>
        /// <returns>A collection of keywords</returns>
        IEnumerable<string> Index(IIndexable item);

        /// <summary>
        /// Insert an indexable item into the memory index
        /// </summary>
        /// <param name="index">Memory index reference</param>
        /// <param name="item"Item to be inserted></param>
        void Insert(InvertedWordIndex index, IIndexable item);

        /// <summary>
        /// Delete an indexable item from the memory index
        /// </summary>
        /// <param name="index">Memory index reference</param>
        /// <param name="item">Item to be removed</param>
        void Remove(InvertedWordIndex index, IIndexable item);
    }
}
