﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Search.Indexing
{
    /// <summary>
    /// A structure that can hold a prefix string and a root string.
    /// Prefixes mainly for defining the type of root data, for more accurate search results
    /// </summary>
    public struct IndexString
    {
        public string Type { get; set; }
        public string Prefix { get; set; }
        public string Root { get; set; }
        public bool IsSplittable { get; set; }

        public IndexString(string key)
            : this()
        {
            string prefix;
            string root;
            StringSplitter.SeparatePrefix(key, out prefix, out root);
            this.Prefix = prefix;
            this.Root = root;
        }

        public IndexString(string prefix, string root)
            : this(null, prefix, root, true)
        {
        }

        public IndexString(string prefix, string root, bool splittable)
            : this(null, prefix, root, splittable)
        {
        }

        public IndexString(string type, string prefix, string root)
            : this(type, prefix, root, true)
        {
        }

        public IndexString(string type, string prefix, string root, bool splittable)
            : this()
        {
            this.Type = type;
            this.Prefix = prefix;
            this.Root = root;
            this.IsSplittable = splittable;
        }

        public override string ToString()
        {
            if (this.Prefix != null && this.Type != null)
            {
                return this.ToString("F");
            }
            else if (this.Type != null)
            {
                return this.ToString("N");
            }
            else
            {
                return this.ToString("R");
            }
        }

        public string ToString(string format)
        {
            switch (format)
            {
                case ("F"):
                    return String.Format("{0}/{1}/{2}", Type, Prefix, Root);
                case ("R"):
                    return this.Root;
                case ("N"):
                    return String.Format("{0}/{1}", Type, Root);
                default:
                    return this.ToString();
            }
        }

        /// <summary>
        /// Parse the root string and form the concatenation of prefix and the parsed parts of the root for inverted word index
        /// </summary>
        /// <returns>A list of part of the root with the same prefix</returns>
        public IEnumerable<IndexString> Split()
        {
            List<IndexString> results = new List<IndexString>();
            if (!this.IsSplittable)
            {
                results.Add(this);
            }
            foreach (string part in StringSplitter.Split(this.Root))
            {
                results.Add(new IndexString(this.Type, this.Prefix, part));
            }
            return results;
        }
    }
}
