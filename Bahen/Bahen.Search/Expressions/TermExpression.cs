﻿using Bahen.Search.Indexing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Search.Expressions
{
    public class TermExpression : IExpression
    {
        private string Term { get; set; }

        public IEnumerable<string> Terms
        {
            get
            {
                return new string[] { Term };
            }
        }

        public TermExpression(string term)
        {
            Term = term;
        }

        public HashSet<long> Evaluate(IIdProvider provider)
        {
            HashSet<long> results = new HashSet<long>();
            ICollection<long> tryResults;
            if (provider.TryGetValue(this.Term, out tryResults))
            {
                results.UnionWith(tryResults);
            }
            return results;
        }

        public override string ToString()
        {
            return this.Term;
        }
    }
}
