﻿using Bahen.Web.Api.Controllers.MessageHandlers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Bahen.Web.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Setup message handlers
            config.MessageHandlers.Add(new OAuthMessageHandler());

            // Setup camel-casing for all webapi json responses 
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter()
            {
                CamelCaseText = true
            });

            // Disable xml-serializer for now. Pretty retarded.
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Setup ignoring null value properties
            jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

            config.Routes.MapHttpRoute(
                name: "NoActionApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { },
                constraints: new { id = @"(\d+)" }
            );
            config.Routes.MapHttpRoute(
                name: "NoIDApi",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { },
                constraints: new { id = @"(\d+)" }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}/{action}/{uid}",
                defaults: new
                {
                    id = RouteParameter.Optional,
                    action = RouteParameter.Optional,
                    uid = RouteParameter.Optional
                },
                constraints: new { id = @"(\d+)" }
            );

            config.Routes.MapHttpRoute(
                name: "NoActionApi2",
                routeTemplate: "{controller}/{id}",
                defaults: new { },
                constraints: new
                {
                    subdomain = new SubdomainRouteConstraint("api"),
                    id = @"(\d+)"
                }
            );
            config.Routes.MapHttpRoute(
                name: "NoIDApi2",
                routeTemplate: "{controller}/{action}",
                defaults: new { },
                constraints: new
                {
                    subdomain = new SubdomainRouteConstraint("api"),
                    id = @"(\d+)|([0-9abcdef]+)"
                }
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi2",
                routeTemplate: "{controller}/{id}/{action}/{uid}",
                defaults: new
                {
                    action = RouteParameter.Optional,
                    uid = RouteParameter.Optional
                },
                constraints: new
                {
                    subdomain = new SubdomainRouteConstraint("api"),
                    id = @"(\d+)|([0-9abcdef]+)"
                }
            );
        }
    }
}
