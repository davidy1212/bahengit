﻿using Bahen.Web.Api.Controllers.Utilities;
using System.Web;
using System.Web.Mvc;

namespace Bahen.Web.Api
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute()); 
        }
    }
}