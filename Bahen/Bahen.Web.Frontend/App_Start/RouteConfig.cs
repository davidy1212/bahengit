﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Bahen.Web.Api
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            //routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            //routes.MapPageRoute("Authorize",
            //    "api/oauth/authorize",
            //    "~/forms/authorize.aspx",
            //    false,
            //    new RouteValueDictionary(),
            //    new RouteValueDictionary());
            routes.MapPageRoute("Eventub", 
                "", 
                "~/app/common/main.aspx", 
                false, 
                new RouteValueDictionary(), 
                new RouteValueDictionary());
            routes.MapPageRoute("welcome",
               "welcome",
               "~/app/forms/welcome.aspx",
               false,
               new RouteValueDictionary(),
               new RouteValueDictionary());
            routes.MapPageRoute("ApiLogin",
                "api/login",
                "~/forms/oauth/apilogin.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary());
            routes.MapPageRoute("RegisterAPI",
                "api/create_app",
                "~/forms/oauth/register.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary());
            routes.MapPageRoute("Grant",
                "api/oauth/grant",
                "~/forms/oauth/grant.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary());
            routes.MapPageRoute("Login",
                "login",
                "~/forms/account/login.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary());

            routes.MapPageRoute("Register",
                "register",
                "~/forms/account/register.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary());
            
        }
    }
}