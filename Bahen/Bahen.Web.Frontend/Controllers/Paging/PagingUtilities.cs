﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.ResponseModels;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Paging
{
    /// <summary>
    /// Utility class for paging data output
    /// </summary>
    public static class PagingUtilities
    {
        /// <summary>
        /// Cursor/Offset based paging on IEntity (long ID)
        /// </summary>
        /// <typeparam name="T">IEntity Type</typeparam>
        /// <param name="rawData">Raw data to be truncated</param>
        /// <param name="options">Paging options (only cursor/offset based paging supported, for time based, check TruncateByTime)"/>)</param>
        /// <returns>Truncated data</returns>
        public static IQueryable<T> TruncateByInt64Id<T>(IQueryable<T> rawData, PagingOptions options) where T : IInt64Id
        {
            if (options.Mode == PagingMode.CursorAfterInt64)
            {
                IQueryable<T> x1 = rawData
                    .Where(x => (x.ID > options.AfterInt64));
                IQueryable<T> x2 = rawData
                    .Where(x => (x.ID > options.AfterInt64))
                    .Take(options.Limit);
                return rawData
                    .Where(x => (x.ID > options.AfterInt64))
                    .Take(options.Limit);
            }
            else if (options.Mode == PagingMode.CursorBeforeInt64)
            {
                IQueryable<T> x1 = rawData
                    .Where(x => (x.ID < options.BeforeInt64))
                    .Reverse();
                IQueryable<T> x2 = rawData
                    .Where(x => (x.ID < options.BeforeInt64))
                    .Reverse()
                    .Take(options.Limit)
                    .Reverse();
                return rawData
                    .Where(x => (x.ID < options.BeforeInt64))
                    .Reverse()
                    .Take(options.Limit)
                    .Reverse();
            }
            else if (options.Mode == PagingMode.Offset)
            {
                return rawData
                    .Skip(options.Offset)
                    .Take(options.Limit);
            }
            return new List<T>().AsQueryable();
        }

        /// <summary>
        /// Trancate by a String ID, used for UserOpinionEntity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rawData"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IQueryable<T> TruncateByStringId<T>(IQueryable<T> rawData, PagingOptions options) where T : TableServiceEntity
        {
            if (options.Mode == PagingMode.CursorAfterString)
            {
                return rawData
                    .Where(x => x.RowKey.CompareTo(options.AfterString) > 0)
                    .Take(options.Limit);
            }
            else if (options.Mode == PagingMode.CursorBeforeString)
            {
                return rawData
                    .Where(x => x.RowKey.CompareTo(options.BeforeString) < 0)
                    .Reverse()
                    .Take(options.Limit)
                    .Reverse();
            }
            return new List<T>().AsQueryable();
        }

        ///// <summary>
        ///// Generate continuous paging links (next/previous pages)
        ///// </summary>
        ///// <typeparam name="TI">Raw data element type</typeparam>
        ///// <typeparam name="TO">Output data element type</typeparam>
        ///// <param name="rawData">Raw data</param>
        ///// <param name="procData">Output data</param>
        ///// <param name="options">Options to generate paging links</param>
        ///// <returns>Paging links</returns>
        //public static PagingLinks GetPagingLinksIInt64Id<TI, TO>(
        //    IQueryable<TI> rawData,
        //    IEnumerable<TO> procData,
        //    PagingLinks options)
        //    where TI : IInt64Id
        //    where TO : IInt64Id
        //{
        //    PagingLinks paging = null;
        //    if (procData.FirstOrDefault() != null)
        //    {
        //        bool hasBefore, hasAfter;

        //        hasBefore = rawData.Where(x => x.ID < procData.First().ID).FirstOrDefault() != null;
        //        if (hasBefore)
        //        {
        //            if (paging == null)
        //            {
        //                paging = new PagingLinks();
        //            }
        //            paging.Previous = String.Format("{0}?before={1}{2}",
        //                options.RootUrl,
        //                HttpServerUtility.UrlTokenEncode(procData.First().ID.GetBytes()),
        //                options.AppendingParameters);
        //        }

        //        hasAfter = rawData.Where(x => x.ID > procData.Last().ID).FirstOrDefault() != null;
        //        if (hasAfter)
        //        {
        //            if (paging == null)
        //            {
        //                paging = new PagingLinks();
        //            }
        //            paging.Next = String.Format("{0}?after={1}{2}",
        //                options.RootUrl,
        //                HttpServerUtility.UrlTokenEncode(procData.Last().ID.GetBytes()),
        //                options.AppendingParameters);
        //        }
        //    }
        //    return paging;
        //}

        ///// <summary>
        ///// Generate continuous paging links for IStringId
        ///// </summary>
        ///// <typeparam name="TI">Raw data element type</typeparam>
        ///// <typeparam name="TO">Output data element type</typeparam>
        ///// <param name="rawData">Raw data</param>
        ///// <param name="procData">Output data</param>
        ///// <param name="options">Options to generate paging links</param>
        ///// <returns></returns>
        //public static PagingLinks GetPagingLinksIStringId<TI, TO>(
        //    IQueryable<TI> rawData,
        //    IEnumerable<TO> procData,
        //    PagingLinks options)
        //    where TI : TableServiceEntity, IStringId
        //    where TO : IStringId
        //{
        //    PagingLinks paging = null;
        //    if (procData.FirstOrDefault() != null)
        //    {
        //        bool hasBefore, hasAfter;

        //        hasBefore = rawData.Where(x => x.RowKey.CompareTo(procData.First().StringId) < 0).Take(1).FirstOrDefault() != null;
        //        if (hasBefore)
        //        {
        //            if (paging == null)
        //            {
        //                paging = new PagingLinks();
        //            }
        //            paging.Previous = String.Format("{0}?{1}={2}{3}",
        //                options.RootUrl,
        //                options.PreviousPageParameter,
        //                procData.First().StringId,
        //                options.AppendingParameters);
        //        }

        //        hasAfter = rawData.Where(x => x.RowKey.CompareTo(procData.Last().StringId) > 0).Take(1).FirstOrDefault() != null;
        //        if (hasAfter)
        //        {
        //            if (paging == null)
        //            {
        //                paging = new PagingLinks();
        //            }
        //            paging.Next = String.Format("{0}?{1}={2}{3}",
        //                options.RootUrl,
        //                options.NextPageParameter,
        //                procData.Last().StringId,
        //                options.AppendingParameters);
        //        }
        //    }
        //    return paging;
        //}
        //}

    }

}