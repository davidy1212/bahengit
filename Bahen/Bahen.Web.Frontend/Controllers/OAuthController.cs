﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using Bahen.Web.Api.Controllers.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace Bahen.Web.Api.Controllers
{
    public class OAuthController : ApiController
    {
        [HttpPost, RequireHttps, Authorize, ActionName("Register")]
        public OAuthRegisterResponse RegisterClient(
            [FromBody] OAuthRegisterRequest request)
        {
            SqlApiClientEntity entity = new SqlApiClientEntity()
            {
                Name = request.Name,
                ClientSecret = Guid.NewGuid(),
                DeveloperID = HttpContext.Current.User.GetID(),
                TrustLevel = (int)ApiClientTrustLevel.Test
            };
            SqlApiClientOperator op = new SqlApiClientOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            op.Insert(entity);

            OAuthRegisterResponse response = new OAuthRegisterResponse()
            {
                Data = new OAuthClient()
                {
                    Id = entity.ID,
                    Secret = entity.ClientSecret.ToString("N"),
                    Name = entity.Name,
                    TrustLevel = ((ApiClientTrustLevel)entity.TrustLevel).ToString().ToLowerInvariant()
                }
            };

            return response;
        }

        [HttpGet]
        public OAuthClient GetClientInfo(
            long id)
        {
            SqlApiClientEntity entity;
            SqlApiClientOperator op = new SqlApiClientOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            entity = op.Get(id);
            OAuthClient result = new OAuthClient()
            {
                Id = entity.ID,
                Name = entity.Name,
                Picture = entity.PictureUrl
            };
            return result;
        }

        [HttpGet, RequireHttps, Authorize]
        public OAuthClient GetClientInfo(
            long id, 
            bool secret)
        {
            if (secret == false)
            {
                return this.GetClientInfo(id);
            }
            else
            {
                SqlApiClientEntity entity;
                SqlApiClientOperator op = new SqlApiClientOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                entity = op.Get(id);
                if (entity.DeveloperID == HttpContext.Current.User.GetID())
                {
                    OAuthClient result = new OAuthClient()
                    {
                        Id = entity.ID,
                        Name = entity.Name,
                        Secret = entity.ClientSecret.ToString("N"),
                        Picture = entity.PictureUrl
                    };
                    return result;
                }
                else
                {
                    throw new AuthenticationException("You are not authorized to request the resource.");
                }
            }
        }

        [HttpGet, RequireHttps, ActionName("Authorize")]
        public HttpResponseMessage AuthorizeGet(
            long client_id,
            string response_type,
            string scope,
            string redirect_uri)
        {

            OAuthAuthorizeRequest request = new OAuthAuthorizeRequest()
            {
                ClientId = client_id,
                ResponseType = response_type,
                Scope = scope,
                RedirectUri = redirect_uri
            };
            return this.AuthorizePost(request);
        }

        [HttpPost, RequireHttps, ActionName("Authorize")]
        public HttpResponseMessage AuthorizePost(
            [FromBody]OAuthAuthorizeRequest request)
        {
            // Check if logged in
            if (!Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                return this.Redirect("/api/login?redirect=" + HttpUtility.UrlEncode(Request.RequestUri.AbsoluteUri.ToString()), UriKind.Relative);
            }
            SqlApiClientOperator opClient = new SqlApiClientOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientId);
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, String.Format("No client id {0} match", request.ClientId));
            }
            OAuthScopes scopes;
            try
            {
                scopes = Bahen.Common.EnumConvert<OAuthScopes>.ToEnum(request.Scope);
                scopes = scopes & ~OAuthScopes.Owner;
            }
            catch
            {
                throw new OAuthException(OAuthError.InvalidScope, "cannot parse scope parameter");
            }
            SqlApiAuthOperator opAuth = new SqlApiAuthOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            // Chech if already authorized
            IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == HttpContext.Current.User.GetID());
            if (result.FirstOrDefault() == null)
            {
                // Redirection to confirm page
                return this.Redirect(
                    String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope), UriKind.Relative);
            }

            // Check if already requested token
            SqlApiAuthRelation auth = result.FirstOrDefault();
            if (!auth.Requested)
            {
                // Redirection to confirm page
                return this.Redirect(
                    String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope), UriKind.Relative);
            }

            // Check if scopes are same
            if (auth.Scopes != Convert.ToInt64(scopes))
            {
                auth = opAuth.Get(auth.ID);
                opAuth.Remove(auth);
                return this.Redirect(
                    String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope), UriKind.Relative);
            }

            // Issue token directly
            string atoken = OAuthTokenFactory.IssueAccessToken(
                HttpContext.Current.User.GetID(),
                request.ClientId,
                new TimeSpan(1, 0, 0),
                scopes,
                (HttpContext.Current.User as LoginPrincipal).Role);
            string rtoken = ((OAuthScopes)auth.Scopes).HasFlag(OAuthScopes.OfflineAccess) ? OAuthTokenFactory.IssueRefreshToken(auth.ID, auth.UserID, auth.ClientID) : null;

            OAuthAccessTokenResponse response = new OAuthAccessTokenResponse()
            {
                AccessToken = atoken,
                ExpiresIn = 3600,
                RefreshToken = rtoken
            };

            HttpResponseMessage message = new HttpResponseMessage();
            message.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(response, new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore
            }), Encoding.UTF8, "application/json");
            return message;
        }

        [HttpPost, RequireHttps, AntiCsrf, ActionName("GrantPost")]
        public HttpResponseMessage GrantPost(
            [FromBody] OAuthAuthorizeRequest request)
        {
            SqlApiClientOperator opClient = new SqlApiClientOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientId);
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, String.Format("No client id {0} match", request.ClientId));
            }
            if (request.Scope == null)
            {
                throw new OAuthException(OAuthError.InvalidScope, "Invalid scopes");
            }
            if (request.RedirectUri == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, "Redirect uri not specified");
            }
            OAuthScopes scopes;
            try
            {
                scopes = Bahen.Common.EnumConvert<OAuthScopes>.ToEnum(request.Scope);
                scopes = scopes & ~OAuthScopes.Owner;
            }
            catch
            {
                throw new OAuthException(OAuthError.InvalidScope, "cannot parse scope parameter");
            }
            SqlApiAuthOperator opAuth = new SqlApiAuthOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiAuthRelation auth = null;

            if (request.ResponseType == "code")
            {
                string requestToken = Guid.NewGuid().ToByteArray().ToBase62();
                IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == HttpContext.Current.User.GetID());
                if (result.FirstOrDefault() != null)
                {
                    auth = result.First();
                    auth = opAuth.Get(auth.ID);
                    auth.Requested = false;
                    opAuth.Update(auth);
                }
                else
                {
                    // Issue request token
                    auth = new SqlApiAuthRelation()
                    {
                        ClientID = request.ClientId,
                        RedirectUri = request.RedirectUri,
                        Scopes = Convert.ToInt64(scopes),
                        UserID = HttpContext.Current.User.GetID(),
                        Requested = false
                    };

                    // Store in sql
                    opAuth.Insert(auth);
                }

                // Redirect to redirect uri
                return this.Redirect(
                    String.Format("{0}?code={1}",
                    request.RedirectUri,
                    OAuthTokenFactory.IssuAuthCode(auth.ID, auth.UserID, auth.ClientID, new TimeSpan(0, 10, 0))),
                    UriKind.Absolute);
            }
            else if (request.ResponseType == "token")
            {
                scopes = scopes & ~OAuthScopes.OfflineAccess;

                // Chech if already has auth entry
                IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == HttpContext.Current.User.GetID());

                if (result.FirstOrDefault() != null)
                {
                    auth = result.First();
                    if (auth.RedirectUri != request.RedirectUri)
                    {
                        throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                    }
                    if (!auth.Requested)
                    {
                        throw new OAuthException(OAuthError.InvalidRequest, "implicit grant flow is not applicable");
                    }
                }
                else
                {
                    auth = new SqlApiAuthRelation()
                    {
                        ClientID = request.ClientId,
                        RedirectUri = request.RedirectUri,
                        Scopes = Convert.ToInt64(scopes),
                        UserID = HttpContext.Current.User.GetID(),
                        Requested = true
                    };
                    opAuth.Insert(auth);
                }

                TableUserOperator uop = new TableUserOperator(
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                string token =
                    OAuthTokenFactory.IssueAccessToken(
                    auth.UserID,
                    auth.ClientID,
                    new TimeSpan(1, 0, 0),
                    (OAuthScopes)auth.Scopes,
                    (UserRoleLevel)uop.Get(HttpContext.Current.User.GetID()).UserGroup);
                return this.Redirect(
                    String.Format("{0}?access_token={1}&expires_in={2}",
                    request.RedirectUri, token, 3600), UriKind.Absolute);
            }
            else
            {
                throw new OAuthException(OAuthError.UnsupportedResponseType, request.ResponseType);
            }
        }

        [NonAction]
        private HttpResponseMessage Redirect(
            string url, 
            UriKind kind)
        {
            HttpResponseMessage message = new HttpResponseMessage();
            message.StatusCode = HttpStatusCode.Redirect;
            message.Headers.Location = new Uri(url, kind);
            return message;
        }

        [NonAction]
        private void RedirectConfirmation(
            HttpResponseMessage message,
            long clientID,
            OAuthScopes scopes)
        {
            // Redirection to confirm page
            int number = new Random().Next(Int32.MaxValue - 1);
            HttpContext.Current.Session["formId"] = number;
            message.StatusCode = HttpStatusCode.Redirect;
            message.Headers.Location = new Uri(String.Format("https://localhost/oauth/authorize/{0}/{1}?scope=", clientID, number + 1, scopes.ToDescriptionString()));
        }

        [HttpGet, ActionName("Access_token")]
        public OAuthAccessTokenResponse GetAccessTokenGet(
            long client_id,
            string client_secret,
            string grant_type,
            string redirect_uri = null,
            string refresh_token = null,
            string code = null,
            string email = null,
            string password = null)
        {
            OAuthAccessTokenRequest request = new OAuthAccessTokenRequest()
            {
                Code = code,
                ClientID = client_id,
                ClientSecret = client_secret,
                RedirectUri = redirect_uri,
                GrantType = grant_type,
                RefreshToken = refresh_token,
                LoginEmail = email,
                LoginPassword = password
            };
            return GetAccessTokenPost(request);
        }

        [HttpPost, ActionName("Access_token")]
        public OAuthAccessTokenResponse GetAccessTokenPost(
            [FromBody] OAuthAccessTokenRequest request)
        {
            SqlApiClientOperator opClient = new SqlApiClientOperator(
                   ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiAuthOperator op = new SqlApiAuthOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientID);
            UserOperator uop = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            SqlApiAuthRelation auth;
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, "invalid client id");
            }
            if (client.ClientSecret != Guid.Parse(request.ClientSecret))
            {
                throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid client secret");
            }
            if (request.GrantType == "code")
            {
                OAuthAuthorizationCode code = null;
                if (!OAuthTokenFactory.ValidateToken<OAuthAuthorizationCode>(request.Code, out code))
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                }
                if (code.Expires < DateTime.UtcNow)
                {
                    try
                    {
                        auth = op.Get(code.AuthId);
                        op.Remove(auth);
                    }
                    catch { }
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "authorization code has expired, please let user authorize again");
                }
                try
                {
                    auth = op.Get(code.AuthId);
                }
                catch
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "user has not authorized the app");
                }
                if (auth.RedirectUri != request.RedirectUri)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                }
                if (auth.ClientID != code.ClientId)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                }
                if (auth.UserID != code.UserId)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                }
            }
            else if (request.GrantType == "refresh_token")
            {
                OAuthRefreshToken token;
                if (!OAuthTokenFactory.ValidateToken<OAuthRefreshToken>(request.RefreshToken, out token))
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                }
                try
                {
                    auth = op.Get(token.AuthId);
                }
                catch
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "user has not authorized the app");
                }
                if (auth.RedirectUri != request.RedirectUri)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                }
                if (auth.ClientID != token.ClientId)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                }
                if (auth.UserID != token.UserId)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                }
            }
            else if (request.GrantType == "password")
            {
                if (client.TrustLevel < (int)ApiClientTrustLevel.Owner)
                {
                    throw new OAuthException(OAuthError.UnsupportedResponseType, "client trust level does not support password grant type");
                }
                SqlUserOperator opUser = new SqlUserOperator(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                SqlUserEntity userLogin = null;
                if (!opUser.TryGet(request.LoginEmail, out userLogin))
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid login credential");
                }
                if (!userLogin.LoginPassword.SequenceEqual(request.LoginPassword.ToSha512()))
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid login credential");
                }
                IEnumerable<SqlApiAuthRelation> result = op.QueryByUser(userLogin.ID).Where(x => x.ClientID == request.ClientID);
                if (result.FirstOrDefault() == null)
                {
                    auth = new SqlApiAuthRelation()
                    {
                        ClientID = request.ClientID,
                        RedirectUri = null,
                        Scopes = Int64.MaxValue,
                        UserID = userLogin.ID,
                        Requested = true
                    };
                    op.Insert(auth);
                }
                else
                {
                    auth = result.First();
                }
            }
            else
            {
                throw new OAuthException(OAuthError.UnsupportedResponseType, "grant type:" + request.GrantType);
            }
            auth = op.Get(auth.ID);
            auth.Requested = true;
            op.Update(auth);
            UserEntity user = uop.Get(auth.UserID);
            TimeSpan expires = (request.RedirectUri == null ? new TimeSpan(14, 0, 0, 0) : new TimeSpan(1, 0, 0));
            return new OAuthAccessTokenResponse()
            {
                AccessToken = OAuthTokenFactory.IssueAccessToken(auth.UserID, request.ClientID, expires, (OAuthScopes)auth.Scopes, user.UserGroup),
                ExpiresIn = (int)expires.TotalSeconds,
                RefreshToken = ((OAuthScopes)auth.Scopes).HasFlag(OAuthScopes.OfflineAccess) ? OAuthTokenFactory.IssueRefreshToken(auth.ID, auth.UserID, auth.ClientID) : null
            };
        }
    }
}
