﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.RequestModels
{
    public class RegisterRequest
    {
        public string LoginEmail { get; set; }
        public string LoginPassword1 { get; set; }
        public string LoginPassword2 { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class LoginRequest
    {
        public string LoginEmail { get; set; }
        public string LoginPassword { get; set; }
        public bool? RememberMe { get; set; }
        public string RefreshToken { get; set; }
    }
}