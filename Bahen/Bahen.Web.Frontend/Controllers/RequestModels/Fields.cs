﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;

namespace Bahen.Web.Api.Controllers.RequestModels
{
    public class FieldsFilter
    {
        private HashSet<string> Set { get; set; }

        public FieldsFilter()
        {
            this.Set = new HashSet<string>();
        }

        public FieldsFilter(string filter)
            : this()
        {
            if (filter != null)
            {
                this.Set.UnionWith(filter.ToLowerInvariant().Split(','));
            }
        }

        public bool Contains(string fields)
        {
            return this.Set.Contains(fields.ToLowerInvariant());
        }
    }

    public class FieldsFilterModelBinder : IModelBinder
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            ValueProviderResult fields = bindingContext.ValueProvider.GetValue("fields");
            if (fields != null)
            {
                bindingContext.Model = new FieldsFilter(fields.AttemptedValue);
                return true;
            }
            else
            {
                if (bindingContext.ModelName == "Optional")
                {
                    bindingContext.Model = new FieldsFilter();
                    return true;
                }
                return false;
            }
        }
    }

    public class FieldsFilterModelBinderProvider : ModelBinderProvider
    {
        public override IModelBinder GetBinder(System.Web.Http.HttpConfiguration configuration, Type modelType)
        {
            return new FieldsFilterModelBinder();
        }
    }
}