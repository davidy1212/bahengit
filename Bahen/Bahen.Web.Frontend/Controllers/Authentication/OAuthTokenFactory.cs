﻿using Bahen.Common;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public static class OAuthTokenFactory
    {
        public static string IssueAccessToken(long userId, long clientId, TimeSpan expire, OAuthScopes scope, UserRoleLevel role)
        {
            OAuthAccessToken token = new OAuthAccessToken()
            {
                Expires = DateTime.UtcNow + expire,
                UserId = userId,
                ClientId = clientId,
                Scope = (long)scope,
                Role = (long)role,
                Type = "a"
            };
            return JwtFactory.Encode(JsonConvert.SerializeObject(token));
        }

        public static string IssueRefreshToken(long authId, long userId, long clientId)
        {
            OAuthRefreshToken token = new OAuthRefreshToken()
            {
                AuthId = authId,
                ClientId = clientId,
                UserId = userId,
                Type = "r"
            };
            return JwtFactory.Encode(JsonConvert.SerializeObject(token),  new MD5CryptoServiceProvider().ComputeHash(authId.GetBytes()));
        }

        public static string IssuAuthCode(long authId, long userId, long clientId, TimeSpan expire)
        {
            OAuthAuthorizationCode token = new OAuthAuthorizationCode()
            {
                AuthId = authId,
                Expires = DateTime.UtcNow + expire,
                ClientId = clientId,
                UserId = userId,
                Type = "c"
            };
            return JwtFactory.Encode(JsonConvert.SerializeObject(token));
        }

        public static bool ValidateToken<T>(string token, out T token2) where T : class
        {
            try
            {
                token2 = JsonConvert.DeserializeObject<T>(JwtFactory.Decode(token));
                return true;
            }
            catch
            {
                token2 = null;
                return false;
            }
        }
    }
}