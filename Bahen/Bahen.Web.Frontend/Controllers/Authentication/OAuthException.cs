﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Bahen.Common;

namespace Bahen.Web.Api.Controllers.Utilities
{
    [Serializable]
    public class OAuthException : Exception
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }

        //public OAuthException() { }

        public OAuthException(OAuthError error, string errorDescription)
            : base(error.ToDescriptionString() + ": " + errorDescription)
        {
            this.Error = error.ToDescriptionString();
            this.ErrorDescription = errorDescription;
        }

        //public OAuthException(string message) : base(message) { }
        //public OAuthException(string message, Exception inner) : base(message, inner) { }
        //protected OAuthException(
        //  System.Runtime.Serialization.SerializationInfo info,
        //  System.Runtime.Serialization.StreamingContext context)
        //    : base(info, context) { }
    }

    public enum OAuthError
    {
        [Description("invalid_request")]
        InvalidRequest,
        [Description("unauthorized_request")]
        UnauthorizedRequest,
        [Description("access_denied")]
        AccessDenied,
        [Description("unsupported_response_type")]
        UnsupportedResponseType,
        [Description("invalid_scope")]
        InvalidScope,
        [Description("server_error")]
        ServerError,
        [Description("temporarily_unavailable")]
        TemporarilyUnavailable
    }
}