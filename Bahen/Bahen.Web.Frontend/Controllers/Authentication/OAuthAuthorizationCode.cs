﻿using Bahen.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public class OAuthAuthorizationCode
    {
        [JsonProperty("exp"), JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Expires { get; set; }

        [JsonProperty("aid")]
        public long AuthId { get; set; }

        [JsonProperty("cid")]
        public long ClientId { get; set; }

        [JsonProperty("uid")]
        public long UserId { get; set; }

        [JsonProperty("typ")]
        public string Type { get; set; }
    }
}