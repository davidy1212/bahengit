﻿using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Utilities;
using Bahen.Web.Api.Controllers.Privacy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Bahen.Web.Api.Controllers.Filters
{
    public class ControllerExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is EntityNotFoundException)
            {
                context.Response = FormatMessage(HttpStatusCode.NotFound, context.Exception.Message);
            }
            else if (context.Exception is FormatException)
            {
                context.Response = FormatMessage(HttpStatusCode.NotAcceptable, "Invalid request.");
            }
            else if (context.Exception is AuthenticationException)
            {
                context.Response = FormatMessage(HttpStatusCode.Unauthorized, context.Exception.Message);
            }
            else if (context.Exception is ArgumentException)
            {
                context.Response = FormatMessage(HttpStatusCode.BadRequest, context.Exception.Message);
            }
            else if (context.Exception is OAuthException)
            {
                string content = "{\"error\":" + (context.Exception as OAuthException).Error + ",\"error_description\":\"" + (context.Exception as OAuthException).ErrorDescription + "\"}";
                context.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                {
                    ReasonPhrase = "An exception occurred",
                    Content = new StringContent(content)
                };
            }
            else if (context.Exception is PrivacyPolicyException)
            {
                context.Response = FormatMessage(HttpStatusCode.Unauthorized, context.Exception.Message);
            }
            else
            {
                context.Response = FormatMessage(HttpStatusCode.NotFound, context.Exception.Message);
            }
        }

        private static HttpResponseMessage FormatMessage(HttpStatusCode code, string message)
        {
            return new HttpResponseMessage(code)
            {
                ReasonPhrase = "An exception occurred",
                Content = new StringContent("{" + String.Format("\"message\":\"{0}\"", message) + "}")
            };
        }
    }
}