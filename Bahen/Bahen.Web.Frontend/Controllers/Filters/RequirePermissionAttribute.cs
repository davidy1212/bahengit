﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Bahen.Web.Api.Controllers.Filters
{
    public class RequirePermissionAttribute : AuthorizationFilterAttribute
    {
        private OAuthScopes ScopesRequired { get; set; }

        public RequirePermissionAttribute(OAuthScopes scopes)
        {
            this.ScopesRequired = scopes;
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                IPrincipal user = HttpContext.Current.User;

                if (!user.Identity.IsAuthenticated)
                {
                    throw new OAuthException(OAuthError.UnauthorizedRequest, "not authorized");
                }

                string type = user.Identity.AuthenticationType;
                string[] part = type.Split(':');
                if (part.Length != 2)
                {
                    throw new OAuthException(OAuthError.InvalidRequest, "invalid access token");
                }
                OAuthScopes scopes = (OAuthScopes)(Int64.Parse(part[1]));
                if (!scopes.HasFlag(this.ScopesRequired))
                {
                    throw new OAuthException(OAuthError.AccessDenied, "no sufficient permission");
                }
            }
            catch (OAuthException ex)
            {
                var err_response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                string content = "{\"error\":\"" + ex.Error + "\",\"error_description\":\"" + ex.ErrorDescription + "\"}";
                err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                actionContext.Response = err_response;
            }
        }
    }
}