﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;

namespace Bahen.Web.Api.Controllers.Filters
{
    public class RequireRoleAttribute : AuthorizationFilterAttribute
    {
        private UserRoleLevel RoleRequired { get; set; }

        public RequireRoleAttribute(UserRoleLevel role)
        {
            this.RoleRequired = role;
        }

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                var user = HttpContext.Current.User as LoginPrincipal;
                if (user.Role < this.RoleRequired)
                {
                    throw new InsufficientRoleException("your role is insufficient for this operation");
                }
            }
            catch(InsufficientRoleException  ex)
            {
                var err_response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                string content = "{\"error\":\"insufficient role\",\"error_description\":\"" + ex.Message + "\"}";
                err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                actionContext.Response = err_response;
            }
        }
    }
}