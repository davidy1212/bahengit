﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.DataProviders
{
    /// <summary>
    /// Provide subscriber/host data (readonly) directly to web api
    /// </summary>
    public class SubscriptionProvider
    {
        /// <summary>
        /// Construct a subscription provider with paging options (optional)
        /// </summary>
        /// <param name="options">Options to specify paging</param>
        public SubscriptionProvider(ListApiOptions options = null)
        {
            this.Options = options;
            this.DataOperator = new SqlSubscriptionOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
        }

        /// <summary>
        /// Paging options
        /// </summary>
        public ListApiOptions Options { get; set; }

        /// <summary>
        /// Sql data operator
        /// </summary>
        public SqlSubscriptionOperator DataOperator { get; set; }

        /// <summary>
        /// Get followers of a user
        /// </summary>
        /// <param name="hostId">User ID of the host</param>
        /// <returns></returns>
        public CountedListResponse<Friend> GetFollowers(long hostId)
        {
            CountedListResponse<Friend> response = new CountedListResponse<Friend>();
            response.Data = this.FinalizeQuery(this.GetFollowersRawData(hostId), x => x.SubscriberID);
            if (this.Options.Summary)
            {
                response.Summary = this.Options.Summary ? CountFollowers(hostId) : null;
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<Friend>(this.Options.RequestUri, response.Data, x => x.SubscriptionID);
            return response;
        }

        /// <summary>
        /// Get following users of a user
        /// </summary>
        /// <param name="subscriberId">User ID of the subscriber</param>
        /// <returns></returns>
        public CountedListResponse<Friend> GetFollowings(long subscriberId)
        {
            CountedListResponse<Friend> response = new CountedListResponse<Friend>();
            response.Data = this.FinalizeQuery(this.GetFollowingsRawData(subscriberId), x => x.HostID);
            if (this.Options.Summary)
            {
                response.Summary = this.Options.Summary ? CountFollowings(subscriberId) : null;
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<Friend>(this.Options.RequestUri, response.Data, x => x.SubscriptionID);
            return response;
        }

        /// <summary>
        /// Get followers data raw result
        /// </summary>
        /// <param name="hostId">User ID of the host</param>
        /// <returns></returns>
        protected IQueryable<SqlSubscriptionRelation> GetFollowersRawData(long hostId)
        {
            return this.DataOperator.Truncate(this.DataOperator.QueryByHost(hostId), this.Options);
        }

        /// <summary>
        /// Get following users data raw result
        /// </summary>
        /// <param name="subscriberId">User ID of the subscriber</param>
        /// <returns></returns>
        protected IQueryable<SqlSubscriptionRelation> GetFollowingsRawData(long subscriberId)
        {
            return this.DataOperator.Truncate(this.DataOperator.QueryBySubscriber(subscriberId), this.Options);
        }

        /// <summary>
        /// Finalize the query to project into output data models
        /// </summary>
        /// <param name="rawData">Raw query</param>
        /// <param name="keySelector">Key selector to join to users table</param>
        /// <returns></returns>
        protected IList<Friend> FinalizeQuery(IQueryable<SqlSubscriptionRelation> rawData, Func<SqlSubscriptionRelation, long> keySelector)
        {
            SqlUserOperator uOp = new SqlUserOperator(this.DataOperator.Context);
            return rawData.Join(
                   uOp.Set,
                   keySelector,
                   y => y.ID,
                   (x, y) => new Friend()
                   {
                       ID = y.ID,
                       Name = y.PreferredName,
                       SubscriptionID = x.ID
                   }).ToList().ToList();
        }

        /// <summary>
        /// Count the total number of followers.
        /// Runs another SQL query.
        /// </summary>
        /// <param name="hostId">User ID of the host</param>
        /// <returns></returns>
        protected CountSummary CountFollowers(long hostId)
        {
            return new CountSummary()
            {
                Count = this.DataOperator.QueryByHost(hostId).Count()
            };
        }

        /// <summary>
        /// Count the total number of following users.
        /// Runs another SQL query
        /// </summary>
        /// <param name="subscriberId">User ID of the subscriber</param>
        /// <returns></returns>
        protected CountSummary CountFollowings(long subscriberId)
        {
            return new CountSummary()
            {
                Count = this.DataOperator.QueryBySubscriber(subscriberId).Count()
            };
        }
    }
}