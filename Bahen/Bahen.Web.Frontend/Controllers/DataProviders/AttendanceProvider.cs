﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.DataProviders
{
    /// <summary>
    /// Provide attendance data direcly to the api controller
    /// </summary>
    public class AttendanceProvider
    {
        /// <summary>
        /// Construct with paging options
        /// </summary>
        /// <param name="options">Paging options</param>
        public AttendanceProvider(ListApiOptions options = null)
        {
            this.Options = options;
            this.DataOperator = new SqlAttendanceOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
        }

        /// <summary>
        /// Paging options
        /// </summary>
        public ListApiOptions Options { get; set; }

        /// <summary>
        /// Attendance data operator
        /// </summary>
        protected SqlAttendanceOperator DataOperator { get; set; }

        /// <summary>
        /// Get attendances of an event
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <returns>See CountedListResponse<Attendant></returns>
        public CountedListResponse<Attendant> GetAttendants(long eventId)
        {
            CountedListResponse<Attendant> response = new CountedListResponse<Attendant>();
            response.Data = this.ProjectAttendants(
                this.DataOperator.Truncate(
                this.DataOperator.QueryByEvent(eventId), this.Options));
            if (this.Options.Summary)
            {
                response.Summary = this.GetSummaryByEvent(eventId);
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<Attendant>(this.Options.RequestUri, response.Data, x => x.AttendanceID);
            return response;
        }

        /// <summary>
        /// Get attendances with specific status
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="status">Attendance status</param>
        /// <returns>See CountedListResponse<Attendant></returns>
        public CountedListResponse<Attendant> GetAttendants(long eventId, AttendanceStatus status)
        {
            CountedListResponse<Attendant> response = new CountedListResponse<Attendant>();
            response.Data = this.ProjectAttendants(
                this.DataOperator.Truncate(
                this.DataOperator.QueryByEvent(eventId, status), this.Options));
            if (this.Options.Summary)
            {
                response.Summary = new CountSummary(this.CountAttendants(eventId, status));
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<Attendant>(this.Options.RequestUri, response.Data, x => x.AttendanceID);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public CountedListResponse<EventBrief> GetEvents(long userId)
        {
            CountedListResponse<EventBrief> response = new CountedListResponse<EventBrief>();
            response.Data = this.ProjectEvents(
                this.DataOperator.Truncate(
                this.DataOperator.QueryByUser(userId), this.Options));
            if (this.Options.Summary)
            {
                response.Summary = this.GetSummaryByUser(userId);
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<EventBrief>(this.Options.RequestUri, response.Data, x => x.AttendanceID);

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public CountedListResponse<EventBrief> GetEvents(long userId, AttendanceStatus status)
        {
            CountedListResponse<EventBrief> response = new CountedListResponse<EventBrief>();
            response.Data = this.ProjectEvents(
                this.DataOperator.Truncate(
                this.DataOperator.QueryByUser(userId, status), this.Options));
            if (this.Options.Summary)
            {
                response.Summary = new CountSummary(this.CountEvents(userId, status));
            }
            response.Paging = PagingLinks.GetInt64PagingLinks<EventBrief>(this.Options.RequestUri, response.Data, x => x.AttendanceID);

            return response;
        }

        /// <summary>
        /// Checks the status of a user to the event
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <param name="userId">User ID</param>
        /// <returns>If not attending, null will be returned</returns>
        public Attendant CheckStatus(long eventId, long userId)
        {
            SqlUserOperator uop = new SqlUserOperator(this.DataOperator.Context);
            SqlAttendanceRelation result = this.DataOperator.QueryByBoth(userId, eventId).ToList().FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            else
            {
                string name = uop.Set.Where(x => x.ID == userId).Select(x => x.PreferredName).FirstOrDefault();
                return new Attendant()
                {
                    ID = userId,
                    AttendanceID = result.ID,
                    Name = name,
                    RsvpStatus = (AttendanceStatus)result.AttendanceStatus
                };
            }
        }

        /// <summary>
        /// Project raw query data to attendant
        /// </summary>
        /// <param name="rawData">Raw data</param>
        /// <returns></returns>
        protected IList<Attendant> ProjectAttendants(IQueryable<SqlAttendanceRelation> rawData)
        {
            SqlUserOperator uOp = new SqlUserOperator(this.DataOperator.Context);
            return rawData.Join(
                uOp.Set,
                x => x.UserID,
                y => y.ID,
                (x, y) => new Attendant()
                {
                    AttendanceID = x.ID,
                    ID = y.ID,
                    Name = y.PreferredName,
                    RsvpStatus = (AttendanceStatus)x.AttendanceStatus
                }).ToList();
        }

        /// <summary>
        /// Project raw query data to event brief
        /// </summary>
        /// <param name="rawData">Raw data</param>
        /// <returns></returns>
        protected IList<EventBrief> ProjectEvents(IQueryable<SqlAttendanceRelation> rawData)
        {
            SqlEventOperator eOp = new SqlEventOperator(this.DataOperator.Context);
            return rawData.Join(
                eOp.Set,
                x => x.EventID,
                y => y.ID,
                (x, y) => new EventBrief()
                {
                    AttendanceID = x.ID,
                    ID = y.ID,
                    Name = y.Name,
                    RsvpStatus = (AttendanceStatus)x.AttendanceStatus,
                    StartTime = y.StartTime,
                    EndTime = y.EndTime,
                    Location = y.Location
                }).ToList();
        }

        /// <summary>
        /// Create attendance summary
        /// </summary>
        /// <param name="eventId">Event ID</param>
        /// <returns></returns>
        protected AttendanceListSummary GetSummaryByEvent(long eventId)
        {
            AttendanceListSummary summaryResult = new AttendanceListSummary();

            summaryResult.Count = this.DataOperator.QueryByEvent(eventId).Count();
            summaryResult.AttendingCount = this.CountAttendants(eventId, AttendanceStatus.Going);
            summaryResult.DeclinedCount = this.CountAttendants(eventId, AttendanceStatus.Declined);
            summaryResult.InterestedCount = this.CountAttendants(eventId, AttendanceStatus.Interested);
            summaryResult.MaybeCount = this.CountAttendants(eventId, AttendanceStatus.Maybe);
            summaryResult.NoreplyCount = this.CountAttendants(eventId, AttendanceStatus.Noreply);

            return summaryResult;
        }

        /// <summary>
        /// Create attendance summary
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns></returns>
        protected AttendanceListSummary GetSummaryByUser(long userId)
        {
            AttendanceListSummary summaryResult = new AttendanceListSummary();

            summaryResult.Count = this.DataOperator.QueryByUser(userId).Count();
            summaryResult.AttendingCount = this.CountEvents(userId, AttendanceStatus.Going);
            summaryResult.DeclinedCount = this.CountEvents(userId, AttendanceStatus.Declined);
            summaryResult.InterestedCount = this.CountEvents(userId, AttendanceStatus.Interested);
            summaryResult.MaybeCount = this.CountEvents(userId, AttendanceStatus.Maybe);
            summaryResult.NoreplyCount = this.CountEvents(userId, AttendanceStatus.Noreply);

            return summaryResult;
        }

        /// <summary>
        /// Count the attendant with one status
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        protected int CountAttendants(long eventId, AttendanceStatus status)
        {
            return this.DataOperator.QueryByEvent(eventId).Where(x => x.AttendanceStatus == (int)status).Count();
        }

        /// <summary>
        /// Count the events
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        protected int CountEvents(long userId, AttendanceStatus status)
        {
            return this.DataOperator.QueryByUser(userId).Where(x => x.AttendanceStatus == (int)status).Count();
        }
    }
}