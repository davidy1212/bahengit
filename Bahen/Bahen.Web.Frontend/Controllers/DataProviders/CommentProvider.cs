﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation.Table;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.DataProviders
{
    public class CommentProvider
    {
        public CommentProvider(ListApiOptions options)
        {
            this.Options = options;
        }

        public ListApiOptions Options { get; set; }

        public CountedListResponse<Comment> GetComments(string targetString, EntityTypeCode targetTypeCode)
        {
            CountedListResponse<Comment> response = new CountedListResponse<Comment>();
            response.Data = this.GetCommentsPagedData(targetString, targetTypeCode);

            if (response.Data.Count == 0)
            {
                response.Paging = null;
            }
            else
            {
                response.Paging = new PagingLinks(
                    this.Options.RequestUri,
                    null,
                    response.Data.Last().StringId);
            }
            return response;
        }

        protected IList<Comment> GetCommentsPagedData(string targetString, EntityTypeCode targetTypeCode)
        {
            TableCommentOperator op = new TableCommentOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            SqlUserOperator uop = new SqlUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            var raw = op.Truncate(
                op.GetByEntityID(targetString, targetTypeCode), this.Options).ToList();

            List<Comment> result = raw
                .Select<TableCommentEntity, Comment>
                (x =>
                {
                    var user = uop.Get(x.UserID);
                    var userComment =
                    new Comment()
                    {
                        UserID = x.UserID,
                        UserName = user.PreferredName,
                        //ProfilePic = user.ProfilePicture,
                        StringId = x.RowKey,
                        Content = x.Content
                    };
                    return userComment;
                }).ToList();

            return result;
        }
    }
}