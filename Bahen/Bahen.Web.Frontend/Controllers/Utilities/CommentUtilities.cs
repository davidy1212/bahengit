﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public class CommentUtilities
    {
        static TableCommentOperator CommentOperator = new TableCommentOperator(
                                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        public static void PostComment(string commentContent, string targetString, EntityTypeCode targetTypeCode, long userID)
        {
            TableCommentEntity commentEntity = new TableCommentEntity(targetString, targetTypeCode, userID);
            commentEntity.Content = commentContent;
            CommentOperator.Insert(commentEntity);
        }

        public static void DeleteComment(string targetString, EntityTypeCode targetTypeCode, string rowkey)
        {
            CommentOperator.Remove(TableCommentEntity.ComputePartitionKey(targetString, targetTypeCode), rowkey);
        }

        //public static IQueryable<TableCommentEntity> GetComments(string targetString, EntityTypeCode targetTypeCode)
        //{
        //    return CommentOperator.GetByEntityID(targetString, targetTypeCode);
        //}


        //public static List<UserComment> GetUsersComment(
        //    string targetString,
        //    EntityTypeCode targetTypeCode,
        //    PagingOptions options
        //    )
        //{
        //    var rawData = GetComments(targetString, targetTypeCode);
        //    return TruncateUserComments(rawData, options).ToList();
        //}

        //public static UserCommentListResponse GetResponse(
        //    string targetString,
        //    EntityTypeCode targetTypeCode,
        //    PagingOptions options,
        //    string root,
        //    string appendix
        //    )
        //{
        //    var rawData = GetComments(targetString, targetTypeCode);
        //    UserCommentListResponse response = new UserCommentListResponse();
        //    response.Data = TruncateUserComments(rawData, options).ToList();
        //    response.Summary = GenerateSummary(rawData);
        //    //response.Paging = PagingUtilities.GetPagingLinksISIdEntity<TableCommentEntity, UserComment>(rawData, response.Data, root, appendix);
        //    return response;
        //}

        //private static IEnumerable<UserComment> TruncateUserComments(IQueryable<TableCommentEntity> rawData, PagingOptions options)
        //{
        //    UserOperator userOperator = new UserOperator(
        //        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
        //        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        //    var userCommentList = PagingUtilities.TruncateByStringId<TableCommentEntity>(
        //        rawData, options).ToList()
        //            .Select<TableCommentEntity, UserComment>
        //            (x =>
        //            {
        //                var user = userOperator.Get(x.UserID);
        //                var userComment =new UserComment()
        //                {
        //                    UserID = x.UserID,
        //                    UserName = user.PreferredName,
        //                    ProfilePic = user.ProfilePicture,
        //                    Content = x.Content,
        //                    StringId = x.StringId
        //                };
        //                return userComment;
        //            }
        //            );

        //    return userCommentList;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="rawData"></param>
        ///// <returns></returns>
        //public static UserCommentListSummary GenerateSummary(IQueryable<TableCommentEntity> rawData)
        //{
        //    return new UserCommentListSummary()
        //    {
        //        Count = rawData.Count()
        //    };
        //}


        /// <summary>
        /// Determine if an user has commented an entity
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="targetID"></param>
        /// <param name="targetTypeCode"></param>
        /// <returns></returns>
        public static bool UserComments(long userID, string targetString, EntityTypeCode targetTypeCode)
        {
            var ListOfComments = CommentOperator.GetByEntityID(targetString, targetTypeCode);
            var result = ListOfComments.Where(x => x.UserID == userID).ToList();
            if (result.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


}
