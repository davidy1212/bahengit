﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Bahen.Web.Api.Controllers.Utilities
{
    /// <summary>
    /// Extension for server requests and responses
    /// </summary>
    public static class ServerExtensions
    {
        /// <summary>
        /// Context key saved in HttpRequestMessage
        /// </summary>
        private const string HttpContextKey = "MS_HttpContext";

        /// <summary>
        /// Get the current logged in user ID
        /// </summary>
        /// <param name="principal">User logged in principal object saved in context</param>
        /// <returns>User ID</returns>
        public static long GetID(this IPrincipal principal)
        {
            long result;
            try
            {
                Int64.TryParse(principal.Identity.Name, out result);
            }
            catch (Exception ex)
            {
                throw new AuthenticationException("Cannot parse ID from authentication data.", ex);
            }
            return result;
        }

        /// <summary>
        /// Get the user data reference from the database using the user ID
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static UserEntity GetUser(this IPrincipal principal)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            UserEntity user = userOperator.Get(principal.GetID());
            return user;
        }

        /// <summary>
        /// Try get the http context from HttpRequestMessage, useful in controller filters
        /// </summary>
        /// <param name="requestMessage">HttpRequestMessage</param>
        /// <param name="httpContext">Result of HttpContext</param>
        /// <returns>Whether successful in retrieving the context</returns>
        public static bool TryGetHttpContext(this HttpRequestMessage requestMessage, out HttpContextBase httpContext)
        {
            httpContext = null;
            object obj;
            if (requestMessage.Properties.TryGetValue(HttpContextKey, out obj))
            {
                httpContext = (HttpContextBase)obj;
            }

            return httpContext != null;
        }

        /// <summary>
        /// Check if an authorization token is valid, if yes then return the logged in user principal
        /// </summary>
        /// <param name="token">Authorization token</param>
        /// <returns>Logged in user principal</returns>
        public static LoginPrincipal CheckToken(string token)
        {
            OAuthAccessToken atoken;
            if (!OAuthTokenFactory.ValidateToken<OAuthAccessToken>(token, out atoken))
            {
                throw new OAuthException(OAuthError.AccessDenied, "invalid access token");
            }
            if (atoken.Expires < DateTime.UtcNow)
            {
                throw new OAuthException(OAuthError.AccessDenied, "access token expired");
            }
            return new LoginPrincipal()
            {
                Identity = new LoginIdentity()
                {
                    Type = AuthType.OAuth,
                    Scopes = (OAuthScopes)atoken.Scope,
                    IsAuthenticated = true,
                    ID = atoken.UserId
                },
                Role = (UserRoleLevel)atoken.Role
            };
        }

        /// <summary>
        /// Authorize the token given the context
        /// </summary>
        /// <param name="context">HttpContext of the request</param>
        public static void Authorize(this HttpContext context)
        {
            LoginPrincipal user = null;
            if (context.Request.Params["access_token"] != null)
            {
                user = CheckToken(context.Request.Params["access_token"]);
            }
            else if (context.Request.Headers["Authorization"] != null)
            {
                string[] part = context.Request.Headers["Authorization"].Split(' ');
                if (part.Length == 2)
                {
                    if (part[0] == "Bearer")
                    {
                        user = CheckToken(part[1]);
                    }
                }
            }

            context.User = user;
            Thread.CurrentPrincipal = user;
        }

        /// <summary>
        /// Read the stream data from form upload data in HttpRequestMessage
        /// </summary>
        /// <param name="message">Request message</param>
        /// <returns>Task of reading stream</returns>
        public static Task<Stream> ReadFromFormData(this HttpRequestMessage message)
        {
            if (!message.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            MultipartMemoryStreamProvider provider = new MultipartMemoryStreamProvider();

            return message.Content.ReadAsMultipartAsync(provider).ContinueWith(
                readTask =>
                {
                    if (readTask.IsFaulted || readTask.IsCanceled)
                    {
                        throw new HttpResponseException(HttpStatusCode.InternalServerError);
                    }
                    HttpContent fileContent = provider.Contents.FirstOrDefault();
                    return fileContent.ReadAsStreamAsync().Result;
                });
        }
    }
}