﻿using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.ResponseModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public class LikeUtilities
    {
        static TableLikeOperator LikeOperator = new TableLikeOperator(
                                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        public static void PostLike(string targetString, EntityTypeCode targetTypeCode, long userID)
        {
            if (!UserLikes(userID, targetString, targetTypeCode))        // only post when currenly not liking
            {
                TableLikeEntity likeEntity = new TableLikeEntity(targetString, targetTypeCode, userID);
                LikeOperator.Insert(likeEntity);
            }
        }

        public static void DeleteLike(string targetString, EntityTypeCode targetTypeCode, long userID)
        {
            var ListOfLikes = LikeOperator.GetByEntityID(targetString, targetTypeCode);
            var result = ListOfLikes.Where(x => x.UserID == userID).ToList();
            if (result.Count() > 0)
            {
                foreach (var item in result)
                {
                    TableLikeEntity likeEntity = item;
                    LikeOperator.Remove(likeEntity.PartitionKey, likeEntity.RowKey);
                }
            }
        }


        /// <summary>
        /// Determine if an user likes an entity
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="targetID"></param>
        /// <param name="targetTypeCode"></param>
        /// <returns></returns>
        public static bool UserLikes(long userID, string targetString, EntityTypeCode targetTypeCode)
        {
            var ListOfLikes = LikeOperator.GetByEntityID(targetString, targetTypeCode);
            var result = ListOfLikes.Where(x => x.UserID == userID).ToList();
            if (result.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}