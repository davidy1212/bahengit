﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public class ProfilePicture
    {
        public long Id { get; set; }
        public Guid Guid { get; set; }
        public ProfilePictureType Type { get; set; }
        public PictureSize Size { get; set; }
    }

    /// <summary>
    /// Picture utility class for processing and upload/downloading
    /// </summary>
    public static class Picture
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="id"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public static void UploadProfilePicture(ProfilePictureType t, long id, Stream image)
        {
            // Generate unique id
            Guid guid = Guid.NewGuid();

            // Upload thumbnail size picture
            // Convert to png format
            BlobProfilePictureOperator op = new BlobProfilePictureOperator(
                ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);

            using (Stream uploadT = new MemoryStream())
            {
                Image original = System.Drawing.Image.FromStream(image);

                Image thumbnailSizeImage = Resize(original, PictureSize.Thumb);
                thumbnailSizeImage.Save(uploadT, System.Drawing.Imaging.ImageFormat.Png);
                uploadT.Seek(0, SeekOrigin.Begin);

                // Cannot user async api call to azure storage. Give up here.
                //op.BeginUpload(
                //    t.ToDescriptionString(),
                //    id,
                //    guid,
                //    PictureSize.Thumb,
                //    uploadT,
                //    (asyncResult) =>
                //    {
                //        SqlUserOperator opUser = new SqlUserOperator(
                //            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                //        SqlUserEntity user = opUser.Get(id);
                //        user.Picture = ((CloudBlockBlob)asyncResult.AsyncState).Uri.AbsoluteUri.ToString();
                //        opUser.Update(user);
                //    });
                op.Upload(
                    t.ToDescriptionString(),
                    id,
                    guid,
                    PictureSize.Thumb,
                    uploadT);

                SqlUserOperator opUser = new SqlUserOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                SqlUserEntity user = opUser.Get(id);
                user.Picture = op.GetBlobAbsoluteUrl(
                    BlobProfilePictureOperator.ComputeProfilePictureUrl(
                    t.ToDescriptionString(), id, guid, PictureSize.Thumb));
                opUser.Update(user);
            }

        }

        /// <summary>
        /// Get the picture 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetProfilePictureUrl(ProfilePictureType t, long id)
        {
            switch (t)
            {
                case (ProfilePictureType.User):
                    {
                        SqlUserOperator op = new SqlUserOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                        SqlUserEntity user;
                        if (op.TryGet(id, out user))
                        {
                            return user.Picture;
                        }
                        else
                        {
                            throw new IdNotFoundException(id);
                        }
                    }
                case (ProfilePictureType.Event):
                    {
                        SqlEventOperator op = new SqlEventOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                        SqlEventEntity ev;
                        if (op.TryGet(id, out ev))
                        {
                            return ev.Picture;
                        }
                        else
                        {
                            throw new IdNotFoundException(id);
                        }
                    }
                case (ProfilePictureType.Cycle):
                    {
                        SqlCycleOperator op = new SqlCycleOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                        SqlCycleEntity cycle;
                        if (op.TryGet(id, out cycle))
                        {
                            return cycle.Picture;
                        }
                        else
                        {
                            throw new IdNotFoundException(id);
                        }
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        /// <summary>
        /// Resize the image by constrain the maximum width or height
        /// </summary>
        /// <param name="original">Original image</param>
        /// <param name="maxWidthHeight">Maximum width or height</param>
        /// <returns>Resized image</returns>
        public static Image Resize(Image original, int maxWidthHeight)
        {
            // Compute the new size for a large image
            Size newSize = new Size();
            if (original.Size.Width > original.Size.Height)
            {
                if (original.Size.Width > maxWidthHeight)
                {
                    newSize.Width = maxWidthHeight;
                    newSize.Height = maxWidthHeight * original.Size.Height / original.Size.Width;
                }
                else
                {
                    newSize = original.Size;
                }
            }
            else
            {
                if (original.Size.Height > maxWidthHeight)
                {
                    newSize.Height = maxWidthHeight;
                    newSize.Width = maxWidthHeight * original.Size.Width / original.Size.Height;
                }
                else
                {
                    newSize = original.Size;
                }
            }
            return new Bitmap(original, newSize);
        }

        /// <summary>
        /// Resize the image by specifying the picture size
        /// </summary>
        /// <param name="original">Original image</param>
        /// <param name="size">Picture size</param>
        /// <returns>Resized image</returns>
        public static Image Resize(Image original, PictureSize size)
        {
            switch (size)
            {
                case (PictureSize.Thumb):
                    {
                        int square = Math.Min(original.Width, original.Height);
                        Rectangle cropRect = new Rectangle(
                            original.Width < original.Height ? 0 : (original.Height - original.Width) / 2,
                            original.Height < original.Width ? (original.Width - original.Height) / 2 : 0,
                        square, square);
                        Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);
                        using (Graphics g = Graphics.FromImage(target))
                        {
                            g.DrawImage(original, new Rectangle(0, 0, target.Width, target.Height),
                                cropRect,
                                GraphicsUnit.Pixel);
                        }
                        return new Bitmap(target, new Size(50, 50));
                    }
                case (PictureSize.Medium):
                    {
                        return Resize(original, 300);
                    }
                case (PictureSize.Large):
                    {
                        return Resize(original, 600);
                    }
                default:
                    {
                        return Resize(original, 300);
                    }
            }
        }
    }

    public enum ProfilePictureType
    {
        [Description("u")]
        User,
        [Description("e")]
        Event,
        [Description("c")]
        Cycle
    }

}