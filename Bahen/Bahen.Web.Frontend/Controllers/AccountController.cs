﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.Utilities;
using Bahen.Web.Api.Controllers.Filters;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace Bahen.Web.Api.Controllers
{
    [Authorize]
    public class AccountController : ApiController
    {
        [HttpPost, RequireHttps, AllowAnonymous, AntiCsrf, ActionName("Register")]
        public PureTextMessageResponse Register([FromBody]RegisterRequest request)
        {
            if (String.IsNullOrEmpty(request.LoginEmail))
            {
                throw new ArgumentException("Please specify login email");
            }
            if (!UserValidators.UserEmailValidator(request.LoginEmail))
            {
                throw new ArgumentException("Please provide valid email address");
            }
            if (String.IsNullOrEmpty(request.LoginPassword1))
            {
                throw new ArgumentException("Please specify login password");
            }
            if (!UserValidators.UserPasswordValidator(request.LoginPassword1, request.LoginPassword2))
            {
                throw new ArgumentException("Please provide valid login password");
            }
            if (String.IsNullOrEmpty(request.FirstName))
            {
                throw new ArgumentException("Please fill in first name");
            }
            if (String.IsNullOrEmpty(request.LastName))
            {
                throw new ArgumentException("Please fill in last name");
            }
            UserEntity user = new UserEntity()
            {
                LoginEmail = request.LoginEmail,
                LoginPassword = request.LoginPassword1.ToSha512(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                PreferredName = request.FirstName + " " + request.LastName,
                SubscriptionPolicy = new PrivacyPolicy(true)
            };
            UserOperator op = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            op.Insert(user);
            return new PureTextMessageResponse("Success");
        }

        [HttpPost, RequireHttps, AllowAnonymous, ActionName("Login")]
        public OAuthAccessTokenResponse Login([FromBody]LoginRequest request, bool oauth = true)
        {
            if (oauth)
            {
                if (String.IsNullOrEmpty(request.LoginEmail))
                {
                    if (request.RefreshToken != null)
                    {
                        OAuthAccessTokenResponse response = new OAuthController().GetAccessTokenGet(
                            Int64.Parse(ConfigurationManager.AppSettings["ClientID"]),
                            ConfigurationManager.AppSettings["ClientSecret"],
                            "refresh_token",
                            null,
                            request.RefreshToken,
                            null,
                            null,
                            null);

                        return response;
                    }
                    throw new AuthenticationException("invalid login email or password");
                }
                else
                {
                    OAuthAccessTokenResponse response = new OAuthController().GetAccessTokenGet(
                        Int64.Parse(ConfigurationManager.AppSettings["ClientID"]),
                        ConfigurationManager.AppSettings["ClientSecret"],
                        "password",
                        null,
                        null,
                        null,
                        request.LoginEmail,
                        request.LoginPassword);

                    return response;
                }
            }
            else
            {
                LoginPrincipal principal = null;
                UserOperator userOperator = new UserOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                UserEntity user = null;
                try
                {
                    user = userOperator.Get(request.LoginEmail);
                }
                catch (Exception ex)
                {
                    throw new AuthenticationException("Invalid username or password", ex);
                }
                if (!request.LoginPassword.ToSha512().SequenceEqual(user.LoginPassword))
                {
                    throw new AuthenticationException("Invalid username or password");
                }

                principal = new LoginPrincipal()
                {
                    Identity = new LoginIdentity()
                    {
                        ID = user.ID,
                        IsAuthenticated = true
                    }
                };
                HttpContext.Current.User = principal;
                FormsAuthentication.SetAuthCookie(principal.Identity.Name, false);

                return new OAuthAccessTokenResponse() { Message = "Success" };
            }
        }

        [HttpGet, ActionName("Logout")]
        public dynamic Logout()
        {
            Thread.CurrentPrincipal = null;
            HttpContext.Current.User = null;
            FormsAuthentication.SignOut();
            return new
            {
                Message = "Success"
            };
        }
    }
}
