﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Privacy
{
    /// <summary>
    /// A judge to examine whether the current logged in user has the permission to view the data from the other user
    /// </summary>
    public static class PrivacyJudge
    {
        /// <summary>
        /// Examine the permission based on the privacy policy settings
        /// </summary>
        /// <param name="applicantId">Current logged in user</param>
        /// <param name="targetId">User of the resource owner</param>
        /// <param name="policy">Resource owner's privacy settings</param>
        /// <returns>Whether the permission is granted</returns>
        public static PrivacyStatus GetUserPrivacyStatus(long applicantId, long targetId, PrivacyPolicy policy)
        {
            if (applicantId == targetId)
            {
                return PrivacyStatus.Allowed;
            }
            if (IsInPrivacyGroup(applicantId, targetId, policy.Blocked))
            {
                return PrivacyStatus.Blocked;
            }
            if (IsInPrivacyGroup(applicantId, targetId, policy.UponRequest))
            {
                return PrivacyStatus.UponRequest;
            }
            if (IsInPrivacyGroup(applicantId, targetId, policy.Allowed))
            {
                return PrivacyStatus.Allowed;
            }
            return PrivacyStatus.Blocked;
        }

        /// <summary>
        /// Check whether the applicant is allowed to access the target user
        /// </summary>
        /// <param name="applicantId">Applicant user ID</param>
        /// <param name="targetId">Target user ID</param>
        /// <param name="policy">Privacy policy from target user</param>
        /// <returns></returns>
        public static bool CheckUserPrivacy(long applicantId, long targetId, PrivacyPolicy policy)
        {
            switch (PrivacyJudge.GetUserPrivacyStatus(applicantId, targetId, policy))
            {
                case (PrivacyStatus.Allowed):
                    {
                        return true;
                    }
                case (PrivacyStatus.UponRequest):
                    {
                        throw new PrivacyPolicyException("Need to request premission");
                    }
                case (PrivacyStatus.Blocked):
                    {
                        throw new PrivacyPolicyException("No premission");
                    }
                default:
                    {
                        return false;
                    }
            }
        }

        /// <summary>
        /// Check whether a user is defined in the privacy group
        /// </summary>
        /// <param name="applicantId">Applicant user ID</param>
        /// <param name="targetId">Target user ID</param>
        /// <param name="group">Privacy group definition</param>
        /// <returns></returns>
        private static bool IsInPrivacyGroup(long applicantId, long targetId, PrivacyGroup group)
        {
            if (group.Relations.HasFlag(Relation.Public))
            {
                return true;
            }

            if (group.Relations.HasFlag(Relation.Followers))
            {
                SqlSubscriptionOperator subOp = new SqlSubscriptionOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                return subOp.GetSubscriberIDsByHost(targetId).Contains(applicantId);
            }

            if (group.Users.Contains(applicantId))
            {
                return true;
            }

            SqlCycleUserOperator cycleOp = new SqlCycleUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            IEnumerable<long> cycles = cycleOp.GetCycleIDsByUser(applicantId);
            foreach (long cycleId in group.Cycles)
            {
                if (cycles.Contains(cycleId))
                {
                    return true;
                }
            }

            return false;
        }
    }
}