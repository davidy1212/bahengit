﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Privacy
{
    public enum PrivacyStatus
    {
        Blocked, UponRequest, Allowed
    }
}