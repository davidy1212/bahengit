﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Filters;
using Bahen.Web.Api.Controllers.ResponseModels;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Bahen.Web.Api.Controllers
{
    public class CommentController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TargetString</param>
        /// <param name="typeCode"></param>
        /// <param name="sId">Rowkey of this comment</param>
        /// <returns></returns>
        [HttpDelete, Authorize, RequirePermission(OAuthScopes.Owner)]
        [ActionName("Comment")]
        public PureTextMessageResponse DeleteComment(string id, int typeCode, string sId)
        {
            long userId = HttpContext.Current.User.GetID();
            CommentUtilities.DeleteComment(id, (EntityTypeCode)typeCode, sId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
    }
}
