﻿using Bahen.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public static class Paging
    {
        public static IEnumerable<T> Truncate<T>(
            IEnumerable<T> rawData, 
            PagingMode mode,
            int limit,
            long reference)
            where T: IEntity
        {

            if (mode == PagingMode.CursorAfter)
            {
                return rawData
                    .OrderBy(x => x.ID)
                    .SkipWhile(x => (x.ID <= reference))
                    .Take(limit);
            }
            else if (mode == PagingMode.CursorBefore)
            {
                return rawData
                    .OrderByDescending(x => x.ID)
                    .SkipWhile(x => (x.ID >= reference))
                    .Take(limit)
                    .Reverse();
            }
            else if (mode == PagingMode.Offset)
            {
                return rawData
                    .OrderBy(x => x.ID)
                    .Skip((int)reference)
                    .Take(limit);
            }
            else
            {
                // If time information can be queried
                if (rawData is IEnumerable<ITimeEntity>)
                {

                }
            }
            return new List<T>();
            throw new NotImplementedException();
        }
    }

}