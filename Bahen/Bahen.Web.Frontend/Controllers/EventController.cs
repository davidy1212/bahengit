﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.DataProviders;
using Bahen.Web.Api.Controllers.Filters;
using Bahen.Web.Api.Controllers.Paging;
using Bahen.Web.Api.Controllers.ResponseModels;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Bahen.Web.Api.Controllers.RequestModels;

namespace Bahen.Web.Api.Controllers
{
    public class EventController : ApiController
    {
        /// <summary>
        /// Default output list max size
        /// </summary>
        public const int AttendanceListDefaultLimit = 50;

        [HttpGet]
        public EventEntity GetDetail(long id)
        {
            EventEntity entity;

            if (new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString)
                .TryGet(id, out entity))
            {
                return entity;
            }
            else
            {
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("No event with ID = {0}", id)),
                    ReasonPhrase = "Event ID Not Found"
                };
                throw new HttpResponseException(response);
            }
        }

        [HttpGet]
        [ActionName("Feed")]
        public string GetFeeds(long id)
        {
            return "feed";
        }

        #region Picture
        [HttpGet]
        [ActionName("Picture")]
        public HttpResponseMessage GetPictureWidthHeight(long id, int width, int height)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            Image image = Image.FromFile(@"d:\test.png");
            MemoryStream memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);
            httpResponseMessage.Content = new ByteArrayContent(memoryStream.ToArray());
            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            httpResponseMessage.StatusCode = HttpStatusCode.OK;

            return httpResponseMessage;
        }

        [HttpGet]
        [ActionName("Picture")]
        public HttpResponseMessage GetPictureSize(long id, string size)
        {
            return new HttpResponseMessage();
        }
        #endregion

        #region Comment
        [HttpGet]
        [ActionName("Comment"), AllowAnonymous]
        public CountedListResponse<Comment> GetCommentsAfter(string id, string after = "0", int limit = Constants.CommentListDefaultLimit)
        {
            ListApiOptions options = new ListApiOptions()
            {
                RequestUri = Request.RequestUri,
                AfterString = after,
                Limit = limit,
                Mode = PagingMode.CursorAfterString,
                Test = false
            };
            return new CommentProvider(options).GetComments(id, EntityTypeCode.Event);
        }

        [HttpPost, Authorize]
        [ActionName("Comment")]
        public PureTextMessageResponse AddComment(string id, [FromBody]string content)
        {
            long userId = HttpContext.Current.User.GetID();
            CommentUtilities.PostComment(content, id, EntityTypeCode.Event, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }


        #endregion

        #region Like
        [HttpGet]
        [ActionName("Like"), AllowAnonymous]
        public CountedListResponse<Like> GetLikesAfter(string id, string after = "0", int limit = Constants.LikeListDefaultLimit)
        {
            ListApiOptions options = new ListApiOptions()
            {
                RequestUri = Request.RequestUri,
                AfterString = after,
                Limit = limit,
                Mode = PagingMode.CursorAfterString,
                Test = false
            };
            return new LikeProvider(options).GetLikes(id, EntityTypeCode.Event);
        }

        //[HttpGet]
        //[ActionName("Like")]
        //public UserLikeListResponse GetLikesBefore(string id, string until, int limit = Constants.LikeListDefaultLimit)
        //{
        //    ListApiOptions options = new ListApiOptions()
        //    {
        //        RequestUri = Request.RequestUri,
        //        BeforeString = until,
        //        Limit = limit,
        //        Mode = PagingMode.CursorBeforeString,
        //        Test = false
        //    };
        //    return new LikeProvider(options).GetLikes(id, EntityTypeCode.Event);
        //}

        [HttpPost, Authorize]
        [ActionName("Like")]
        public PureTextMessageResponse AddLike(string id)
        {
            long userId = HttpContext.Current.User.GetID();
            LikeUtilities.PostLike(id, EntityTypeCode.Event, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }

        
        #endregion

        #region Attendance
        /// <summary>
        /// Get attendance information after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Part of the attendance list</returns>
        [HttpGet, ActionName("Invited"), Authorize]
        public CountedListResponse<Attendant> GetInvitedUsersAfter(
            long id, 
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id);
        }
        
        /// <summary>
        /// Get the attendance status of a specific user to an event
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="uid">User ID</param>
        /// <param name="test">Whether in test mode</param>
        /// <returns>Contains user data and attendance information, 
        /// if user is not related to the event, then an empty list will be returned</returns>
        [HttpGet]
        [ActionName("Invited")]
        public ListResponse<Attendant> GetAttendanceStatus(
            long id,
            long uid,
            bool test = false)
        {
            Attendant result = new AttendanceProvider().CheckStatus(id, uid);
            if (result == null)
            {
                return new ListResponse<Attendant>()
                {
                    Data = new List<Attendant>()
                };
            }
            else
            {
                return new ListResponse<Attendant>()
                {
                    Data = new List<Attendant>() { result }
                };
            }
        }

        /// <summary>
        /// Get users that attend the event after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Users that attend the event</returns>
        [HttpGet]
        [ActionName("Going")]
        public CountedListResponse<Attendant> GetGoingUsers(
            long id, 
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, AttendanceStatus.Going);
        }

        /// <summary>
        /// Get users that maybe attend the event after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Users that maybe attend the event</returns>
        [HttpGet]
        [ActionName("Maybe")]
        public CountedListResponse<Attendant> GetMaybeUsers(
            long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, AttendanceStatus.Maybe);
        }

        /// <summary>
        /// Get users that are interested to the event after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Users that are interested to attend the event</returns>
        [HttpGet]
        [ActionName("Interested")]
        public CountedListResponse<Attendant> GetInterestedUsers(
            long id, 
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, AttendanceStatus.Interested);
        }

        /// <summary>
        /// Get users that declined to the event after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Users that declined to attend the event</returns>
        [HttpGet]
        [ActionName("Declined")]
        public CountedListResponse<Attendant> GetDeclinedUsers(
            long id, 
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, AttendanceStatus.Declined);
        }


        /// <summary>
        /// Get attendance information after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Part of the attendance list</returns>
        [HttpGet, ActionName("Noreply"), Authorize]
        public CountedListResponse<Attendant> GetNoreplyUsers(
            long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, AttendanceStatus.Noreply);
        }
        
        #endregion

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
