﻿using Bahen.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;
using System.Threading;
using Bahen.Data.Operation;
using System.Configuration;
using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Utilities;
using System.Security.Claims;
using System.Text;
using Bahen.Data.Models;

namespace Bahen.Web.Api.Controllers.MessageHandlers
{
    public class OAuthMessageHandler : DelegatingHandler
    {
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpContextBase context;
            try
            {
                if (!request.TryGetHttpContext(out context))
                {
                    throw new OAuthException(OAuthError.InvalidRequest, "invalid request");
                }
                if (request.Headers.Authorization != null)
                {
                    string[] part = context.Request.Headers["Authorization"].Split(' ');
                    if (part.Length == 2)
                    {
                        if (part[0] == "Bearer")
                        {
                            this.Authorize(context, part[1]);
                        }
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, string> pair in request.GetQueryNameValuePairs())
                    {
                        if (pair.Key == "access_token")
                        {
                            this.Authorize(context, pair.Value);
                            break;
                        }
                    }
                }
            }
            catch (OAuthException ex)
            {
                var err_response = request.CreateResponse();
                string content = "{\"error\":\"" + ex.Error + "\",\"error_description\":\"" + ex.ErrorDescription + "\"}";
                err_response.Content = new StringContent(content, Encoding.UTF8, "application/json");
                return err_response;
            }
            var response = await base.SendAsync(request, cancellationToken);
            return response;
        }

        private void Authorize(HttpContextBase context, string token)
        {
            LoginPrincipal user = ServerExtensions.CheckToken(token);
            context.User = user;
            Thread.CurrentPrincipal = user;
        }
    }
}