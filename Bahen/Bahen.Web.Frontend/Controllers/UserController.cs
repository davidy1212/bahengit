﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using Bahen.Web.Api.Controllers.DataProviders;
using Bahen.Web.Api.Controllers.Filters;
using Bahen.Web.Api.Controllers.Privacy;
using Bahen.Web.Api.Controllers.RequestModels;
using Bahen.Web.Api.Controllers.ResponseModels;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Bahen.Web.Api.Controllers
{
    [Authorize]
    public class UserController : ApiController
    {
        /// <summary>
        /// Get user basic info
        /// </summary>
        /// <returns>Simple user info response object</returns>
        [HttpGet, ActionName("me")]
        public User GetUserInfo()
        {
            long userID = HttpContext.Current.User.GetID();
            UserEntity user = null;
            user = HttpContext.Current.User.GetUser();
            return new User()
            {
                ID = user.ID,
                Name = user.PreferredName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.LoginEmail
            };
        }

        /// <summary>
        /// Update user info
        /// </summary>
        /// <returns>Success or not</returns>
        [HttpPost, RequirePermission(OAuthScopes.Owner), ActionName("me")]
        public PureTextMessageResponse UpdateUserInfo()
        {
            return new PureTextMessageResponse("Success");
        }

        [HttpGet, ActionName("info"), AllowAnonymous]
        public User GetUserInfo(long id,
            [ModelBinder(typeof(FieldsFilterModelBinderProvider), Name = "Optional")] FieldsFilter fields)
        {
            if (id == HttpContext.Current.User.GetID())
            {
                return this.GetUserInfo();
            }
            UserEntity user = null;
            user = this.GetRealUser(id);
            Uri hostUri = new Uri(Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority);
            return new User()
            {
                ID = user.ID,
                Name = user.PreferredName,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                Link = user.Alias != null ?
                hostUri.AbsoluteUri + user.Alias :
                hostUri.AbsoluteUri + "user/" + user.ID.ToString(),
                Email = PrivacyJudge.CheckUserPrivacy(
                HttpContext.Current.User.GetID(), user.ID, user.EmailVisibilityPolicy) ?
                user.LoginEmail : null,
                Picture = fields.Contains("picture") ?
                Picture.GetProfilePictureUrl(
                ProfilePictureType.User, user.ID) : null,
                Gender =
                user.Gender != Gender.NotDisclosed ?
                user.Gender.ToString().ToLowerInvariant() : null
            };
        }

        [HttpPost, RequirePermission(OAuthScopes.Owner), RequireRole(UserRoleLevel.Admin)]
        public PureTextMessageResponse UpdateUserInfo(long id)
        {
            return new PureTextMessageResponse("Success");
        }

        private UserEntity GetTestUser(long id)
        {
            UserEntity user = new UserEntity()
            {
                FirstName = "Mengye",
                LastName = "Ren",
                PreferredName = "Mengye Ren",
                LoginEmail = "renmengye@gmail.com",
                UserGroup = UserRoleLevel.SuperAdmin
            };
            user.SqlEntity.ID = id;
            user.TableEntity.UserID = id;
            return user;
        }

        private UserEntity GetRealUser(long id)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            return userOperator.Get(id);
        }

        #region Picture
        // temporary picture handler for testing
        // currently only allow thumbnail
        // will implement larger profile picture in album api
        [HttpGet, AllowAnonymous, ActionName("Picture")]
        public HttpResponseMessage GetProfilePicture(long id, PictureSize size = PictureSize.Thumb, bool redirect = true)
        {
            if (size != PictureSize.Thumb)
            {
                throw new ArgumentException("profile picture size option not supported");
            }
            HttpResponseMessage response = new HttpResponseMessage();
            string url = Picture.GetProfilePictureUrl(ProfilePictureType.User, id);
            if (String.IsNullOrEmpty(url))
            {
                if (redirect)
                {
                    if (response.Content == null)
                    {
                        response.Content = new StringContent("");
                    }
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                    response.StatusCode = HttpStatusCode.Redirect;
                    response.Headers.Location = new Uri("/img/default_thumb.png", UriKind.Relative);
                }
                else
                {
                    response.Content = new StringContent("{\"id\":" + id + ",\"picture\":\"null\"}");
                }
            }
            else
            {
                if (redirect)
                {
                    if (response.Content == null)
                    {
                        response.Content = new StringContent("");
                    }
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
                    response.StatusCode = HttpStatusCode.Redirect;
                    response.Headers.Location = new Uri(url, UriKind.RelativeOrAbsolute);
                }
                else
                {
                    response.Content = new StringContent("{\"id\":" + id + ",\"picture\":\"" + url + "\"}");
                }
            }
            return response;
        }

        // Only uploads thumbnail picture for now.
        // Will incorporate larger picture in album api
        [HttpPost, ActionName("Picture")]
        public async Task<PureTextMessageResponse> UploadProfilePicture()
        {
            Stream stream = await Request.ReadFromFormData();
            Picture.UploadProfilePicture(ProfilePictureType.User, User.GetID(), stream);
            return new PureTextMessageResponse("Success");
        }
        #endregion

        #region Newsfeeds
        /// <summary>
        /// Get user's newsfeed
        /// </summary>
        /// <param name="id">User ID</param>
        /// <param name="options">List API options</param>
        /// <returns>List of newsfeeds</returns>
        [HttpGet, AllowAnonymous, ActionName("Feeds")]
        public ListResponse<Newsfeed> GetNewsfeeds(long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "String")]ListApiOptions options)
        {
            // If it is until mode, then convert it into after
            if (options.Mode == PagingMode.TimeUntil)
            {
                options.Mode = PagingMode.CursorAfterString;
                DateTime untilDt = options.Until.FromUnixTime();
                string afterCursor = TableNewsfeedEntity.ComputeRowKey(untilDt, Guid.Empty);
                options.AfterString = afterCursor;
            }
            return new NewsfeedProvider(options).GetNewsfeeds(id);
        }
        #endregion

        #region Followers/Followings
        /// <summary>
        /// Follow the user
        /// </summary>
        /// <param name="id">User ID of the user being followed</param>
        /// <returns></returns>
        [HttpPost, RequirePermission(OAuthScopes.ManageFollowings), ActionName("Follow")]
        public PureTextMessageResponse FollowUser(long id)
        {
            if (HttpContext.Current.User.GetID() == id)
            {
                throw new ArgumentException("cannot follow yourself");
            }

            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            UserEntity user = userOp.Get(id);

            // Check user's privacy settings
            if (PrivacyJudge.CheckUserPrivacy(HttpContext.Current.User.GetID(), id, user.SubscriptionPolicy))
            {
                userOp.AddSubscription(HttpContext.Current.User.GetID(), id);
                return new PureTextMessageResponse("success");
            }
            else
            {
                return new PureTextMessageResponse("fail");
            }
        }

        /// <summary>
        /// Get followers of the user after a cursor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="options">List API options</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous, ActionName("Followers")]
        public CountedListResponse<Friend> GetFollowers(long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            // Check user privacy settings here
            // Check OAuth permissions here (read_followings)
            // Authorize this operation based on the settings here
            return new SubscriptionProvider(options).GetFollowers(id);
        }

        /// <summary>
        /// Get the following of the user after a cursor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="options">List API options</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous, ActionName("Followings")]
        public CountedListResponse<Friend> GetFollowings(
            long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            // Check user privacy settings here
            // Check OAuth permissions here (read_followings)
            // Authorize this operation based on the settings here
            return new SubscriptionProvider(options).GetFollowings(id);
        }
        #endregion

        #region Events
        /// <summary>
        /// Get user's events
        /// </summary>
        /// <param name="id">User ID</param>
        /// <param name="options">List API options</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous, ActionName("Events")]
        public CountedListResponse<EventBrief> GetEvents(
            long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetEvents(id);
        }

        /// <summary>
        /// Get user's event with specific attendance
        /// </summary>
        /// <param name="id">User ID</param>
        /// <param name="status">Attendance status</param>
        /// <param name="options">List API options</param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous, ActionName("Events")]
        public CountedListResponse<EventBrief> GetEventsWithStatus(
            long id,
            string status,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            AttendanceStatus aStatus;
            Enum.TryParse<AttendanceStatus>(status, true, out aStatus);
            return new AttendanceProvider(options).GetEvents(id, aStatus);
        }
        #endregion
    }
}
