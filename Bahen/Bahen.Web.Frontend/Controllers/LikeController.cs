﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Filters;
using Bahen.Web.Api.Controllers.ResponseModels;
using Bahen.Web.Api.Controllers.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace Bahen.Web.Api.Controllers
{
    public class LikeController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TargetString</param>
        /// <returns></returns>
        [HttpDelete, Authorize, RequirePermission(OAuthScopes.Owner)]
        [ActionName("Like")]
        public PureTextMessageResponse DeleteLike(string id, int typeCode)
        {
            long userId = HttpContext.Current.User.GetID();
            LikeUtilities.DeleteLike(id, (EntityTypeCode)typeCode, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
    }
}
