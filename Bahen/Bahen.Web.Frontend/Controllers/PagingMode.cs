﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.Utilities
{
    public enum PagingMode
    {
        CursorAfter, CursorBefore, Offset, TimeSince, TimeUntil
    }
}