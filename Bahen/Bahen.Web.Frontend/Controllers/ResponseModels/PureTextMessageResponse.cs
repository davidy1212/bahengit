﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public class PureTextMessageResponse
    {
        public PureTextMessageResponse()
        {
        }

        public PureTextMessageResponse(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}