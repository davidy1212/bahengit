﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public class OAuthAccessTokenResponse
    {
        public string Message { get; set; }

        [JsonProperty("Access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("Expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }
    }

    public class OAuthClient
    {
        public long Id { get; set; }
        public string Secret { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string TrustLevel { get; set; }
    }

    public class OAuthRegisterResponse
    {
        public OAuthClient Data { get; set; }
        public string Message { get; set; }
    }
}