using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Paging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    /// <summary>
    /// Simple output data type representing an user who comments an entity
    /// </summary>
    public class Comment : IStringId
    {
        [JsonProperty(PropertyName = "Name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public long UserID { get; set; }

        //[JsonProperty(PropertyName = "Pic")]
        //public string ProfilePic { get; set; }

        [JsonProperty(PropertyName = "Content")]
        public String Content { get; set; }

        [JsonProperty(PropertyName = "SId")]
        public string StringId { get; set; }
    }
}