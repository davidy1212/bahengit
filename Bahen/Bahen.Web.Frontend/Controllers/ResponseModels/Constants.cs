﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public static class Constants
    {
        public const int ListDefaultLimit = 50;

        /// <summary>
        /// Default output list max size of comment list
        /// </summary>
        public const int CommentListDefaultLimit = 10;

        /// <summary>
        /// Default output list max size of like list
        /// </summary>
        public const int LikeListDefaultLimit = 10;

        /// <summary>
        /// Default output list max size of friend list
        /// </summary>
        public const int FriendListDefaultLimit = 50;

        /// <summary>
        /// Default output list max size of newsfeed list
        /// </summary>
        public const int NewsfeedListDefaultLimit = 50;

        public const int EventListDefaultLimit = 50;

        /// <summary>
        /// Maximum guid
        /// </summary>
        public static Guid GuidMax = Guid.Parse("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
    }
}