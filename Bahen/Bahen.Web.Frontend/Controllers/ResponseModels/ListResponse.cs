﻿using Bahen.Web.Api.Controllers.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public class ListResponse<T>
    {
        public IList<T> Data { get; set; }
        public PagingLinks Paging { get; set; }
    }

    public class CountedListResponse<T> : ListResponse<T>
    {
        public CountSummary Summary { get; set; }
    }

    public class CountSummary
    {
        public int Count { get; set; }

        public CountSummary()
        {
        }

        public CountSummary(int count)
        {
            this.Count = count;
        }
    }
}