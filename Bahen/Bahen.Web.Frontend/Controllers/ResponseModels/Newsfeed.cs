﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Paging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public class Newsfeed : IStringId
    {
        [JsonProperty("Id")]
        public string StringId { get; set; }
        public string Content { get; set; }
        public NewsfeedSender From { get; set; }
        [JsonProperty("Time")]
        public long TimeSent { get; set; }
    }

    public class NewsfeedSender
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
    }
}