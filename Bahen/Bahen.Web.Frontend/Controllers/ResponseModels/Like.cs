﻿using Bahen.Data.Models;
using Bahen.Web.Api.Controllers.Paging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    /// <summary>
    /// Simple output data type representing an user who likes an entity
    /// </summary>
    public class Like : IStringId
    {
        [JsonProperty(PropertyName = "Name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public long UserID { get; set; }

        //[JsonProperty(PropertyName = "Pic")]
        //public string ProfilePic { get; set; }

        [JsonIgnore]
        public string StringId { get; set; }
    }
}