﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Api.Controllers.ResponseModels
{
    public class EventBrief : IInt64Id
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public DateTimeOffset EndTime { get; set; }
        public string Location { get; set; }
        public AttendanceStatus RsvpStatus { get; set; }

        [JsonIgnore]
        public long AttendanceID { get; set; }
    }
}