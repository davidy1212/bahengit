﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Bahen.Web.Common.Security" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/lib/jquery-cookie/jquery.cookie.min.js"></script>
    <script src="/js/util/web.js"></script>
    <script>
        var csrfToken = "<%=Response.GetCsrfToken()%>";
        $(function () {
            WebUtilities.tryRelogin(function () {
            }, function () {
                window.location = "/login?redirect=" + window.location;
            });
            $("#submit").click(function () {
                $("#submit").attr("disabled", "true");
                var request = new Object({
                    name: $("#name").val()
                });
                $.ajax({
                    type: "POST",
                    url: "/api/oauth/register",
                    beforeSend: WebUtilities.setAuthHeader,
                    data: JSON.stringify(request),
                    success: function (response) {
                        $("#message").html(JSON.stringify(response.data));
                    },
                    error: function (response) {
                        var message = JSON.parse(response.responseText).message;
                        $("#message").html(message);
                        $("#submit").removeAttr("disabled");
                    },
                    dataType: "json",
                    contentType: "application/json",
                    headers: {
                        "X-CSRFTOKEN": csrfToken
                    }
                });
            });
        });
    </script>
    <title></title>
</head>
<body>
    <div>
        App Name:<br />
        <input id="name" name="name" type="text" /><br />
        <span id="message"></span>
        <br />
        <input id="submit" type="button" value="Create App" />
    </div>
</body>
</html>
