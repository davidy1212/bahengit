﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Bahen.Web.Api.Controllers.Authentication" %>
<%@ Import Namespace="Bahen.Data.Models" %>
<%@ Import Namespace="Bahen.Data.Operation" %>
<%@ Import Namespace="Bahen.Web.Api.Controllers.RequestModels" %>
<%@ Import Namespace="Bahen.Web.Common.Security" %>
<%
    SqlApiClientEntity client;
    OAuthAuthorizeRequest request;
    try
    {
        // Parse client ID and form ID.
        long clientId;
        if (!Int64.TryParse((string)this.Request.QueryString["client_id"], out clientId))
        {
            throw new OAuthException(OAuthError.InvalidRequest, "Invalid client id");
        }

        SqlApiClientOperator opClient = new SqlApiClientOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
        client = opClient.Get(clientId);

        request = new OAuthAuthorizeRequest()
        {
            ClientId = clientId,
            ResponseType = this.Request.QueryString["response_type"],
            Scope = this.Request.QueryString["scope"],
            RedirectUri = this.Request.QueryString["redirect_uri"]
        };
    }
    catch (OAuthException ex)
    {
        Response.ContentType = "application/json";
        Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
        string content = "{\"error\":" + ex.Error + "\"error_description\":\"{0}\"" + ex.ErrorDescription + "}";
        Response.Write(content);
        return;
    }
    //catch (Exception ex2)
    //{
    //    Response.ContentType = "application/json";
    //    Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
    //    string content = "{\"error\":\"temporarily_unavailable\"}";
    //    Response.Write(content);
    //    return;
    //}
%>
<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width" />
    <title></title>
</head>
<body>
    Please confirm that you are granting <%=client.Name %> with following accesses:<br />
    <%=request.Scope %><br />
    <form action="/api/oauth/grantpost" method="post">
        <%=Response.GetCsrfHtml() %>
        <input name="clientId" type="hidden" value="<%=request.ClientId %>" />
        <input name="redirectUri" type="hidden" value="<%=request.RedirectUri %>" />
        <input name="responseType" type="hidden" value="<%=request.ResponseType %>" />
        <input name="scope" type="hidden" value="<%=request.Scope %>" />
        <input type="submit" value="Confirm" />
    </form>
    <form action='<%=request.RedirectUri%>' method="get">
        <input name="error" type="hidden" value="access_denied" />
        <input type="submit" value="Cancel" />
    </form>
</body>
</html>
