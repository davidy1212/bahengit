﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/lib/jquery-cookie/jquery.cookie.min.js"></script>
    <script src="/js/util/web.js"></script>
    <script>
        function successLogin(response) {
            if (response["message"] == "Success") {
                var redirect = WebUtilities.getUrlParams()["redirect"];
                if (redirect) {
                    window.location = redirect;
                } else {
                    window.location = "/";
                }
            }
        }

        function errorLogin(response) {
            var message = JSON.parse(response.responseText).message;
            $("#errorMessage").html(message);
            $("#submit").removeAttr("disabled");
        }

        $(document).ready(function () {
            $("#submit").click(function () {
                $("#submit").attr("disabled", "true");
                var request = new Object({
                    loginEmail: $("#email").val(),
                    loginPassword: $("#password").val(),
                    oauth: false,
                    rememberMe: false
                });
                var str = JSON.stringify(request);
                $.ajax({
                    type: "POST",
                    url: "/api/account/login?oauth=false",
                    data: str,
                    success: successLogin,
                    error: errorLogin,
                    dataType: "json",
                    contentType: "application/json"
                });
            });
        });</script>
    <title></title>
</head>
<body>
    Email:<br />
    <input id="email" name="email" type="text" /><br />
    Password:<br />
    <input id="password" name="password" type="password" /><br />
    <br />
    <span id="errorMessage"></span><br />
    <input id="submit" type="button" value="Login" />
</body>
</html>
