﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/lib/jquery-cookie/jquery.cookie.min.js"></script>
    <script src="/js/util/web.js"></script>
    <script>
        WebUtilities.tryRelogin(function () {
            $.ajax({
                type: "GET",
                url: "http://localhost:12345/user/me",
                beforeSend: WebUtilities.setAuthHeader,
                success: function (response) {
                    $("#result").html(JSON.stringify(response));
                }
            });
        }, function () {
            alert("no token");
        });
    </script>
    <title></title>
</head>
<body>
    <span id="result"></span>
</body>
</html>
