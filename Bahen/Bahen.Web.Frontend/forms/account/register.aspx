﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<%@ Import Namespace="Bahen.Web.Common.Security" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/js/util/web.js"></script>
    <script>
        var csrfToken = "<%=Response.GetCsrfToken()%>";



        for (var i = 10; i < 100; i++) {
            var request = new Object({
                loginEmail: "test_" + i + "@test.com",
                loginPassword1: "testPassword",
                loginPassword2: "testPassword",
                firstName: i,
                lastName: "Test",
            });
            $.ajax({
                type: "POST",
                url: "/api/account/register",
                data: JSON.stringify(request),
                success: function (response) {
                    console.log("success");
                },
                error: function (response) {
                    console.log( JSON.parse(response.responseText).message);
                   
                },
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "X-CSRFTOKEN": csrfToken
                }
            });

        }



       


        $(function () {
            $("#submit").click(function () {
                $("#submit").attr("disabled", "true");
                var request = new Object({
                    loginEmail: $("#email").val(),
                    loginPassword1: $("#password1").val(),
                    loginPassword2: $("#password2").val(),
                    firstName: $("#firstName").val(),
                    lastName: $("#lastName").val()
                });
                $.ajax({
                    type: "POST",
                    url: "/api/account/register",
                    data: JSON.stringify(request),
                    success: function (response) {
                        if (response["message"] == "Success") {
                            window.location = "/login"
                        }
                    },
                    error: function (response) {
                        var message = JSON.parse(response.responseText).message;
                        $("#errorMessage").html(message);
                        $("#submit").removeAttr("disabled");
                    },
                    dataType: "json",
                    contentType: "application/json",
                    headers: {
                        "X-CSRFTOKEN": csrfToken
                    }
                });
            });
        });
    </script>
    <title></title>
</head>
<body>
    Email:<br />
    <input id="email" name="email" type="text" /><br />
    Password:<br />
    <input id="password1" name="password1" type="password" /><br />
    Password Again:<br />
    <input id="password2" name="password2" type="password" /><br />
    First Name:<br />
    <input id="firstName" name="firstName" type="text" /><br />
    Last Name:<br />
    <input id="lastName" name="lastName" type="text" /><br />
    <span id="errorMessage"></span>
    <br />
    <input id="submit" type="button" value="Register" />
</body>
</html>
