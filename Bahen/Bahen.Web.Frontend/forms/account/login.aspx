﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <script src="/lib/jquery/jquery.min.js"></script>
    <script src="/lib/jquery-cookie/jquery.cookie.min.js"></script>
    <script src="/js/util/web.js"></script>
    <script>
        var remember;

        function successLogin(response) {
            if (response["message"] == "Success") {
                WebUtilities.storeToken(response, remember);
                var redirect = WebUtilities.getUrlParams()["redirect"];
                if (redirect) {
                    window.location = redirect;
                } else {
                    window.location = "/";
                }
            }
        }

        function errorLogin(response) {
            var message = JSON.parse(response.responseText).message;
            $("#errorMessage").html(message);
            $("#submit").removeAttr("disabled");
        }

        $(document).ready(function () {
            $("#submit").click(function () {
                remember = $("#remember")[0].checked;
                $("#submit").attr("disabled", "true");
                var request = new Object({
                    loginEmail: $("#email").val(),
                    loginPassword: $("#password").val()
                });
                var str = JSON.stringify(request);
                WebUtilities.requestToken(str, successLogin, errorLogin);
            });
        });
    </script>
    <title></title>
</head>
<body>
    Email:<br />
    <input id="email" name="email" type="text" /><br />
    Password:<br />
    <input id="password" name="password" type="password" /><br />
    <input id="remember" name="remember" type="checkbox" />Remember me<br />
    <span id="errorMessage"></span>
    <br />
    <input id="submit" type="button" value="Login" />
</body>
</html>
