﻿define(['common/Eventub', 'backbone', 'jquery', 'text!frame/frame.html!strip'], function (Eventub, Backbone, $, page_html) {
    
    var Frame = Backbone.View.extend({
        _template : _.template(page_html),

        initialize: function () {  //load models, inject to dom,
            console.log("frame initialized");
            this.listenTo(Eventub.GlobalEvents, 'nav:/profile', this.showProfile);
            this.listenTo(Eventub.GlobalEvents, 'nav:/feed', this.showFeed);
            this.$el.html(this._template({ 'Eventub': Eventub }));
            Eventub.Module.queueLoad();
         
           
        },
       
     
      
        showProfile: function () {
        
            $("#profile_pane").show();
            $("#feed").hide();
        },

        showFeed: function () {
            $("#profile_pane").hide();
            $("#feed").show();
        }


    });


    return Frame;
});

