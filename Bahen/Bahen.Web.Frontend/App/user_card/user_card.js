﻿define(['backbone', 'common/Eventub', 'jquery', 'text!user_card/user_card.html'], function (Backbone, Eventub, $, template) {

    var User_Card = Backbone.View.extend({
        model: new Eventub.Models.User(),

        _template: _.template(template),
        

        initialize: function () {
            this.listenTo(Eventub.GlobalEvents, "show:floating_user_card", this.show);
            this.listenTo(Eventub.GlobalEvents,"hide:floating_user_card",this.hide);
            this.$el.css("position", "absolute");
            this.$el.hide();
            
        },
        hide:function(){
            this.$el.hide();
        },
        show: function (userId, x, y) {
            this.render(userId);
            this.$el.css("top", y + "px");
            this.$el.css("left", x + "px");
            this.$el.show();

        },
        render:function (userId){
            this.model.set(userId);
            this.model.fetchUserDetails();
            this.$el.html(this._template(this.model.toJSON()));
            
        }


    });

    return User_Card;


});