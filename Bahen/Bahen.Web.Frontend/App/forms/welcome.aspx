﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
      <link rel=  "stylesheet" type="text/css" href ="/lib/bootstrap/css/bootstrap.min.css" />
    <link rel ="stylesheet" type="text/css" href="/lib/bootstrap/css/bootstrap-responsive.css" />
    <script  data-main="/app/forms/welcome.js" src ="/lib/require/require.js"></script>
    <title></title>
</head>
<body>
    <form class="form-horizontal">
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email"/>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Password</label>
    <div class="controls">
      <input type="password" id="inputPassword" placeholder="Password"/>
    </div>
    <label class ="help-block" id ="errorMessage"></label>
  </div>
  <div class="control-group">
    <div class="controls">
      <label class="checkbox">
       <input id="remember" name="remember" type="checkbox" />Remember me<br />
      </label>
      <button type="button" id="submit" class="btn">Sign in</button>
    </div>
  </div>
</form>

</body>
</html>

