﻿requirejs.config({
    baseUrl: '/App',
    shim: {
        'backbone': {
            //These script dependencies should be loaded before loading
            //backbone.js
            deps: ['underscore', 'jquery'],
            //Once loaded, use the global 'Backbone' as the
            //module value.
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        },
        'jquery.cookie': {     //<-- cookie depends on Jquery and exports nothing
            deps: ['jquery']
        },

    },
    paths: {
        'jquery': '/lib/jquery/jquery',
        'backbone': '/lib/backbone/backbone',
        'underscore': '/lib/underscore/underscore',
        'jquery-cookie': '/lib/jquery-cookie/jquery.cookie',
    }
});


require(["common/Eventub","jquery"], function (Eventub,$) {
    var remember;
    
    function successLogin(response) {
        if (response["message"] == "Success") {
            
            Eventub.Auth.storeToken(response, remember);
            var redirect = Eventub.Auth.getUrlParams()["redirect"];
            if (redirect) {
                window.location = redirect;
            } else {
                window.location = "/";
            }
        }
    }

    function errorLogin(response) {
        var message = JSON.parse(response.responseText).message;
        $("#errorMessage").html(message);
        $("#submit").removeAttr("disabled");
    }

    $(document).ready(function () {
        $("#submit").click(function () {
            remember = $("#remember")[0].checked;
            $("#submit").attr("disabled", "true");
            var request = new Object({
                loginEmail: $("#inputEmail").val(),
                loginPassword: $("#inputPassword").val()
            });
            var str = JSON.stringify(request);
            Eventub.Auth.requestToken(str, successLogin, errorLogin);
        });
    });


});


