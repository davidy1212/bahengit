﻿define(['common/Eventub', 'backbone', 'jquery', 'text!feed/feed.html!strip'], function (Eventub, Backbone, $, page_html) {
    var Feed = Backbone.View.extend({
        _template: _.template(page_html),

        initialize: function () {
            console.log("feed initialized");
            this.listenTo(Eventub.GlobalEvents, 'nav:/feed', this.onNagivate);

            this.$el.html(this._template({ 'Eventub': Eventub }));
        },

        onNagivate: function () {
            console.log("feed navigated");
        },
    });

   

    return Feed;
});

