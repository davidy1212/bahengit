﻿define( ['jquery', 'jquery-cookie'], function(){

    function WebUtilities() {

    }

    WebUtilities.getUrlParams = function () {
        var urlParams = {};
        var e,
                    a = /\+/g,  // Regex for replacing addition symbol with a space
                    r = /([^&=]+)=?([^&]*)/g,
                    d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
                    q = window.location.search.substring(1);

        while (e = r.exec(q))
            urlParams[d(e[1])] = d(e[2]);

        return urlParams;
    };

    WebUtilities.getAccessToken = function () {
        var token = $.cookie("atok");
        var expire = $.cookie("atokex");
        if (!token) { return null; }
        if (new Date().getTime() > expire) {
            return null;
        }
        return token;
    };

    WebUtilities.requestToken = function (data, successHandler, errorHandler) {
        $.ajax({
            type: "POST",
            url: "/api/account/login",
            data: data,
            success: successHandler,
            error: errorHandler,
            dataType: "json",
            contentType: "application/json"
        });
    };

    WebUtilities.storeToken = function (response, remember) {
        if (response["message"] === "Success") {
            $.cookie("atok", response.token);
            $.cookie("atokex", new Date().getTime() + response.expires_in * 1000);
            if (remember) {
                $.cookie("reftok", response.refresh_token, { expires: 14, path: "/" });
            }
        }
    };

    WebUtilities.setAuthHeader = function (xhr) {
        xhr.setRequestHeader('Authorization', "Bearer " + WebUtilities.getAccessToken());
    };

    WebUtilities.tryRelogin = function (success, fail) {
        if (!WebUtilities.getAccessToken()) {
            var reftok = $.cookie("reftok");
            if (reftok) {
                WebUtilities.requestToken(
                    "{\"refreshToken\":\"" + reftok + "\"}",
                    function (response) {
                        WebUtilities.storeToken(response);
                        success();
                    }, fail);
            } else {
                fail();
            }
        }
        else {
            success();
        }
    };
    WebUtilities.logined = function () {
        var state;
        WebUtilities.tryRelogin(function () { state = true; }, function () { state = false; });
        return state;
    }
    WebUtilities.logout = function () {
        $.removeCookie("atok");
        $.removeCookie("reftok");
        $.removeCookie("atokdt");
        $.removeCookie("atokex");
    };

    WebUtilities.testLogout = function () {
        $.removeCookie("atok")
    };
    return WebUtilities;
});