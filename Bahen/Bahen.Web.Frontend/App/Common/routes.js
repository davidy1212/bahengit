﻿define(['backbone','common/Eventub'],
    function (Backbone,Eventub) {
        var routes = Backbone.Router.extend({
            routes: {
                "profile": "showProfile",
                "feed": "showFeed",
                "logout": "logout",
                "profile/followings": "showFollowings",
                "profile/events": "showEvents",
                "profile/followers" : "showFollowers",
                

       

            },

            showProfile: function () {
                var after = function () { Eventub.Url.trigger("/profile") };
                this.beforeEach(after);
               

            },
            showFollowers: function(){
                var after = function () { Eventub.Url.trigger("/profile/followers") };
                this.beforeEach(after);

            },
            showFollowings: function () {
                var after = function () { Eventub.Url.trigger("/profile/followings") };
                this.beforeEach(after);
            },
            showEvents: function () {
                var after = function () { Eventub.Url.trigger("/profile/events") };
                this.beforeEach(after);
            },

            showFeed: function () {
                var after = function () { Eventub.Url.trigger("/feed"); };
                this.beforeEach(after);
            },
            logout:function(){
                Eventub.Auth.logout("/welcome");
            },
            beforeEach: function (after) {
              /*  if (!Eventub.Auth.logined()) {
                    window.location = "/welcome";
                }*/
                if (!Eventub.Module.hasInstance('frame')) {
                    require(['frame/frame', 'common/Eventub'], function (Frame, Eventub) {
                        if (!Eventub.Module.hasInstance('frame')){
                            var frame = new Frame({ el: $(document.body) });
                            Eventub.Module.registerInstance("frame", frame, Frame);
                        }
                        after();
                    });
                } else {
                    after();
                }
            }
        });
        return routes;
});