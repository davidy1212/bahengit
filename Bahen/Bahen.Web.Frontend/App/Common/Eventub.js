﻿define(["backbone","jquery","underscore","jquery-cookie"], function(Backbone,$,_){
    console.log("Eventub loaded");
    var Eventub = {};
    
    (function (Eventub) {

        Eventub.Util = {
            HashTable : function () {
                this.length = 0;
                this.items = new Array();
                for (var i = 0; i < arguments.length; i += 2) {
                    if (typeof (arguments[i + 1]) != 'undefined') {
                        this.items[arguments[i]] = arguments[i + 1];
                        this.length++;
                    }
                }

                this.removeItem = function (in_key) {
                    var tmp_value;
                    if (typeof (this.items[in_key]) != 'undefined') {
                        this.length--;
                        var tmp_value = this.items[in_key];
                        delete this.items[in_key];
                    }
                    return tmp_value;
                }

                this.getItem = function (in_key) {
                    return this.items[in_key];
                }

                this.setItem = function (in_key, in_value) {
                    if (typeof (in_value) != 'undefined') {
                        if (typeof (this.items[in_key]) == 'undefined') {
                            this.length++;
                        }

                        this.items[in_key] = in_value;
                    }
                    return in_value;
                }

                this.hasItem = function (in_key) {
                    return typeof (this.items[in_key]) != 'undefined';
                }
            }
        }



        var instanceStore = new Eventub.Util.HashTable();
        var urlLoadStore = new Eventub.Util.HashTable();
        var moduleLoadQueue = new Array();

        var app_path = "";
        var csrfToken = "";

        this.routes = null;
        Eventub.GlobalEvents = _.clone(Backbone.Events);

       

        Eventub.Util = {
            setAppPath: function (path) {
                Eventub.app_path = path;
            },
            getDomContainerHtml: function (obj) {
                var id = (obj.id == undefined) ? "" : "id=\"" + obj.id + "\"";
                var htmlClass = (obj.class == undefined) ? "" : "id=\"" + obj.class + "\"";
                return "<div " + id+" "+ htmlClass+" ></div>";

            }
          
        }

        Eventub.Module = {
            registerInstance: function (instanceName,instance, moduleClass) {
                instanceStore.setItem(instanceName, { $el: null,'instance': instance, 'class': moduleClass });
            },

            hasInstance: function (instanceName) {
                
                return instanceStore.hasItem(instanceName);
            },

            deleteInstance: function (instanceName) {
                return instanceStore.removeItem(instanceName);
            },
            getInstance: function(instanceName){
                return instanceStore.getItem(instanceName).instance;
            },

            getInstanceClass:function (instanceName){
                return instanceStore.getItem(instanceName).class;
            },
          
            getNumberOfInstances: function () {
                return instanceStore.length;
            },
            queueModule: function (modulePath, name, options, el, success) {
                moduleLoadQueue.push({ 'path': modulePath, 'name': name, 'options': options, 'el': el, 'success': success });
            },
            queueLoad: function () {
                for (var i = 0; i < moduleLoadQueue.length; i++) {
                    this.loadModuleInstance(moduleLoadQueue[i].path,
                                            moduleLoadQueue[i].name,
                                            moduleLoadQueue[i].options,
                                            moduleLoadQueue[i].el,
                                            moduleLoadQueue[i].success);
                }
                moduleLoadQueue = new Array();
            },
            loadModuleInstance: function (modulePath, name, options, el, success, keys) {
                var instance;
                if (instance = instanceStore.getItem(name)) {
                    success(instance);
                } else {
                    require([modulePath], function (ModuleClass) {
                       if (!instanceStore.hasItem(name)){
                            if (el != '') options.el = $("#" + el);

                            if (options == null) {
                                var instance = new ModuleClass();
                            } else {
                                var instance = new ModuleClass(options);
                            }
                            Eventub.Module.registerInstance(name, instance, ModuleClass);
                       }
                        if (keys != null) {
                            delete urlLoadStore.getItem(keys['url'])[keys['id']];
                        }
                        if (success != null) success(instance);

                    });
                }
            }

        }
        Eventub.Url = {
            _loadState: 0,
            _loadParams: null,
            _loadUrl: null,
            _workingUrl:null,
            _urlBeginIndex: 0,
            _urlEndIndex:0,
            addLoadTrigger: function (path, name,options,el,url) {
                if (urlLoadStore.hasItem(url)) {
                    var list = urlLoadStore.getItem(url);
                } else {
                    var list = new Array();
                    urlLoadStore.setItem(url, list);
                }
                list.push({ 'path': path, 'name': name, 'options': options, 'el': el });
            },
            trigger: function (url, params) {
              
               
              
                this._urlBeginIndex = url.indexOf('/');
                this._urlEndIndex = this._urlBeginIndex;
                this._loadParams = params;
                this._loadUrl = url;
                
                console.log("seeing: " + "nav:" + Eventub.Url._loadUrl);
                this._triggerLoadModule();
            },
            //callback
            _triggerOnComplete: function () {
                
                Eventub.Url._loadState = Eventub.Url._loadState - 1;
                
                if (Eventub.Url._loadState <= 0 && Eventub.Url._workingUrl != null) {
                    console.log("firing: " + "nav:" + Eventub.Url._workingUrl);
                    Eventub.GlobalEvents.trigger("nav:" + Eventub.Url._workingUrl, Eventub.Url._loadParams);
                    Eventub.Url._workingUrl = null;
                    Eventub.Url._loadState = 0;
                    Eventub.Url._triggerLoadModule();
                }
               

            },
            _triggerLoadModule: function () {
                var moduleList;
                var i;
                var modules;
                var suburl;
                if (this._urlBeginIndex != -1 && this._urlEndIndex != -1) {
                    this._urlEndIndex = this._loadUrl.indexOf('/', this._urlEndIndex + 1);
                    if (this._urlEndIndex == -1) {
                        suburl = this._loadUrl;
                        this._urlBeginIndex = -1;
                    } else {
                        suburl = this._loadUrl.substring(this._urlBeginIndex, this._urlEndIndex);
                    }
                    this._workingUrl = suburl;
                    if (modules = urlLoadStore.getItem(suburl)) {
                        this._loadState = modules.length;

                        for (i = 0; i < modules.length; i++) {
                            if (modules[i] != undefined) {

                                Eventub.Module.loadModuleInstance(modules[i].path,
                                                                  modules[i].name,
                                                                  modules[i].options,
                                                                  modules[i].el,
                                                                  Eventub.Url._triggerOnComplete,
                                                                  { 'url': suburl, 'id': i });
                            } else {
                                Eventub.Url._triggerOnComplete();
                            }
                        }

                    } else {
                        Eventub.Url._triggerOnComplete();
                    }
                } else {
                    this._loadParams = null;
                    this._loadUrl = null;
                }

            },
          

        },
        Eventub.Classes = { 
            Element: function (template) {
                this._template = _.template(template);
            },
            ListView: function (element, collection, el,listEvents,success) {
                this.collection = collection;
                this.$el = $(el);
                if (listEvents != null) this.listEvents = listEvents;
                
                _.extend(this, Backbone.Events);
                var that = this;
                Eventub.Elements.getElement(element, function (elementInstance) {
                    that.element = elementInstance;
                    that.initialize();
                    if (success != null) success(this);
                });
            }


        }
        Eventub.Classes.Element.prototype = {
            render: function (data, idName, className) {
                this.$el = $(this._template(data));
                if (idName != null) this.$el.attr('id', idName);
                if (className != null) this.$el.addClass(className);
                return this.$el;
            }
        }
        Eventub.Classes.ListView.prototype = {
            initialize: function () {
                this.listenTo(this.collection, 'add', this.addOne);
                this.collection.forEach(this.addOne,this);
            },
            addOne: function (model) {
                var newElement = this.element.render(model.toJSON());
                if(this.listEvents!=null) this.listEvents(newElement);
                this.$el.append(newElement);
            }
        }




        Eventub.Elements = {
           


            renderElement: function (name, data,idName,className) {
                if (!Eventub.Module.hasInstance(name)) {
                    require('text!elements/' + name + '.html', function (template) {
                        var elementInstance = new Eventub.Classes.Element(template);
                        Eventub.Module.registerInstance(name, elementInstance, Eventub.Classes.Element);
                    });
                }
                var elementInstance = Eventub.Module.getInstance(name);
                 elementInstance.render(data);

            },
            getElement: function (name,success) {
                if (!Eventub.Module.hasInstance(name)) {
                    require(['text!elements/' + name + '.html!strip'], function (template) {
                        var elementInstance = new Eventub.Classes.Element(template);
                        Eventub.Module.registerInstance(name, elementInstance, Eventub.Classes.Element);
                        if (success != null) success(elementInstance);
                    });
                } else {
                    success( Eventub.Module.getInstance(name));
                }
            }
        }

        Eventub.Models = {
            User: Backbone.Model.extend({

                defaults: {
                    id: -1,
                    name: "",
                    firstName: "",
                    lastName: "",
                    email: "",
                    gender: "",
                    link: "",
                  
                },
                fetchUserDetails: function (options, success, error) {
                    var that = this;
                    $.ajax({
                        url: "api/user/" + this.id + "/info",
                        method: "GET",
                        contentType: "application/json",
                        data: options,
                        success: function (response) {
                            that.set(response);
                            if (success != null) success(that);

                        },
                        error: function (response) {
                            if (error != null) error(response);
                        }
                    });

                },
                fetchFollowings: function (options, success, error) {
                    $.ajax({
                        url: "api/user/" + this.id + "/followings",
                        method: "GET",
                        contentType: "application/json",
                        data: options,
                        success: function (response) {
                            if (success != null) success(response);

                        },
                        error: function (response) {
                            if (error != null) error(response);
                        }
                    });

                },

                fetchFollowers: function (options, success,error) {
                     $.ajax({
                        url: "api/user/" + this.id + "/followers",
                        method: "GET",
                        contentType: "application/json",
                        data: options,
                        success: function (response) {
                            if (success != null) success(response);

                        },
                        error: function (response) {
                            if (error != null) error(response);
                        }
                    });

                },
                fetchEvents :function(options,success,error){
                    $.ajax({
                        url: "api/user/"+this.id+"/events",
                        method: "GET",
                        contentType: "application/json",
                        data: options,
                        success: function (response) {
                            if (success!=null) success(response);
                        },
                        error: function (response) {
                            if (error!=null) error(response);
                        },
                        beforeSend: Eventub.Auth.setAuthHeader,
                    });

                },
                register: function (password1,password2,success, error) {
                    var payload = {
                        loginEmail: this.email,
                        loginPassword1: password1,
                        loginPassword2: password2,
                        firstName: this.firstName,
                        lastName: this.lastName,
                    }
                    $.ajax({
                        url: "api/account/register",
                        method: "POST",
                        contentType: "application/json",
                        data: payload,
                        success: function (response) {

                            success(response);
                        },
                        error: function(response){
                            error(response);
                        },
                        beforeSend: Eventub.Auth.setAuthHeader,
                    });

                },
            }),
            Event: Backbone.Model.extend({
                defaults: {
                    id: -1,
                    name: "",
                    startTime: "",
                    endTime: "",
                    rsvpStatus: "",
                },
            }),



        },

        Eventub.Collections = {
            Users: Backbone.Collection.extend({
                model: Eventub.Models.User,
            }),
            Events: Backbone.Collection.extend({
                model: Eventub.Models.Event,
            }),



        },

        Eventub.Objects = {
            currentUser : new Eventub.Models.User(),
        },

        Eventub.CurrentUser = {

            follow: function (id, success, error) {
                $.ajax({
                    url: "api/user/" + id + "/follow",
                    method: "POST",
                    contentType: "application/json",
                    data: null,
                    beforeSend: Eventub.Auth.setAuthHeader,
                    success: function (response) {
                        success(response);
                    },
                    error: function (response) {
                        error(response);
                    },
                });

            },


        }
        Eventub.Auth = {

            setCsrfToken: function(csrfToken){
                Eventub.csrfToken = csrfToken;

            },
              getUrlParams : function () {
                    var urlParams = {};
                    var e,
                                a = /\+/g,  // Regex for replacing addition symbol with a space
                                r = /([^&=]+)=?([^&]*)/g,
                                d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
                                q = window.location.search.substring(1);

                    while (e = r.exec(q))
                        urlParams[d(e[1])] = d(e[2]);

                    return urlParams;
                },

            getAccessToken: function () {
                var token = $.cookie("atok");
                var expire = $.cookie("atokex");
                if (!token) { return null; }
                if (new Date().getTime() > expire) {
                    return null;
                }
                return token;
            },
            retrieveCurrentUser: function (success_func) {
               /* $.ajax({
                    url: "/api/user/me",
                    type: "GET",
                    contentType: "application/json",
                    beforeSend: Eventub.Auth.setAuthHeader,
                    success: function (response) {
                        Eventub.Objects.currentUser.set(response);
                        success_func(Eventub.Objects.currentUser);
                        console.log("Current user retrieved:"+ JSON.stringify(Eventub.Objects.currentUser.toJSON()));
                    },
                    error: function (error) {
                        Eventub.Objects.currentUser.clear().set(Eventub.Objects.currentUser.defaults);
                    

                });*/

                Eventub.Objects.currentUser.set({ id: 2 });
                Eventub.Objects.currentUser.fetchUserDetails({}, success_func);
                



            },
            getCurrentUser: function (success) {
                if (Eventub.Objects.currentUser.get('id') ) {
                    this.retrieveCurrentUser(success);
                }
                console.log("Access Code: " + Eventub.Auth.getAccessToken());
                return Eventub.Objects.currentUser;
            },
            requestToken: function (data, successHandler, errorHandler) {
                
                $.ajax({
                    type: "POST",
                    url: "/api/account/login",
                    data: data,
                    success: successHandler,
                    error: errorHandler,
                    dataType: "json",
                    contentType: "application/json"
                });
            },

            storeToken : function (response, remember) {
                if (response["message"] === "Success") {
                    $.cookie("atok", response.token);
                    $.cookie("atokex", new Date().getTime() + response.expires_in * 1000);
                    if (remember) {
                        $.cookie("reftok", response.refresh_token, { expires: 14, path: "/" });
                    }
                }
            },

            setAuthHeader: function (xhr) {

                xhr.setRequestHeader('Authorization', "Bearer " + Eventub.Auth.getAccessToken());
                xhr.setRequestHeader('X-CSRFTOKEN', Eventub.csrfToken);
            },

            tryRelogin : function (success, fail) {
                if (!this.getAccessToken()) {
                    var reftok = $.cookie("reftok");
                    if (reftok) {
                        this.requestToken(
                            "{\"refreshToken\":\"" + reftok + "\"}",
                            function (response) {
                                this.storeToken(response);
                                this.retrieveCurrentUser();
                                success();
                            }, fail);
                    } else {
                        fail();
                    }
                }
                else {
                    success();
                }
            },
           logined :function () {
                var state;
                this.tryRelogin(function () { state = true; }, function () { state = false; });
                
                return state;
            },
            logout : function (redirectUrl) {
                $.removeCookie("atok");
                $.removeCookie("reftok");
                $.removeCookie("atokdt");
                $.removeCookie("atokex");
                window.location = redirectUrl;
            },
          

            testLogout : function () {
                $.removeCookie("atok")
            },

        }
    })(Eventub);
   
    return Eventub;
});