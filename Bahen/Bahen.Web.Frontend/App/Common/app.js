﻿requirejs.config({
    baseUrl: '/App',
    shim: {
        'backbone': {
            //These script dependencies should be loaded before loading
            //backbone.js
            deps: ['underscore', 'jquery'],
            //Once loaded, use the global 'Backbone' as the
            //module value.
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        },
        'jquery.cookie': {     //<-- cookie depends on Jquery and exports nothing
            deps: ['jquery']
        },
       
    },
    paths:{
        'jquery' :'/lib/jquery/jquery',
        'backbone': '/lib/backbone/backbone',
        'underscore': '/lib/underscore/underscore',
        'jquery-cookie':'/lib/jquery-cookie/jquery.cookie',
    }
});



require(["common/Eventub",  "common/routes", 'backbone'],
    function (Eventub, Routes, Backbone) {
        Eventub.Auth.setCsrfToken(csrfToken);
        Eventub.Util.setAppPath('/App/');
        var routes = new Routes();
        Backbone.history.start();
        Eventub.routes = routes;
        Eventub.Auth.retrieveCurrentUser(function (currentUser) {
            Eventub.routes.navigate("profile", true);
        });

    });
