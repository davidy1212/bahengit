﻿define(['common/Eventub', 'backbone', 'jquery', 'text!profile_pane/profile_summary/profile_summary.html!strip'], function (Eventub, Backbone, $, template) {

    var Profile_Summary = {

        _template: _.template(template),

        initialize: function () {
            console.log("profile_summary loaded");
            this.render();
            this.listenTo(this.model, "change", this.render);
        },
        render: function () {
            this.$el.html(this._template(this.model.toJSON()));
            return this;

        },


    }


    return Backbone.View.extend(Profile_Summary);
});

