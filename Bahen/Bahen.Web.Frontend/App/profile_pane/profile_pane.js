define(['common/Eventub', 'backbone', 'jquery', 'text!profile_pane/profile_pane.html!strip'], function (Eventub, Backbone, $, page_html) {
   

    var Profile_Pane = Backbone.View.extend({

        _template: _.template(page_html),
        _currentUser: Eventub.Auth.getCurrentUser(),

        initialize: function () {  //load models, inject to dom,
            this.listenTo(Eventub.GlobalEvents, 'nav:/profile', this.profileNavigated);
            this.listenTo(Eventub.GlobalEvents, 'nav:/profile/followings', this.showFollowings);
            this.listenTo(Eventub.GlobalEvents, 'nav:/profile/events', this.showEvents);
            this.listenTo(Eventub.GlobalEvents, 'nav:/profile/followers', this.showFollowers);
            console.log("profile pane loaded");

            this.$el.html(this._template({"Eventub":Eventub}));
            Eventub.Module.queueLoad();

          
            console.log(this._currentUser);
            Eventub.GlobalEvents.trigger('nav:/profile/followings');

            $('.nav li a').on('click', function () {
                $(this).parent().parent().find('.active').removeClass('active');
                $(this).parent().addClass('active').css('font-weight', 'bold');
            });
        
        },
        

        profileNavigated: function () {
            console.log("profile_pane onNavigate");
        },

        showFollowings: function () {

            var followingCollectionEvents = function (element_dom) {
                element_dom.click(function () {
                    Eventub.GlobalEvents.trigger(')

                });
            }
            if (this._followingsCollection == null) {
                this._followingsCollection = new Eventub.Collections.Users();
                this._followingsListView = new Eventub.Classes.ListView('userListViewCard', this._followingsCollection, '#followings_list', followingCollectionEvents);

                var that = this;
                var success = function (response) {
                    that._followingsCollection.add(response['data']);
                }
                this._currentUser.fetchFollowings({ 'limit': 10, 'offset': 0 }, success);
            } else {

            }
            $("#followings_list").show();
            $("#followers_list").hide();
            $("#events_list").hide();

        },

        showFollowers: function () {
            if (this._followersCollection == null) {
                this._followersCollection = new Eventub.Collections.Users();
                this._followersListView = new Eventub.Classes.ListView('userListViewCard', this._followersCollection, '#followers_list');

                var that = this;
                var success = function (response) {
                    that._followersCollection.add(response['data']);
                }
                this._currentUser.fetchFollowers({ 'limit': 10, 'offset': 0 }, success);
            } else {

            }
            $("#followings_list").hide();
            $("#events_list").hide();
            $("#followers_list").show();

        },

        showEvents: function () {
            if (this._eventsCollection == null) {
                this._eventsCollection = new Eventub.Collections.Users();
                this._eventsListView = new Eventub.Classes.ListView('eventListViewCard', this._eventsCollection, '#events_list');

                var that = this;
                var success = function (response) {
                    that._eventsCollection.add(response['data']);
                }

                this._currentUser.fetchEvents({ 'limit': 10 },success);


            } else {

            }
            $("#followings_list").hide();
            $("#followers_list").hide();
            $("#events_list").show();
        }

    });



    
    return Profile_Pane;
});

