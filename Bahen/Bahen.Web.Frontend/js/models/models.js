var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Models;
(function (Models) {
    var Event = (function (_super) {
        __extends(Event, _super);
        function Event(id) {
                _super.call(this);
            this.id = id;
            this.url = function () {
                return "/api/event/" + id;
            };
        }
        return Event;
    })(Backbone.Model);
    Models.Event = Event;    
    var Header = (function (_super) {
        __extends(Header, _super);
        function Header(option) {
                _super.call(this);
            this.option = option;
        }
        return Header;
    })(Backbone.Model);
    Models.Header = Header;    
})(Models || (Models = {}));
