var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Components;
(function (Components) {
    var Header = (function (_super) {
        __extends(Header, _super);
        function Header() {
            _super.apply(this, arguments);

        }
        return Header;
    })(Backbone.View);
    Components.Header = Header;    
    var LoginStateButton = (function (_super) {
        __extends(LoginStateButton, _super);
        function LoginStateButton() {
            _super.apply(this, arguments);

        }
        return LoginStateButton;
    })(Backbone.View);
    Components.LoginStateButton = LoginStateButton;    
})(Components || (Components = {}));
