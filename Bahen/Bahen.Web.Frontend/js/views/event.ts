/// <reference path="../../lib/backbone/backbone.d.ts"/>
/// <reference path="../models/models.ts"/>

module Components {
    export class EventDetailPane extends Backbone.View {
        constructor(public el: HTMLElement, public model: Models.Event) {
            super({ el: el });
        }
    }
}