var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Components;
(function (Components) {
    var EventDetailPane = (function (_super) {
        __extends(EventDetailPane, _super);
        function EventDetailPane(el, model) {
                _super.call(this, {
        el: el
    });
            this.el = el;
            this.model = model;
        }
        return EventDetailPane;
    })(Backbone.View);
    Components.EventDetailPane = EventDetailPane;    
})(Components || (Components = {}));
