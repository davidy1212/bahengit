﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using System.Net;
using System.IO;
using Bahen.DataOperationLayer.Blob;
using System.Drawing;
using Bahen.Web.Images;

namespace Bahen.Web
{
    public partial class EditUser : System.Web.UI.Page
    {
        public UserEntity UserEntity { get; set; }
        public bool CanEdit { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                // This page has to have a parameter of event id
                if (String.IsNullOrEmpty((string)RouteData.Values["uid"]))
                {
                    Response.Write("Invalid user id");
                    return;
                }

                string userIdString = (string)RouteData.Values["uid"];

                // Check if the id is numeric
                long userID;
                if (!Int64.TryParse(userIdString, out userID))
                {
                    Response.Write("Invalid user id");
                    return;
                }

                // Check if the event exists
                UserOperator userOperator = new UserOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                UserEntity userEntity;
                if (!userOperator.TryGet(userID, out userEntity))
                {
                    Response.Write("Invalid user id");
                    return;
                }
                this.UserEntity = userEntity;

                // Check if the user is the administrator of the cycle
                this.CanEdit = false;

                if (Session.GetAuthenticatedUser().ID == UserEntity.ID)
                {
                    this.CanEdit = true;
                    Session["User"] = UserEntity;
                }
                else
                {
                    //if (Session.GetAuthenticatedUser().UserGroup == DataModels.Constants.Privacy.UserRoleLevel.Admin)
                    //{
                    //    this.CanEdit = true;
                    //}
                }

                if (!CanEdit)
                {
                    Response.Write("You don't have permission to edit the cycle");
                    return;
                }

                // Check if needs to delete the event or edit
                if (Request.QueryString["delete"] == "1")
                {
                    // Logout user if it is deleting the current loggedin user
                    if (this.UserEntity.ID == Session.GetAuthenticatedUser().ID)
                    {
                        Session["User"] = null;
                    }
                    userOperator.Remove(this.UserEntity);
                    if (Request.QueryString["redirect"] != null)
                    {
                        Response.Redirect(Request.QueryString["redirect"]);
                    }
                    else
                    {
                        Response.Redirect("/users/" + (Session["User"] as IEntity).ID);
                    }
                    return;
                }

                // Handle request from itself
                if (Request.Params["FirstName"] != null)
                {
                    // Copy the field
                    this.UserEntity.FirstName = Request.Params["FirstName"];
                    this.UserEntity.LastName = Request.Params["LastName"];
                    this.UserEntity.PreferredName = this.UserEntity.FirstName + (String.IsNullOrEmpty(this.UserEntity.LastName) ? "" : (" " + this.UserEntity.LastName));

                    // Upload profile picture
                    if (ProfilePictureUploadControl.HasFile)
                    {
                        string extension = Path.GetExtension(ProfilePictureUploadControl.PostedFile.FileName).ToLowerInvariant();
                        if (extension == ".jpg" || extension == ".png" || extension == ".bmp" || extension == ".gif")
                        {
                            try
                            {
                                // Convert to png format
                                System.Drawing.Image original = System.Drawing.Image.FromStream(ProfilePictureUploadControl.PostedFile.InputStream);

                                BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
                                    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                                Guid pictureID = Guid.NewGuid();

                                // Upload medium size picture
                                using (Stream uploadStreamLargeSize = new MemoryStream())
                                {
                                    System.Drawing.Image largeSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Medium);
                                    largeSizeImage.Save(uploadStreamLargeSize, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamLargeSize.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "u",
                                        this.UserEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Medium,
                                        uploadStreamLargeSize);
                                }

                                // Remember the picture ID
                                //this.UserEntity.ProfilePicture = pictureID;

                                // Upload thumbnail size picture
                                using (Stream uploadStreamThumbnail = new MemoryStream())
                                {
                                    System.Drawing.Image thumbnailSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Thumb);
                                    thumbnailSizeImage.Save(uploadStreamThumbnail, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamThumbnail.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "u",
                                        this.UserEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Thumb,
                                        uploadStreamThumbnail);
                                }
                            }
                            catch
                            {
                                Response.Write("Invalid image format");
                                return;
                            }
                        }
                        else
                        {
                            Response.Write("Invalid image format");
                            return;
                        }
                    }

                    userOperator.Update(this.UserEntity);


                    Response.Redirect("/users/" + this.UserEntity.ID);
                }
            }
        }
    }
}