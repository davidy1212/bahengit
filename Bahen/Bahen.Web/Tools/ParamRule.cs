﻿using Bahen.DataModels.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public struct ParamRule
    {
        public ParamRule(Type type, RequestMethod requestMethod)
        {
            this.Type = type;
            this.RequestMethod = requestMethod;
        }
        public Type Type;
        public RequestMethod RequestMethod;
    }
}