﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class UriTools
    {
        /// <summary>
        /// Add a pair of query string to the uri
        /// </summary>
        /// <param name="uri">Uri</param>
        /// <param name="key">Key of the query string</param>
        /// <param name="value">Value of the query string</param>
        /// <returns>Combined url</returns>
        public static string AddQueryString(this Uri uri, string key, string value)
        {
            return uri.PathAndQuery + (String.IsNullOrEmpty(uri.Query) ? "?" : "&") + key + "=" + value;
        }
    }
}