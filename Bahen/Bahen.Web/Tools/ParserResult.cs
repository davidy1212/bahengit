﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public struct ParserResult
    {
        public Object Value;
        public bool IsValid;
    }

}