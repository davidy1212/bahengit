﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class Memory
    {
        public static void SetObject(this HttpApplicationState application, string key, object item)
        {
            application[key] = item;
        }
        public static T GetObject<T>(this HttpApplicationState application, string key)
        {
            return (T)application[key];
        }
    }
}