using Bahen.DataModels;
using Bahen.DataModels.Sql;
using Bahen.DataOperationLayer;
using Bahen.DataOperationLayer.Sql;
using Bahen.Web.Common.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Bahen.Web.Tools
{
    public static class Authentication
    {
        static string sqlConnectionString = ConfigurationManager.ConnectionStrings["Sql"].ConnectionString;
        static string tableConnectionString = ConfigurationManager.ConnectionStrings["Table"].ConnectionString;

        /// <summary>
        /// Try to authenticate the user and return the authentication status. 
        /// Will redirect to login page if the user is not already logged in or auto-login from cookies failed.
        /// </summary>
        /// <param name="context">Current http context</param>
        /// <returns>Returns true if login succeded. Returns false if failed to redirect to the login page</returns>
        public static bool TryAuthenticate(this HttpContext context)
        {
            return TryAuthenticate(context, true);
        }

        /// <summary>
        /// Try to authenticate the user and return the authentication status.
        /// </summary>
        /// <param name="context">Current http context</param>
        /// <param name="forceRedirect">Whether force redirect to login page if not logged in</param>
        /// <returns>If the login succeeded</returns>
        public static bool TryAuthenticate(this HttpContext context, bool forceRedirect)
        {
            if (context.Session["User"] == null)
            {
                if (!TryLoginFromCookies(context))
                {
                    if (forceRedirect)
                    {
                        context.Response.Redirect(String.Format(@"/login?redirect={0}", context.Request.Url.PathAndQuery));
                    }
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Try login with credential saved in context.Request
        /// </summary>
        /// <param name="context">Current http context</param>
        /// <returns></returns>
        public static LoginResult TryLogin(this HttpContext context)
        {
            // Check if is already logged in
            if (context.Session.IsAuthenticated())
            {
                return LoginResult.AlreadyLoggedIn;
            }

            /*
             * Request has following parameters
             * Login email
             * Login password
             * Remember me
             */
            string loginEmail = context.Request.Params["LoginEmail"];

            if (String.IsNullOrEmpty(loginEmail))
            {
                return LoginResult.InvalidCredential;
            }

            string loginPassword = context.Request.Params["LoginPassword"];
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["Sql"].ConnectionString;
            string tableConnectionString = ConfigurationManager.ConnectionStrings["Table"].ConnectionString;
            UserOperator userOperator = new UserOperator(sqlConnectionString, tableConnectionString);

            UserEntity userEntity;

            // Check if login email is in the database
            if (!userOperator.TryGet(loginEmail, out userEntity))
            {
                return LoginResult.InvalidCredential;
            }

            // Check password
            if (!Bahen.Common.HashUtilities.AreEqual(
                Bahen.Common.HashUtilities.ComputeHashSecure(loginPassword),
                userEntity.LoginPassword))
            {
                return LoginResult.InvalidCredential;
            }

            // Add remember me
            context.Session["User"] = userEntity;
            string rememberMe = context.Request.Params["RememberMe"];
            if (rememberMe == "on")
            {
                Authentication.SetAutoLoginCookies(context, userEntity);
            }

            return LoginResult.Successful;
        }

        /// <summary>
        /// Check if is currently authenticated
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static bool IsAuthenticated(this HttpSessionState session)
        {
            return session["User"] as UserEntity != null;
        }

        /// <summary>
        /// Get the currently authenticated user entity reference
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static UserEntity GetAuthenticatedUser(this HttpSessionState session)
        {
            return session["User"] as UserEntity;
        }

        /// <summary>
        /// Get the currently authenticated user entity id
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static long GetAuthenticatedUserID(this HttpSessionState session)
        {
            if (GetAuthenticatedUser(session) == null)
            {
                return 0;
            }
            else
            {
                return (GetAuthenticatedUser(session).ID);
            }
        }

        /// <summary>
        /// Logout the current user
        /// </summary>
        /// <param name="Request"></param>
        /// <param name="Response"></param>
        /// <param name="Session"></param>
        public static void Logout(this HttpContext context)
        {
            if (context.Session["User"] != null)
            {
                if (context.Request.Cookies["rID"] != null && context.Request.Cookies["rTo"] != null && context.Request.Cookies["rt"] != null)
                {
                    var cookieEntity = new SqlCookieEntity();
                    var cookieOperator = new SqlCookieOperator(sqlConnectionString);
                    System.Guid cookieID = new System.Guid();
                    if (System.Guid.TryParse(context.Request.Cookies["rID"].Value, out cookieID))
                    {
                        if (cookieOperator.TryGet(cookieID, out cookieEntity))
                        {
                            if (cookieEntity.UserID == (context.Session["User"] as UserEntity).ID)
                            {
                                cookieOperator.Remove(cookieEntity);
                            }
                        }
                    }
                }
            }
            context.Session["User"] = null;
            DeleteAutoLoginCookies(context.Response);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Response"></param>
        /// <param name="Request"></param>
        /// <param name="userEntity"></param>
        public static void SetAutoLoginCookies(HttpContext context, UserEntity userEntity)
        {
            var cookieOperator = new SqlCookieOperator(sqlConnectionString);
            SqlCookieEntity cookie = new SqlCookieEntity();
            cookie.CookieID = System.Guid.NewGuid();
            cookie.UserID = userEntity.ID;
            cookie.UserAgent = context.Request.UserAgent;
            cookie.TimeLastLogin = DateTime.Now;
            cookieOperator.Insert(cookie);

            DateTime currentTime = cookie.TimeLastLogin;
            context.Response.Cookies["rID"].Value = Convert.ToString(cookie.CookieID);         // Cookie id
            context.Response.Cookies["rID"].Expires = currentTime.AddDays(30);
            context.Response.Cookies["rTo"].Value = Bahen.Common.HashUtilities.ComputeHashInsecure(userEntity.ID);     // Cookie Token
            context.Response.Cookies["rTo"].Expires = DateTime.Now.AddDays(30);
            context.Response.Cookies["rt"].Value = Bahen.Common.HashUtilities.ComputeHashInsecure(Convert.ToString(cookie.TimeLastLogin));                           // Last login time
            context.Response.Cookies["rt"].Expires = DateTime.Now.AddDays(30);
        }

        private static bool TryLoginFromCookies(HttpContext context)
        {
            if (context.Request.Cookies["rID"] != null && context.Request.Cookies["rTo"] != null && context.Request.Cookies["rt"] != null)
            {
                var cookieEntity = new SqlCookieEntity();
                var cookieOperator = new SqlCookieOperator(sqlConnectionString);
                System.Guid cookieID = new System.Guid();
                if (System.Guid.TryParse(context.Request.Cookies["rID"].Value, out cookieID))
                {
                    if (cookieOperator.TryGet(cookieID, out cookieEntity))
                    {
                        if (Bahen.Common.HashUtilities.ComputeHashInsecure(cookieEntity.UserID) == context.Request.Cookies["rTo"].Value &&
                           Bahen.Common.HashUtilities.ComputeHashInsecure(Convert.ToString(cookieEntity.TimeLastLogin)) == context.Request.Cookies["rt"].Value &&
                            cookieEntity.UserAgent == context.Request.UserAgent)
                        {
                            UserOperator userOperator = new UserOperator(sqlConnectionString, tableConnectionString);
                            UserEntity userEntity;
                            if (userOperator.TryGet(cookieEntity.UserID, out userEntity))
                            {
                                context.Session["User"] = userEntity;
                                DateTime loginTime = DateTime.Now;
                                cookieEntity.TimeLastLogin = loginTime;
                                cookieOperator.Update(cookieEntity);

                                context.Response.Cookies["rt"].Value = Bahen.Common.HashUtilities.ComputeHashInsecure(Convert.ToString(loginTime));
                                context.Response.Cookies["rt"].Expires = loginTime.AddDays(30);
                                context.Response.Cookies["rTo"].Value = context.Request.Cookies["rTo"].Value;
                                context.Response.Cookies["rTo"].Expires = loginTime.AddDays(30);
                                context.Response.Cookies["rID"].Value = context.Request.Cookies["rID"].Value;
                                context.Response.Cookies["rID"].Expires = loginTime.AddDays(30);
                                return true;
                            }
                            else
                            {
                                DeleteAutoLoginCookies(context.Response);
                                return false;
                            }
                        }
                        else
                        {
                            DeleteAutoLoginCookies(context.Response);
                            return false;
                        }
                    }
                    else
                    {
                        DeleteAutoLoginCookies(context.Response);
                        return false;
                    }
                }
                else
                {
                    DeleteAutoLoginCookies(context.Response);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private static void DeleteAutoLoginCookies(HttpResponse Response)
        {
            Response.Cookies["rID"].Expires = DateTime.Now.AddDays(-1); // Delete invalid cookies
            Response.Cookies["rTo"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["rt"].Expires = DateTime.Now.AddDays(-1);
        }
    }
}