﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class Cache
    {
        public static bool IsClientCached(HttpContext context, DateTime contentModified)
        {
            string header = context.Request.Headers["If-Modified-Since"];

            if (header != null)
            {
                DateTime isModifiedSince;
                if (DateTime.TryParse(header, out isModifiedSince))
                {
                    return isModifiedSince > contentModified;
                }
            }

            return false;
        }
    }
}