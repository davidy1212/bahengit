﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class HyperLinks
    {
        public static string GetUserProfileUrl(long userID)
        {
            return String.Format("/users/{0}", userID);
        }

        public static string GetUserProfilePictureThumbnailUrl(long userID, Guid pictureGuid)
        {
            return String.Format("/Images/ProfilePictureHandler.ashx?prefix={0}&id={1}&guid={2}&size=thumbnail",
                "u", userID, pictureGuid);
        }
    }
}