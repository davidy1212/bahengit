﻿using Bahen.DataModels.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public class RequestQueryParser
    {
        public Dictionary<string, ParserResult> Parse(Dictionary<string, ParamRule> paramRuleDict, HttpRequest Request)
        {
            Dictionary<string, ParserResult> Result = new Dictionary<string, ParserResult>();
            
            foreach (var item in paramRuleDict)
            {
                string valueToParse = null;
                bool isValid;
                Object valueParsed = null;

                switch (item.Value.RequestMethod)
                {
                    case RequestMethod.GetOrPost:
                        valueToParse = Request.Params[item.Key];
                        break;
                    case RequestMethod.Get:
                        valueToParse = Request.QueryString[item.Key];
                        break;
                    case RequestMethod.Post:
                        valueToParse = Request.Form[item.Key];
                        break;
                    default:
                        break;
                }
                if (item.Value.Type.Equals(typeof(bool)))
                {
                    if (String.IsNullOrEmpty(valueToParse))
                    {
                        valueParsed = false;
                        isValid = true;
                    }
                    else
                    {
                        try
                        {
                            valueParsed = Convert.ToBoolean(valueToParse);
                            isValid = true;
                        }
                        catch (System.FormatException)
                        {
                            isValid = false;
                        }
                    }
                }
                else if (item.Value.Type.Equals(typeof(long)))
                {
                    if (String.IsNullOrEmpty(valueToParse))
                    {
                        valueParsed = null;
                        isValid = false;
                    }
                    else
                    {
                        try
                        {
                            valueParsed = Convert.ToInt64(valueToParse);
                            isValid = true;
                        }
                        catch (System.FormatException)
                        {
                            isValid = false;
                        }
                        catch (System.OverflowException)
                        {
                            isValid = false;
                        }
                    }
                }
                else if (item.Value.Type.Equals(typeof(int)))
                {
                    if (String.IsNullOrEmpty(valueToParse))
                    {
                        valueParsed = null;
                        isValid = false;
                    }
                    else
                    {
                        try
                        {
                            valueParsed = Convert.ToInt32(valueToParse);
                            isValid = true;
                        }
                        catch (System.FormatException)
                        {
                            isValid = false;
                        }
                        catch (System.OverflowException)
                        {
                            isValid = false;
                        }
                    }
                }
                else if (item.Value.Type.Equals(typeof(Guid)))
                {
                    Guid targetGuid;
                    if (Guid.TryParseExact(valueToParse, "N", out targetGuid))
                    {
                        isValid = true;
                        valueParsed = targetGuid;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                else if (item.Value.Type.Equals(typeof(string)))
                {
                    if (String.IsNullOrEmpty(valueToParse))
                    {
                        valueParsed = null;
                        isValid = false;
                    }
                    else
                    {

                        valueParsed = valueToParse;
                        isValid = true;

                    }
                }
                else
                {
                    isValid = false;
                }


                ParserResult parserResult = new ParserResult();
                parserResult.Value = valueParsed;
                parserResult.IsValid = isValid;
                Result.Add(item.Key, parserResult);
            }
            return Result;
        }
    }
}