﻿using Bahen.DataModels.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class GuidOrIDTester
    {
        public static string GuidOrID(int tc)
        {
            string guid = "Guid";
            string id = "ID";
            string error = "Neither";
            EntityTypeCode typeCode = (EntityTypeCode)tc;
            switch (typeCode)
            {
                case EntityTypeCode.NewsFeed:
                    return guid;
                    break;
                case EntityTypeCode.Comment:
                    return guid;
                    break;
                case EntityTypeCode.Event:
                    return id;
                    break;
                case EntityTypeCode.Cycle:
                    return id;
                    break;
                case EntityTypeCode.User:
                    return id;
                    break;
                case EntityTypeCode.Like:
                    return guid;
                    break;
                default:
                    //return error;
                    throw new Exception("TypeCode not Defined");
                    break;
            }
        }
    }
}