﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Constants.Privacy;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bahen.Web.Tools
{
    public static class Privacy
    {
        public static bool IsProfileVisible(this UserEntity me, UserEntity targetUser, out UserProfileNotVisibleReason reason)
        {
            reason = ProfileNotVisibleReason(me, targetUser);
            if (reason == UserProfileNotVisibleReason.Visible)
            {
                return true;
            }
            return false;
        }

        public static bool IsProfileVisible(this UserEntity me, UserEntity targetUser)
        {
            if (ProfileNotVisibleReason(me, targetUser) == UserProfileNotVisibleReason.Visible)
            {
                return true;
            }
            return false;
        }

        public static bool IsProfileVisible(this UserEntity me, long targetUserID)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            UserEntity targetUser = new UserEntity();
            if (userOperator.TryGet(targetUserID, out targetUser))
            {
                return IsProfileVisible(me, targetUser);
            }
            return false;
        }

        public static bool IsProfileVisible(this UserEntity me, long targetUserID, out UserProfileNotVisibleReason reason)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            UserEntity targetUser = new UserEntity();
            if (userOperator.TryGet(targetUserID, out targetUser))
            {
                return IsProfileVisible(me, targetUser, out reason);
            }
            reason = UserProfileNotVisibleReason.Other;
            return false;
        }

        private static UserProfileNotVisibleReason ProfileNotVisibleReason(this UserEntity me, UserEntity targetUser)
        {
            UserProfileNotVisibleReason reason = UserProfileNotVisibleReason.Other;

            if (me == null)
            {
                if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.Public))
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                return UserProfileNotVisibleReason.MustLogIn;
            }

            if (me.ID == targetUser.ID)
            {
                return UserProfileNotVisibleReason.Visible;
            }

            // Can only block user currently
            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.BlockedEntities))
            {
                if (targetUser.ProfileBlockedEntities.Contains(me.UniversalID))
                {
                    return UserProfileNotVisibleReason.Blocked;
                }
            }

            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.Public))
            {
                return UserProfileNotVisibleReason.Visible;
            }
            
            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.EveryoneLogedIn))
            {
                return UserProfileNotVisibleReason.Visible;
            }

            // Allowed users will be able to view even if the visibility level is OnlyMe
            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.AllowedEntities))
            {
                if (targetUser.ProfileAllowedEntities.Contains(me.UniversalID))
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                
                // Test allowed cycles
                if (targetUser.ProfileAllowedEntities.Intersect(me.SqlEntity.CycleUsers.Select(c => ComputeUID(c.CycleID, EntityTypeCode.Cycle))).Any())
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                reason = UserProfileNotVisibleReason.NotInAllowList;
            }

            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.OnlyMe))
            {
                return UserProfileNotVisibleReason.OnlyOwnerCanView;
            }

            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.MeSubscribed))
            {
                if (targetUser.SqlEntity.SubscribersSubscriptions.Select(c => c.HostID).Contains(me.ID))
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                reason = UserProfileNotVisibleReason.OwnerMustSubscribeYou;
            }

            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.SubscribedMe))
            {
                if (targetUser.SqlEntity.HostSubscriptions.Select(c => c.SubscriberID).Contains(me.ID))
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                reason = UserProfileNotVisibleReason.YouMustSubscribeOwner;
            }

            if (((UserProfileVisibilityLevel)targetUser.ProfilePrivacyLevel).HasFlag(UserProfileVisibilityLevel.MeSubscribedAndSubscribedMe))
            {
                if (targetUser.SqlEntity.HostSubscriptions.Select(c => c.SubscriberID).Contains(me.ID) &&
                    targetUser.SqlEntity.SubscribersSubscriptions.Select(c => c.HostID).Contains(me.ID))
                {
                    return UserProfileNotVisibleReason.Visible;
                }
                reason = UserProfileNotVisibleReason.MustMutualSubscribe;
            }

            return reason;
        }

        

        public static string ComputeUID(long id, EntityTypeCode type)
        {
            return type.ToString() + id.ToString();
        }
    }
}