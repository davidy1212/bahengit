﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using System.Configuration;

namespace Bahen.Web
{
    public partial class Header : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write(GetHeader());
        }

        public string GetHeader()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<div id='header'>");
            stringBuilder.Append(String.Format("<a href='/'>{0}</a>",
            ConfigurationManager.AppSettings["SiteName"]));
            if (Session.IsAuthenticated())
            {
                stringBuilder.Append(String.Format("<div class='header-link' onclick=\"location.href='/users/{0}'\"><img src='/Images/profile.png' width='15px'/></div>", Session.GetAuthenticatedUserID()));
                stringBuilder.Append("<div class='header-link' id='notificationButton'><img src='/Images/message.png' width='20px' style='margin-top:3px;'/></div>");
                stringBuilder.Append(@"<script type='text/javascript'>
        $(function () {
            var notificationButton = new Bahen.Web.NotificationDropdownMenu($('#notificationButton'), '/Ajax/NotificationHandler.ashx');
});</script>");
            }
            else
            {
                stringBuilder.Append("<div class='header-link' onclick=\"location.href='/login'\">Login</div>");
            }
            stringBuilder.Append("</div>");
            return stringBuilder.ToString();
        }
    }
}