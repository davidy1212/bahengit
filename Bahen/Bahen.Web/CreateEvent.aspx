﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="Bahen.Web.CreateEvent"%>

<!DOCTYPE html>

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

    <!-- Skin CSS file -->
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/assets/skins/sam/skin.css" />
    <!-- Utility Dependencies -->
    <script src="http://yui.yahooapis.com/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/element/element-min.js"></script>
    <!-- Needed for Menus, Buttons and Overlays used in the Toolbar -->
    <script src="http://yui.yahooapis.com/2.9.0/build/container/container_core-min.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/menu/menu-min.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/button/button-min.js"></script>
    <!-- Source file for Rich Text Editor-->
    <script src="http://yui.yahooapis.com/2.9.0/build/editor/editor-min.js"></script>


    <script type="text/javascript">

        function htmlEncode(value) {
            //create a in-memory div, set it's inner text(which jQuery automatically encodes)
            //then grab the encoded contents back out.  The div never exists on the page.
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $('<div/>').html(value).text();
        }

        $(function () {
            $("#StartDatePicker").datepicker();
            $("#EndDatePicker").datepicker();

            $("#CycleList_0").click(function () {
                if ($(this).attr('checked') === 'checked') {
                    $.each($('[id^="CycleList_"]'), function () {
                        $(this).attr('checked', 'checked');
                    });
                } else {
                    $.each($('[id^="CycleList_"]'), function () {
                        $(this).removeAttr('checked');
                    });
                }
            });
            var myEditor = new YAHOO.widget.Editor('editor', {
                height: '300px',
                width: '522px',
                dompath: true, //Turns on the bar at the bottom
                animate: true //Animates the opening, closing and moving of Editor windows
            });
            myEditor.render();

            $("#submit2").click(function () {
                myEditor.saveHTML();
                $("#editor").text(htmlEncode(myEditor.get('textarea').value));
                $("#submit").click();
            });
        });
    </script>
    <title></title>
</head>
<body class="yui-skin-sam">
    <%=new Bahen.Web.Header().GetHeader() %>
    <h1>Create a new event</h1>
    <form id="CreateEventForm" runat="server">
        <div>
            Event Name(*):
        <asp:TextBox ID="EventName" runat="server"></asp:TextBox>
            <asp:CheckBox ID-="IsDayEvent" runat="server" Text="Day Event?" />
            <asp:CheckBox ID="IsRecurring" runat="server" Text="Repeating Event?" />
            <br />
            Event Picture:
            <asp:FileUpload ID="ProfilePictureUploadControl" runat="server" /><br />
            Starts(MM/DD/YYYY)(*):
        <input name="StartDate" id="StartDatePicker" type="text" />
            <asp:DropDownList ID="StartHour" runat="server"></asp:DropDownList>
            :
        <asp:DropDownList ID="StartMinute" runat="server"></asp:DropDownList>
            <br />
            Ends(MM/DD/YYYY)(*):
        <input name="EndDate" id="EndDatePicker" type="text" />
            <asp:DropDownList ID="EndHour" runat="server"></asp:DropDownList>
            :
        <asp:DropDownList ID="EndMinute" runat="server"></asp:DropDownList>
            <br />
            Publish to Cycles:
            <asp:CheckBoxList ID="CycleList" runat="server"></asp:CheckBoxList>
            <br />
            Recurring Frequency:
        <asp:RadioButtonList runat="server">
            <asp:ListItem>Daily</asp:ListItem>
            <asp:ListItem>Weekly</asp:ListItem>
            <asp:ListItem>Monthly</asp:ListItem>
            <asp:ListItem>Yearly</asp:ListItem>
        </asp:RadioButtonList>
            <br />
            Recurring Times:
        <asp:TextBox ID="RecurringTimes" runat="server"></asp:TextBox>
            <br />
            Privacy: 
        <br />
            Who can view the event:
        <asp:DropDownList ID="Visibility" runat="server">
            <asp:ListItem Value="0">Everyone</asp:ListItem>
            <asp:ListItem Value="1">Members of my cycles</asp:ListItem>
            <asp:ListItem Value="2">My Subscribers</asp:ListItem>
            <asp:ListItem Value="4">Publishers subscribed by me</asp:ListItem>
            <asp:ListItem Value="8">Invited Only</asp:ListItem>
            <asp:ListItem Value="16">Private</asp:ListItem>
        </asp:DropDownList>
            Who can join the event:
            
        <asp:DropDownList ID="Restriction" runat="server">
            <asp:ListItem Value="0">Everyone</asp:ListItem>
            <asp:ListItem Value="1">Members of my cycles</asp:ListItem>
            <asp:ListItem Value="2">My Subscribers</asp:ListItem>
            <asp:ListItem Value="4">Publishers subscribed by me</asp:ListItem>
            <asp:ListItem Value="8">Invited Only</asp:ListItem>
            <asp:ListItem Value="16">Private</asp:ListItem>
        </asp:DropDownList>
            <br />
            Address: 
        <br />
            Name of the Place:
            <input name="Venue" id="Venue" type="text" />
            <br />
            Street Address:
            <input name="Street" id="Street" type="text" />
            <br />
            City:
            <input name="City" id="Text1" type="text" />
            <br />
            State/Province:
            <input name="Province" id="Province" type="text" />
            <br />
            Country:
            <input name="Country" id="Country" type="text" />
            <br />
            Brief Description:
            <asp:TextBox ID="DescriptionAbstract" TextMode="MultiLine" runat="server"></asp:TextBox><br />
            More Description:
            <textarea id="editor" name="Description" cols="50" rows="10" style="display: none"></textarea>
            <br />
            <input id="submit" value="submit" type="submit" style="display: none" />
        </div>
    </form>
    <button id="submit2">Submit</button>
    <asp:Label ID="ResultLabel" runat="server"></asp:Label>
    <%=new Bahen.Web.Footer().GetFooter() %>
</body>
</html>
