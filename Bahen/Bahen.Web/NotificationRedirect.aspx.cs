﻿using Bahen.DataOperationLayer.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;

namespace Bahen.Web
{
    public partial class NotificationRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                if (String.IsNullOrEmpty((string)RouteData.Values["guid"]) || String.IsNullOrEmpty((string)RouteData.Values["uid"]))
                {
                    return;
                }
                Guid guid;
                long userID;
                if (!Guid.TryParse((string)RouteData.Values["guid"], out guid) || !Int64.TryParse((string)RouteData.Values["uid"], out userID))
                {
                    return;
                }

                if (Session.GetAuthenticatedUserID() == userID)
                {
                    TableNotificationOperator notificationOperator = new TableNotificationOperator(
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                    //var notification = notificationOperator.Get(guid, userID);
                    notification.Read = true;
                    notificationOperator.Update(notification);
                    Response.Redirect(notification.Url);
                }
                else
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
            }
        }
    }
}