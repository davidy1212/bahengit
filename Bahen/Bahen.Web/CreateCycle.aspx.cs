﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataMiddleLayer.Newsfeed;
using Bahen.DataOperationLayer.Blob;
using System.IO;
using Bahen.Web.Images;

namespace Bahen.Web
{
    public partial class CreateCycle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                if (Request.Params["CycleName"] != null)
                {
                    // Check if the cycle id exists, get the cycle entity
                    CycleOperator cycleOperator = new CycleOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                    CycleEntity cycleEntity = new CycleEntity()
                    {
                        Name = Request.Params["CycleName"],
                        DescriptionAbstract = Request.Params["DescriptionAbstract"]
                    };

                    cycleOperator.Insert(cycleEntity);
                    cycleOperator.AddMemebership(cycleEntity.ID, Session.GetAuthenticatedUserID(), DataModels.Constants.CycleMembershipType.Administrator);


                    // Upload profile picture
                    if (ProfilePictureUploadControl.HasFile)
                    {
                        string extension = Path.GetExtension(ProfilePictureUploadControl.PostedFile.FileName).ToLowerInvariant();
                        if (extension == ".jpg" || extension == ".png" || extension == ".bmp" || extension == ".gif")
                        {
                            try
                            {
                                // Convert to png format
                                System.Drawing.Image original = System.Drawing.Image.FromStream(ProfilePictureUploadControl.PostedFile.InputStream);

                                BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
                                    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                                Guid pictureID = Guid.NewGuid();

                                // Upload large size picture
                                using (Stream uploadStreamMedium = new MemoryStream())
                                {
                                    System.Drawing.Image mediumSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Medium);
                                    mediumSizeImage.Save(uploadStreamMedium, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamMedium.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "c",
                                        cycleEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Medium,
                                        uploadStreamMedium);
                                }

                                // Remember the picture ID
                                //cycleEntity.Picture = pictureID;

                                // Upload thumbnail size picture
                                using (Stream uploadStreamThumbnail = new MemoryStream())
                                {
                                    System.Drawing.Image thumbnailSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Thumb);
                                    thumbnailSizeImage.Save(uploadStreamThumbnail, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamThumbnail.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "c",
                                        cycleEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Thumb,
                                        uploadStreamThumbnail);
                                }
                            }
                            catch
                            {
                                Response.Write("Invalid image format");
                                return;
                            }
                        }
                        else
                        {
                            Response.Write("Invalid image format");
                            return;
                        }
                    }


                    // Update the cycle
                    cycleOperator.Update(cycleEntity);

                    // Add news feed, don't have to wait
                    (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                    {
                        Content = "created a new cycle",
                        SenderID = Session.GetAuthenticatedUserID(),
                        ReferenceType = typeof(CycleEntity).ToString(),
                        ReferenceLongID = cycleEntity.ID
                    });

                    Response.Redirect("/cycles/" + cycleEntity.ID);
                }
            }
        }
    }
}