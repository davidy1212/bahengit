﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using Bahen.Web.Common.Parsing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer.Blob;
using System.IO;
using Bahen.Web.Common.Validating;
using Bahen.Web.Images;
using Bahen.DataMiddleLayer.Newsfeed;

namespace Bahen.Web
{
    public partial class CreateEvent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if already logged in
            if (Context.TryAuthenticate())
            {
                // Check if it creating a new event
                if (Request.Params["EventName"] != null)
                {
                    EventParser parser = new EventParser();
                    EventEntity eventEntity;

                    if (!parser.TryParse(Context, out eventEntity))
                    {
                        Response.Write("Failed to create the event");
                        return;
                    }

                    // Add publisher information since it is not in the request parameters
                    eventEntity.PublisherID = (Context.Session["User"] as IEntity).ID;

                    try
                    {
                        EventOperator eventOperator = new EventOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                            ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                        // Insert the event
                        eventOperator.Insert(eventEntity);

                        // Add cycle-event relation
                        for (int i = 0; i < Request.Form.Count; i++)
                        {
                            if (Request.Form.GetKey(i).StartsWith("CycleList") && Request.Form[i] != "0")
                            {
                                long cycleID;
                                if (Int64.TryParse(Request.Form[i], out cycleID))
                                {
                                    eventOperator.AddCycle(eventEntity.ID, cycleID);
                                }
                            }
                        }

                        // Add picture
                        if (ProfilePictureUploadControl.HasFile)
                        {
                            string extension = Path.GetExtension(ProfilePictureUploadControl.PostedFile.FileName).ToLowerInvariant();
                            if (extension == ".jpg" || extension == ".png" || extension == ".bmp" || extension == ".gif")
                            {
                                try
                                {
                                    // Convert to png format
                                    System.Drawing.Image original = System.Drawing.Image.FromStream(ProfilePictureUploadControl.PostedFile.InputStream);

                                    BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
                                        ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                                    Guid pictureID = Guid.NewGuid();

                                    // Upload large size picture
                                    using (Stream uploadStreamLarge = new MemoryStream())
                                    {
                                        System.Drawing.Image largeSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Large);
                                        largeSizeImage.Save(uploadStreamLarge, System.Drawing.Imaging.ImageFormat.Png);
                                        uploadStreamLarge.Seek(0, SeekOrigin.Begin);

                                        profilePictureOperator.Upload(
                                            "e",
                                            eventEntity.ID,
                                            pictureID,
                                            DataModels.Constants.PictureSize.Large,
                                            uploadStreamLarge);
                                    }

                                    // Remember the picture ID
                                    //eventEntity.PictureGuid = pictureID;

                                    // Upload thumbnail size picture
                                    using (Stream uploadStreamMedium = new MemoryStream())
                                    {
                                        System.Drawing.Image thumbnailSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Medium);
                                        thumbnailSizeImage.Save(uploadStreamMedium, System.Drawing.Imaging.ImageFormat.Png);
                                        uploadStreamMedium.Seek(0, SeekOrigin.Begin);

                                        profilePictureOperator.Upload(
                                            "e",
                                            eventEntity.ID,
                                            pictureID,
                                            DataModels.Constants.PictureSize.Medium,
                                            uploadStreamMedium);
                                    }
                                }
                                catch
                                {
                                    Response.Write("Invalid image format");
                                    return;
                                }
                            }
                            else
                            {
                                Response.Write("Invalid image format");
                                return;
                            }
                        }

                        // Add description
                        if (!String.IsNullOrEmpty(Request.Params["Description"]))
                        {
                            BlobDescriptionOperator descriptionOperator = new BlobDescriptionOperator(
                                ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);

                            Guid descriptionID = Guid.NewGuid();
                            eventEntity.DescriptionBlobGuid = descriptionID;

                            using (MemoryStream uploadStream = new MemoryStream())
                            {
                                StreamWriter writer = new StreamWriter(uploadStream);
                                writer.Write(
                                    new RichTextValidator().Filter(
                                    Server.HtmlDecode(
                                    Request.Params["Description"])));
                                writer.Flush();
                                uploadStream.Seek(0, SeekOrigin.Begin);

                                descriptionOperator.Upload(
                                    "e",
                                    eventEntity.ID,
                                    descriptionID,
                                    uploadStream
                                    );
                            }
                        }

                        // Update event entity
                        eventOperator.Update(eventEntity);

                        // Add news feed, don't have to wait
                        (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                        {
                            Content = String.Format("created a new event", Session.GetAuthenticatedUser().PreferredName),
                            SenderID = eventEntity.PublisherID,
                            ReferenceType = typeof(EventEntity).ToString(),
                            ReferenceLongID = eventEntity.ID
                        });

                        Response.Redirect("/events/" + eventEntity.ID);
                    }
                    catch
                    {
                        Response.Write("Failed to create the event");
                        return;
                    }
                }
                else
                {
                    // Add time
                    for (int i = 0; i < 24; i++)
                    {
                        StartHour.Items.Add(new ListItem(((i + 8) % 24).ToString()));
                        EndHour.Items.Add(new ListItem(((i + 8) % 24).ToString()));
                    }

                    for (int i = 0; i < 60; i = i + 5)
                    {
                        StartMinute.Items.Add(new ListItem(i.ToString()));
                        EndMinute.Items.Add(new ListItem(i.ToString()));
                    }

                    // Add publish to cycles
                    CycleOperator cycleOperator = new CycleOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                            ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                    var cycles = cycleOperator.GetCyclesByMember((Session["User"] as IEntity).ID, DataModels.Constants.CycleMembershipType.Publisher, true);

                    CycleList.Items.Add(new ListItem("All", "0"));
                    foreach (var item in cycles)
                    {
                        CycleList.Items.Add(new ListItem(item.Name, item.ID.ToString()));
                    }
                }
            }
        }
    }
}