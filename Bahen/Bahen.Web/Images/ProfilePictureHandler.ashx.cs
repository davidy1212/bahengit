﻿using Bahen.DataOperationLayer.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer;
using Bahen.DataModels.Constants;
using System.Net;

namespace Bahen.Web.Images
{
    /// <summary>
    /// Summary description for ProfilePictureHandler
    /// </summary>
    public class ProfilePictureHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "image/png";

            //// Parse size
            //PictureSize size;
            //switch (context.Request.QueryString["size"])
            //{
            //    case ("large"):
            //        size = PictureSize.Large;
            //        break;
            //    case ("medium"):
            //        size = PictureSize.Medium;
            //        break;
            //    case ("thumbnail"):
            //        size = PictureSize.Thumb;
            //        break;
            //    default:
            //        size = PictureSize.Thumb;
            //        break;
            //}

            //// Parse id
            //long id;
            //Guid guid;
            //if (!Int64.TryParse(context.Request.QueryString["id"], out id))
            //{
            //    return;
            //}
            //if (!Guid.TryParse(context.Request.QueryString["guid"], out guid))
            //{
            //    return;
            //}

            //BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
            //    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
            //DateTime lastModified = profilePictureOperator.GetLastModifiedDate(
            //        context.Request.QueryString["prefix"],
            //        id,
            //        guid,
            //        size);
            //if (Cache.IsClientCached(context, lastModified))
            //{
            //    context.Response.StatusCode = 304;
            //    context.Response.SuppressContent = true;
            //}
            //else
            //{
            //    try
            //    {
            //        //context.Response.Cache.SetLastModified(profilePictureOperator.GetLastModifiedDate(
            //        //    context.Request.QueryString["prefix"],
            //        //    id,
            //        //    guid,
            //        //    size));
            //        //// Download image
            //        //Stream outputStream = context.Response.OutputStream;
            //        //profilePictureOperator.Download(
            //        //    context.Request.QueryString["prefix"],
            //        //    id,
            //        //    guid,
            //        //    size,
            //        //    outputStream);
            //    }
            //    catch
            //    {
            //        if (size == PictureSize.Thumb)
            //        {
            //            context.Response.WriteFile("/Images/default_thumbnail.png");
            //        }
            //        else
            //        {
            //            context.Response.WriteFile("/Images/default_medium.png");
            //        }
            //    }
            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}