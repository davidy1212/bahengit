﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataOperationLayer;
using Bahen.Web.Formatting;
using Bahen.Web.Tools;
using System;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Bahen.Web
{
    public partial class EventList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                Response.Write(GetUserEventList(Session.GetAuthenticatedUserID()));
            }
        }

        public string GetUserEventList(long userID)
        {
            EventOperator eventOperator = new EventOperator(
                         ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                         ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var events = eventOperator.GetEventsByAttendant(userID);
            StringBuilder sb = new StringBuilder();
            foreach (var item in events)
            {
                sb.Append("<div style=\"width:270px; height:135px; font-size:10pt; display:inline-block; vertical-align:top;\">");
                sb.Append(String.Format("<div style=\"display:inline-block; vertical-align:top; width:100px; height:130px; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=e&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                    item.ID, item.Picture));
                sb.Append("<div style=\"margin-left:15px; vertical-align:top; display:inline-block; width:155px\">");
                sb.Append(String.Format("<a href='/events/{0}' style=\"font-weight:700; font-size:10pt;\">{1}</a><br/>", item.ID, item.Name));
                sb.Append(item.StartTime.ToString("f"));
                sb.Append("<br/>");

                int peopleGoing = item.Attendances.Where(x =>
                    x.AttendanceStatus != (int)AttendanceStatus.Declined &&
                    x.AttendanceStatus != (int)AttendanceStatus.Maybe &&
                    x.AttendanceStatus != (int)AttendanceStatus.Invited &&
                    x.AttendanceStatus != (int)AttendanceStatus.Interested)
                    .Count();

                sb.Append(String.Format("{0} people going<br/>", peopleGoing));

                int peopleReached = item.Attendances.Count;
                sb.Append(String.Format("{0} people interested<br/>", peopleReached));

                var userAttendances = item.Attendances.Where(x => x.UserID == userID);
                AttendanceStatus userAttendance = AttendanceStatus.None;
                if (userAttendances.FirstOrDefault() != null)
                {
                    userAttendance = (AttendanceStatus)userAttendances.FirstOrDefault().AttendanceStatus;
                }

                if (userID == Session.GetAuthenticatedUserID())
                {
                    switch (userAttendance)
                    {
                        case (AttendanceStatus.Invited):
                            {
                                sb.Append("You are invited.<br/>");
                                //sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "going", "Going"));
                                //sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "maybe", "Maybe"));
                                //sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "leave", "Not going"));
                                break;
                            }
                        case (AttendanceStatus.Going):
                            {
                                sb.Append("You are going.<br/>");
                                break;
                            }
                        case (AttendanceStatus.Organizer):
                            {
                                sb.Append("You are going (Organizer).<br/>");
                                break;
                            }
                        case (AttendanceStatus.Publisher):
                            {
                                sb.Append("You are going (Publisher).<br/>");
                                break;
                            }
                        case (AttendanceStatus.Copublisher):
                            {
                                sb.Append("You are going (Co-Publisher).<br/>");
                                break;
                            }
                        case (AttendanceStatus.Maybe):
                            {
                                sb.Append("You are probably going.<br/>");
                                sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "going", "Going"));
                                sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "leave", "Not going"));
                                break;
                            }
                        case (AttendanceStatus.Interested):
                            {
                                sb.Append("You are probably interested.<br/>");
                                sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "going", "Going"));
                                sb.Append(String.Format("<a class=\"button-large\" style=\"color:white;\" href=\"/events/{0}?action={1}\">{2}</a>", item.ID, "leave", "Not going"));
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }

                sb.Append("<br/>");
                sb.Append("</div></div>");
            }
            return sb.ToString();
        }

        public string GetCycleEventList(long cycleID)
        {
            EventOperator eventOperator = new EventOperator(
                   ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                   ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var events = eventOperator.GetEventsByCycle(cycleID);

            StringBuilder sb = new StringBuilder();
            foreach (var item in events)
            {
                sb.Append("<div style=\"width:270px; height:135px; font-size:10pt; display:inline-block; vertical-align:top;\">");
                sb.Append(String.Format("<div style=\"display:inline-block; vertical-align:top; width:100px; height:130px; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=e&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                    item.ID, item.Picture));
                sb.Append("<div style=\"margin-left:15px; vertical-align:top; display:inline-block; width:155px\">");
                sb.Append(String.Format("<a href='/events/{0}' style=\"font-weight:700; font-size:10pt;\">{1}</a><br/>", item.ID, item.Name));
                sb.Append(item.StartTime.ToString("f"));
                sb.Append("<br/>");

                int peopleGoing = item.Attendances.Where(x =>
                    x.AttendanceStatus != (int)AttendanceStatus.Declined &&
                    x.AttendanceStatus != (int)AttendanceStatus.Maybe &&
                    x.AttendanceStatus != (int)AttendanceStatus.Invited &&
                    x.AttendanceStatus != (int)AttendanceStatus.Interested)
                    .Count();

                sb.Append(String.Format("{0} people going<br/>", peopleGoing));

                int peopleReached = item.Attendances.Count;
                sb.Append(String.Format("{0} people interested", peopleReached));
                sb.Append("</div></div>");
            }
            return sb.ToString();
        }
    }
}