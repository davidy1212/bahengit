﻿using Bahen.DataModels.Sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Web.Formatting
{
    public class UserFormatter : IEntityFormatter<SqlUserEntity>
    {
        public string FormatList(IEnumerable<SqlUserEntity> list)
        {
            StringBuilder listBuilder = new StringBuilder();
            listBuilder.Append("<div class='thumbnail-list box'>");
            foreach (var item in list)
            {
                Format(listBuilder, item);
            }
            listBuilder.Append("</div>");

            return listBuilder.ToString();
        }

        public string Format(SqlUserEntity item)
        {
            StringBuilder sb = new StringBuilder();
            Format(sb, item);
            return sb.ToString();
        }

        public void Format(StringBuilder sb, SqlUserEntity item)
        {
            //sb.Append(String.Format(
            //        "<img class='profile-picture-thumbnail' src='/Images/ProfilePictureHandler.ashx?prefix=u&id={0}&guid={1}&size=thumbnail' onclick=\"location.href='/users/{0}'\" alt title=\"{2}\"/>",
            //        item.ID, item.ProfilePicture.ToString("N"), item.PreferredName));
        }
    }
}
