﻿using Bahen.DataModels.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Web.Formatting
{
    public class CommentFormatter : IEntityFormatter<TableCommentEntity>
    {
        public string FormatList(IEnumerable<TableCommentEntity> list)
        {
            if (list.Count() == 0)
            {
                return "No comments";
            }
            else
            {
                StringBuilder stringBuilder = new StringBuilder();
                //stringBuilder.Append(String.Format(@"<div id='CommentList' targetID='{0}'><h2>Comments</h2>", list.First().TargetID));
                foreach (var comment in list)
                {
                    stringBuilder.Append(this.Format(comment));
                }
                stringBuilder.Append(@"</div>");
                return stringBuilder.ToString();
            }
        }

        public string Format(TableCommentEntity item)
        {
            return "";
            //string deleteAddress = String.Format(@"/UserOpinion/DeleteCommentHandler.ashx?pk={0}&rk={1}",
            //    item.PartitionKey, item.RowKey);
            ////string profilePicAddress = String.Format(@"/Images/ProfilePictureHandler.ashx?prefix=u&id={0}&guid={1}&size=thumbnail", item.UserID, item.UserProfilePicGuid);
            //string profileAddress = String.Format(@"/users/{0}", item.UserID);
            //StringBuilder sb = new StringBuilder();
            //sb.Append(String.Format(@"<div class='comment-wrapper' commentID='{0}'>", item.ID));
            //sb.Append(String.Format(@"<a href='{0}'><img class='profile-picture-thumbnail' src='{1}' alt title='{2}'></a>", profileAddress, profilePicAddress, item.UserName));
            //sb.Append(String.Format(@"<div class='comment-username'><a href='{0}'>{1}</a>:</div>", profileAddress, item.UserName));
            //sb.Append(String.Format(@"<div class='comment-time'>{0}</div>", item.ActionTime.ToLocalTime().ToString()));
            //sb.Append(String.Format(@"<a class='comment-delete' href='{0}'>Delete</a>", deleteAddress));
            //sb.Append(String.Format(@"<div class='comment-content'>{0}</div>", item.Content));
            //sb.Append(String.Format(@"</div>"));
            //return sb.ToString();
        }
    }
}