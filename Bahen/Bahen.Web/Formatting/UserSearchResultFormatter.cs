﻿using Bahen.DataModels.Sql;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Web.Formatting
{
    public class UserSearchResultFormatter : IEntityFormatter<SqlUserEntity>
    {
        public string Format(SqlUserEntity item)
        {
            StringBuilder builder = new StringBuilder();
            this.Format(builder, item);
            return builder.ToString();
        }


        public void Format(StringBuilder sb, SqlUserEntity item)
        {
            sb.Append(String.Format("<div class='listItem' style=\"margin-bottom:5px; cursor:pointer;\" data-name='{0}' data-email='{1}' onclick='inputBox.addElement($(this).attr(\"data-name\"),$(this).attr(\"data-email\"))'>",
                item.PreferredName, item.LoginEmail));
            sb.Append("<div style=\"display:inline-block; vertical-align:top;\">");
            //sb.Append(String.Format(
            //       "<img src='{0}' alt title='{1}'/>",
            //       HyperLinks.GetUserProfilePictureThumbnailUrl(item.ID, item.ProfilePicture),
            //       item.PreferredName));
            sb.Append("</div><div style=\"display:inline-block; vertical-align:top; margin-left:10px;\">");
            sb.Append("<span style=\"color:blue; font-size:10pt;\">");
            sb.Append(item.PreferredName);
            sb.Append("</span><br/><span style=\"color:#ccc; font-size:10pt;\">");
            sb.Append(item.LoginEmail);
            sb.Append("</span></div>");
            sb.Append("</div>");
        }

        public string FormatList(IEnumerable<SqlUserEntity> list)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in list)
            {
                this.Format(builder, item);
            }
            return builder.ToString();
        }

        public string FormatList(IEnumerable<SqlUserEntity> list, int start, int number)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var item in list.Skip(start).Take(number))
            {
                this.Format(builder, item);
            }
            return builder.ToString();
        }
    }
}
