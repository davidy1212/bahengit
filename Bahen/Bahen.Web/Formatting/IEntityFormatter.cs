﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Web.Formatting
{
    public interface IEntityFormatter<T>
    {
        string Format(T item);
        string FormatList(IEnumerable<T> list);
    }
}
