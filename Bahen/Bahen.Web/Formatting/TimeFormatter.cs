﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Web.Formatting
{
    public class TimeFormatter
    {
        public string Format(DateTime time)
        {
            TimeSpan span = DateTime.UtcNow - time.ToUniversalTime();
            if (span < TimeSpan.FromMinutes(1))
            {
                return "Just now";
            }
            if (span < TimeSpan.FromHours(1))
            {
                return String.Format("{0} minute{1} ago", span.Minutes, span.Minutes > 1 ? "s" : "");
            }
            if (span < TimeSpan.FromDays(1))
            {
                return String.Format("{0} hour{1} ago", span.Hours, span.Hours > 1 ? "s" : "");
            }
            if (span < TimeSpan.FromDays(2))
            {
                return String.Format("Yesterday at {0}", time.ToLocalTime().ToLongTimeString());
            }
            if (span < TimeSpan.FromDays(7))
            {
                return String.Format("{0} at {1}", time.ToLocalTime().ToString("m"), time.ToLocalTime().ToString("t"));
            }
            else
            {
                return String.Format("{0} at {1}", time.ToLocalTime().ToString("yyyy/MM/dd"), time.ToLocalTime().ToString("t"));
            }
        }
    }
}
