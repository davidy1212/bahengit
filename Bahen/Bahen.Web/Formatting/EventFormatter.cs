﻿using Bahen.DataModels.Sql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bahen.DataModels.Constants;

namespace Bahen.Web.Formatting
{
    public class EventFormatter : IEntityFormatter<SqlEventEntity>
    {
        public string FormatList(IEnumerable<SqlEventEntity> list)
        {
            StringBuilder listBuilder = new StringBuilder();

            foreach (var item in list)
            {
                Format(listBuilder, item);
            }

            return listBuilder.ToString();
        }

        public string Format(SqlEventEntity item)
        {
            StringBuilder sb = new StringBuilder();
            return sb.ToString();
        }

        public void Format(StringBuilder sb, SqlEventEntity item)
        {
            sb.Append("<div class='box' style=\"width:445px; height:190px;\">");
            sb.Append(String.Format("<div style=\"display:inline-block; vertical-align:top; width:250px; height:180px; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=e&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                item.ID, item.Picture));
            sb.Append("<div style=\"margin-left:20px; vertical-align:top; display:inline-block; width:150px\">");
            sb.Append(String.Format("<a href='/events/{0}'>{1}</a><br/>", item.ID, item.Name));

            int peopleGoing = item.Attendances.Where(x=>
                x.AttendanceStatus!=(int)AttendanceStatus.Declined &&
                x.AttendanceStatus!=(int)AttendanceStatus.Maybe &&
                x.AttendanceStatus!=(int)AttendanceStatus.Invited &&
                x.AttendanceStatus!=(int)AttendanceStatus.Interested)
                .Count();

            sb.Append(String.Format("{0} people going<br/>", peopleGoing));

            int peopleReached = item.Attendances.Count;
            sb.Append(String.Format("{0} people reached", peopleReached));

            sb.Append("<br/>");
            sb.Append(item.StartTime.ToString());
            sb.Append("</div></div>");
        }
    }
}
