﻿using Bahen.DataModels;
using Bahen.DataModels.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Web.Formatting
{
    public class NewsfeedFormatter : IEntityFormatter<TableNewsfeedEntity>
    {
        public string FormatList(IEnumerable<TableNewsfeedEntity> list)
        {
            StringBuilder listBuilder = new StringBuilder();
            foreach (var item in list)
            {
                listBuilder.Append("<div class='box'>");
                listBuilder.Append(item.Content);
                listBuilder.Append("<br/>");
                listBuilder.Append(String.Format("<span style=\"font-size:9pt;color:#ccc;\">{0}</span>", item.Time.ToString()));
                listBuilder.Append("</div>");
            }
            return listBuilder.ToString();
        }

        public string Format(TableNewsfeedEntity item)
        {
            throw new NotImplementedException();
        }

        public void Format(StringBuilder sb, TableNewsfeedEntity item)
        {
            throw new NotImplementedException();
        }
    }
}
