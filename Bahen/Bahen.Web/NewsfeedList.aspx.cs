﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Sql;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer;
using Bahen.DataOperationLayer.Sql;
using Bahen.DataOperationLayer.Table;
using Bahen.Web.Formatting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;

namespace Bahen.Web
{
    public partial class NewsfeedList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string FormatNewsfeedList(IEnumerable<TableNewsfeedEntity> feedList)
        {
            SqlUserOperator userOperator = new SqlUserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            EventOperator eventOperator = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            CycleOperator cycleOperator = new CycleOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            StringBuilder sb = new StringBuilder();

            foreach (TableNewsfeedEntity item in feedList.OrderByDescending(x => x.TimeSent))
            {
                SqlUserEntity publisher = userOperator.Get(item.SenderID);
                sb.Append("<div class='separator' style=\"width:450px;\">");
                //sb.Append(String.Format(
                //        "<div class='profile-picture-thumbnail box' style=\"display:inline-block; background-image:url('/Images/ProfilePictureHandler.ashx?prefix=u&id={0}&guid={1}&size=thumbnail');\" onclick=\"location.href='/users/{0}'\" data-tooltip=\"{2}\"></div>",
                //        publisher.ID, publisher.ProfilePicture, publisher.PreferredName));
                sb.Append("<div style=\"margin-left:10px; display:inline-block; width:370px;\">");
                sb.Append(String.Format("<a href='/users/{0}' style=\"font-weight:700;\">{1}</a> ", publisher.ID, publisher.PreferredName));
                sb.Append(item.Content);
                if (item.ReferenceType == typeof(EventEntity).ToString())
                {
                    EventEntity eventEntity;
                    if (eventOperator.TryGet(item.ReferenceLongID, out eventEntity))
                    {
                        //    sb.Append(String.Format(" <a href='/events/{0}'>{1}</a><br/>", eventEntity.ID, eventEntity.Name));
                        //    sb.Append("</div>");
                        //    sb.Append("<div class='shaded-box' style=\"margin-left:80px; width:300px; height:130px; font-size:10pt;\">");
                        //    sb.Append(String.Format("<div style=\"display:inline-block; margin:5px; width:90px; height:120px;vertical-align: top; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=e&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                        //        eventEntity.ID, eventEntity.PictureGuid));
                        //    sb.Append("<div style=\"margin-left:5px; margin-top:5px; display:inline-block; width:180px; vertical-align: top;\">");
                        //    sb.Append(String.Format("<a href='/events/{0}'>{1}</a><br/>", eventEntity.ID, eventEntity.Name));
                        //    sb.Append(eventEntity.StartTime.ToString("f"));
                        //    sb.Append("<br/>");

                        int peopleGoing = eventEntity.SqlEntity.Attendances.Where(x =>
                            x.AttendanceStatus != (int)AttendanceStatus.Declined &&
                            x.AttendanceStatus != (int)AttendanceStatus.Maybe &&
                            x.AttendanceStatus != (int)AttendanceStatus.Invited &&
                            x.AttendanceStatus != (int)AttendanceStatus.Interested)
                            .Count();

                        sb.Append(String.Format("{0} people going<br/>", peopleGoing));

                        int peopleReached = eventEntity.SqlEntity.Attendances.Count;
                        sb.Append(String.Format("{0} people reached", peopleReached));

                        sb.Append("<br/>");
                        sb.Append(eventEntity.DescriptionAbstract);
                        sb.Append("</div></div>");
                    }

                }
                else if (item.ReferenceType == typeof(CycleEntity).ToString())
                {
                    CycleEntity cycleEntity = cycleOperator.Get(item.ReferenceLongID);
                    sb.Append(String.Format(" <a href='/cycles/{0}'>{1}</a><br/>", cycleEntity.ID, cycleEntity.Name));
                    sb.Append("</div>");
                    sb.Append("<div class='shaded-box' style=\"margin-left:80px; width:300px; height:130px; font-size:10pt;\">");
                    sb.Append(String.Format("<div style=\"display:inline-block; margin:5px; width:90px; height:120px; vertical-align: top; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=c&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                        cycleEntity.ID, cycleEntity.Picture));
                    sb.Append("<div style=\"margin-left:5px; margin-top:5px; display:inline-block; width:180px; clear:both;vertical-align: top;\">");
                    sb.Append(String.Format("<a href='/cycles/{0}'>{1}</a><br/>", cycleEntity.ID, cycleEntity.Name));

                    int memberCount = cycleEntity.SqlEntity.CycleUsers.Where(x => x.MembershipType > (int)Bahen.DataModels.Constants.CycleMembershipType.Invited).Count();
                    sb.Append(String.Format("{0} member{1}", memberCount, memberCount > 1 ? "s" : ""));
                    sb.Append("<br/>");
                    sb.Append(cycleEntity.DescriptionAbstract);
                    sb.Append("</div></div>");

                }
                else if (item.ReferenceType == typeof(UserEntity).ToString())
                {
                    SqlUserEntity userEntity = userOperator.Get(item.ReferenceLongID);
                    sb.Append(String.Format(" <a href='/users/{0}'>{1}</a><br/>", userEntity.ID, userEntity.PreferredName));
                    sb.Append("</div>");
                    sb.Append("<div class='shaded-box' style=\"margin-left:80px; width:300px; height:130px; font-size:10pt;\">");
                    //sb.Append(String.Format("<div style=\"display:inline-block; margin:5px; width:90px; height:120px; vertical-align: top; background-image:url('/Images/ProfilePictureHandler.ashx?size=medium&prefix=u&id={0}&guid={1}'); background-position:left; background-size:cover; background-repeat:no-repeat;\"></div>",
                    //    userEntity.ID, userEntity.ProfilePicture));
                    sb.Append("<div style=\"margin-left:5px; margin-top:5px; display:inline-block; width:180px; clear:both;vertical-align: top;\">");
                    sb.Append(String.Format("<a href='/users/{0}'>{1}</a><br/>", userEntity.ID, userEntity.PreferredName));
                    sb.Append(String.Format("{0} subscriber{1}", userEntity.HostSubscriptions.Count, userEntity.HostSubscriptions.Count > 1 ? "s" : ""));
                    sb.Append("</div></div>");
                }
                sb.Append(String.Format("<div style=\"font-size:9pt;color:#ccc; display:inline-block; margin-left:350px;\">{0}</div>", new TimeFormatter().Format(item.TimeSent)));

                UserOpinion.GetCommentHandler comment = new UserOpinion.GetCommentHandler();
                UserOpinion.GetLikeHandler like = new UserOpinion.GetLikeHandler();
                sb.Append(like.GetLikeButton(item.OriginalNewsfeedID, item.TypeCode, Session.GetAuthenticatedUser()));
                sb.Append(item.ID);
                sb.Append("</br>" + item.OriginalNewsfeedID);
                sb.Append("</br>" + item.IsOriginal);
                sb.Append(like.GetLikeNumberWithLink(item.OriginalNewsfeedID, item.TypeCode));
                sb.Append(comment.GetComments(item.OriginalNewsfeedID, item.TypeCode));
                sb.Append(comment.GetCommentBoxHTML(item.OriginalNewsfeedID, item.TypeCode));
                sb.Append("</div>");
            }
            return sb.ToString();
        }

        public IEnumerable<TableNewsfeedEntity> GetNewsfeedList(long userID)
        {
            TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            IEnumerable<TableNewsfeedEntity> feedList = null;

            try
            {
                //feedList = newsfeedOperator.GetAllItems(userID).ToList();
            }
            catch
            {
                return new List<TableNewsfeedEntity>();
            }

            return feedList;
        }

        public IEnumerable<TableNewsfeedEntity> GetNewsfeedList(long userID, int start, int count)
        {
            TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            IEnumerable<TableNewsfeedEntity> feedList;

            try
            {
                feedList = newsfeedOperator.GetAllItems(userID).ToList();
            }
            catch
            {
                return new List<TableNewsfeedEntity>();
            }
            return feedList.Skip(start).Take(count);
        }
    }
}