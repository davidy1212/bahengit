﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CycleProfile.aspx.cs" Inherits="Bahen.Web.CycleProfile" %>
<%@ Import Namespace="Bahen.Web.Tools" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <link type="text/css" href="/Styles/jquery-ui.min.css" rel="stylesheet" />
    <link type="text/css" href="/Styles/Tokenized.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.form.min.js"></script>
    <script type="text/javascript" src="/Scripts/Tokenized.js"></script>
    <script type="text/javascript">
        var inviteBox;
        $(function () {
            inviteBox = new Bahen.Web.UserInviteBox($("#InviteBox"), "<%=Request.Url.AbsolutePath %>/invite");
        });
    </script>
    <title></title>
</head>
<body>
    <%if (CycleEntity != null)
      { %>
    <%=new Bahen.Web.Header().GetHeader() %>

    <div style="display: inline-block; vertical-align: top;">
        <h1><%=CycleEntity.Name %></h1>
        <img src="/images/ProfilePictureHandler.ashx?size=medium&prefix=c&id=<%=CycleEntity.ID %>&guid=<%=CycleEntity.PictureGuid %>" /><br />
        <%if (this.Membership == Bahen.DataModels.Constants.CycleMembershipType.Invited)
          { %>
        You are invited 
        <%} %>
        <%if (CanJoin)
          { %>
        <a class="button-large" href="/cycles/<%=CycleEntity.ID %>?action=join">Join</a>
        <%} %>
        <%if ((Session["User"] as Bahen.DataModels.UserEntity) != null)
          { %>
        <a class="button-large" href="javascript:inviteBox.start();">Invite</a>
        <%} %>
        <%if (CanEdit)
          { %>
        <a class="button-large" href="/cycles/<%=CycleEntity.ID %>/edit">Edit</a>
        <%} %>

        <div id="InviteBox"></div>
        <br />
        Description: <%=CycleEntity.DescriptionAbstract %>
        <h2><%=CycleEntity.Name %>'s Members</h2>
        <%=new Bahen.Web.Formatting.UserFormatter()
        .FormatList(
        CycleEntity.SqlEntity.CycleUsers
        .Where(x=>x.MembershipType>(int)Bahen.DataModels.Constants.CycleMembershipType.Invited)
                .Select(x => x.User))%>
    </div>

    <div style="display: inline-block; vertical-align: top;">
        <h2><%=CycleEntity.Name %>'s Events</h2>
        <%=new Bahen.Web.EventList().GetCycleEventList(CycleEntity.ID) %>
        <div>
        <%=new Bahen.Web.UserOpinion.GetLikeHandler().GetLikeButton(CycleEntity.ID, CycleEntity.TypeCode, Session.GetAuthenticatedUser()) %>
        <%=new Bahen.Web.UserOpinion.GetCommentHandler().GetComments(CycleEntity.ID, CycleEntity.TypeCode) %>
        <%=new Bahen.Web.UserOpinion.GetCommentHandler().GetCommentBoxHTML(CycleEntity.ID, CycleEntity.TypeCode) %>
        </div>
    </div>

    <%=new Bahen.Web.Footer().GetFooter() %>
    <%} %>
</body>
</html>
