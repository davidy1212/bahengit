﻿using Bahen.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bahen.Web
{
    public partial class UserHomepage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.Session["User"] != null)
            {
                BodyLabel.Text = String.Format(@"<h1>Already logged in as {0}</h1>", (Context.Session["User"] as UserEntity).PreferredName);
                return;
            }
            else
            {
                Response.Redirect("Login.aspx", true);
            }
        }
    }
}