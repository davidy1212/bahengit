﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using Bahen.Web.Formatting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bahen.Web
{
    public partial class UserList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetEventAttendantList(long eventID)
        {
            UserOperator userOperator = new UserOperator(
                      ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                      ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var users = userOperator.GetAttendantsByEvent(eventID);

            return new UserFormatter().FormatList(users);
        }

        public string GetCycleMemberList(long cycleID)
        {
            UserOperator userOperator = new UserOperator(
                      ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                      ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var users = userOperator.GetMembersByCycle(cycleID);

            return new UserFormatter().FormatList(users);
        }

        public string GetSubscriberList(long userID)
        {
            UserOperator userOperator = new UserOperator(
                      ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                      ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var users = userOperator.GetSubscribersByHost(userID);

            return new UserFormatter().FormatList(users);
        }
    }
}