﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditEvent.aspx.cs" Inherits="Bahen.Web.EditEvent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

    <!-- Skin CSS file -->
    <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.9.0/build/assets/skins/sam/skin.css" />
    <!-- Utility Dependencies -->
    <script src="http://yui.yahooapis.com/2.9.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/element/element-min.js"></script>
    <!-- Needed for Menus, Buttons and Overlays used in the Toolbar -->
    <script src="http://yui.yahooapis.com/2.9.0/build/container/container_core-min.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/menu/menu-min.js"></script>
    <script src="http://yui.yahooapis.com/2.9.0/build/button/button-min.js"></script>
    <!-- Source file for Rich Text Editor-->
    <script src="http://yui.yahooapis.com/2.9.0/build/editor/editor-min.js"></script>

    <script type="text/javascript">

        function htmlEncode(value) {
            //create a in-memory div, set it's inner text(which jQuery automatically encodes)
            //then grab the encoded contents back out.  The div never exists on the page.
            return $('<div/>').text(value).html();
        }

        function htmlDecode(value) {
            return $('<div/>').html(value).text();
        }

        $(function () {
            $("#StartDatePicker").datepicker();
            $("#EndDatePicker").datepicker();

            $("#CycleList_0").click(function () {
                if ($(this).attr('checked') === 'checked') {
                    $.each($('[id^="CycleList_"]'), function () {
                        $(this).attr('checked', 'checked');
                    });
                } else {
                    $.each($('[id^="CycleList_"]'), function () {
                        $(this).removeAttr('checked');
                    });
                }
            });
            var myEditor = new YAHOO.widget.Editor('editor', {
                height: '300px',
                width: '522px',
                dompath: true, //Turns on the bar at the bottom
                animate: true //Animates the opening, closing and moving of Editor windows
            });
            myEditor.render();

            $("#submit2").click(function () {
                myEditor.saveHTML();
                $("#editor").text(htmlEncode(myEditor.get('textarea').value));
                $("#submit").click();
            });
        });
    </script>
    <title></title>
</head>
<body class="yui-skin-sam">
    <%
        if (EventEntity != null)
        {
    %>
    <%=new Bahen.Web.Header().GetHeader() %>
    <h1>Edit: <a href="/events/<%=EventEntity.ID %>"><%=EventEntity.Name %></a></h1>
    <a href="/events/<%=EventEntity.ID %>/edit?delete=1" style="color: red;">Delete this event</a><br />
    <form id="EditEventForm" runat="server">
        <div>
            Event Name(*):
        <input name="EventName" id="EventName" value="<%=EventEntity.Name %>" />
            <input type="checkbox" name="IsDayEvent" <%=EventEntity.IsDayEvent?"checked=\"checked\"":"" %> />Day Event?
            <input type="checkbox" name="IsRecurring" <%=EventEntity.IsRecurring?"checked=\"checked\"":""%> />Repeating Event?
            <br />
            Event Picture:<br />
    <img src="/images/ProfilePictureHandler.ashx?size=medium&prefix=e&id=<%=EventEntity.ID %>&guid=<%=EventEntity.PictureGuid %>" /><br />
            <asp:FileUpload ID="ProfilePictureUploadControl" runat="server" /><br />
            Starts(MM/DD/YYYY)(*):
        <input name="StartDate" id="StartDatePicker" type="text" value="<%=EventEntity.StartTime.ToString("M/dd/yyyy") %>" />
            <asp:DropDownList ID="StartHour" runat="server"></asp:DropDownList>
            :
        <asp:DropDownList ID="StartMinute" runat="server"></asp:DropDownList>
            <br />
            Ends(MM/DD/YYYY)(*):
        <input name="EndDate" id="EndDatePicker" type="text" value="<%=EventEntity.EndTime.ToString("M/dd/yyyy") %>" />
            <asp:DropDownList ID="EndHour" runat="server"></asp:DropDownList>
            :
        <asp:DropDownList ID="EndMinute" runat="server"></asp:DropDownList>
            <br />
            Publish to Cycles:
            <asp:CheckBoxList ID="CycleList" runat="server"></asp:CheckBoxList>
            <br />
            Recurring Frequency:
            <input type="radio" name="RecurringFrequency" value="127" <%=EventEntity.RecurringFrequency==(int)Bahen.DataModels.Constants.EventRecurringType.Daily?"checked=\"checked\"":"" %> />Daily<br />
            <input type="radio" name="RecurringFrequency" value="0" <%=EventEntity.RecurringFrequency==(int)Bahen.DataModels.Constants.EventRecurringType.Weekly?"checked=\"checked\"":"" %> />Weekly<br />
            <input type="radio" name="RecurringFrequency" value="256" <%=EventEntity.RecurringFrequency==(int)Bahen.DataModels.Constants.EventRecurringType.Monthly?"checked=\"checked\"":"" %> />Daily<br />
            <input type="radio" name="RecurringFrequency" value="512" <%=EventEntity.RecurringFrequency==(int)Bahen.DataModels.Constants.EventRecurringType.Yearly?"checked=\"checked\"":"" %> />Daily<br />

            Recurring Times:
        <input name="RecurringTimes" type="text" value="<%=EventEntity.RecurringTimes.ToString() %>" />
            <br />
            Privacy: 
        <br />
            Who can view the event:
            <select id="Visibility" name="Visibility">
                <option value="0">Everyone</option>
                <option value="1">Members of my cycles</option>
                <option value="2">My Subscribers</option>
                <option value="4">Publishers subscribed by me</option>
                <option value="8">Invited Only</option>
                <option value="16">Private</option>
            </select>
            Who can join the event:
            <select id="Restriction" name="Restriction">
                <option value="0">Everyone</option>
                <option value="1">Members of my cycles</option>
                <option value="2">My Subscribers</option>
                <option value="4">Publishers subscribed by me</option>
                <option value="8">Invited Only</option>
                <option value="16">Private</option>
            </select>
            <br />
            Address: 
        <br />
            Name of the Place:
            <input name="Venue" id="Venue" type="text" value="<%=EventEntity.Address.Venue %>" />
            <br />
            Street Address:
            <input name="Street" id="Street" type="text" value="<%=EventEntity.Address.Street %>" />
            <br />
            City:
            <input name="City" id="Text1" type="text" value="<%=EventEntity.Address.City %>" />
            <br />
            State/Province:
            <input name="Province" id="Province" type="text" value="<%=EventEntity.Address.Province %>" />
            <br />
            Country:
            <input name="Country" id="Country" type="text" value="<%=EventEntity.Address.Country %>" />
            <br />
            <%this.DescriptionAbstract.Text = EventEntity.DescriptionAbstract; %>
            Brief Description:
            <asp:TextBox ID="DescriptionAbstract" TextMode="MultiLine" runat="server"></asp:TextBox><br />
            More Description:
            <textarea id="editor" name="Description" cols="50" rows="10" style="display: none"><%=Server.HtmlEncode(EventDescription) %></textarea>
            <br />
            <input id="submit" value="submit" type="submit" style="display: none;" />
        </div>
    </form>
    <button id="submit2">Submit</button>
    <%=new Bahen.Web.Footer().GetFooter() %>
    <%
        }
    %>
</body>
</html>
