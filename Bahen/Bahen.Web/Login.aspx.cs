﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using Bahen.Web.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web;
using Bahen.DataOperationLayer.Sql;
using Bahen.DataModels.Sql;
using System.Diagnostics;
using Bahen.Web.Tools;
using Bahen.Web.Common.Constants;

namespace Bahen.Web
{
    public partial class Login : System.Web.UI.Page
    {
        const string DefaultRedirect = "/";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["LoginEmail"] != null)
            {
                switch (Context.TryLogin())
                {
                    case (LoginResult.Successful):
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["redirect"]))
                            {
                                Response.Redirect(Request.QueryString["redirect"]);
                            }
                            else
                            {
                                Response.Redirect(DefaultRedirect);
                            }
                            break;
                        }
                    case (LoginResult.InvalidCredential):
                        {
                            ResultLabel.Text = "Invalid login email or password.";
                            break;
                        }
                    case (LoginResult.AlreadyLoggedIn):
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString["redirect"]))
                            {
                                Response.Redirect(Request.QueryString["redirect"]);
                            }
                            else
                            {
                                Response.Redirect(DefaultRedirect);
                            }
                            break;
                        }
                }
            }
        }

    }
}