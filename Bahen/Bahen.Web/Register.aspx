<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Bahen.Web.Register" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Register</h1>
    <form name="register" method="post" runat="server">
        Login Email(*):
            <input name="LoginEmail" <%=EmailBoxEnabled?"":"disabled" %> value="<%=EmailString %>" /><br />
        Password(*):
            <input name="LoginPass" id="pass_LoginPass" type="password" /><br />
        Repeat Password(*):
            <input name="LoginPassRepeat" id="pass_LoginPassRepeat" type="password" /><br />
        First name(*):
            <input name="FirstName" id="txt_FirstName" type="text" /><br />
        Last name:
            <input name="LastName" id="txt_LastName" type="text" /><br />
        <input name="Register" id="bt_Register" type="submit" value="Register" />
    </form>
    <%=outputString %>
</asp:Content>
