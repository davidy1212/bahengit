﻿using Bahen.DataModels;
using Bahen.DataModels.CompositePropoties;
using Bahen.DataModels.Sql;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Bahen.Web
{
    public class UserRelationWorker
    {
        private class SimpleUser : IEntity
        {
            public long ID { get; set; }

            public SimpleUser(long id)
            {
                this.ID = id;
            }
        }

        public UserRelation FindRelation(UserEntity user1, UserEntity user2)
        {
            return this.FindRelation(user1.ID, user2.ID);
        }

        public UserRelation FindRelation(long user1ID, long user2ID)
        {
            UserRelation result = new UserRelation();

            // Check if it is self
            if (user1ID == user2ID)
            {
                result.IsSelf = true;
                return result;
            }

            // Check if user2 subscribes user1
            string sqlConnectionString = ConfigurationManager.ConnectionStrings["Sql"].ConnectionString;
            string tableConnectionString = ConfigurationManager.ConnectionStrings["Table"].ConnectionString;
            UserOperator userOperator = new UserOperator(sqlConnectionString, tableConnectionString);

            IQueryable<long> user1SubscriberIDs = userOperator.GetSubscribersByHost(user1ID).Select<SqlUserEntity, long>(x => x.ID);
            result.IsHostSubsriber = user1SubscriberIDs.Contains(user2ID);

            // Check if user1 subscribes user2
            IQueryable<long> user2SubscriberIDs = userOperator.GetSubscribersByHost(user2ID).Select<SqlUserEntity, long>(x => x.ID);
            result.IsSubscriberHost = user2SubscriberIDs.Contains(user1ID);

            // Check the common hosts
            IQueryable<long> commonHosts =
                userOperator.GetHostsBySubscriber(user1ID).Select<SqlUserEntity, long>(x => x.ID)
                .Join(
                userOperator.GetHostsBySubscriber(user2ID).Select<SqlUserEntity, long>(x => x.ID),
                (x => x),
                (x => x),
                ((x, y) => x)
                );
            result.CommonHostIDs = commonHosts.ToList();

            // Check the common cycles
            CycleOperator cycleOperator = new CycleOperator(sqlConnectionString, tableConnectionString);
            IQueryable<long> commonCycles =
                cycleOperator.GetCyclesByMember(user1ID).Select<SqlCycleEntity, long>(x => x.ID)
                .Join(
                cycleOperator.GetCyclesByMember(user2ID).Select<SqlCycleEntity, long>(x => x.ID),
                (x => x),
                (x => x),
                ((x, y) => x));

            return result;
        }
    }
}
