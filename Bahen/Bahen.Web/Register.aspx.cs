﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Bahen.Common;
using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System.Configuration;
using Bahen.Web.Common.Validating;
using Bahen.Web.Tools;
using Bahen.Search.Indexing;

namespace Bahen.Web
{
    public partial class Register : System.Web.UI.Page
    {
        const string LoginRedirAdd = "Login.aspx";
        string[] ResponseStrings = { "Success", 
                                    "Invalid login email",
                                    "Invalid password",
                                    "Invalid first name",
                                    "Invalid last name",
                                    "Create user failed" };

        public bool isSubmit = false;
        public string outputString;
        public int output;
        public bool isRedirecting = false;

        public bool EmailBoxEnabled { get; set; }
        public string EmailString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            EmailBoxEnabled = true;

            if (Session.IsAuthenticated())
            {
                Response.Redirect("/", true);
                return;
            }

            if (Request.Params["uid"] == null)
            {
                if (Request.Params["LoginEmail"] != null)
                {
                    isSubmit = true;

                    string loginEmail = Request.Params["LoginEmail"];
                    string firstName = Request.Params["FirstName"];
                    string lastName = Request.Params["LastName"];
                    string password = Request.Params["LoginPass"];
                    string passwordRepeat = Request.Params["LoginPassRepeat"];
                    string preferedName = "";
                    Byte[] passwordHashed;

                    if (!UserValidators.UserEmailValidator(loginEmail))
                    {
                        output = 1;
                    }
                    else if (!UserValidators.UserPasswordValidator(password, passwordRepeat))
                    {
                        output = 2;
                    }
                    else if (!UserValidators.UserNameValidator(firstName))
                    {
                        output = 3;
                    }
                    else if (!String.IsNullOrWhiteSpace(lastName))
                    {
                        if (!UserValidators.UserNameValidator(lastName))
                        {
                            output = 4;
                        }
                        else
                        {
                            passwordHashed = HashUtilities.ComputeHashSecure(password);
                            preferedName = firstName + " " + lastName;
                            output = NewUser(loginEmail, firstName, lastName, preferedName, passwordHashed);
                        }
                    }
                    else
                    {
                        passwordHashed = HashUtilities.ComputeHashSecure(password);
                        preferedName = firstName;
                        output = NewUser(loginEmail, firstName, lastName, preferedName, passwordHashed);
                    }
                    if (output == 0)
                    {
                        isRedirecting = true;
                        //BodyLabel.Text = @"<h1>successed, redirecting to login page</h1>";
                        if (Request.Params["redirect"] != null)
                        {
                            Response.Redirect(LoginRedirAdd + @"?redirect=" + Request.Params["redirect"], true);
                        }
                        else
                        {
                            Response.Redirect(LoginRedirAdd, true);
                        }

                    }
                    outputString = ResponseStrings[output];
                }


            }
            // Register user from invitation
            else
            {
                if (Request.QueryString["uid"] != null &&
                    Request.QueryString["emailID"] != null)
                {
                    long userID;
                    Guid guid;
                    if (!Guid.TryParse(Request.QueryString["emailID"], out guid) || !Int64.TryParse(Request.QueryString["uid"], out userID))
                    {
                        Response.Redirect("/register");
                        return;
                    }
                    UserEntity userEntity;
                    using (UserOperator userOperator = new UserOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString))
                    {
                        if (!userOperator.TryGet(userID, out userEntity))
                        {
                            Response.Redirect("/register");
                            return;
                        }
                        if (!userEntity.UnderInvitation)
                        {
                            Response.Redirect("/register");
                            return;
                        }

                        EmailBoxEnabled = false;
                        EmailString = userEntity.LoginEmail;

                        isSubmit = true;

                        if (Request.Params["LoginPass"] != null)
                        {
                            string firstName = Request.Params["FirstName"];
                            string lastName = Request.Params["LastName"];
                            string password = Request.Params["LoginPass"];
                            string passwordRepeat = Request.Params["LoginPassRepeat"];

                            if (!UserValidators.UserPasswordValidator(password, passwordRepeat))
                            {
                                output = 2;
                                outputString = ResponseStrings[output];
                                return;
                            }
                            else if (!UserValidators.UserNameValidator(firstName))
                            {
                                output = 3;
                                outputString = ResponseStrings[output];
                                return;
                            }
                            else if (!String.IsNullOrWhiteSpace(lastName))
                            {
                                if (!UserValidators.UserNameValidator(lastName))
                                {
                                    output = 4;
                                    outputString = ResponseStrings[output];
                                    return;
                                }
                                else
                                {
                                    userEntity.FirstName = firstName;
                                    userEntity.LastName = lastName;
                                    userEntity.PreferredName = firstName + " " + lastName;
                                }
                            }
                            else
                            {
                                userEntity.FirstName = firstName;
                                userEntity.PreferredName = firstName;
                            }
                            userEntity.UnderInvitation = false;
                            userEntity.LoginEmailConfirmed = true;
                            userEntity.LoginPassword = HashUtilities.ComputeHashSecure(password);
                            userOperator.Update(userEntity);
                            if (Request.Params["redirect"] != null)
                            {
                                Response.Redirect(LoginRedirAdd + @"?redirect=" + Request.Params["redirect"], true);
                            }
                            else
                            {
                                Response.Redirect(LoginRedirAdd, true);
                            }
                        }
                    }
                }
            }
        }

        private int NewUser(string loginEmail, string firstName, string lastName, string preferedName, Byte[] password)
        {
            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //ConfigurationManager.ConnectionStrings["Blob"].ConnectionString,
            //Application.GetObject<InvertedWordIndex>("InvertedWordIndex"));
            UserEntity user = new UserEntity()
            {
                LoginEmail = loginEmail,
                LoginPassword = password,
                PreferredName = preferedName,
                FirstName = firstName,
                LastName = lastName
            };
            try
            {
                userOp.Insert(user);
            }
            catch
            {
                return 5;
            }

            return 0;
        }
    }
}