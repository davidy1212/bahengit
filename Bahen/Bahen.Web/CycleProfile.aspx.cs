﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Sql;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataMiddleLayer.Newsfeed;
using Bahen.DataOperationLayer.Sql;
using Bahen.Web.Tools;

namespace Bahen.Web
{
    public partial class CycleProfile : System.Web.UI.Page
    {
        public CycleEntity CycleEntity { get; set; }
        public bool CanJoin { get; set; }
        public bool CanEdit { get; set; }
        public CycleMembershipType Membership { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Context.TryAuthenticate(false);

            // Check if has cid parameter
            if (String.IsNullOrEmpty((string)RouteData.Values["cid"]))
            {
                Response.Write("Invalid cycle id");
                return;
            }
            string cycleIdString = (string)RouteData.Values["cid"];

            // Check if the parameter is numeric
            long cycleID;
            if (!Int64.TryParse(cycleIdString, out cycleID))
            {
                Response.Write("Invalid cycle id");
                return;
            }

            // Check if the cycle id exists, get the cycle entity
            CycleOperator cycleOperator = new CycleOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //{

            CycleEntity cycleEntity;
            if (!cycleOperator.TryGet(cycleID, out cycleEntity))
            {
                Response.Write("Invalid cycle id");
                return;
            }
            this.CycleEntity = cycleEntity;

            // Calculate membership
            this.Membership = cycleOperator.GetMembership(cycleID, Session.GetAuthenticatedUserID());

            this.CanEdit = this.Membership == CycleMembershipType.Administrator;

            // Calculate if the logged in user can join the cycle
            this.CanJoin = (int)this.Membership < (int)CycleMembershipType.Member;

            // Check if request to join the cycle
            if (Request.QueryString["action"] == "join")
            {
                if (!CanJoin)
                {
                    Response.Redirect(Request.Path);
                    return;
                }
                if (Session.IsAuthenticated())
                {
                    if (this.Membership < CycleMembershipType.Member)
                    {
                        cycleOperator.SetMembership(this.CycleEntity.ID, Session.GetAuthenticatedUserID(), CycleMembershipType.Member);
                        this.CanJoin = false;

                        // Add news feed, don't have to wait
                        (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                        {
                            Content = "joined",
                            SenderID = Session.GetAuthenticatedUserID(),
                            ReferenceType = typeof(CycleEntity).ToString(),
                            ReferenceLongID = this.CycleEntity.ID
                        });
                    }
                }
                else
                {
                    Response.Redirect("/login?redirect=" + Server.UrlEncode(Request.Path + "?action=join"));
                    return;
                }
            }
            //}
        }
    }
}