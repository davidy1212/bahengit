var Bahen;
(function (Bahen) {
    (function (Web) {
        var SettingsDropdownMenu = (function () {
            function SettingsDropdownMenu(placeholder) {
                this.placeholder = placeholder;
                this.menu = document.createElement("ul");
                $(this.menu).append("<a href='/settings'><li>Settings</li></a>");
                $(this.menu).append("<a href='/logout?redirect=/'><li>Logout</li></a>");
                $(this.menu).append("<a href='/'><li>Help</li></a>");
                $(this.menu).menu();
                $(this.menu).css("padding", "5px");
                $(this.menu).css("position", "absolute");
                $(this.menu).css("font-size", "10pt");
                $(this.menu).css("font-weight", "500");
                $(this.menu).css("top", 38);
                $(this.menu).css("left", -50);
                $(this.menu).width(80);
                $(this.menu).hide();
                $.collapsible(this.menu, this.placeholder);
                $(this.placeholder).append(this.menu);
                $(this.placeholder).css("position", "relative");
            }
            return SettingsDropdownMenu;
        })();
        Web.SettingsDropdownMenu = SettingsDropdownMenu;        
        var NotificationDropdownMenu = (function () {
            function NotificationDropdownMenu(placeholder, contentAjaxAddress) {
                this.placeholder = placeholder;
                this.contentAjaxAddress = contentAjaxAddress;
                this.menu = document.createElement("ul");
                $.getJSON(contentAjaxAddress, null, function (data) {
                    this.formatMenu(data);
                }.bind(this));
                $(this.menu).menu();
                $(this.menu).addClass("collapsed");
                $(this.menu).css("padding", "5px");
                $(this.menu).css("position", "absolute");
                $(this.menu).css("top", 38);
                $(this.menu).css("left", -140);
                $(this.menu).hide();
                $.collapsible(this.menu, this.placeholder);
                $(this.menu).width(180);
                $(this.menu).css("font-size", '9pt');
                $(this.placeholder).append(this.menu);
                $(this.placeholder).css("position", "relative");
            }
            NotificationDropdownMenu.prototype.formatMenu = function (data) {
                if(data != null) {
                    if(data.length > 0) {
                        $(this.menu).append("<div>Notifications</div>");
                        for(var i = 0; i < data.length; i++) {
                            $(this.menu).append($("<li class='notification" + (data[i].read ? "" : " unread") + "' onclick=\"location.href='" + data[i].url + "'\"></li>").append($("<div></div>").append("<div style='display:inline-block; vertical-align:top; padding:5px;'><img src='" + data[i].picture + "' width='25px'/></div>" + "<div style='display:inline-block; vertical-align:top; width:140px;'>" + data[i].content + "</div>")));
                        }
                    } else {
                        $(this.menu).append("<div>No Notification</div>");
                    }
                } else {
                    $(this.menu).append("<div>No Notification</div>");
                }
            };
            return NotificationDropdownMenu;
        })();
        Web.NotificationDropdownMenu = NotificationDropdownMenu;        
    })(Bahen.Web || (Bahen.Web = {}));
    var Web = Bahen.Web;
})(Bahen || (Bahen = {}));
