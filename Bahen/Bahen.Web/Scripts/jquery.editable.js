jQuery.editable = function (selector, options) {
    var editButton;
    var editPane;
    editButton = document.createElement("span");
    $(editButton).text("Edit");
    $(editButton).css("position", "absolute");
    $(editButton).css("left", $(selector).offset()["left"] + $(selector).outerWidth());
    $(editButton).css("top", $(selector).offset()["top"] + $(selector).outerWidth());
    editPane = document.createElement("input");
    $(editButton).hide();
    $(editPane).hide();
    $(selector).hover(function () {
        $(selector).css("background-color", "yellow");
        $(editButton).show();
    }, function () {
        $(editButton).hide();
        $(editPane).hide();
    });
};
