jQuery.collapsible = function (selector, activator) {

    $(selector).hover(function () {
        $(selector).data("mouseState", true);
    }, function () {
        $(selector).data("mouseState", false);
    });

    // auto hide feature
    $(document).click(function (event) {
        if ($(event.target).is($(activator)) || $(event.target).parents('#' + $(activator).attr('id')).length > 0) {
            if (!$(selector).data("mouseState")) {
                $(selector).toggle("blind", null, 200);
                return false;
            } else {
                return true;
            }
        } else {
            if (!$(selector).data("mouseState")) {
                $(selector).hide("blind", null, 200);
            }
            return true;
        }
    });
};