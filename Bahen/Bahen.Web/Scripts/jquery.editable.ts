/// <reference path="jquery.d.ts" />
/// <reference path="jquery-ui.d.ts" />
jQuery.editable = function (selector: any, options: any) {
    var editButton: HTMLElement;
    var editPane: HTMLElement;

    editButton = document.createElement("span");
    $(editButton).text("Edit");
    $(editButton).css("position", "absolute");
    $(editButton).css("left", $(selector).offset()["left"]+$(selector).outerWidth());
    $(editButton).css("top", $(selector).offset()["top"]+$(selector).outerWidth());


    editPane = document.createElement("input");



    $(editButton).hide();
    $(editPane).hide();

    $(selector).hover(function () {
        $(selector).css("background-color", "yellow");
        $(editButton).show();
    }, function () {
        $(editButton).hide();
        $(editPane).hide();
    });
};