var Bahen;
(function (Bahen) {
    (function (Web) {
        var LoginBox = (function () {
            function LoginBox(placeholder, callback) {
                this.placeholder = placeholder;
                this.callback = callback;
                this.form = document.createElement("form");
                $(this.form).attr("id", "loginForm");
                $(this.form).attr("name", "Login");
                $(this.form).attr("method", "post");
                $(this.form).attr("action", "/ajax/loginhandler.ashx");
                $(this.form).append("Email: <input name='LoginEmail' type='text' />");
                $(this.form).append("<br />");
                $(this.form).append("Password: <input name='LoginPassword' id='LoginPasswordInput' type='password' />");
                $(this.form).append("<br />");
                $(this.form).append("<br />");
                $(this.form).append("<input name='RememberMe' id='RememberMeCheckBox' type='checkbox' style='margin-bottom:15px' />Remember Me");
                $(this.form).append("<br />");
                $(this.form).append("<input class='button-large' name='Submit' value='Login' id='LoginSubmit' type='submit' />");
                $(this.form).append("<a href='/register'><button class='button-large' type='button'>Register</button></a>");
                this.resultLabel = document.createElement("div");
                $(this.resultLabel).text("Invalid login credential");
                $(this.resultLabel).hide();
                $(this.resultLabel).css("color", "red");
                $(this.resultLabel).css("border", "1px solid red");
                $(this.resultLabel).css("margin-top", "5px");
                $(this.resultLabel).css("padding", "5px");
                $(this.form).append(this.resultLabel);
                this.popup = document.createElement("div");
                $(this.popup).hide();
                $(this.popup).css("position", "absolute");
                $(this.popup).css("top", 38);
                $(this.popup).css("left", -140);
                $(this.popup).append(this.form);
                $(this.placeholder).css("position", "relative");
                $(this.placeholder).append(this.popup);
                $.collapsible(this.popup, this.placeholder);
                $(this.form).submit(function () {
                    $(this.form).ajaxSubmit({
                        success: function (data) {
                            if(data === "Successful") {
                                $("header").load(location.href + " #header", null, this.callback);
                            } else if(data === "Unsuccessful") {
                                $(this.resultLabel).show("blind", null, 200);
                            }
                        }.bind(this)
                    });
                    return false;
                }.bind(this));
            }
            return LoginBox;
        })();
        Web.LoginBox = LoginBox;        
    })(Bahen.Web || (Bahen.Web = {}));
    var Web = Bahen.Web;
})(Bahen || (Bahen = {}));
