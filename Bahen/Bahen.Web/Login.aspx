﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Bahen.Web.Login" %>


<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="loginBox" style="margin-left: auto; margin-right: auto; width: 200px; text-align: center;" class="box">
        <form name="Login" method="post">
            Email:<br />
            <input name="LoginEmail" id="Login.aspx" type="text" />
            <br />
            Password:<br />
            <input name="LoginPassword" id="LoginPasswordInput" type="password" />
            <br />
            <br />
            <input name="RememberMe" id="RememberMeCheckBox" type="checkbox" />Remember Me
            <br />
            <br />
            <input name="Submit" class="button-large" value="Login" id="LoginSubmit" type="submit" />
            <a href="/register<%="?redirect="+Request.Params["redirect"] %>">
                <button type="button" class="button-large">Register</button>
            </a>
        </form>
        <asp:Label ID="ResultLabel" runat="server"></asp:Label>
    </div>
</asp:Content>
