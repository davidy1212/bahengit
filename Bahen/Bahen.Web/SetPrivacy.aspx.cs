﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer;
using System.Configuration;
using Bahen.DataModels;
using Bahen.DataModels.Constants.Privacy;

namespace Bahen.Web
{
    public partial class SetPrivacy : System.Web.UI.Page
    {
        UserOperator userOperator = new UserOperator(
                   ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                   ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                int profilePrivacy = Convert.ToInt32(Request.Form["ProfilePrivacy"]);

                UserEntity userEntity;

                if (userOperator.TryGet(Session.GetAuthenticatedUserID(), out userEntity))
                {

                    userEntity.ProfilePrivacyLevel = (UserProfileVisibilityLevel)profilePrivacy;

                    userOperator.Update(userEntity);

                    Response.Redirect(Request.UrlReferrer.PathAndQuery);
                }
            }
        }

        public void AddAllowedEntity(UserEntity userEntity, string targetUID)
        {
            var tempList = userEntity.ProfileAllowedEntities;
            tempList.Add(targetUID);
            userEntity.ProfileAllowedEntities = tempList;
            userOperator.Update(userEntity);
        }

        public void RemoveAllowedEntity(UserEntity userEntity, string targetUID)
        {
            var tempList = userEntity.ProfileAllowedEntities;
            tempList.Remove(targetUID);
            userEntity.ProfileAllowedEntities = tempList;
            userOperator.Update(userEntity);
        }
        public void AddBlockedEntity(UserEntity userEntity, string targetUID)
        {
            var tempList = userEntity.ProfileBlockedEntities;
            tempList.Add(targetUID);
            userEntity.ProfileAllowedEntities = tempList;
            userOperator.Update(userEntity);
        }

        public void RemoveBlockedEntity(UserEntity userEntity, string targetUID)
        {
            var tempList = userEntity.ProfileBlockedEntities;
            tempList.Remove(targetUID);
            userEntity.ProfileAllowedEntities = tempList;
            userOperator.Update(userEntity);
        }
    }
}