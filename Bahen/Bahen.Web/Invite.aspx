﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Invite.aspx.cs" Inherits="Bahen.Web.Invite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <link type="text/css" href="/Styles/jquery-ui.min.css" rel="stylesheet" />
    <script type="text/javascript" src="/Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery.form.min.js"></script>
    <script type="text/javascript" src="/Scripts/Tokenized.js"></script>
    <script type="text/javascript">
        var inviteBox;
        $(function () {
            inviteBox = new Bahen.Web.UserInviteBox($("#InviteBox"), "");
        });
    </script>
    <title></title>
</head>
<body>
    <div id="InviteBox"></div>
</body>
</html>
