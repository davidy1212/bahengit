﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using Bahen.Web.Common.Parsing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer.Blob;
using System.IO;
using Bahen.Web.Common.Validating;
using Bahen.Web.Images;

namespace Bahen.Web
{
    public partial class EditEvent : System.Web.UI.Page
    {
        public EventEntity EventEntity { get; set; }
        public string EventDescription { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if has logged in
            if (Context.TryAuthenticate())
            {
                // This page has to have a parameter of event id
                if (String.IsNullOrEmpty((string)RouteData.Values["eid"]))
                {
                    Response.Write("Invalid event id");
                    return;
                }

                string eventIdString = (string)RouteData.Values["eid"];

                // Check if the id is numeric
                long eventID;
                if (!Int64.TryParse(eventIdString, out eventID))
                {
                    Response.Write("Invalid event id");
                    return;
                }

                // Check if the event exists
                EventOperator eventOperator = new EventOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                EventEntity eventEntity;
                if (!eventOperator.TryGet(eventID, out eventEntity))
                {
                    Response.Write("Invalid event id");
                    return;
                }
                this.EventEntity = eventEntity;

                // Check if the user is the publisher
                long currentUserID = (Session["User"] as IEntity).ID;
                if (currentUserID != eventEntity.PublisherID)
                {
                    Response.Write("You don't have the permission to edit the event");
                    return;
                }

                // Check if needs to delete the event or edit
                if (Request.QueryString["delete"] == "1")
                {
                    eventOperator.Remove(this.EventEntity);
                    if (Request.QueryString["redirect"] != null)
                    {
                        Response.Redirect(Request.QueryString["redirect"]);
                    }
                    else
                    {
                        Response.Redirect("/users/" + (Session["User"] as IEntity).ID);
                    }
                    return;
                }

                // Handle request from itself
                if (Request.Params["EventName"] != null)
                {
                    EventParser parser = new EventParser();
                    EventEntity eventEntityNew;

                    if (!parser.TryParse(Context, out eventEntityNew))
                    {
                        Response.Write("Failed to parse the event");
                        return;
                    }

                    // Copy the field
                    eventEntity.Name = eventEntityNew.Name;
                    eventEntity.StartTime = eventEntityNew.StartTime;
                    eventEntity.EndTime = eventEntityNew.EndTime;
                    eventEntity.RecurringFrequency = eventEntityNew.RecurringFrequency;
                    eventEntity.RecurringTimes = eventEntityNew.RecurringTimes;
                    eventEntity.Address = eventEntityNew.Address;
                    eventEntity.DescriptionAbstract = eventEntityNew.DescriptionAbstract;

                    // Get current cycle-event relation
                    CycleOperator cycleOperator = new CycleOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                    var cycles = cycleOperator.GetCyclesByEvent(eventEntity.ID);
                    var cycleIDs = cycles.ToList().Select(x => x.ID).ToList();

                    // Add cycle-event relation
                    for (int i = 0; i < Request.Form.Count; i++)
                    {
                        if (Request.Form.GetKey(i).StartsWith("CycleList") && Request.Form[i] != "0")
                        {
                            long cycleID;
                            if (Int64.TryParse(Request.Form[i], out cycleID))
                            {
                                if (!cycleIDs.Contains(cycleID))
                                {
                                    eventOperator.AddCycle(eventEntity.ID, cycleID);
                                }
                            }
                        }
                    }

                    // Add picture
                    if (ProfilePictureUploadControl.HasFile)
                    {
                        string extension = Path.GetExtension(ProfilePictureUploadControl.PostedFile.FileName).ToLowerInvariant();
                        if (extension == ".jpg" || extension == ".png" || extension == ".bmp" || extension == ".gif")
                        {
                            try
                            {
                                // Convert to png format
                                System.Drawing.Image original = System.Drawing.Image.FromStream(ProfilePictureUploadControl.PostedFile.InputStream);

                                BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
                                    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                                Guid pictureID = Guid.NewGuid();

                                // Upload large size picture
                                using (Stream uploadStreamLarge = new MemoryStream())
                                {
                                    System.Drawing.Image largeSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Large);
                                    largeSizeImage.Save(uploadStreamLarge, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamLarge.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "e",
                                        eventEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Large,
                                        uploadStreamLarge);
                                }

                                // Remember the picture ID
                                //eventEntity.PictureGuid = pictureID;

                                // Upload medium size picture
                                using (Stream uploadStreamMedium = new MemoryStream())
                                {
                                    System.Drawing.Image mediumSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Medium);
                                    mediumSizeImage.Save(uploadStreamMedium, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamMedium.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "e",
                                        eventEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Medium,
                                        uploadStreamMedium);
                                }

                                // Upload thumbnail size picture
                                using (Stream uploadStreamThumbnail = new MemoryStream())
                                {
                                    System.Drawing.Image thumbnailSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Thumb);
                                    thumbnailSizeImage.Save(uploadStreamThumbnail, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamThumbnail.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "e",
                                        eventEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Thumb,
                                        uploadStreamThumbnail);
                                }
                            }
                            catch
                            {
                                Response.Write("Invalid image format");
                                return;
                            }
                        }
                        else
                        {
                            Response.Write("Invalid image format");
                            return;
                        }
                    }

                    // Update Description
                    if (!String.IsNullOrEmpty(Request.Params["Description"]))
                    {
                        BlobDescriptionOperator descriptionOperator = new BlobDescriptionOperator(
                            ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);

                        Guid descriptionID =
                            eventEntity.DescriptionBlobGuid == Guid.Empty ?
                            Guid.NewGuid() :
                            eventEntity.DescriptionBlobGuid;
                        eventEntity.DescriptionBlobGuid = descriptionID;

                        eventEntity.DescriptionBlobGuid = descriptionID;

                        using (MemoryStream uploadStream = new MemoryStream())
                        {
                            StreamWriter writer = new StreamWriter(uploadStream);
                            writer.Write(
                                    new RichTextValidator().Filter(
                                    Server.HtmlDecode(
                                    Request.Params["Description"])));
                            writer.Flush();
                            uploadStream.Seek(0, SeekOrigin.Begin);

                            descriptionOperator.Upload(
                                "e",
                                eventEntity.ID,
                                descriptionID,
                                uploadStream
                                );
                        }
                    }

                    // Update the event
                    eventOperator.Update(eventEntity);

                    Response.Redirect("/events/" + eventEntity.ID);
                }
                else
                {
                    // Add start and end hours
                    for (int i = 0; i < 24; i++)
                    {
                        StartHour.Items.Add(new ListItem(((i + 8) % 24).ToString(), ((i + 8) % 24).ToString(), ((i + 8) % 24) == eventEntity.StartTime.Hour));
                        EndHour.Items.Add(new ListItem(((i + 8) % 24).ToString(), ((i + 8) % 24).ToString(), ((i + 8) % 24) == eventEntity.EndTime.Hour));
                    }

                    for (int i = 0; i < 60; i = i + 5)
                    {
                        StartMinute.Items.Add(new ListItem(i.ToString(), i.ToString(), i == eventEntity.StartTime.Minute));
                        EndMinute.Items.Add(new ListItem(i.ToString(), i.ToString(), i == eventEntity.EndTime.Minute));
                    }

                    // Add publish to cycles
                    CycleOperator cycleOperator = new CycleOperator(
                            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                            ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                    var cycles = cycleOperator.GetCyclesByMember(Session.GetAuthenticatedUserID(), DataModels.Constants.CycleMembershipType.Publisher, true).ToList();
                    var cycleEvents = cycleOperator.GetCyclesByEvent(this.EventEntity.ID).ToList();

                    if (cycles.Count > 0)
                    {
                        CycleList.Items.Add(new ListItem("All", "0")
                        {
                            Selected = cycles.SequenceEqual(cycleEvents)
                        });
                    }

                    foreach (var item in cycles.Intersect(cycleEvents))
                    {
                        CycleList.Items.Add(new ListItem(item.Name, item.ID.ToString())
                        {
                            Selected = true
                        });
                    }
                    foreach (var item in cycles.Except(cycleEvents))
                    {
                        CycleList.Items.Add(new ListItem(item.Name, item.ID.ToString()));
                    }

                    // Get event description
                    try
                    {
                        BlobDescriptionOperator descriptionOperator = new BlobDescriptionOperator(
                            ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                        using (MemoryStream downloadStream = new MemoryStream())
                        {
                            descriptionOperator.Download(
                                "e",
                                this.EventEntity.ID,
                                this.EventEntity.DescriptionBlobGuid,
                                downloadStream
                                );
                            downloadStream.Seek(0, SeekOrigin.Begin);
                            StreamReader reader = new StreamReader(downloadStream);
                            this.EventDescription = reader.ReadToEnd();
                        }
                    }
                    catch
                    {
                        this.EventDescription = String.Empty;
                    }

                }
            }
        }
    }
}