﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Web.Texts
{
    /// <summary>
    /// Summary description for DescriptionHandler
    /// </summary>
    public class DescriptionHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            long id;
            Guid guid;
            if (!Int64.TryParse(context.Request.QueryString["id"], out id))
            {
                return;
            }
            if (!Guid.TryParse(context.Request.QueryString["guid"], out guid))
            {
                return;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}