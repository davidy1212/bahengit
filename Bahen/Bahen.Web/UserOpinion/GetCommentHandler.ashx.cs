﻿using Bahen.DataModels.Constants;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bahen.Web.Tools;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer.Table;
using System.Configuration;
using Bahen.Common;
using Bahen.Web.Formatting;
using System.Web.SessionState;
using Newtonsoft.Json;

namespace Bahen.Web.UserOpinion
{
    /// <summary>
    /// Summary description for GetCommentHandler
    /// </summary>
    public class GetCommentHandler : IHttpHandler, IRequiresSessionState
    {

        private static Dictionary<string, ParamRule> GetCommentParamRuleDict;
        static GetCommentHandler()
        {
            GetCommentParamRuleDict = new Dictionary<string, ParamRule>();
            GetCommentParamRuleDict.Add("tid", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            GetCommentParamRuleDict.Add("tc", new ParamRule(typeof(int), RequestMethod.GetOrPost));
            GetCommentParamRuleDict.Add("order", new ParamRule(typeof(string), RequestMethod.GetOrPost));   //ASC or DEC
            GetCommentParamRuleDict.Add("from", new ParamRule(typeof(int), RequestMethod.GetOrPost));
            GetCommentParamRuleDict.Add("n", new ParamRule(typeof(int), RequestMethod.GetOrPost));
            GetCommentParamRuleDict.Add("type", new ParamRule(typeof(string), RequestMethod.GetOrPost));    //JSON or HTML
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/HTML";
            RequestQueryParser requestParser = new RequestQueryParser();
            Dictionary<string, ParserResult> parsedQuery = requestParser.Parse(GetCommentParamRuleDict, context.Request);
            if (!parsedQuery["tc"].IsValid || !parsedQuery["tid"].IsValid)
            {
                context.Response.Write("error");
                return;
            }
            IEnumerable<TableCommentEntity> commentList;
            switch (GuidOrIDTester.GuidOrID((int)parsedQuery["tc"].Value))
            {
                case "Guid":
                    Guid targetGuid = new Guid();
                    if (Guid.TryParseExact(parsedQuery["tid"].Value.ToString(), "N", out targetGuid))
                    {
                        commentList = GetCommentList(targetGuid, (int)parsedQuery["tc"].Value);
                    }
                    else
                    {
                        context.Response.Write("error");
                        return;
                    }
                    break;
                case "ID":
                    try
                    {
                        long targetID = Convert.ToInt64(parsedQuery["tid"].Value);
                        commentList = GetCommentList(targetID, (int)parsedQuery["tc"].Value);
                    }
                    catch (System.FormatException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    catch (System.InvalidCastException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    catch (System.OverflowException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    break;
                default:
                    context.Response.Write("error");
                    return;
                    break;
            }
            if (!parsedQuery["order"].IsValid)      // Order not specified - asc
            {
                commentList = commentList.ToList().OrderBy(c => c.ActionTime);
            }
            else
            {
                switch (parsedQuery["order"].Value.ToString())
                {
                    case "asc":
                        commentList = commentList.ToList().OrderBy(c => c.ActionTime);
                        break;
                    case "dec":
                        commentList = commentList.ToList().OrderByDescending(c => c.ActionTime);
                        break;
                    default:
                        goto case "asc";
                        break;
                }
            }
            if (parsedQuery["from"].IsValid)
            {
                commentList = commentList.Skip((int)parsedQuery["from"].Value);
            }
            if (parsedQuery["n"].IsValid)
            {
                commentList = commentList.Take((int)parsedQuery["n"].Value);
            }
            if (!parsedQuery["type"].IsValid)           // type not specified - JSON
            {
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(commentList.ToArray()));
                return;
            }
            switch (parsedQuery["type"].Value.ToString())
            {
                case "JSON":
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(commentList.ToArray()));
                    return;
                    break;
                case "HTML":
                    context.Response.ContentType = "text/HTML";
                    CommentFormatter commentFormatter = new CommentFormatter();
                    context.Response.Write(commentFormatter.FormatList(commentList));
                    return;
                    break;
                default:
                    goto case "JSON";
                    break;
            }
        }

        private IQueryable<TableCommentEntity> GetCommentList(Guid targetGuid, int typeCode)
        {
            TableCommentOperator commentOperator = new TableCommentOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //var comments = commentOperator.GetByEntityGuid(targetGuid, (EntityTypeCode)typeCode);
            //return comments;
            throw new NotImplementedException();
        }

        private IQueryable<TableCommentEntity> GetCommentList(long targetID, int typeCode)
        {
            TableCommentOperator commentOperator = new TableCommentOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //var comments = commentOperator.GetByEntityID(targetID, (EntityTypeCode)typeCode);
            //return comments;
            throw new NotImplementedException();
        }

        #region Public methods for ASP.NET to call

        public int GetCommentNumber(long targetID, int typeCode)
        {
            return GetCommentList(targetID, typeCode).Count();
        }

        public int GetCommentNumber(Guid targetGuid, int typeCode)
        {
            return GetCommentList(targetGuid, typeCode).Count();
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetID"/> and <paramref name="typeCode"/> ordered in descending time
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <returns>HTML string contaning the ordered, formatted list of comments ordered in descending time</returns>
        public string GetComments(long targetID, int typeCode)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            return commentFormatter.FormatList(GetCommentList(targetID, typeCode).ToList().OrderByDescending(c => c.ActionTime));
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with specidied <paramref name="targetID"/> and <paramref name="typeCode"/>
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="descendingTime">True for ordering according descending time, False for ascending time</param>
        /// <returns>HTML string contaning the orderd, formatted list of comments</returns>
        public string GetComments(long targetID, int typeCode, bool descendingTime)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            if (descendingTime)
            {
                return commentFormatter.FormatList(GetCommentList(targetID, typeCode).ToList().OrderByDescending(c => c.ActionTime));
            }
            else
            {
                return commentFormatter.FormatList(GetCommentList(targetID, typeCode).ToList().OrderBy(c => c.ActionTime));
            }
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetID"/> and <paramref name="typeCode"/> ordered in descending time,
        /// with specified number and starting index
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="startIndex">Zero-based starting index</param>
        /// <param name="number">The number of comments returned</param>
        /// <returns>HTML string contaning the ordered, formatted list of comments begining at <paramref name="startIndex"/> with <paramref name="number"/> comments</returns>
        public string GetComments(long targetID, int typeCode, int startIndex, int number)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            var comments = GetCommentList(targetID, typeCode).ToList().OrderByDescending(c => c.ActionTime).Skip(startIndex).Take(number);
            return commentFormatter.FormatList(comments);
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetID"/> and <paramref name="typeCode"/> ordered in descending time,
        /// with specified number and starting index
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="startIndex">Zero-based starting index</param>
        /// <param name="number">The number of comments returned</param>
        /// <param name="descendingTime">True for ordering according descending time, False for ascending time</param>
        /// <returns>HTML string contaning the ordered, formatted list of comments begining at <paramref name="startIndex"/> with <paramref name="number"/> comments</returns>
        public string GetComments(long targetID, int typeCode, int startIndex, int number, bool descendingTime)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            if (descendingTime)
            {
                var comments = GetCommentList(targetID, typeCode).ToList().OrderByDescending(c => c.ActionTime).Skip(startIndex).Take(number);
                return commentFormatter.FormatList(comments);
            }
            else
            {
                var comments = GetCommentList(targetID, typeCode).ToList().OrderBy(c => c.ActionTime).Skip(startIndex).Take(number);
                return commentFormatter.FormatList(comments);
            }
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetGuid"/> and <paramref name="typeCode"/> ordered in descending time
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <returns>HTML string contaning the ordered, formatted list of comments ordered in descending time</returns>
        public string GetComments(Guid targetGuid, int typeCode)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            return commentFormatter.FormatList(GetCommentList(targetGuid, typeCode).ToList().OrderByDescending(c => c.ActionTime));
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with specidied <paramref name="targetGuid"/> and <paramref name="typeCode"/>
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="descendingTime">True for ordering according descending time, False for ascending time</param>
        /// <returns>HTML string contaning the orderd, formatted list of comments</returns>
        public string GetComments(Guid targetGuid, int typeCode, bool descendingTime)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            if (descendingTime)
            {
                return commentFormatter.FormatList(GetCommentList(targetGuid, typeCode).ToList().OrderByDescending(c => c.ActionTime));
            }
            else
            {
                return commentFormatter.FormatList(GetCommentList(targetGuid, typeCode).ToList().OrderBy(c => c.ActionTime));
            }
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetGuid"/> and <paramref name="typeCode"/> ordered in descending time,
        /// with specified number and starting index
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="startIndex">Zero-based starting index</param>
        /// <param name="number">The number of comments returned</param>
        /// <returns>HTML string contaning the ordered, formatted list of comments begining at <paramref name="startIndex"/> with <paramref name="number"/> comments</returns>
        public string GetComments(Guid targetGuid, int typeCode, int startIndex, int number)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            var comments = GetCommentList(targetGuid, typeCode).ToList().OrderByDescending(c => c.ActionTime).Skip(startIndex).Take(number);
            return commentFormatter.FormatList(comments);
        }

        /// <summary>
        /// Get the formatted list of comments of the entity with the specidied <paramref name="targetGuid"/> and <paramref name="typeCode"/> ordered in descending time,
        /// with specified number and starting index
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the TypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <param name="startIndex">Zero-based starting index</param>
        /// <param name="number">The number of comments returned</param>
        /// <param name="descendingTime">True for ordering according descending time, False for ascending time</param>
        /// <returns>HTML string contaning the ordered, formatted list of comments begining at <paramref name="startIndex"/> with <paramref name="number"/> comments</returns>
        public string GetComments(Guid targetGuid, int typeCode, int startIndex, int number, bool descendingTime)
        {
            CommentFormatter commentFormatter = new CommentFormatter();
            if (descendingTime)
            {
                var comments = GetCommentList(targetGuid, typeCode).ToList().OrderByDescending(c => c.ActionTime).Skip(startIndex).Take(number);
                return commentFormatter.FormatList(comments);
            }
            else
            {
                var comments = GetCommentList(targetGuid, typeCode).ToList().OrderBy(c => c.ActionTime).Skip(startIndex).Take(number);
                return commentFormatter.FormatList(comments);
            }
        }

        /// <summary>
        /// Get the HTML string of a comment box that post comment to entity with specified ID and EntityTypeCode
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the EntityTypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <returns></returns>
        public string GetCommentBoxHTML(long targetID, int typeCode)
        {
            return String.Format(CommentBoxHTML, targetID, typeCode);
        }

        /// <summary>
        /// Get the HTML string of a comment box that post comment to entity with specified Guid and EntityTypeCode
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the EntityTypeCode of the target entity <see cref="EntityTypeCode"/></param>
        /// <returns></returns>
        public string GetCommentBoxHTML(Guid targetGuid, int typeCode)
        {
            return String.Format(CommentBoxHTML, targetGuid.ToString("N"), typeCode);
        }

        private const string CommentBoxHTML =
            @"<div id='CommentBox'><form name='Comment' method='post' action='/UserOpinion/PostCommentHandler.ashx'>" +
            @"Comment:" +
            @"<input name='content' id='CommentContent' type='text' />" +
            @"<input name='Submit' value='Post' id='Submit' type='submit' />" +
            @"<input name='tid' value='{0}' hidden='hidden' type='hidden' />" +
            @"<input name='tc' value='{1}' hidden='hidden' type='hidden' />" +
            @"</form></div>";

        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}