﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer.Table;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Bahen.Web.UserOpinion
{
    /// <summary>
    /// Summary description for ChangeLikeHandler
    /// </summary>
    public class ChangeLikeHandler : IHttpHandler, IRequiresSessionState
    {
        TableLikeOperator LikeOperator = new TableLikeOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
        private static Dictionary<string, ParamRule> ChangeLikeParamRuleDict;
        static ChangeLikeHandler()
        {
            ChangeLikeParamRuleDict = new Dictionary<string, ParamRule>();
            ChangeLikeParamRuleDict.Add("tid", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            ChangeLikeParamRuleDict.Add("tc", new ParamRule(typeof(int), RequestMethod.GetOrPost));
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/HTML";
            if (context.Session.IsAuthenticated())
            {
                var userEntity = context.Session.GetAuthenticatedUser();
                RequestQueryParser requestParser = new RequestQueryParser();
                var parsedQuery = requestParser.Parse(ChangeLikeParamRuleDict, context.Request);
                if (!parsedQuery["tc"].IsValid || !parsedQuery["tid"].IsValid)
                {
                    context.Response.Write("error");
                    return;
                }
                switch (GuidOrIDTester.GuidOrID((int)parsedQuery["tc"].Value))
                {
                    case "Guid":
                        Guid targetGuid = new Guid();
                        if (Guid.TryParseExact(parsedQuery["tid"].Value.ToString(), "N", out targetGuid))
                        {
                            LikeOrUnlike(GetLikeList(targetGuid, (int)parsedQuery["tc"].Value), targetGuid, (int)parsedQuery["tc"].Value, context.Session.GetAuthenticatedUser());
                        }
                        else
                        {
                            context.Response.Write("error");
                            return;
                        }
                        break;
                    case "ID":
                        try
                        {
                            long targetID = Convert.ToInt64(parsedQuery["tid"].Value);
                            LikeOrUnlike(GetLikeList(targetID, (int)parsedQuery["tc"].Value), targetID, (int)parsedQuery["tc"].Value, context.Session.GetAuthenticatedUser());
                        }
                        catch (System.FormatException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        catch (System.InvalidCastException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        catch (System.OverflowException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        break;
                    default:
                        context.Response.Write("error");
                        return;
                        break;
                }
                context.Response.Write("Success");
                return;
            }
            context.Response.Write("You have to login");
            return;
        }
        public void LikeOrUnlike(IEnumerable<TableLikeEntity> likeList, long targetID, int typeCode, UserEntity userEntity)
        {
            if (likeList.Any(like => like.UserID == userEntity.ID))
            {
                DeleteLike(likeList, userEntity.ID);
            }
            else
            {
                PostLike(targetID, typeCode, userEntity.ID, userEntity.PreferredName);
            }
        }

        public void LikeOrUnlike(IEnumerable<TableLikeEntity> likeList, Guid targetGuid, int typeCode, UserEntity userEntity)
        {
            if (likeList.Any(like => like.UserID == userEntity.ID))
            {
                DeleteLike(likeList, userEntity.ID);
            }
            else
            {
                PostLike(targetGuid, typeCode, userEntity.ID, userEntity.PreferredName);
            }
        }

        private void PostLike(long targetID, int typeCode, long userID, string userName)
        {
            //TableLikeEntity likeEntity = new TableLikeEntity();
            //likeEntity.TargetID = targetID;
            //likeEntity.TargetTypeCode = typeCode;
            //likeEntity.UserID = userID;
            //likeEntity.UserName = userName;
            //likeEntity.ActionTime = DateTime.UtcNow;
            //LikeOperator.Insert(likeEntity);
        }

        private void PostLike(Guid targetGuid, int typeCode, long userID, string userName)
        {
            //TableLikeEntity likeEntity = new TableLikeEntity();
            //likeEntity.TargetGuid = targetGuid;
            //likeEntity.TargetTypeCode = typeCode;
            //likeEntity.UserID = userID;
            //likeEntity.UserName = userName;
            //likeEntity.ActionTime = DateTime.UtcNow;
            //LikeOperator.Insert(likeEntity);
        }

        private void DeleteLike(IEnumerable<TableLikeEntity> likeList, long userID)
        {
            TableLikeEntity likeEntity = new TableLikeEntity();
            likeEntity = likeList.First(like => like.UserID == userID);
            LikeOperator.Remove(likeEntity.PartitionKey, likeEntity.RowKey);
        }

        private IEnumerable<TableLikeEntity> GetLikeList(long targetID, int typeCode)
        {
            //var likes = LikeOperator.GetByEntityID(targetID, (EntityTypeCode)typeCode).ToList();
            //return likes;
            throw new NotImplementedException();
        }

        private IEnumerable<TableLikeEntity> GetLikeList(Guid targetGuid, int typeCode)
        {
            //var likes = LikeOperator.GetByEntityGuid(targetGuid, (EntityTypeCode)typeCode).ToList();
            //return likes;
            throw new NotImplementedException();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}