﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer.Table;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Bahen.Web.UserOpinion
{
    /// <summary>
    /// Summary description for PostCommentHandler
    /// </summary>
    public class PostCommentHandler : IHttpHandler, IRequiresSessionState
    {
        private static Dictionary<string, ParamRule> PostCommentParamRuleDict;
        static PostCommentHandler()
        {
            PostCommentParamRuleDict = new Dictionary<string, ParamRule>();
            PostCommentParamRuleDict.Add("content", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            PostCommentParamRuleDict.Add("tid", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            PostCommentParamRuleDict.Add("tc", new ParamRule(typeof(int), RequestMethod.GetOrPost));
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/HTML";
            if (context.Session.IsAuthenticated())
            {
                var userEntity = context.Session.GetAuthenticatedUser();
                RequestQueryParser requestParser = new RequestQueryParser();
                var parsedQuery = requestParser.Parse(PostCommentParamRuleDict, context.Request);
                if (!parsedQuery["tc"].IsValid || !parsedQuery["content"].IsValid || !parsedQuery["tid"].IsValid)
                {
                    context.Response.Write("error");
                    return;
                }
                switch (GuidOrIDTester.GuidOrID((int)parsedQuery["tc"].Value))
                {
                    case "Guid":
                        Guid targetGuid = new Guid();
                        if (Guid.TryParseExact(parsedQuery["tid"].Value.ToString(), "N", out targetGuid))
                        {
                            PostComment(targetGuid, (int)parsedQuery["tc"].Value, parsedQuery["content"].Value.ToString(), userEntity);
                        }
                        else
                        {
                            context.Response.Write("error");
                            return;
                        }
                        break;
                    case "ID":
                        try
                        {
                            long targetID = Convert.ToInt64(parsedQuery["tid"].Value);
                            PostComment(targetID, (int)parsedQuery["tc"].Value, parsedQuery["content"].Value.ToString(), userEntity);
                        }
                        catch (System.FormatException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        catch (System.InvalidCastException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        catch (System.OverflowException)
                        {
                            context.Response.Write("error");
                            return;
                        }
                        break;
                    default:
                        context.Response.Write("error");
                        return;
                        break;
                }
                context.Response.Write("Success");
                return;
            }
            context.Response.Write("You have to login");
            return;
        }

        /// <summary>
        /// Post a comment to a entity specified by its ID and EntityTypeCode
        /// </summary>
        /// <param name="targetID">the ID of the target entity</param>
        /// <param name="typeCode">the EntityTypeCode of the target entity</param>
        /// <param name="commentContent">the content of the comment</param>
        public void PostComment(long targetID, int typeCode, string commentContent, UserEntity userEntity)
        {
            TableCommentEntity commentEntity = new TableCommentEntity();
            
            //commentEntity.TargetID = targetID;
            //commentEntity.TargetTypeCode = typeCode;
            //commentEntity.UserID = userEntity.UserID;
            //commentEntity.UserName = userEntity.PreferredName;
            //commentEntity.UserProfilePicGuid = userEntity.ProfilePictureGuid;
            //commentEntity.UserProfileUrl = userEntity.ProfileUrl;
            //commentEntity.Content = commentContent;
            //commentEntity.ActionTime = DateTime.UtcNow;

            //TableCommentOperator commentOperator = new TableCommentOperator(
            //    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //commentOperator.Insert(commentEntity);
        }

        /// <summary>
        /// Post a comment to a entity specified by its ID and EntityTypeCode
        /// </summary>
        /// <param name="targetGuid">the Guid of the target entity</param>
        /// <param name="typeCode">the EntityTypeCode of the target entity</param>
        /// <param name="commentContent">the content of the comment</param>
        public void PostComment(Guid targetGuid, int typeCode, string commentContent, UserEntity userEntity)
        {
            //TableCommentEntity commentEntity = new TableCommentEntity();

            //commentEntity.TargetGuid = targetGuid;
            //commentEntity.TargetTypeCode = typeCode;
            //commentEntity.UserID = userEntity.UserID;
            //commentEntity.UserName = userEntity.PreferredName;
            //commentEntity.UserProfilePicGuid = userEntity.ProfilePictureGuid;
            //commentEntity.UserProfileUrl = userEntity.ProfileUrl;
            //commentEntity.Content = commentContent;
            //commentEntity.ActionTime = DateTime.UtcNow;

            //TableCommentOperator commentOperator = new TableCommentOperator(
            //    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            //commentOperator.Insert(commentEntity);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}