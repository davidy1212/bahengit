﻿using Bahen.Common;
using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Sql;
using Bahen.DataModels.Table;
using Bahen.DataOperationLayer;
using Bahen.DataOperationLayer.Table;
using Bahen.Web.Formatting;
using Bahen.Web.Tools;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Bahen.Web.UserOpinion
{
    /// <summary>
    /// Summary description for GetLikeHandler
    /// </summary>
    public class GetLikeHandler : IHttpHandler, IRequiresSessionState
    {
        TableLikeOperator LikeOperator = new TableLikeOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

        private static Dictionary<string, ParamRule> GetLikeParamRuleDict;
        static GetLikeHandler()
        {
            GetLikeParamRuleDict = new Dictionary<string, ParamRule>();
            GetLikeParamRuleDict.Add("tid", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            GetLikeParamRuleDict.Add("tc", new ParamRule(typeof(int), RequestMethod.GetOrPost));
            GetLikeParamRuleDict.Add("type", new ParamRule(typeof(string), RequestMethod.GetOrPost));       // "JSON" or "HTML"
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/HTML";
            RequestQueryParser requestParser = new RequestQueryParser();
            Dictionary<string, ParserResult> parsedQuery = requestParser.Parse(GetLikeParamRuleDict, context.Request);
            if (!parsedQuery["tc"].IsValid || !parsedQuery["tid"].IsValid)
            {
                context.Response.Write("error");
                return;
            }
            IEnumerable<SqlUserEntity> likeUserList;
            switch (GuidOrIDTester.GuidOrID((int)parsedQuery["tc"].Value))
            {
                case "Guid":
                    Guid targetGuid = new Guid();
                    if (Guid.TryParseExact(parsedQuery["tid"].Value.ToString(), "N", out targetGuid))
                    {
                        likeUserList = GetLikeUsersList(targetGuid, (int)parsedQuery["tc"].Value);
                    }
                    else
                    {
                        context.Response.Write("error");
                        return;
                    }
                    break;
                case "ID":
                    try
                    {
                        long targetID = Convert.ToInt64(parsedQuery["tid"].Value);
                        likeUserList = GetLikeUsersList(targetID, (int)parsedQuery["tc"].Value);
                    }
                    catch (System.FormatException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    catch (System.InvalidCastException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    catch (System.OverflowException)
                    {
                        context.Response.Write("error");
                        return;
                    }
                    break;
                default:
                    context.Response.Write("error");
                    return;
                    break;
            }
            if (!parsedQuery["type"].IsValid)           // type not specified - JSON
            {
                context.Response.Write(JsonConvert.SerializeObject(likeUserList.ToArray()));
                return;
            }
            switch (parsedQuery["type"].Value.ToString())
            {
                case "JSON":
                    context.Response.ContentType = "application/json";
                    //context.Response.Write(JsonConvert.SerializeObject(likeUserList.Select(
                    //    u => new
                    //    {
                    //        Name = u.PreferredName,
                    //        Picture = u.ProfilePicture,
                    //        ID = u.ID
                    //    })));
                    return;
                    break;
                case "HTML":
                    context.Response.ContentType = "text/HTML";
                    UserFormatter userFormatter = new UserFormatter();
                    context.Response.Write(userFormatter.FormatList(likeUserList));
                    return;
                    break;
                default:
                    goto case "JSON";
                    break;
            }
        }
        public IEnumerable<SqlUserEntity> GetLikeUsersList(long targetID, int typeCode)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            return GetLikeList(targetID, typeCode).Select(like => userOperator.Get(like.UserID).SqlEntity);
        }

        public IEnumerable<SqlUserEntity> GetLikeUsersList(Guid targetGuid, int typeCode)
        {
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            return GetLikeList(targetGuid, typeCode).Select(like => userOperator.Get(like.UserID).SqlEntity);
        }

        private IEnumerable<TableLikeEntity> GetLikeList(long targetID, int typeCode)
        {
            //var likes = LikeOperator.GetByEntityID(targetID, (EntityTypeCode)typeCode).ToList();
            //return likes;
            throw new NotImplementedException();
        }

        private IEnumerable<TableLikeEntity> GetLikeList(Guid targetGuid, int typeCode)
        {
            //var likes = LikeOperator.GetByEntityGuid(targetGuid, (EntityTypeCode)typeCode).ToList();
            //return likes;
            throw new NotImplementedException();
        }

        public string GetLikeButton(long targetID, int typeCode, UserEntity userEntity)
        {
            long userID = 0;
            if (userEntity != null)
            {
                userID = userEntity.ID;
            }

            if (userID == 0 || !GetLikeList(targetID, typeCode).Any(like => like.UserID == userID))
            {
                return String.Format("<div class='like_button'><a href='/UserOpinion/ChangeLikeHandler.ashx?tid={0}&tc={1}'>Like</a></div>",
                    targetID, typeCode);
            }
            else
            {
                return String.Format("<div class='like_button'><a href='/UserOpinion/ChangeLikeHandler.ashx?tid={0}&tc={1}'>Unlike</a></div>",
                    targetID, typeCode);
            }
        }

        public string GetLikeButton(Guid targetGuid, int typeCode, UserEntity userEntity)
        {
            long userID = 0;
            if (userEntity != null)
            {
                userID = userEntity.ID;
            }
            if (userID == 0 || !GetLikeList(targetGuid, typeCode).Any(like => like.UserID == userID))
            {
                return String.Format("<div class='like_button'><a href='/UserOpinion/ChangeLikeHandler.ashx?tid={0}&tc={1}'>Like</a></div>",
                    targetGuid.ToString("N"), typeCode);
            }
            else
            {
                return String.Format("<div class='like_button'><a href='/UserOpinion/ChangeLikeHandler.ashx?tid={0}&tc={1}'>Unlike</a></div>",
                    targetGuid.ToString("N"), typeCode);
            }
        }

        public int GetLikeNumber(long targetID, int typeCode)
        {
            return GetLikeList(targetID, typeCode).Count();
        }

        public int GetLikeNumber(Guid targetGuid, int typeCode)
        {
            return GetLikeList(targetGuid, typeCode).Count();
        }

        public string GetLikeNumberWithLink(long targetID, int typeCode)
        {
            string url = String.Format(@"/UserOpinion/GetLikeHandler.ashx?tid={0}&tc={1}&type={2}", targetID, typeCode, "HTML");
            return String.Format(@"<a href='{0}'>{1}</a>", url, GetLikeNumber(targetID, typeCode));
        }

        public string GetLikeNumberWithLink(Guid targetGuid, int typeCode)
        {
            string url = String.Format(@"/UserOpinion/GetLikeHandler.ashx?tid={0}&tc={1}&type={2}", targetGuid.ToString("N"), typeCode, "HTML");
            return String.Format(@"<a href='{0}'>{1}</a>", url, GetLikeNumber(targetGuid, typeCode));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}