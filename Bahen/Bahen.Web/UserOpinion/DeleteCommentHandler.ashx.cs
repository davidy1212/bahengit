﻿using Bahen.DataModels.Constants;
using Bahen.DataOperationLayer.Table;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Bahen.Web.UserOpinion
{
    /// <summary>
    /// Summary description for DeleteCommentHandler
    /// </summary>
    public class DeleteCommentHandler : IHttpHandler, IRequiresSessionState
    {
        private static Dictionary<string, ParamRule> DeleteCommentParamRuleDict;
        static DeleteCommentHandler()
        {
            DeleteCommentParamRuleDict = new Dictionary<string, ParamRule>();
            DeleteCommentParamRuleDict.Add("pk", new ParamRule(typeof(string), RequestMethod.GetOrPost));
            DeleteCommentParamRuleDict.Add("rk", new ParamRule(typeof(string), RequestMethod.GetOrPost));
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/HTML";
            if (context.Session.IsAuthenticated())
            {
                RequestQueryParser requestParser = new RequestQueryParser();
                var parsedQuery = requestParser.Parse(DeleteCommentParamRuleDict, context.Request);

                if (parsedQuery["pk"].IsValid && parsedQuery["rk"].IsValid)
                {
                    this.DeleteComment(parsedQuery["pk"].Value.ToString(), parsedQuery["rk"].Value.ToString());
                    context.Response.Write("Success");
                    return;
                }
                else
                {
                    context.Response.Write("error");
                    return;
                }
            }
            else
            {
                context.Response.Write("You must login");
                return;
            }
        }

        public void DeleteComment(string partitionKey, string rowKey)
        {
            TableCommentOperator commentOperator = new TableCommentOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            commentOperator.Remove(partitionKey, rowKey);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}