﻿using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Bahen.Web.Styles
{
    /// <summary>
    /// Summary description for StyleHandler
    /// </summary>
    public class StyleHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/css";
            DateTime lastModified = File.GetLastAccessTimeUtc("Default.min.css");
            if (Cache.IsClientCached(context, lastModified))
            {
                context.Response.StatusCode = 304;
                context.Response.SuppressContent = true;
            }
            else
            {
                context.Response.WriteFile("Default.min.css");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}