﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditUser.aspx.cs" Inherits="Bahen.Web.EditUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <title></title>
</head>
<body>
    <%if (UserEntity != null && CanEdit)
      { %>
    <%=new Bahen.Web.Header().GetHeader() %>
    <form id="form1" runat="server">
        <div>
            First Name:
            <input name="FirstName" type="text" value="<%=UserEntity.FirstName %>" /><br />
            Last Name:
            <input name="LastName" type="text" value="<%=UserEntity.LastName %>" /><br />
            Profile Picture:<br />
            <img src="/images/ProfilePictureHandler.ashx?size=medium&prefix=u&id=<%=UserEntity.ID %>&guid=<%=UserEntity.ProfilePictureGuid %>" /><br />
            <asp:FileUpload ID="ProfilePictureUploadControl" runat="server" />
            <br />
            <input name="submit" type="submit" value="Save" />
            <br />
        </div>
    </form>
    Who can view my profile? Currently: <%=UserEntity.ProfilePrivacyLevel %>
    <br />
    <form name="ProfilePrivacyF" action="/SetPrivacy.aspx" method="post">
        <input id="Radio1" name="ProfilePrivacy" type="radio"  value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.MeSubscribed%>"/> I subscribed
        <input id="Radio2" name="ProfilePrivacy" type="radio" value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.SubscribedMe%>"/> Subscribed me
        <input id="Radio3" name="ProfilePrivacy" type="radio" value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.OnlyMe%>"/> Only me
        <input id="Radio4" name="ProfilePrivacy" type="radio" value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.MeSubscribedAndSubscribedMe%>"/> I subscribed who also subscribed me
        <input id="Radio5" name="ProfilePrivacy" type="radio" value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.EveryoneLogedIn%>"/> Everyone logged in
        <input id="Radio6" name="ProfilePrivacy" type="radio" value="<%=(int)Bahen.DataModels.Constants.PrivacyRestriction.UserProfileVisibilityLevel.Public%>"/> Public
        <br />
        <input id="Submit1" type="submit" value="Set" />
    </form>
    
    <%=new Bahen.Web.Footer().GetFooter() %>
    <%} %>
</body>
</html>
