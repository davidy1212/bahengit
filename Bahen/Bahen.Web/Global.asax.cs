﻿using Bahen.DataMiddleLayer.Email;
using Bahen.DataMiddleLayer.Newsfeed;
using Bahen.DataMiddleLayer.Notification;
using Bahen.DataOperationLayer.Index;
using Bahen.Search.Indexing;
using Bahen.Web.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace Bahen.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            // Newsfeed
            Application["NewsfeedStorageManager"] = new NewsfeedStorageManager(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            // Notification
            Application["NotificationStorageManager"] = new NotificationStorageManager(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            // Email
            Application["EmailManager"] = new EmailManager(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString,
                ConfigurationManager.AppSettings["SiteName"],
                ConfigurationManager.AppSettings["SiteUrl"]);

            // Indexer
            DefaultIndexer indexer = new DefaultIndexer(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Blob"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Queue"].ConnectionString);
            Application.SetObject("Indexer", indexer);

            InvertedWordIndex index;
            indexer.Initialize(false, out index);

            // Index
            Application.SetObject("InvertedWordIndex", index);
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("EventDetail",
                "events/{eid}",
                "~/EventDetail.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"eid","[0-9]+"}
                });
            routes.MapPageRoute("CycleProfile",
                "cycles/{cid}",
                "~/CycleProfile.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"cid","[0-9]+"}
                });
            routes.MapPageRoute("UserProfile",
                "users/{uid}",
                "~/UserProfile.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"uid","[0-9]+"}
                });
            routes.MapPageRoute("EditEventDetail",
                "events/{eid}/edit",
                "~/EditEvent.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"eid","[0-9]+"}
                });
            routes.MapPageRoute("EditCycleProfile",
                "cycles/{cid}/edit",
                "~/EditCycle.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"cid","[0-9]+"}
                });
            routes.MapPageRoute("EditUserProfile",
                "users/{uid}/edit",
                "~/EditUser.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary(){
                    {"uid","[0-9]+"}
                });
            routes.MapPageRoute("Login",
                "login",
                "~/Login.aspx");
            routes.MapPageRoute("Logout",
                "logout",
                "~/Logout.aspx");
            routes.MapPageRoute("Register",
                "register",
                "~/Register.aspx");
            routes.MapPageRoute("CreateEvent",
                "events/create",
                "~/CreateEvent.aspx");
            routes.MapPageRoute("CreateCycle",
                "cycles/create",
                "~/CreateCycle.aspx");
            routes.MapPageRoute("Invite",
                "{refer}/{id}/invite",
                "~/Invite.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary()
                {
                    {"refer","(events)|(cycles)|(users)"},
                    {"id","[0-9]+"}
                });
            routes.MapPageRoute("NotificationRedirect",
                "notifications/{uid}/{guid}",
                "~/NotificationRedirect.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary()
                {
                    {"uid","[0-9]+"},
                    {"guid","[abcdef0-9-]+"}
                });
            routes.MapPageRoute("NotificationList",
                "notifications",
                "~/NotificationList.aspx");
            routes.MapPageRoute("Error",
                "error",
                "~/Error.aspx");
            routes.MapPageRoute("Email",
                "emails/{uid}/{guid}",
                "~/EmailRedirect.aspx",
                false,
                new RouteValueDictionary(),
                new RouteValueDictionary()
                {
                    {"uid","[0-9]+"},
                    {"guid","[abcdef0-9-]+"}
                });
            //routes.MapPageRoute("Test",
            //    "test",
            //    "~/test.html");
            routes.MapRoute("test", "test.html");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            IndexBackUp();
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            IndexBackUp();
        }

        protected void IndexBackUp()
        {
            Application.GetObject<DefaultIndexer>("Indexer")
            .Serialize(Application.GetObject<InvertedWordIndex>("InvertedWordIndex"));
        }
    }
}