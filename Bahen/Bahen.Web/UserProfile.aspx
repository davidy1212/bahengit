﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="Bahen.Web.UserProfile" %>

<%@ Import Namespace="Bahen.Web.Tools" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <%if(CanView){ %>
    <%if (UserEntity != null)
      { %>
    <div style="display: inline-block; vertical-align: top;">
        <h1><%=UserEntity.PreferredName %></h1>
        <img src="/images/ProfilePictureHandler.ashx?size=medium&prefix=u&id=<%=UserEntity.ID %>&guid=<%=UserEntity.ProfilePictureGuid %>" /><br />
        <%if (CanSubscribe)
          { %>
        <a class="button-large" href="/users/<%=UserEntity.ID %>?action=subscribe">Subscribe</a><br />
        <%} %>
        <%if (CanEdit)
          { %>
        <a class="button-large" href="/users/<%=UserEntity.ID %>/edit">Edit</a><br />
        <%} %>
        <div>
            <%if (UserEntity.SqlEntity.HostSubscriptions.Count > 0)
              { %>
            <h2><%=UserEntity.SqlEntity.HostSubscriptions.Count %> Subscriber<%=UserEntity.SqlEntity.HostSubscriptions.Count>1?"s":"" %></h2>
            <%=new Bahen.Web.UserList().GetSubscriberList(UserEntity.ID) %>
            <%} %>
        </div>
        <div>
            <%if (this.CycleCount > 0)
              { %>
            <h2><%=this.CycleCount %> Cycle<%=this.CycleCount>1?"s":"" %></h2>
            <%=new Bahen.Web.CycleList().GetCycleList(UserEntity.ID) %>
            <%} %>
        </div>
    </div>
    <div style="display: inline-block; vertical-align: top;">
        <h2><%=UserEntity.PreferredName %>'s News Feed</h2>
        <% 
              Bahen.Web.NewsfeedList list = new Bahen.Web.NewsfeedList();
              var feedList = list.GetNewsfeedList(UserEntity.ID);
              Response.Write(list.FormatNewsfeedList(feedList.Where(x => x.SenderID == UserEntity.ID)));%>
        <div>
            <%if (this.EventCount > 0)
              { %>
            <h2><%=EventCount %> Event<%=EventCount>1?"s":"" %></h2>
            <%=new Bahen.Web.EventList().GetUserEventList(UserEntity.ID) %>
            <%} %>
        </div>
    </div>
    <br />
    <%} }
      else{%>
    <h2><%=NotVisibleReasonString %></h2>
    <%} %>
</asp:Content>
