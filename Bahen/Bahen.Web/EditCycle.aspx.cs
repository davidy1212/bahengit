﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer.Blob;
using System.IO;
using Bahen.Web.Images;

namespace Bahen.Web
{
    public partial class EditCycle : System.Web.UI.Page
    {
        public CycleEntity CycleEntity { get; set; }
        public bool CanEdit { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if user has logged in
            if (Context.TryAuthenticate())
            {
                // This page has to have a parameter of event id
                if (String.IsNullOrEmpty((string)RouteData.Values["cid"]))
                {
                    Response.Write("Invalid cycle id");
                    return;
                }

                string cycleIdString = (string)RouteData.Values["cid"];

                // Check if the id is numeric
                long cycleID;
                if (!Int64.TryParse(cycleIdString, out cycleID))
                {
                    Response.Write("Invalid cycle id");
                    return;
                }

                // Check if the event exists
                CycleOperator cycleOperator = new CycleOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                CycleEntity cycleEntity;
                if (!cycleOperator.TryGet(cycleID, out cycleEntity))
                {
                    Response.Write("Invalid cycle id");
                    return;
                }
                this.CycleEntity = cycleEntity;

                // Check if the user is the administrator of the cycle
                this.CanEdit = false;
                var cycles = cycleOperator.GetCyclesByMember(Session.GetAuthenticatedUser().ID, DataModels.Constants.CycleMembershipType.Administrator);
                if (cycles.ToList().Select(x => x.ID).Contains(CycleEntity.ID))
                {
                    this.CanEdit = true;
                }

                if (!CanEdit)
                {
                    Response.Write("You don't have permission to edit the cycle");
                    return;
                }

                // Check if needs to delete the event or edit
                if (Request.QueryString["delete"] == "1")
                {
                    cycleOperator.Remove(this.CycleEntity);
                    if (Request.QueryString["redirect"] != null)
                    {
                        Response.Redirect(Request.QueryString["redirect"]);
                    }
                    else
                    {
                        Response.Redirect("/users/" + (Session["User"] as IEntity).ID);
                    }
                    return;
                }

                // Handle request from itself
                if (Request.Params["CycleName"] != null)
                {
                    // Copy the field
                    this.CycleEntity.Name = Request.Params["CycleName"];
                    this.CycleEntity.DescriptionAbstract = Request.Params["DescriptionAbstract"];

                    // Upload profile picture
                    if (ProfilePictureUploadControl.HasFile)
                    {
                        string extension = Path.GetExtension(ProfilePictureUploadControl.PostedFile.FileName).ToLowerInvariant();
                        if (extension == ".jpg" || extension == ".png" || extension == ".bmp" || extension == ".gif")
                        {
                            try
                            {
                                // Convert to png format
                                System.Drawing.Image original = System.Drawing.Image.FromStream(ProfilePictureUploadControl.PostedFile.InputStream);

                                BlobProfilePictureOperator profilePictureOperator = new BlobProfilePictureOperator(
                                    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                                Guid pictureID = Guid.NewGuid();

                                // Upload large size picture
                                using (Stream uploadStreamMedium = new MemoryStream())
                                {
                                    System.Drawing.Image mediumSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Medium);
                                    mediumSizeImage.Save(uploadStreamMedium, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamMedium.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "c",
                                        cycleEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Medium,
                                        uploadStreamMedium);
                                }

                                // Remember the picture ID
                                //this.CycleEntity.Picture = pictureID;

                                // Upload thumbnail size picture
                                using (Stream uploadStreamThumbnail = new MemoryStream())
                                {
                                    System.Drawing.Image thumbnailSizeImage = new PictureResizer().ResizeImage(original, DataModels.Constants.PictureSize.Thumb);
                                    thumbnailSizeImage.Save(uploadStreamThumbnail, System.Drawing.Imaging.ImageFormat.Png);
                                    uploadStreamThumbnail.Seek(0, SeekOrigin.Begin);

                                    profilePictureOperator.Upload(
                                        "c",
                                        cycleEntity.ID,
                                        pictureID,
                                        DataModels.Constants.PictureSize.Thumb,
                                        uploadStreamThumbnail);
                                }
                            }
                            catch
                            {
                                Response.Write("Invalid image format");
                                return;
                            }
                        }
                        else
                        {
                            Response.Write("Invalid image format");
                            return;
                        }
                    }

                    // Update the cycle
                    cycleOperator.Update(this.CycleEntity);

                    Response.Redirect("/cycles/" + this.CycleEntity.ID);
                }
            }
        }
    }
}