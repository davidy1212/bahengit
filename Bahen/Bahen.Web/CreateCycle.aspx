﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateCycle.aspx.cs" Inherits="Bahen.Web.CreateCycle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <title></title>
</head>
<body>
    <%=new Bahen.Web.Header().GetHeader() %>
    <h1>Create a Cycle</h1>
    <form id="form1" runat="server">
        <div>
            Cycle Name:
            <input type="text" name="CycleName" /><br />
            Profile Picture:
            <asp:FileUpload ID="ProfilePictureUploadControl" runat="server" /><br />
            Description:
            <asp:TextBox ID="DescriptionAbstract" TextMode="MultiLine" runat="server"></asp:TextBox><br />
            <br />
            <input type="submit" name="submit" value="Submit" /><br />

        </div>
    </form>
    <%=new Bahen.Web.Footer().GetFooter() %>
</body>
</html>
