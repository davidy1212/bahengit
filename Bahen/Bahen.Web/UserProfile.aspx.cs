﻿using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataMiddleLayer.Newsfeed;
using Bahen.DataOperationLayer.Table;
using System.Threading.Tasks;
using Bahen.DataModels.Table;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Constants.Privacy;

namespace Bahen.Web
{
    public partial class UserProfile : System.Web.UI.Page
    {
        public UserEntity UserEntity { get; set; }
        public bool CanView { get; set; }
        public UserProfileNotVisibleReason NotVisibleReason;
        public string NotVisibleReasonString { get; set; }
        public bool CanSubscribe { get; set; }
        public bool CanEdit { get; set; }
        public int EventCount { get; set; }
        public int CycleCount { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Context.TryAuthenticate(false);

            // Check if has cid parameter
            if (String.IsNullOrEmpty((string)RouteData.Values["uid"]))
            {
                Response.Write("Invalid user id");
                return;
            }
            string userIdString = (string)RouteData.Values["uid"];

            // Check if the parameter is numeric
            long userId;
            if (!Int64.TryParse(userIdString, out userId))
            {
                Response.Write("Invalid user id");
                return;
            }

            // Check if the cycle id exists, get the cycle entity
            UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            UserEntity userEntity;
            if (!userOperator.TryGet(userId, out userEntity))
            {
                Response.Write("Invalid user id");
                return;
            }
            this.UserEntity = userEntity;

            
            if (Session.GetAuthenticatedUser().IsProfileVisible(UserEntity, out NotVisibleReason))
            {
                this.CanView = true;
            }
            else
            {
                this.CanView = false;
                switch (NotVisibleReason)
                {
                    case UserProfileNotVisibleReason.Blocked:
                        NotVisibleReasonString = "Cannot view profile because you can blocked";
                        break;
                    case UserProfileNotVisibleReason.OnlyOwnerCanView:
                        NotVisibleReasonString = "Cannot view profile because only owner can view";
                        break;
                    case UserProfileNotVisibleReason.MustLogIn:
                        NotVisibleReasonString = "Cannot view profile because you must login";
                        break;
                    case UserProfileNotVisibleReason.OwnerMustSubscribeYou:
                        NotVisibleReasonString = "Cannot view profile because owner must subscribe you";
                        break;
                    case UserProfileNotVisibleReason.YouMustSubscribeOwner:
                        NotVisibleReasonString = "Cannot view profile because you must subscribe user";
                        break;
                    case UserProfileNotVisibleReason.MustMutualSubscribe:
                        NotVisibleReasonString = "Cannot view profile because you and owner must subscribe each other";
                        break;
                    case UserProfileNotVisibleReason.NotInAllowList:
                        NotVisibleReasonString = "Cannot view profile because you are not in allowed list";
                        break;
                    case UserProfileNotVisibleReason.Other:
                        NotVisibleReasonString = "Something went wrong while loading profile";
                        break;
                    case UserProfileNotVisibleReason.Visible:
                        break;
                    default:
                        break;
                }
            }

            // Calculate if has subscribed
            this.CanSubscribe = true;
            if (Session.IsAuthenticated())
            {
                if (this.UserEntity.ID == Session.GetAuthenticatedUserID())
                {
                    this.CanSubscribe = false;
                }

                var users = userOperator.GetHostsBySubscriber(Session.GetAuthenticatedUserID());
                if (users.ToList().Select(x => x.ID).Contains(this.UserEntity.ID))
                {
                    this.CanSubscribe = false;
                }
            }

            // Check event count
            this.EventCount = this.UserEntity.SqlEntity.Attendances.Where(x=>x.AttendanceStatus!=(int)(AttendanceStatus.Declined)).Count();

            // Check cycle count
            this.CycleCount = this.UserEntity.SqlEntity.CycleUsers.Where(x => x.MembershipType != (int)(CycleMembershipType.Invited)).Count();

            // Check if request to subscribe the user
            if (Request.QueryString["action"] == "subscribe")
            {
                if (!CanSubscribe)
                {
                    Response.Redirect(Request.Path);
                    return;
                }
                if (Session.GetAuthenticatedUser() != null)
                {
                    userOperator.AddSubscription(Session.GetAuthenticatedUser().ID, UserEntity.ID);

                    // Add news feed, don't have to wait
                    (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                    {
                        Content = "subscribed to",
                        SenderID = Session.GetAuthenticatedUserID(),
                        ReferenceType = typeof(UserEntity).ToString(),
                        ReferenceLongID = this.UserEntity.ID
                    });

                    // Add the host's news feed to subscriber's list
                    Task.Run(delegate
                    {
                        long userID = Session.GetAuthenticatedUserID();
                        TableNewsfeedOperator newsfeedOperator = new TableNewsfeedOperator(
                            ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

                        IEnumerable<TableNewsfeedEntity> feedList = newsfeedOperator.GetAllItems(this.UserEntity.ID)
                            .Where(x => x.SenderID == this.UserEntity.ID).ToList().Select(entity => new TableNewsfeedEntity()
                            {
                                SenderID = entity.SenderID,
                                RecipientID = userID,
                                ReferenceType = entity.ReferenceType,
                                Content = entity.Content,
                                TimeSent = entity.TimeSent,
                                ReferenceLongID = entity.ReferenceLongID,
                                ReferenceRowKey = entity.ReferenceRowKey
                            });
                        (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteBatchAsync(feedList, false);
                    }
                    );

                    this.CanSubscribe = false;
                }
                else
                {
                    Response.Redirect("/login?redirect=" + Server.UrlEncode(Request.Path + "?subscribe=1"));
                    return;
                }
            }

            // Check if the logged in user can edit this page
            this.CanEdit = false;
            if (Session.IsAuthenticated())
            {
                if (Session.GetAuthenticatedUserID() == UserEntity.ID)
                {
                    this.CanEdit = true;
                    Session["User"] = UserEntity;
                }
                else
                {
                    //if (Session.GetAuthenticatedUser().UserGroup == DataModels.Constants.Privacy.UserGroupLevel.Administrator)
                    //{
                    //    this.CanEdit = true;
                    //}
                }
            }
        }
    }
}