﻿using Bahen.DataOperationLayer.Index;
using Bahen.DataOperationLayer.Sql;
using Bahen.Search.Indexing;
using Bahen.Web.Formatting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bahen.Web.Tools;
using Bahen.Search.Correctors;
using Bahen.Common;
using System.Configuration;
using Newtonsoft.Json;

namespace Bahen.Web.Search
{
    /// <summary>
    /// Summary description for UserSearchHandler
    /// </summary>
    public class UserSearchHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Params["q"] != null)
            {
                UserQueryBuilder queryBuilder = new UserQueryBuilder();
                var index = context.Application.GetObject<InvertedWordIndex>("InvertedWordIndex");
                var results = queryBuilder.Build(context.Request.Params["q"], new SuffixCorrector(index.Keys)).Evaluate(index);
                SqlUserOperator userOperator = new SqlUserOperator(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

                var data = results.Select(x => userOperator.Get(x)).Take(5);

                if (context.Request.Params["type"] == "jquery")
                {
                    context.Response.ContentType = "application/json";
                    //context.Response.Write(JsonConvert.SerializeObject(data.Select(x => new
                    //{
                    //    label = x.PreferredName,
                    //    value = x.LoginEmail,
                    //    picture = HyperLinks.GetUserProfilePictureThumbnailUrl(x.ID, x.ProfilePicture)
                    //})));
                }
                else if (context.Request.Params["type"] == "json")
                {
                    context.Response.ContentType = "application/json";
                    //context.Response.Write(JsonConvert.SerializeObject(data.Select(x => new
                    //{
                    //    Name = x.PreferredName,
                    //    Email = x.LoginEmail,
                    //    Picture = HyperLinks.GetUserProfilePictureThumbnailUrl(x.ID, x.ProfilePicture)
                    //})));

                }
                else
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write(new UserSearchResultFormatter().FormatList(data));
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}