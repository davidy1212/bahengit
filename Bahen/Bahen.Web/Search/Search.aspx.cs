﻿using Bahen.DataOperationLayer.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.Search.Indexing;
using Bahen.Search.Correctors;
using Bahen.DataOperationLayer.Sql;
using System.Configuration;
using Bahen.Web.Formatting;

namespace Bahen.Web.Search
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["q"] != null)
            {
                UserQueryBuilder queryBuilder = new UserQueryBuilder();
                var index = Application.GetObject<InvertedWordIndex>("InvertedWordIndex");
                var results = queryBuilder.Build(Request.Params["q"], new SuffixCorrector(index.Keys)).Evaluate(index);
                SqlUserOperator userOperator = new SqlUserOperator(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                Response.Write(new UserSearchResultFormatter().FormatList(results.Select(x => userOperator.Get(x))));
            }
        }
    }
}