﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditCycle.aspx.cs" Inherits="Bahen.Web.EditCycle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link type="text/css" href="/Styles/StyleHandler.ashx" rel="stylesheet" />
    <title></title>
</head>
<body>
    <%if (CycleEntity!=null && CanEdit)
      { %>
    <%=new Bahen.Web.Header().GetHeader() %>
    <h1>Edit: <a href="/cycles/<%=CycleEntity.ID%>"><%=CycleEntity.Name %></a></h1>
    <a href="/cycles/<%=CycleEntity.ID %>/edit?delete=1" style="color: red;">Delete this cycle</a><br />
    <form id="form1" runat="server">
        <div>
            Cycle Name:
            <input type="text" name="CycleName" value="<%=CycleEntity.Name %>" /><br />
            Profile Picture:
            <asp:FileUpload ID="ProfilePictureUploadControl" runat="server" /><br />
            Description:
            <%
                this.DescriptionAbstract.Text = CycleEntity.DescriptionAbstract;
            %>
            <asp:TextBox ID="DescriptionAbstract" TextMode="MultiLine" runat="server"></asp:TextBox><br />
            <br />
            <input type="submit" name="submit" value="Submit" /><br />

        </div>
    </form>
    <%=new Bahen.Web.Footer().GetFooter() %>
    <%} %>
</body>
</html>
