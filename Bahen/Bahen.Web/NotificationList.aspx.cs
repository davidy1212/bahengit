﻿using Bahen.DataOperationLayer.Table;
using Bahen.Web.Formatting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataModels.Table;
using Bahen.Common;
using Newtonsoft.Json;

namespace Bahen.Web
{
    public partial class NotificationList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                if (Request.Params["json"] != null)
                {
                    Response.Write(
                        JsonConvert.SerializeObject(this.GetNotificationList(Session.GetAuthenticatedUserID()).Select(x => new
                    {
                        content = x.Content,
                        read = x.Read,
                        time = x.TimeSent,
                        id = x.ID,
                        picture = x.PictureUrl
                    })));
                }
            }
        }

        public IEnumerable<TableNotificationEntity> GetNotificationList(long userID)
        {
            if (Context.TryAuthenticate())
            {
                TableNotificationOperator notificationOperator = new TableNotificationOperator(
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                return notificationOperator.GetAllItems(userID);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<TableNotificationEntity> GetNotificationList(long userID, int start, int count)
        {
            if (Context.TryAuthenticate())
            {
                TableNotificationOperator notificationOperator = new TableNotificationOperator(
                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                var notifications = notificationOperator.GetAllItems(userID);
                return notifications.Skip(start).Take(count);
            }
            else
            {
                return null;
            }
        }

        public string FormatNotificationList(IEnumerable<TableNotificationEntity> data)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in data.ToList().OrderByDescending(x => x.TimeSent))
            {
                sb.Append(String.Format("<div class=\"notification\" style=\"{0}\" onclick='location.href=\"/notifications/{1}/{2}\"'>", item.Read ? "border-left:solid 5px white;" : "border-left:solid 5px blue;", Session.GetAuthenticatedUserID(), item.ID));
                sb.Append(String.Format("<div style=\" background-image:url('{0}'); background-position:center; background-repeat:no-repeat; display:inline-block; width:50px; height:60px; vertical-align:top;\"></div>", item.PictureUrl));
                sb.Append("<div style=\"display:inline-block; margin-top:5px; margin-left:10px; min-height:35px; vertical-align:top; clear:both; width:330px;\">");
                sb.Append(item.Content);
                sb.Append("<br/>");
                sb.Append(String.Format("<div style=\"display:inline-block; vertical-align:bottom; font-size:8pt; color:#ccc;\">{0}</div>", new TimeFormatter().Format(item.TimeSent)));
                sb.Append("</div>");
                sb.Append("</div>");
            }
            return sb.ToString();
        }
    }
}