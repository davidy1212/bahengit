﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EventDetail.aspx.cs" Inherits="Bahen.Web.EventDetail" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <%
        if (EventEntity != null)
        {
    %>
    <h1><%=EventEntity.Name %></h1>
    <img src="/images/ProfilePictureHandler.ashx?size=large&prefix=e&id=<%=EventEntity.ID %>&guid=<%=EventEntity.PictureGuid %>" /><br />
    <%=GetAttendanceMessage(AttendanceStatus) %>

    <%=GetAttendanceButtonsHtml(AttendanceStatus) %>

    <%if (LoggedIn)
      { %>
    <a class="button-large" href="javascript:inviteBox.start();">Invite</a>
    <%} %>
    <%if (CanEdit)
      { %>
    <a class="button-large" href="<%=Request.Url.AbsolutePath %>/edit">Edit</a>
    <%} %>

    <div id="InviteBox"></div>
    <br />
    Start: <%=EventEntity.StartTime.ToString() %><br />
    End: <%=EventEntity.EndTime.ToString() %><br />
    Address: <%=EventEntity.Address.ToString() %><br />
    <%=EventEntity.DescriptionAbstract%><br />
    <h2>Description</h2>
    <%=EventDescription %>

    <%
      // People going to the event
      var attendanceGoing = EventEntity.SqlEntity.Attendances
        .Where(x =>
  x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Going ||
  x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Publisher ||
          x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Copublisher);
      if (attendanceGoing.Count() > 0)
      {
          Response.Write("<h2>" + attendanceGoing.Count());
    %>
    People Going</h2>
    <%Response.Write(new Bahen.Web.Formatting.UserFormatter().FormatList(attendanceGoing.Select(x => x.User)));%>
    <% }%>
    <%
      // People invited to the event
      var attendanceInvited =
            EventEntity.SqlEntity.Attendances
            .Where(x => x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Invited);
      if (attendanceInvited.Count() > 0)
      {
          Response.Write("<h2>" + attendanceInvited.Count()); %> People Invited</h2>
    <%Response.Write(new Bahen.Web.Formatting.UserFormatter().FormatList(attendanceInvited.Select(x => x.User)));%>
    <%} %>
    <%
      // People maybe going to the event
      var attendanceMaybe = EventEntity.SqlEntity.Attendances
        .Where(x =>
  x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Maybe ||
  x.AttendanceStatus == (int)Bahen.DataModels.Constants.AttendanceStatus.Interested);
      if (attendanceMaybe.Count() > 0)
      {
          Response.Write("<h2>" + attendanceMaybe.Count());
    %> People Probably Going</h2>
    <%Response.Write(new Bahen.Web.Formatting.UserFormatter().FormatList(attendanceMaybe.Select(x => x.User)));%>
    <% }
        }%>
</asp:Content>
