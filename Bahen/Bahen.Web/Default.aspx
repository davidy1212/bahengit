﻿<%@ Page Title="Home Page" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Bahen.Web._Default" %>

<%@ Import Namespace="Bahen.Web.Tools" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="Server">
    <%if (Session.IsAuthenticated())
      {%>
    <div style="display: inline-block; vertical-align: top;">
        <h2>Menu</h2>
        <a href="/events/create">Create an Event</a><br />
        <a href="/cycles/create">Create a Cycle</a><br />
        <a href="/users/<%=(Session["User"] as Bahen.DataModels.IEntity).ID %>">My Profile</a><br />
        <a href="/logout?redirect=/">Logout</a>
    </div>
    <div style="display: inline-block; vertical-align: top; margin-left: 10px; width: 470px">
        <h2>Your news feed</h2>
        <% var newsfeedList = new Bahen.Web.NewsfeedList();
           Response.Write(
               newsfeedList.FormatNewsfeedList(
               newsfeedList.GetNewsfeedList(
               (Session.GetAuthenticatedUserID()))));%>
    </div>
    <div style="display: inline-block; vertical-align: top; margin-left: 10px; width: 570px">
        <h2>Ongoing events</h2>
        <%=new Bahen.Web.EventList().GetUserEventList((Session["User"] as Bahen.DataModels.IEntity).ID)%>
    </div>
    <%} %>
</asp:Content>
