﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Bahen.Web.Tools;
using Bahen.Web.Common.Constants;

namespace Bahen.Web.Ajax
{
    /// <summary>
    /// Summary description for LoginHandler
    /// </summary>
    public class LoginHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            switch (context.TryLogin())
            {
                case (LoginResult.Successful):
                    {
                        context.Response.Write("Successful");
                        break;
                    }
                case (LoginResult.InvalidCredential):
                    {
                        context.Response.Write("Unsuccessful");
                        break;
                    }
                case (LoginResult.AlreadyLoggedIn):
                    {
                        context.Response.Write("Already logged in");
                        break;
                    }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}