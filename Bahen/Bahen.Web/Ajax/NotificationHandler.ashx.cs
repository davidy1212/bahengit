﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bahen.Web.Tools;
using Bahen.Common;
using Bahen.Web.Formatting;
using System.Web.SessionState;
using Newtonsoft.Json;

namespace Bahen.Web.Ajax
{
    /// <summary>
    /// Summary description for NotificationHandler
    /// </summary>
    public class NotificationHandler : IHttpHandler, IReadOnlySessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            if (context.TryAuthenticate())
            {
                context.Response.Write(
                    JsonConvert.SerializeObject(new NotificationList().GetNotificationList(context.Session.GetAuthenticatedUserID()).Select(x => new
                    {
                        content = x.Content,
                        read = x.Read,
                        time = new TimeFormatter().Format(x.TimeSent),
                        id = x.ID,
                        picture = x.PictureUrl,
                        url = String.Format("/notifications/{0}/{1}", x.RecipientID, x.ID)
                    })));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}