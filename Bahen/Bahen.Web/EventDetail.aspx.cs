﻿using Bahen.DataModels;
using Bahen.DataModels.CompositePropoties;
using Bahen.DataModels.Constants;
using Bahen.DataModels.Constants.Privacy;
using Bahen.DataOperationLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.DataOperationLayer.Blob;
using System.IO;
using Bahen.DataMiddleLayer.Newsfeed;
using Bahen.DataOperationLayer.Sql;
using Bahen.DataModels.Sql;
using System.Text;

namespace Bahen.Web
{
    public partial class EventDetail : System.Web.UI.Page
    {
        protected EventEntity EventEntity { get; set; }
        protected bool CanEdit { get; set; }
        protected string EventDescription { get; set; }
        protected AttendanceStatus AttendanceStatus { get; set; }
        protected bool LoggedIn { get; set; }
        protected IQueryable<SqlUserEntity> AttendanceGoing { get; set; }
        protected IQueryable<SqlUserEntity> AttendanceMaybe { get; set; }
        protected IQueryable<SqlUserEntity> AttendanceInvited { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoggedIn = Context.TryAuthenticate(false);

            // This page has to have a parameter of event id
            if (String.IsNullOrEmpty((string)RouteData.Values["eid"]))
            {
                Response.Write("Invalid event id");
                return;
            }

            string eventIdString = (string)RouteData.Values["eid"];

            // Check if the id is numeric
            long eventId;
            if (!Int64.TryParse(eventIdString, out eventId))
            {
                Response.Write("Invalid event id");
                return;
            }

            // Check if the event exists
            EventOperator eventOperator = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            EventEntity eventEntity;
            if (!eventOperator.TryGet(eventId, out eventEntity))
            {
                Response.Write("Invalid event id");
                return;
            }

            this.EventEntity = eventEntity;

            // Get event description
            try
            {
                BlobDescriptionOperator descriptionOperator = new BlobDescriptionOperator(
                    ConfigurationManager.ConnectionStrings["Blob"].ConnectionString);
                this.EventDescription = descriptionOperator.GetEventDescription(this.EventEntity);
            }
            catch
            {
                this.EventDescription = String.Empty;
            }

            // Check if the user has the permission
            long currentUserID = 0;
            if (LoggedIn)
            {
                currentUserID = (Session["User"] as IEntity).ID;
            }

            if (currentUserID == EventEntity.PublisherID)
            {
                this.CanEdit = true;
            }

            // Calculate if the attendance status of the current logged in user
            this.AttendanceStatus = eventOperator.GetAttendance(this.EventEntity.ID, currentUserID);

            // Check if request to attend
            switch (Request.QueryString["action"])
            {
                case ("going"):
                    {
                        if (Context.TryAuthenticate())
                        {
                            if (this.AttendanceStatus < AttendanceStatus.Going)
                            {
                                eventOperator.SetAttendance(EventEntity.ID, Session.GetAuthenticatedUser().ID, AttendanceStatus.Going);
                                this.AttendanceStatus = AttendanceStatus.Going;

                                // Add news feed, don't have to wait
                                (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                                {
                                    Content = "is going to",
                                    SenderID = Session.GetAuthenticatedUserID(),
                                    ReferenceType = typeof(EventEntity).ToString(),
                                    ReferenceLongID = eventEntity.ID
                                });
                            }
                        }
                        break;
                    }
                case ("maybe"):
                    {
                        if (Context.TryAuthenticate())
                        {
                            if (this.AttendanceStatus < AttendanceStatus.Going &&
                                this.AttendanceStatus != AttendanceStatus.Maybe)
                            {
                                eventOperator.SetAttendance(this.EventEntity.ID, currentUserID, AttendanceStatus.Maybe);
                                this.AttendanceStatus = AttendanceStatus.Maybe;
                            }
                        }
                        break;
                    }
                case ("interested"):
                    {
                        if (Context.TryAuthenticate())
                        {
                            if (this.AttendanceStatus < AttendanceStatus.Going &&
                                this.AttendanceStatus != AttendanceStatus.Interested)
                            {
                                eventOperator.SetAttendance(this.EventEntity.ID, currentUserID, AttendanceStatus.Interested);
                                this.AttendanceStatus = AttendanceStatus.Interested;

                                // Add news feed, don't have to wait
                                (Application["NewsfeedStorageManager"] as NewsfeedStorageManager).ExecuteAsync(new DataModels.Table.TableNewsfeedEntity()
                                {
                                    Content = "is interested in",
                                    SenderID = Session.GetAuthenticatedUserID(),
                                    ReferenceType = typeof(EventEntity).ToString(),
                                    ReferenceLongID = eventEntity.ID
                                });
                            }
                        }
                        break;
                    }
                case ("leave"):
                    {
                        if (Context.TryAuthenticate())
                        {
                            if (this.AttendanceStatus != AttendanceStatus.Declined && 
                                this.AttendanceStatus != AttendanceStatus.None &&
                                this.AttendanceStatus < AttendanceStatus.Organizer)
                            {
                                eventOperator.SetAttendance(this.EventEntity.ID, currentUserID, AttendanceStatus.Declined);
                                this.AttendanceStatus = AttendanceStatus.Declined;
                            }
                        }
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// Generates the attendance message regarding to the attendance status
        /// </summary>
        /// <param name="status">Current attendance status of the user</param>
        /// <returns>Attendance message</returns>
        protected string GetAttendanceMessage(AttendanceStatus status)
        {
            switch (AttendanceStatus)
            {
                case (Bahen.DataModels.Constants.AttendanceStatus.Going):
                    {
                        return ("You are going.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Interested):
                    {
                        return ("You are interested.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Invited):
                    {
                        return ("You are invited.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Maybe):
                    {
                        return ("You are probably going.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Declined):
                    {
                        return ("You are not going.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Organizer):
                    {
                        return ("You are organizer of the event.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Publisher):
                    {
                        return ("You are publisher of the event.");
                    }
                case (Bahen.DataModels.Constants.AttendanceStatus.Copublisher):
                    {
                        return ("You are copublisher of the event.");
                    }
                default:
                    {
                        return String.Empty;
                    }
            }
        }

        /// <summary>
        /// Generates the attendance buttons in html
        /// </summary>
        /// <param name="status">Current attendance status of the user</param>
        /// <returns>Html string of the attendance buttons</returns>
        protected string GetAttendanceButtonsHtml(AttendanceStatus status)
        {
            StringBuilder builder = new StringBuilder();

            if (status < AttendanceStatus.Going)
            {
                builder.Append(this.GetButtonHtml(Request.Url.AddQueryString("action", "going"), "Going"));
            }

            if (status != AttendanceStatus.Maybe &&
                status < AttendanceStatus.Going)
            {
                builder.Append(this.GetButtonHtml(Request.Url.AddQueryString("action", "maybe"), "Maybe"));
            }

            if (status != AttendanceStatus.Interested &&
                status < AttendanceStatus.Going)
            {
                builder.Append(this.GetButtonHtml(Request.Url.AddQueryString("action", "interested"), "Interested"));
            }

            if (status != AttendanceStatus.None &&
                status != AttendanceStatus.Declined &&
                status < AttendanceStatus.Going)
            {
                builder.Append(this.GetButtonHtml(Request.Url.AddQueryString("action", "leave"), "Not going"));
            }

            return builder.ToString();
        }

        /// <summary>
        /// Get html of a button
        /// </summary>
        /// <param name="href">Hyperlink of the button</param>
        /// <param name="text">Text on the button</param>
        /// <returns>Html of button</returns>
        private string GetButtonHtml(string href, string text)
        {
            return "<a class='button-large' href='" + href + "'>" + text + "</a>";
        }
    }
}