﻿using Bahen.DataModels;
using Bahen.DataModels.Constants;
using Bahen.DataOperationLayer;
using Bahen.DataOperationLayer.Table;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;

namespace Bahen.Web
{
    public partial class EmailRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Context.TryAuthenticate(false);
            if (String.IsNullOrEmpty((string)RouteData.Values["guid"]) || String.IsNullOrEmpty((string)RouteData.Values["uid"]))
            {
                return;
            }
            Guid guid;
            long userID;
            if (!Guid.TryParse((string)RouteData.Values["guid"], out guid) || !Int64.TryParse((string)RouteData.Values["uid"], out userID))
            {
                return;
            }

            if (Session.GetAuthenticatedUserID() != userID)
            {
                Context.Logout();
            }

            TableEmailOperator notificationOperator = new TableEmailOperator(
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            var email = notificationOperator.Get(guid, userID);

            UserEntity userEntity;
            using (UserOperator userOperator = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString))
            {
                if (!userOperator.TryGet(email.RecipientID, out userEntity))
                {
                    return;
                }

                if (email.EmailAction == (int)EmailAction.Register)
                {
                    if (!userEntity.LoginEmailConfirmed)
                    {
                        userEntity.LoginEmailConfirmed = true;
                    }
                    userOperator.Update(userEntity);
                }

                if (email.EmailAction == (int)EmailAction.Invitation)
                {
                    if (userEntity.UnderInvitation)
                    {
                        Response.Redirect(String.Format("/register?uid={0}&emailID={1}&redirect={2}", email.RecipientID, email.ID, email.Url));
                    }
                    else
                    {
                        Context.TryAuthenticate();
                    }
                }
            }
            Response.Redirect(email.Url);
        }
    }
}