﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bahen.Web.Tools;
using Bahen.Web.Common.Parsing;
using Bahen.DataModels.Sql;
using Bahen.DataOperationLayer.Sql;
using System.Configuration;
using Bahen.DataModels.Constants;
using Bahen.DataMiddleLayer.Notification;
using Bahen.DataOperationLayer;
using Bahen.DataModels;
using Bahen.Common;
using Bahen.DataMiddleLayer.Email;
using Bahen.DataModels.Table;

namespace Bahen.Web
{
    public partial class Invite : System.Web.UI.Page
    {
        public bool Valid { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Context.TryAuthenticate())
            {
                Valid = true;
                if (RouteData.Values["refer"] == null || RouteData.Values["id"] == null)
                {
                    Valid = false;
                    Response.Write("invalid request");
                    return;
                }

                long ID;
                if (!Int64.TryParse((string)RouteData.Values["id"], out ID))
                {
                    Valid = false;
                    Response.Write("invalid request");
                    return;
                }
                if (!String.IsNullOrEmpty(Request.Params["Emails"]))
                {
                    // Parse all email addresses
                    IEnumerable<string> emails = new EmailParser().Parse(Request.Params["Emails"]);
                    if (emails.Count() == 0)
                    {
                        return;
                    }

                    List<SqlUserEntity> foundUserList = new List<SqlUserEntity>();
                    List<SqlUserEntity> notFoundEmailUserList = new List<SqlUserEntity>();
                    List<SqlUserEntity> invited = new List<SqlUserEntity>();

                    using (UserOperator userOperator = new UserOperator(
                        ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString))
                    {
                        // Find email addresses from UserOperator
                        foreach (string email in emails)
                        {
                            SqlUserEntity user;
                            if (userOperator.SqlOperator.TryGet(email, out user))
                            {
                                foundUserList.Add(user);
                            }
                            else
                            {
                                // Create a new user if the email does not exist
                                UserEntity insertUser = new UserEntity()
                                {
                                    LoginEmail = email,
                                    LoginPassword = HashUtilities.ComputeHashSecure(""),
                                    PreferredName = email,
                                    UnderInvitation = true,
                                    LoginEmailConfirmed = false
                                };
                                userOperator.Insert(insertUser);

                                notFoundEmailUserList.Add(insertUser.SqlEntity);
                            }
                        }
                    }

                    // Parse which table to insert to
                    switch ((string)RouteData.Values["refer"])
                    {
                        // Ask people to subscribe user
                        case ("users"):
                            {
                                break;
                            }
                        // Ask people to attend event
                        case ("events"):
                            {
                                SqlEventEntity eventEntity;
                                using (EventOperator eventOperator = new EventOperator(
                                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString))
                                {
                                    // Check if we really have this event
                                    if (!eventOperator.SqlOperator.TryGet(ID, out eventEntity))
                                    {
                                        Response.Write("Invalid event ID");
                                        return;
                                    }

                                    // Add invitatiion attendance status, construct email entity
                                    foreach (SqlUserEntity user in foundUserList.Concat(notFoundEmailUserList))
                                    {

                                        if (eventOperator.GetAttendance(ID, user.ID) == AttendanceStatus.None)
                                        {
                                            eventOperator.AddAttendance(ID, user.ID, AttendanceStatus.Invited);
                                            invited.Add(user);
                                        }
                                    }
                                }

                                string message = String.Format("{0} invited you to event {1}", Session.GetAuthenticatedUser().PreferredName, eventEntity.Name);
                                // Add notification
                                (Application["NotificationStorageManager"] as NotificationStorageManager).ExecuteBatchAsync(
                                    new DataModels.Table.TableNotificationEntity()
                                    {
                                        Content = message,
                                        PictureUrl = String.Format("/Images/ProfilePictureHandler.ashx?size=thumbnail&prefix={0}&id={1}&guid={2}", "e", ID, eventEntity.Picture),
                                        SenderID = Session.GetAuthenticatedUserID(),
                                        Url = String.Format("/events/{0}", ID)
                                    }, invited);

                                // Send emails
                                TableEmailEntity emailTemplate = new TableEmailEntity()
                                {
                                    Url = String.Format("{0}/events/{1}", ConfigurationManager.AppSettings["SiteUrl"], ID),
                                    Content = message,
                                    SenderID = Session.GetAuthenticatedUserID(),
                                    EmailAction = (int)EmailAction.Invitation
                                };

                                // Send email to all users if "SendEmails" is selected
                                if (Request.Params["SendEmails"] != null)
                                {
                                    (Application["EmailManager"] as EmailManager).ExecuteBatchAsync(emailTemplate, invited);
                                }
                                else
                                {
                                    (Application["EmailManager"] as EmailManager).ExecuteBatchAsync(emailTemplate, notFoundEmailUserList);
                                }
                                break;
                            }
                        // Ask people to join cycle
                        case ("cycles"):
                            {
                                SqlCycleEntity cycleEntity;
                                using (CycleOperator cycleOperator = new CycleOperator(
                                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                                    ConfigurationManager.ConnectionStrings["Table"].ConnectionString))
                                {
                                    // Check if we really have this cycle
                                    if (!cycleOperator.SqlOperator.TryGet(ID, out cycleEntity))
                                    {
                                        Response.Write("Invalid cycle ID");
                                        return;
                                    }

                                    foreach (SqlUserEntity user in foundUserList.Concat(notFoundEmailUserList))
                                    {
                                        if (cycleOperator.GetMembership(ID, user.ID) == CycleMembershipType.None)
                                        {
                                            cycleOperator.AddMemebership(ID, user.ID, CycleMembershipType.Invited);
                                            invited.Add(user);
                                        }
                                    }
                                }

                                string message = String.Format("{0} invited you to cycle {1}", Session.GetAuthenticatedUser().PreferredName, cycleEntity.Name);
                                // Add notification
                                (Application["NotificationStorageManager"] as NotificationStorageManager).ExecuteBatchAsync(
                                    new DataModels.Table.TableNotificationEntity()
                                    {
                                        Content = message,
                                        PictureUrl = String.Format("/Images/ProfilePictureHandler.ashx?size=thumbnail&prefix={0}&id={1}&guid={2}", "c", ID, cycleEntity.Picture),
                                        SenderID = Session.GetAuthenticatedUserID(),
                                        Url = String.Format("/cycles/{0}", ID)
                                    }, invited);

                                // Send emails
                                TableEmailEntity emailTemplate = new TableEmailEntity()
                                {
                                    Url = String.Format("{0}/cycles/{1}", ConfigurationManager.AppSettings["SiteUrl"], ID),
                                    Content = message,
                                    SenderID = Session.GetAuthenticatedUserID(),
                                    EmailAction = (int)EmailAction.Invitation
                                };

                                // Send email to all users if "SendEmails" is selected
                                if (Request.Params["SendEmails"] != null)
                                {
                                    (Application["EmailManager"] as EmailManager).ExecuteBatchAsync(emailTemplate, invited);
                                }
                                else
                                {
                                    (Application["EmailManager"] as EmailManager).ExecuteBatchAsync(emailTemplate, notFoundEmailUserList);
                                }
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }

                    // Output result
                    if (invited.Count > 0)
                    {
                        //ResultLabel.Text += String.Format(
                        //    "<span style=\"color:green;\">Successfully invited user: {0}</span><br/>",
                        //    String.Join(", ", invited.Select(x => x.LoginEmail)));
                    }
                }
            }
        }
    }
}