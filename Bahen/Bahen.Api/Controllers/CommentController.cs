﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Api.Models;
using Bahen.Data.Models;
using System.Threading;
using System.Web.Http;

namespace Bahen.Api
{
    public class CommentController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TargetString</param>
        /// <param name="typeCode"></param>
        /// <param name="sId">Rowkey of this comment</param>
        /// <returns></returns>
        [DELETE("comment/{id}"), Authorize, RequirePermission(OAuthScopes.Owner)]
        public PureTextMessageResponse DeleteComment(string id, int typeCode, string sId)
        {
            long userId = Thread.CurrentPrincipal.GetID();
            CommentUtilities.DeleteComment(id, (EntityTypeCode)typeCode, sId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
    }
}
