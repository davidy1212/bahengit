﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Api.Models;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Bahen.Api
{
    public class EventController : ApiController
    {
        /// <summary>
        /// Get details of an event
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <returns></returns>
        [GET("event/{id}")]
        public EventDetail GetDetail(long id,
            [ModelBinder(typeof(FieldsFilterModelBinderProvider), Name = "Optional")] FieldsFilter fields)
        {
            EventEntity entity;
            var op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            if (!op.TryGet(id, out entity))
            {
                throw new IdNotFoundException(id);
            }
            if (!PrivacyJudge.CheckPrivacy(Thread.CurrentPrincipal, EntityTypeCode.Event, id, entity.Visibility))
            {
                throw new InsufficientPermissionException();
            }
            return new EventProvider().GetDetail(entity, fields);
        }

        /// <summary>
        /// Create an event
        /// </summary>
        /// <param name="e">Event detail</param>
        /// <returns></returns>
        [POST("event"), Authorize, RequirePermission(OAuthScopes.CreateEvent)]
        public PureTextMessageResponse CreateEvent([FromBody] EventDetail e)
        {
            EventEntity entity = new EventEntity()
            {
                PublisherID = Thread.CurrentPrincipal.GetID(),
                Name = e.Name,
                Alias = e.Alias,
                Categories = e.Categories,
                StartTime = e.StartTime,
                EndTime = e.EndTime,
                TimePublished = DateTime.UtcNow,
                Address = e.Address,
                DescriptionAbstract = e.BriefDescription,
                Visibility = e.Visibility ?? new PrivacyPolicy(),
                Attendability = e.Attendability ?? new PrivacyPolicy(),
                Coordinate = e.Coordinate,
                RecurringFrequency = e.RecurringType ?? EventRecurringType.Once,
                RecurringTimes = e.RecurringTimes ?? 0,
                IsDayEvent = e.IsDayEvent
            };

            EventOperator op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            op.Insert(entity);

            return new PureTextMessageResponse("success");
        }

        /// <summary>
        /// Update an event
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="e">Event detail to update</param>
        /// <returns></returns>
        [PUT("event/{id}"), Authorize, RequirePermission(OAuthScopes.CreateEvent)]
        public PureTextMessageResponse UpdateEvent(long id, [FromBody] EventDetail e)
        {
            if (!HasCurrentUserFullPermissionToEvent(id))
            {
                throw new InsufficientPermissionException();
            } 
            
            EventEntity entity;
            var op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            if (!op.TryGet(id, out entity))
            {
                throw new IdNotFoundException(id);
            }

            if (entity.ID != e.ID)
            {
                throw new ArgumentException("ID from the payload and from the uri do not match");
            }

            entity.Name = e.Name;
            entity.StartTime = e.StartTime;
            entity.EndTime = e.EndTime;
            entity.IsDayEvent = e.IsDayEvent;
            entity.RecurringFrequency = e.RecurringType ?? entity.RecurringFrequency;
            entity.RecurringTimes = e.RecurringTimes ?? entity.RecurringTimes;
            entity.DescriptionAbstract = e.BriefDescription;
            entity.Coordinate = e.Coordinate ?? entity.Coordinate;
            entity.SearchKeywords = e.Keywords ?? entity.SearchKeywords;
            entity.Visibility = e.Visibility ?? entity.Visibility;
            entity.Attendability = e.Attendability ?? entity.Attendability;

            op.Update(entity);

            return new PureTextMessageResponse("success");
        }

        /// <summary>
        /// Delete an event
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <returns></returns>
        [DELETE("event/{id}"), Authorize, RequirePermission(OAuthScopes.CreateEvent)]
        public PureTextMessageResponse DeleteEvent(long id)
        {
            if (!HasCurrentUserFullPermissionToEvent(id))
            {
                throw new InsufficientPermissionException();
            }
            EventEntity entity;

            var op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            if (!op.TryGet(id, out entity))
            {
                throw new IdNotFoundException(id);
            }
            op.Remove(entity);
            return new PureTextMessageResponse("success");

        }

        [GET("event/{id}/feeds")]
        public string GetFeeds(long id)
        {
            return "feed";
        }

        #region Picture
        /// <summary>
        /// Get the picture of the event (public)
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <returns></returns>
        [GET("event/{id}/picture")]
        public HttpResponseMessage GetPicture(long id)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage.Headers.Location = new Uri(Picture.GetProfilePictureUrl(ProfilePictureType.Event, id));
            httpResponseMessage.StatusCode = HttpStatusCode.Redirect;

            return httpResponseMessage;
        }

        // Only uploads thumbnail picture for now.
        // Will incorporate larger picture in album api
        [POST("event/{id}/picture"), Authorize, RequirePermission(OAuthScopes.CreateEvent)]
        public async Task<PureTextMessageResponse> UploadProfilePicture(long id)
        {
            if (!HasCurrentUserFullPermissionToEvent(id))
            {
                throw new InsufficientPermissionException();
            }
            Stream stream = await Request.ReadFromFormData();
            Picture.UploadProfilePicture(ProfilePictureType.Event, id, stream);
            return new PureTextMessageResponse("success");
        }
        #endregion

        #region Comment
        [GET("event/{id}/comments")]
        public CountedListResponse<Comment> GetCommentsAfter(string id, string after = "0", int limit = Models.Constants.CommentListDefaultLimit)
        {
            ListApiOptions options = new ListApiOptions()
            {
                RequestUri = Request.RequestUri,
                AfterString = after,
                Limit = limit,
                Mode = PagingMode.CursorAfterString
            };
            return new CommentProvider(options).GetComments(id, EntityTypeCode.Event);
        }

        [POST("event/{id}/comments"), Authorize]
        public PureTextMessageResponse AddComment(string id, [FromBody]string content)
        {
            long userId = Thread.CurrentPrincipal.GetID();
            CommentUtilities.PostComment(content, id, EntityTypeCode.Event, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
        #endregion

        #region Like
        [GET("event/{id}/like")]
        public CountedListResponse<Like> GetLikesAfter(string id, string after = "0", int limit = Models.Constants.LikeListDefaultLimit)
        {
            ListApiOptions options = new ListApiOptions()
            {
                RequestUri = Request.RequestUri,
                AfterString = after,
                Limit = limit,
                Mode = PagingMode.CursorAfterString
            };
            return new LikeProvider(options).GetLikes(id, EntityTypeCode.Event);
        }

        [POST("event/{id}/like"), Authorize]
        public PureTextMessageResponse AddLike(string id)
        {
            long userId = Thread.CurrentPrincipal.GetID();
            LikeUtilities.PostLike(id, EntityTypeCode.Event, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
        #endregion

        #region Attendance
        /// <summary>
        /// Get attendance information after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Part of the attendance list</returns>
        [GET("event/{id}/invited")]
        public CountedListResponse<Attendant> GetInvitedUsers(
            long id,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id);
        }

        /// <summary>
        /// Delete invitation
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="uid">User ID</param>
        /// <returns></returns>
        [DELETE("event/{id}/invited/{uid}"), Authorize]
        public PureTextMessageResponse DeleteInvitation(
            long id,
            long uid)
        {
            if (!Thread.CurrentPrincipal.HasRole(UserRoleLevel.Admin) ||
                !Thread.CurrentPrincipal.HasRole(UserRoleLevel.SuperAdmin))
            {
                Attendant currentUser = new AttendanceProvider().CheckStatus(id, uid);
                if (currentUser.RsvpStatus < AttendanceStatus.Copublisher)
                {
                    throw new InsufficientPermissionException();
                }
            }
            var op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            op.RemoveAttendance(id, uid);
            return new PureTextMessageResponse("success");
        }

        /// <summary>
        /// Get the attendance status of a specific user to an event
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="uid">User ID</param>
        /// <param name="test">Whether in test mode</param>
        /// <returns>Contains user data and attendance information, 
        /// if user is not related to the event, then an empty list will be returned</returns>
        [GET("event/{id}/invited/{uid}")]
        public ListResponse<Attendant> GetAttendanceStatus(
            long id,
            long uid)
        {
            Attendant result = new AttendanceProvider().CheckStatus(id, uid);
            if (result == null)
            {
                return new ListResponse<Attendant>()
                {
                    Data = new List<Attendant>()
                };
            }
            else
            {
                return new ListResponse<Attendant>()
                {
                    Data = new List<Attendant>() { result }
                };
            }
        }

        /// <summary>
        /// Get users that attend the event after a cursor
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="options">List API options</param>
        /// <returns>Users that attend the event</returns>
        [GET("event/{id}/{status}")]
        public CountedListResponse<Attendant> GetAttendantWithStatus(
            long id,
            AttendanceStatus status,
            [ModelBinder(typeof(ListApiBinderProvider), Name = "Int64")]ListApiOptions options)
        {
            return new AttendanceProvider(options).GetAttendants(id, status);
        }

        /// <summary>
        /// Add/update the attendance for a user
        /// </summary>
        /// <param name="id">Event ID</param>
        /// <param name="status">Status that the user wants to enrol in</param>
        /// <returns></returns>
        [POST("event/{id}/{status}"), Authorize]
        public PureTextMessageResponse AddAttendance(
            long id,
            AttendanceStatus status)
        {
            // Cannot use this API to set attendance.
            // This API is for other users to attend the event.
            // For editing the admins of the event, please submit an Update Event request.
            if (status >= AttendanceStatus.Organizer)
            {
                throw new ArgumentException("invalid attendance status");
            }

            EventOperator op = new EventOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            EventEntity entity;
            if (!op.TryGet(id, out entity))
            {
                throw new IdNotFoundException(id);
            }

            if (!PrivacyJudge.CheckPrivacy(Thread.CurrentPrincipal.GetID(), EntityTypeCode.Event, id, entity.Attendability))
            {
                throw new InsufficientPermissionException();
            }

            op.SetAttendance(id, Thread.CurrentPrincipal.GetID(), status);
            return new PureTextMessageResponse("success");
        }

        /// <summary>
        /// Check if the current logged in user has the permission to the event
        /// </summary>
        /// <param name="eid">Event ID</param>
        /// <returns>Has permission or not</returns>
        private bool HasCurrentUserFullPermissionToEvent(long eid)
        {
            if (!Thread.CurrentPrincipal.HasRole(UserRoleLevel.Admin) ||
                   !Thread.CurrentPrincipal.HasRole(UserRoleLevel.SuperAdmin))
            {
                Attendant currentUser = new AttendanceProvider().CheckStatus(eid, Thread.CurrentPrincipal.GetID());
                if (currentUser.RsvpStatus < AttendanceStatus.Publisher)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
