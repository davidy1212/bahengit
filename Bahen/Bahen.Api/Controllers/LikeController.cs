﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Api.Models;
using Bahen.Data.Models;
using System.Threading;
using System.Web.Http;


namespace Bahen.Api
{
    public class LikeController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">TargetString</param>
        /// <returns></returns>
        [DELETE("like/{id}"), Authorize, RequirePermission(OAuthScopes.Owner)]
        public PureTextMessageResponse DeleteLike(string id, int typeCode)
        {
            long userId = Thread.CurrentPrincipal.GetID();
            LikeUtilities.DeleteLike(id, (EntityTypeCode)typeCode, userId);
            return new PureTextMessageResponse() { Message = "Success" };
        }
    }
}
