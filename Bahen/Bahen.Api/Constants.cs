﻿using System.Configuration;

namespace Bahen.Api
{
    public static class Constants
    {
        public static string Endpoint
        {
            get
            {
                return string.Format("http://{0}:{1}",
                ConfigurationManager.AppSettings["Root"],
                ConfigurationManager.AppSettings["Port"]);
            }
        }

        public static string Port
        {
            get
            {
                return ConfigurationManager.AppSettings["Port"];
            }
        }
    }
}
