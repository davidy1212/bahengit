﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using System.Configuration;

namespace Bahen.Api
{
    class Program
    {
        static void Main(string[] args)
        {
            string uri = Constants.Endpoint;
            using (WebApp.Start<Startup>(uri))
            {
                Console.WriteLine("Starting OWIN at {0}", uri);

                // Open the browser
                //Process.Start(uri);

                // Press any key to stop the server
                Console.ReadKey();
                Console.WriteLine("Stopping");
            }
        }
    }
}
