﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using System.Diagnostics;

namespace Bahen.Data.Models.Tests
{
    [TestClass]
    public class UserOpinionEntityTest
    {
        [TestMethod]
        public void CreateLikeEntityID()
        {
            long targetID = 123456789;
            long userID = 987654321;
            TableLikeEntity likeEntity = new TableLikeEntity(targetID.ToString(), EntityTypeCode.Event, userID);
            Trace.WriteLine("Partition Key: " + likeEntity.PartitionKey);
            Trace.WriteLine("Row Key: " + likeEntity.RowKey);
            //Trace.WriteLine("Target ID: " + likeEntity.TargetID);
            //Trace.WriteLine("Target GUID: " + likeEntity.TargetGuid.ToString());
            Trace.WriteLine("User ID: " + likeEntity.UserID);
            Trace.WriteLine("T_TypeCode: " + likeEntity.TargetTypeCode);
            Trace.WriteLine("TypeCode: " + likeEntity.TypeCode);
            Trace.WriteLine("Action Time" + likeEntity.Time);
            Assert.AreEqual(targetID.ToString(), likeEntity.TargetString);
            //Assert.AreEqual(Guid.Empty, likeEntity.TargetGuid);
            Assert.AreEqual(userID, likeEntity.UserID);
        }
        [TestMethod]
        public void CreateLikeEntityGuid()
        {
            Guid targetGuid = Guid.NewGuid();
            long userID = 987654321;
            TableLikeEntity likeEntity = new TableLikeEntity(targetGuid.ToString(), EntityTypeCode.Event, userID);
            Trace.WriteLine("targetGuid: " + targetGuid.ToString());
            Trace.WriteLine("Partition Key: " + likeEntity.PartitionKey);
            Trace.WriteLine("Row Key: " + likeEntity.RowKey);
            //Trace.WriteLine("Target ID: " + likeEntity.TargetID);
            //Trace.WriteLine("Target GUID: " + likeEntity.TargetGuid.ToString());
            Trace.WriteLine("User ID: " + likeEntity.UserID);
            Trace.WriteLine("T_TypeCode: " + likeEntity.TargetTypeCode);
            Trace.WriteLine("TypeCode: " + likeEntity.TypeCode);
            Trace.WriteLine("Action Time" + likeEntity.Time);
            //Assert.AreEqual(0, likeEntity.TargetID);
            Assert.AreEqual(targetGuid.ToString(), likeEntity.TargetString);
            Assert.AreEqual(userID, likeEntity.UserID);
            //Assert.AreNotEqual(Guid.Empty, likeEntity.TargetGuid);
        }

        [TestMethod]
        public void CopyLikeEntityGuid()
        {
            Guid targetGuid = Guid.NewGuid();
            long userID = 987654321;
            TableLikeEntity likeEntity_ori = new TableLikeEntity(targetGuid.ToString(), EntityTypeCode.Event, userID);
            TableLikeEntity likeEntity = new TableLikeEntity();
            likeEntity = likeEntity_ori;
            Trace.WriteLine("targetGuid: " + targetGuid.ToString());
            Trace.WriteLine("Partition Key: " + likeEntity.PartitionKey);
            Trace.WriteLine("Row Key: " + likeEntity.RowKey);
            //Trace.WriteLine("Target ID: " + likeEntity.TargetID);
            //Trace.WriteLine("Target GUID: " + likeEntity.TargetGuid.ToString());
            Trace.WriteLine("User ID: " + likeEntity.UserID);
            Trace.WriteLine("T_TypeCode: " + likeEntity.TargetTypeCode);
            Trace.WriteLine("TypeCode: " + likeEntity.TypeCode);
            Trace.WriteLine("Action Time" + likeEntity.Time);
            //Assert.AreEqual(0, likeEntity.TargetID);
            Assert.AreEqual(targetGuid.ToString(), likeEntity.TargetString);
            Assert.AreEqual(userID, likeEntity.UserID);
            //Assert.AreNotEqual(Guid.Empty, likeEntity.TargetGuid);
        }
    }
}
