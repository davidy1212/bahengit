﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using Bahen.Data.Models;
using System.Collections.Generic;

namespace Bahen.Data.Models.Tests
{
    //[TestClass]
    //public class PhoneNumberTests
    //{
    //    [TestMethod]
    //    public void PhoneNumberToJsonStringBasics()
    //    {
    //        PhoneNumber phoneNumber1 = new PhoneNumber()
    //        {
    //            Value = "444-444-4444",
    //            Type = UserContactPhoneType.Work,
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        Assert.AreEqual("{\"Value\":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", phoneNumber1.ToString());

    //        // Try multiples
    //        List<PhoneNumber> numbers = new List<PhoneNumber>();
    //        PhoneNumber phoneNumber2 = new PhoneNumber()
    //        {
    //            Value = "555-444-4444",
    //            Type = UserContactPhoneType.Custom,
    //            TypeString = "Favorite",
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        numbers.Add(phoneNumber1);
    //        numbers.Add(phoneNumber2);
    //        Assert.AreEqual("[{\"Value\":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", PhoneNumber.ToStringBatch(numbers));
    //    }

    //    [TestMethod]
    //    public void PhoneNumberReadJsonBasics()
    //    {
    //        PhoneNumber phoneNumber;
    //        Assert.IsTrue(PhoneNumber.TryReadJson("{\"Value\":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", out phoneNumber));
    //        Assert.AreEqual("444-444-4444", phoneNumber.Value);
    //        Assert.AreEqual(UserContactPhoneType.Work, phoneNumber.Type);
    //        Assert.AreEqual("Work", phoneNumber.TypeString);
    //        Assert.AreEqual(UserPrivacyLevel.Friends, phoneNumber.PrivacyLevel);
    //    }

    //    [TestMethod]
    //    public void PhoneNumberReadNotJson()
    //    {
    //        PhoneNumber phoneNumber;
    //        Assert.IsFalse(PhoneNumber.TryReadJson("xxxxxxxxxx", out phoneNumber));
    //    }

    //    [TestMethod]
    //    public void PhoneNumberBatchReadJsonBasics()
    //    {
    //        ICollection<PhoneNumber> phoneNumbers;
    //        Assert.IsTrue(PhoneNumber.TryReadJsonBatch("[{\"Value\":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out phoneNumbers));
    //        Assert.AreEqual("{\"Value\":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}", String.Join<PhoneNumber>(",", phoneNumbers));
    //    }

    //    [TestMethod]
    //    public void PhoneNumberBatchReadNotJson()
    //    {
    //        ICollection<PhoneNumber> phoneNumbers;
    //        Assert.IsFalse(PhoneNumber.TryReadJsonBatch(":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out phoneNumbers));
    //    }
    //}
}
