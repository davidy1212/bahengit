﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using Bahen.Data.Models;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;

namespace Bahen.Data.Models.Tests
{
    //[TestClass]
    //public class AddressTests
    //{
    //    [TestMethod]
    //    public void AddressToJsonStringBasics()
    //    {
    //        Address address1 = new Address()
    //        {
    //            Street = "444-444-4444",
    //            Type = UserContactAddressType.Work,
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        Assert.AreEqual("{\"Street\":\"444-444-4444\",\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", address1.ToString());

    //        // Try multiples
    //        List<Address> numbers = new List<Address>();
    //        Address address2 = new Address()
    //        {
    //            Street = "555-444-4444",
    //            Type = UserContactAddressType.Custom,
    //            TypeString = "Favorite",
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        numbers.Add(address1);
    //        numbers.Add(address2);
    //        Assert.AreEqual("[{\"Street\":\"444-444-4444\",\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Street\":\"555-444-4444\",\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", Address.ToStringBatch(numbers));
    //    }

    //    [TestMethod]
    //    public void AddressReadJsonBasics()
    //    {
    //        Address address;
    //        Assert.IsTrue(Address.TryReadJson("{\"Street\":\"444-444-4444\",\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", out address));
    //        Assert.AreEqual("444-444-4444", address.Street);
    //        Assert.AreEqual(UserContactAddressType.Work, address.Type);
    //        Assert.AreEqual("Work", address.TypeString);
    //        Assert.AreEqual(UserPrivacyLevel.Friends, address.PrivacyLevel);
    //    }

    //    [TestMethod]
    //    public void AddressReadNotJson()
    //    {
    //        Address address;
    //        Assert.IsFalse(Address.TryReadJson("xxxxxxxxxx", out address));
    //    }

    //    [TestMethod]
    //    public void AddressBatchReadJsonBasics()
    //    {
    //        ICollection<Address> addresses;
    //        Assert.IsTrue(Address.TryReadJsonBatch("[{\"Street\":\"444-444-4444\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out addresses));
    //        Assert.AreEqual("{\"Street\":\"444-444-4444\",\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Street\":null,\"City\":null,\"Province\":null,\"Country\":null,\"PostalCode\":null,\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}", String.Join<Address>(",", addresses));
    //    }

    //    [TestMethod]
    //    public void AddressBatchReadNotJson()
    //    {
    //        ICollection<Address> addresses;
    //        Assert.IsFalse(Address.TryReadJsonBatch(":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out addresses));
    //    }
    //    [TestMethod]
    //    public void MyTestMethod()
    //    {
    //        UserEntity entity = new UserEntity();
    //        entity.Address = new Address()
    //        {
    //            City = "Toronto"
    //        };
    //        Address x = (Address) entity.Address.Clone();
    //        x.City = "New York";
    //        entity.Address = x;

    //        //entity.Address.City = "New York";
    //        CultureInfo info = new CultureInfo("fr-FR");
    //        string x2 = JsonConvert.SerializeObject(info);
    //        CultureInfo info2 = JsonConvert.DeserializeObject<CultureInfo>(x2);
    //    }
    //}
}
