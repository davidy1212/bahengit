﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using System.Collections.Generic;
using Bahen.Common;
using System.Diagnostics;

namespace Bahen.Data.Models.Tests
{
    [TestClass]
    public class UserFullEntityTests
    {
        //[TestMethod]
        //public void CastBasics()
        //{
        //    SqlUserEntity sqlEntity = new SqlUserEntity()
        //    {
        //        LoginEmail = "renmengye@gmail.com",
        //        LoginPassword = Bahen.Common.HashUtilities.ComputeHashSecure("hahah"),
        //        PreferredName = "Mengye Ren"
        //    };
        //    TableUserEntity tableEntity = new TableUserEntity()
        //    {
        //        EmailAddresses = Bahen.Common.JsonConvert.SerializeObject(
        //        new EmailAddress[]{
        //            new EmailAddress(){
        //                Value="renmengye@gmail.com",
        //                TypeString="Personal",
        //                PrivacyLevel=UserProfileVisibilityLevel.Public
        //            },
        //            new EmailAddress(){
        //                Value="m.ren@mail.utoronto.ca",
        //                TypeString="Work",
        //                PrivacyLevel=UserProfileVisibilityLevel.Public
        //            }
        //        }),
        //        Addresses = Bahen.Common.JsonConvert.SerializeObject(
        //        new Address[]{
        //            new Address(){
        //                City="Toronto",
        //                Street="761 Bay St.",
        //                Province="Ontario",
        //                Country="Canada"
        //            },
        //            new Address(){
        //                City="Vancouver",
        //                Street="3939 16th Ave.",
        //                Province="British Columbia",
        //                Country="Canada"
        //            },
        //        }
        //        ),
        //        FirstName = "Mengye",
        //        LastName = "Ren"
        //    };
        //    UserEntity fullEntity = new UserEntity(sqlEntity, tableEntity);
        //    SimpleUserEntity simpleEntity = fullEntity.Cast<SimpleUserEntity>();

        //    Assert.AreEqual("renmengye@gmail.com", simpleEntity.LoginEmail);
        //}

        [TestMethod]
        public void TestPartitionKeyRowKey()
        {
            long num = 1000;
            string hex = BaseConvert.ToHexString(num);
            Trace.WriteLine(hex);
            Trace.WriteLine(String.Format("Length: {0}", hex.Length));
        }

    }
}
