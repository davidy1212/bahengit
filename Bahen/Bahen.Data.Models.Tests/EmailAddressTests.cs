﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using Bahen.Data.Models;
using System.Collections.Generic;

namespace Bahen.Data.Models.Tests
{
    //[TestClass]
    //public class EmailAddressTests
    //{
    //    [TestMethod]
    //    public void EmailAddressToJsonStringBasics()
    //    {
    //        EmailAddress emailAddress1 = new EmailAddress()
    //        {
    //            Value = "renmengye@gmail.com",
    //            Type = UserContactEmailType.Work,
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        Assert.AreEqual("{\"Value\":\"renmengye@gmail.com\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", emailAddress1.ToString());

    //        // Try multiples
    //        List<EmailAddress> addresses = new List<EmailAddress>();
    //        EmailAddress emailAddress2 = new EmailAddress()
    //        {
    //            Value = "renmengye@yahoo.com",
    //            Type = UserContactEmailType.Custom,
    //            TypeString = "Favorite",
    //            PrivacyLevel = UserPrivacyLevel.Friends
    //        };
    //        addresses.Add(emailAddress1);
    //        addresses.Add(emailAddress2);
    //        Assert.AreEqual("[{\"Value\":\"renmengye@gmail.com\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"renmengye@yahoo.com\",\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", EmailAddress.ToStringBatch(addresses));
    //    }

    //    [TestMethod]
    //    public void EmailAddressReadJsonBasics()
    //    {
    //        EmailAddress emailAddress;
    //        Assert.IsTrue(EmailAddress.TryReadJson("{\"Value\":\"renmengye@gmail.com\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1}", out emailAddress));
    //        Assert.AreEqual("renmengye@gmail.com", emailAddress.Value);
    //        Assert.AreEqual(UserContactEmailType.Work, emailAddress.Type);
    //        Assert.AreEqual("Work", emailAddress.TypeString);
    //        Assert.AreEqual(UserPrivacyLevel.Friends, emailAddress.PrivacyLevel);
    //    }

    //    [TestMethod]
    //    public void EmailAddressReadNotJson()
    //    {
    //        EmailAddress emailAddress;
    //        Assert.IsFalse(EmailAddress.TryReadJson("xxxxxxxxxx", out emailAddress));
    //    }

    //    [TestMethod]
    //    public void EmailAddressBatchReadJsonBasics()
    //    {
    //        ICollection<EmailAddress> emailAddresses;
    //        Assert.IsTrue(EmailAddress.TryReadJsonBatch("[{\"Value\":\"renmengye@gmail.com\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"renmengye@yahoo.com\",\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out emailAddresses));
    //        Assert.AreEqual("{\"Value\":\"renmengye@gmail.com\",\"Type\":1,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"renmengye@yahoo.com\",\"Type\":2,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}", String.Join<EmailAddress>(",", emailAddresses));
    //    }

    //    [TestMethod]
    //    public void EmailAddressBatchReadNotJson()
    //    {
    //        ICollection<EmailAddress> emailAddresses;
    //        Assert.IsFalse(EmailAddress.TryReadJsonBatch(":\"444-444-4444\",\"Type\":2,\"TypeString\":\"Work\",\"PrivacyLevel\":1},{\"Value\":\"555-444-4444\",\"Type\":5,\"TypeString\":\"Favorite\",\"PrivacyLevel\":1}]", out emailAddresses));
    //    }
    //}
}