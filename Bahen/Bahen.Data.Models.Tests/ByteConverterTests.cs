﻿using Bahen.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models.Tests
{
    [TestClass]
    public class ByteConverterTests
    {
        [TestMethod]
        public void MyTestMethod()
        {
            long[] test = new long[] { 152867, 645466, 2345, 8438, 32363483 };
            byte[] convert = ByteConvert.GetBytes(test);
            var test2 = ByteConvert.GetInt64(convert);
            Assert.IsTrue(test.SequenceEqual(test2));
        }
    }
}
