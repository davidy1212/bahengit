﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Data.Models;
using System.Linq;

namespace Bahen.Data.Models.Tests
{
    [TestClass]
    public class PrivacyPolicyTest
    {
        [TestMethod]
        public void PrivacyPolicySerialization()
        {
            PrivacyPolicy policy = new PrivacyPolicy()
            {
                Allowed = new PrivacyGroup()
                {
                    Users = new long[] { 2, 5, 8, 10 },
                    Cycles = new long[] { 3, 133, 1040 },
                    Relations = Relation.Followers | Relation.Invited
                },
                Blocked = new PrivacyGroup()
                {
                    Relations = Relation.Public
                },
                UponRequest = new PrivacyGroup()
                {
                    Cycles = new long[] { 22 }
                }
            };
            byte[] output = policy.Serialize();
            PrivacyPolicy policy2 = PrivacyPolicy.Deserialize(output);
            Assert.AreEqual("2 5 8 10", String.Join(" ", policy2.Allowed.Users));
            Assert.AreEqual("3 133 1040", String.Join(" ", policy2.Allowed.Cycles));
            Assert.AreEqual(Relation.Followers | Relation.Invited, policy2.Allowed.Relations);
            Assert.AreEqual(Relation.Public, policy2.Blocked.Relations);
            Assert.AreEqual("22", String.Join(" ", policy2.UponRequest.Cycles));
        }
    }
}
