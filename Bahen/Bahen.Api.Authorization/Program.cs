﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace Bahen.Api.Authorization
{
    class Program
    {
        static void Main(string[] args)
        {
            string uri = Constants.Endpoint;

            //string certPath = Constants.CertPath;
            //string port = Constants.Port;
            //X509Certificate2 certificate = new X509Certificate2(certPath);

            //// netsh http add urlacl url=https://+:<port>/ user=EVERYONE
            //Process urlAcl = new Process();
            //urlAcl.StartInfo.FileName = "netsh.exe";
            //urlAcl.StartInfo.Arguments = string.Format("http add urlacl url=https://+:{0}/ user={1}", port, "EVERYONE");
            //urlAcl.Start();
            //urlAcl.WaitForExit();

            //// netsh http add sslcert ipport=0.0.0.0:<port> certhash={<thumbprint>} appid={<some GUID>}
            //Process bindPortToCertificate = new Process();
            //bindPortToCertificate.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
            ////bindPortToCertificate.StartInfo.FileName = "Certs\\netsh.exe";
            //bindPortToCertificate.StartInfo.Arguments = string.Format("http add sslcert ipport=0.0.0.0:{0} certhash={1} appid={{{2}}}", port, certificate.Thumbprint, Guid.NewGuid());
            //bindPortToCertificate.Start();
            //bindPortToCertificate.OutputDataReceived += (obj, e) =>
            //    {
            //    };

            using (WebApp.Start<Startup>(uri))
            {
                Console.WriteLine("Starting OWIN at {0}", uri);

                // Press any key to stop the server
                Console.ReadKey();
                Console.WriteLine("Stopping");

                //// netsh http delete sslcert ipport=0.0.0.0:<port>           
                //Process unbindPortToCertificate = new Process();
                //unbindPortToCertificate.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
                //unbindPortToCertificate.StartInfo.Arguments = string.Format("http delete sslcert ipport=0.0.0.0:{0}", port);
                //unbindPortToCertificate.Start();
                //unbindPortToCertificate.WaitForExit();

                //// netsh http delete urlacl url=https://+:<port>/
                //Process deleteAcl = new Process();
                //deleteAcl.StartInfo.FileName = "netsh.exe";
                //deleteAcl.StartInfo.Arguments = string.Format("http delete urlacl url=https://+:{0}/", port);
            }
        }
    }
}
