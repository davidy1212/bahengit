﻿using Newtonsoft.Json;

namespace Bahen.Api.Authorization
{
    public class OAuthRefreshTokenRequest
    {
        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        [JsonProperty("Client_id")]
        public long ClientID { get; set; }

        [JsonProperty("Client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("Grant_type")]
        public string GrantType { get; set; }
    }
}
