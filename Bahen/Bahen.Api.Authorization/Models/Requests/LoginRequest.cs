﻿
namespace Bahen.Api.Authorization
{
    public class LoginRequest
    {
        public string LoginEmail { get; set; }
        public string LoginPassword { get; set; }
        public bool? RememberMe { get; set; }
        public string RefreshToken { get; set; }
    }
}
