﻿
namespace Bahen.Api.Authorization
{
    public class UserRegisterRequest
    {
        public string LoginEmail { get; set; }
        public string LoginPassword1 { get; set; }
        public string LoginPassword2 { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
