﻿using Newtonsoft.Json;

namespace Bahen.Api.Authorization
{
    public class AuthorizeRequest
    {
        [JsonProperty("Client_id")]
        public long ClientId { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        public string Scope { get; set; }

        [JsonProperty("Response_type")]
        public string ResponseType { get; set; }
    }
}
