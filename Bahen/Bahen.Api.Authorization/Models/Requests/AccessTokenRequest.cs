﻿using Newtonsoft.Json;

namespace Bahen.Api.Authorization
{
    public class AccessTokenRequest
    {
        public string Code { get; set; }

        [JsonProperty("Client_id")]
        public long ClientID { get; set; }

        [JsonProperty("Client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        [JsonProperty("Grant_type")]
        public string GrantType { get; set; }

        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("Email")]
        public string LoginEmail { get; set; }

        [JsonProperty("Password")]
        public string LoginPassword { get; set; }
    }
}
