﻿
namespace Bahen.Api.Authorization
{
    public enum GrantType
    {
        Code, RefreshToken, Password
    }
}
