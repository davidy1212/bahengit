﻿
namespace Bahen.Api.Authorization
{
    public class MessageResponse
    {
        public string Message { get; set; }

        public MessageResponse(string msg)
        {
            this.Message = msg;
        }
    }
}
