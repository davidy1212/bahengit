﻿using Newtonsoft.Json;

namespace Bahen.Api.Authorization
{
    public class AccessTokenResponse
    {
        public string Message { get; set; }

        [JsonProperty("Access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("Expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }
    }

}
