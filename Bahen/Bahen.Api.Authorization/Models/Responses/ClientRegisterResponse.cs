﻿
namespace Bahen.Api.Authorization
{
    public class ClientRegisterResponse
    {
        public Client Data { get; set; }
        public string Message { get; set; }
    }
}
