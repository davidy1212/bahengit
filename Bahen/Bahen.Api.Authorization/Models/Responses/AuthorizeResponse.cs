﻿
namespace Bahen.Api.Authorization
{
    public class RedirectResponse
    {
        public string Redirect { get; set; }
    }
}
