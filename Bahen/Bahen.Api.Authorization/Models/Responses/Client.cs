﻿
namespace Bahen.Api.Authorization
{
    public class Client
    {
        public long Id { get; set; }
        public string Secret { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string TrustLevel { get; set; }
    }
}
