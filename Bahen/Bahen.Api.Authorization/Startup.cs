using AttributeRouting.Web.Http.SelfHost.Logging;
using Bahen.Api.Common;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;
using System.Web.Http.SelfHost;

namespace Bahen.Api.Authorization
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Setup endpoint
            var config = new HttpSelfHostConfiguration(Constants.Endpoint);

            // Setup OAuth authorization handler
            config.MessageHandlers.Add(new OAuthMessageHandler());

            // Setup camel case
            var jsonFormatter = config.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter()
            {
                CamelCaseText = true
            });

            // Setup ignoring null value properties
            jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

            // Setup attribute routing
            AttributeRoutingConfig.RegisterRoutes(config);
            config.Routes.Cast<HttpRoute>().ToArray().LogTo(Console.Out);

            // Setup CORS
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            app.UseWebApi(config);
        }
    }
}
