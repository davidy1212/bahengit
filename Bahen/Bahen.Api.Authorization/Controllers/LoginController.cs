﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Common;
using System;
using System.Configuration;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    /// <summary>
    /// APIs that associates with login functionality.
    /// Use loginEmail and loginPassword (or refresh token) to exchange for an api token.
    /// </summary>
    public class LoginController : ApiController
    {
        /// <summary>
        /// Login with email and password
        /// </summary>
        /// <param name="request">See login request object</param>
        /// <returns>Response contains api token</returns>
        [POST("login")]
        public AccessTokenResponse Login([FromBody]LoginRequest request)
        {
            if (String.IsNullOrEmpty(request.LoginEmail))
            {
                if (request.RefreshToken != null)
                {
                    return new TokenProvider(GrantType.RefreshToken).GetAccessToken(
                        new AccessTokenRequest()
                        {
                            ClientID = Int64.Parse(ConfigurationManager.AppSettings["ClientID"]),
                            ClientSecret = ConfigurationManager.AppSettings["ClientSecret"],
                            GrantType = "refresh_token",
                            Code = null,
                            RefreshToken = request.RefreshToken,
                            RedirectUri = null,
                            LoginEmail = null,
                            LoginPassword = null
                        });
                }
                throw new AuthenticationException("invalid login email or password");
            }
            else
            {
                return new TokenProvider(GrantType.Password).GetAccessToken(
                    new AccessTokenRequest()
                    {
                        ClientID = Int64.Parse(ConfigurationManager.AppSettings["ClientID"]),
                        ClientSecret = ConfigurationManager.AppSettings["ClientSecret"],
                        GrantType = "password",
                        Code = null,
                        RefreshToken = null,
                        RedirectUri = null,
                        LoginEmail = request.LoginEmail,
                        LoginPassword = request.LoginPassword
                    });
            }
        }
    }
}
