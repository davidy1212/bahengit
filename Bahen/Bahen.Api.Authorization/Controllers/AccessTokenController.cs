﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    public class AccessTokenController : ApiController
    {
        [GET("access_token"), POST("access_token")]
        public AccessTokenResponse GetAccessToken(AccessTokenRequest request)
        {
            if (request.GrantType == "password")
            {
                return new TokenProvider(GrantType.Password).GetAccessToken(request);
            }
            else if (request.GrantType == "code")
            {
                return new TokenProvider(GrantType.Code).GetAccessToken(request);
            }
            else if (request.GrantType == "refresh_token")
            {
                return new TokenProvider(GrantType.RefreshToken).GetAccessToken(request);
            }
            else
            {
                throw new OAuthException(OAuthError.UnsupportedResponseType, "grant type:" + request.GrantType);
            }
        }
    }
}
