﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    public class AuthorizeController : ApiController
    {
        [POST("authorize"), Authorize]
        public object AuthorizePost(
            [FromBody]AuthorizeRequest request)
        {
            SqlApiClientOperator opClient = new SqlApiClientOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientId);
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, String.Format("No client id {0} match", request.ClientId));
            }
            OAuthScopes scopes;
            try
            {
                scopes = Bahen.Common.EnumConvert<OAuthScopes>.ToEnum(request.Scope);
                scopes = scopes & ~OAuthScopes.Owner;
            }
            catch
            {
                throw new OAuthException(OAuthError.InvalidScope, "cannot parse scope parameter");
            }
            SqlApiAuthOperator opAuth = new SqlApiAuthOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);

            // Chech if already authorized
            IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == Thread.CurrentPrincipal.GetID());
            if (result.FirstOrDefault() == null)
            {
                // Redirection to confirm page
                return new RedirectResponse()
                {
                    Redirect = String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope)
                };
            }

            // Check if already requested token
            SqlApiAuthRelation auth = result.FirstOrDefault();
            if (!auth.Requested)
            {
                // Redirection to confirm page
                return new RedirectResponse()
                {
                    Redirect = String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope)
                };
            }

            // Check if scopes are same
            if (auth.Scopes != Convert.ToInt64(scopes))
            {
                auth = opAuth.Get(auth.ID);
                opAuth.Remove(auth);

                // Redirection to confirm page
                return new RedirectResponse()
                {
                    Redirect = String.Format("/api/oauth/grant?client_id={0}&response_type={1}&redirect_uri={2}&scope={3}",
                    request.ClientId, request.ResponseType, request.RedirectUri, request.Scope)
                };
            }

            // Issue token directly
            string atoken = TokenFactory.IssueAccessToken(
                Thread.CurrentPrincipal.GetID(),
                request.ClientId,
                new TimeSpan(1, 0, 0),
                scopes,
                (Thread.CurrentPrincipal as LoginPrincipal).Role);
            string rtoken = ((OAuthScopes)auth.Scopes).HasFlag(OAuthScopes.OfflineAccess) ? TokenFactory.IssueRefreshToken(auth.ID, auth.UserID, auth.ClientID) : null;

            return new AccessTokenResponse()
            {
                AccessToken = atoken,
                ExpiresIn = 3600,
                RefreshToken = rtoken
            };
        }
    }
}
