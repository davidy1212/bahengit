﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Api.Models;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Configuration;
using System.Threading;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    /// <summary>
    /// APIs that associates with API clients
    /// </summary>
    public class ClientController : ApiController
    {
        /// <summary>
        /// Get an API client info
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [GET("client/{id}")]
        public Client GetClientInfo(long id)
        {
            SqlApiClientEntity entity;
            SqlApiClientOperator op = new SqlApiClientOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            entity = op.Get(id);
            Client result = new Client()
            {
                Id = entity.ID,
                Name = entity.Name,
                Picture = entity.PictureUrl
            };
            return result;
        }

        /// <summary>
        /// Get an API client info with secret
        /// </summary>
        /// <param name="id">Client ID</param>
        /// <param name="secret">Client secret</param>
        /// <returns></returns>
        [GET("client/{id}"), Authorize]
        public Client GetClientInfo(
            long id,
            bool secret)
        {
            if (secret == false)
            {
                return this.GetClientInfo(id);
            }
            else
            {
                SqlApiClientEntity entity;
                SqlApiClientOperator op = new SqlApiClientOperator(
                    ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                entity = op.Get(id);
                if (entity.DeveloperID == Thread.CurrentPrincipal.GetID())
                {
                    Client result = new Client()
                    {
                        Id = entity.ID,
                        Name = entity.Name,
                        Secret = entity.ClientSecret.ToString("N"),
                        Picture = entity.PictureUrl
                    };
                    return result;
                }
                else
                {
                    throw new AuthenticationException("You are not authorized to request the resource.");
                }
            }
        }

        /// <summary>
        /// Register an API client
        /// </summary>
        /// <param name="request">Register request</param>
        /// <returns></returns>
        [POST("client/register"), Authorize]
        public OAuthRegisterResponse RegisterClient(
            [FromBody] OAuthRegisterRequest request)
        {
            SqlApiClientEntity entity = new SqlApiClientEntity()
            {
                Name = request.Name,
                ClientSecret = Guid.NewGuid(),
                DeveloperID = Thread.CurrentPrincipal.GetID(),
                TrustLevel = (int)ApiClientTrustLevel.Test
            };

            SqlApiClientOperator op = new SqlApiClientOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            op.Insert(entity);

            OAuthRegisterResponse response = new OAuthRegisterResponse()
            {
                Data = new OAuthClient()
                {
                    Id = entity.ID,
                    Secret = entity.ClientSecret.ToString("N"),
                    Name = entity.Name,
                    TrustLevel = ((ApiClientTrustLevel)entity.TrustLevel).ToString().ToLowerInvariant()
                }
            };

            return response;
        }
    }
}
