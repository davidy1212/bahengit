﻿using AttributeRouting.Web.Http;
using Bahen.Api.Common;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    public class GrantController : ApiController
    {
        [POST("grant"), Authorize]
        public object Grant(
            [FromBody] AuthorizeRequest request)
        {
            SqlApiClientOperator opClient = new SqlApiClientOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientId);
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, String.Format("No client id {0} match", request.ClientId));
            }
            if (request.Scope == null)
            {
                throw new OAuthException(OAuthError.InvalidScope, "Invalid scopes");
            }
            if (request.RedirectUri == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, "Redirect uri not specified");
            }
            OAuthScopes scopes;
            try
            {
                scopes = Bahen.Common.EnumConvert<OAuthScopes>.ToEnum(request.Scope);
                scopes = scopes & ~OAuthScopes.Owner;
            }
            catch
            {
                throw new OAuthException(OAuthError.InvalidScope, "cannot parse scope parameter");
            }
            SqlApiAuthOperator opAuth = new SqlApiAuthOperator(
            ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiAuthRelation auth = null;

            if (request.ResponseType == "code")
            {
                string requestToken = Guid.NewGuid().ToByteArray().ToBase62();
                IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == Thread.CurrentPrincipal.GetID());
                if (result.FirstOrDefault() != null)
                {
                    auth = result.First();
                    auth = opAuth.Get(auth.ID);
                    auth.Requested = false;
                    opAuth.Update(auth);
                }
                else
                {
                    // Issue request token
                    auth = new SqlApiAuthRelation()
                    {
                        ClientID = request.ClientId,
                        RedirectUri = request.RedirectUri,
                        Scopes = Convert.ToInt64(scopes),
                        UserID = Thread.CurrentPrincipal.GetID(),
                        Requested = false
                    };

                    // Store in sql
                    opAuth.Insert(auth);
                }

                // Redirect to redirect uri
                return new RedirectResponse()
                {
                    Redirect =
                    String.Format("{0}?code={1}",
                    request.RedirectUri,
                    TokenFactory.IssuAuthCode(auth.ID, auth.UserID, auth.ClientID, new TimeSpan(0, 10, 0)))
                };
            }
            else if (request.ResponseType == "token")
            {
                scopes = scopes & ~OAuthScopes.OfflineAccess;

                // Chech if already has auth entry
                IEnumerable<SqlApiAuthRelation> result = client.ApiAuths.Where(x => x.UserID == Thread.CurrentPrincipal.GetID());

                if (result.FirstOrDefault() != null)
                {
                    auth = result.First();
                    if (auth.RedirectUri != request.RedirectUri)
                    {
                        throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                    }
                    if (!auth.Requested)
                    {
                        throw new OAuthException(OAuthError.InvalidRequest, "implicit grant flow is not applicable");
                    }
                }
                else
                {
                    auth = new SqlApiAuthRelation()
                    {
                        ClientID = request.ClientId,
                        RedirectUri = request.RedirectUri,
                        Scopes = Convert.ToInt64(scopes),
                        UserID = Thread.CurrentPrincipal.GetID(),
                        Requested = true
                    };
                    opAuth.Insert(auth);
                }

                TableUserOperator uop = new TableUserOperator(
                        ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
                string token =
                    TokenFactory.IssueAccessToken(
                    auth.UserID,
                    auth.ClientID,
                    new TimeSpan(1, 0, 0),
                    (OAuthScopes)auth.Scopes,
                    (UserRoleLevel)uop.Get(Thread.CurrentPrincipal.GetID()).UserGroup);

                return new RedirectResponse(){
                    Redirect = 
                    String.Format("{0}?access_token={1}&expires_in={2}",
                    request.RedirectUri, token, 3600)};
            }
            else
            {
                throw new OAuthException(OAuthError.UnsupportedResponseType, request.ResponseType);
            }
        }
    }
}
