﻿using AttributeRouting.Web.Http;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Configuration;
using System.Web.Http;

namespace Bahen.Api.Authorization
{
    /// <summary>
    /// APIs associates with registering a user
    /// </summary>
    public class RegisterController : ApiController
    {
        /// <summary>
        /// Register a user (needs captcha in the future)
        /// </summary>
        /// <param name="request">See UserRegisterRequest object</param>
        /// <returns></returns>
        [POST("register")]
        public MessageResponse Register([FromBody]UserRegisterRequest request)
        {
            if (String.IsNullOrEmpty(request.LoginEmail))
            {
                throw new ArgumentException("Please specify login email");
            }
            if (!UserValidators.UserEmailValidator(request.LoginEmail))
            {
                throw new ArgumentException("Please provide valid email address");
            }
            if (String.IsNullOrEmpty(request.LoginPassword1))
            {
                throw new ArgumentException("Please specify login password");
            }
            if (!UserValidators.UserPasswordValidator(request.LoginPassword1, request.LoginPassword2))
            {
                throw new ArgumentException("Please provide valid login password");
            }
            if (String.IsNullOrEmpty(request.FirstName))
            {
                throw new ArgumentException("Please fill in first name");
            }
            if (String.IsNullOrEmpty(request.LastName))
            {
                throw new ArgumentException("Please fill in last name");
            }
            UserEntity user = new UserEntity()
            {
                LoginEmail = request.LoginEmail,
                LoginPassword = request.LoginPassword1.ToSha512(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                PreferredName = request.FirstName + " " + request.LastName,
                SubscriptionPolicy = new PrivacyPolicy(true)
            };
            UserOperator op = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);
            op.Insert(user);
            return new MessageResponse("Success");
        }
    }
}
