﻿using Bahen.Api.Common;
using Bahen.Api.Models;
using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Data.Operation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Bahen.Api.Authorization
{
    public class TokenProvider
    {
        public GrantType Type { get; set; }

        public TokenProvider(GrantType type)
        {
            this.Type = type;
        }

        public AccessTokenResponse GetAccessToken(AccessTokenRequest request)
        {
            SqlApiClientOperator opClient = new SqlApiClientOperator(
                   ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiAuthOperator op = new SqlApiAuthOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
            SqlApiClientEntity client = opClient.Get(request.ClientID);
            UserOperator uop = new UserOperator(
                ConfigurationManager.ConnectionStrings["Sql"].ConnectionString,
                ConfigurationManager.ConnectionStrings["Table"].ConnectionString);

            SqlApiAuthRelation auth;
            if (client == null)
            {
                throw new OAuthException(OAuthError.InvalidRequest, "invalid client id");
            }
            if (client.ClientSecret != Guid.Parse(request.ClientSecret))
            {
                throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid client secret");
            }
            switch (this.Type)
            {
                case GrantType.Code:
                    {
                        AuthorizationCode code = null;
                        if (!TokenFactory.ValidateToken<AuthorizationCode>(request.Code, out code))
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                        }
                        if (code.Expires < DateTime.UtcNow)
                        {
                            try
                            {
                                auth = op.Get(code.AuthId);
                                op.Remove(auth);
                            }
                            catch { }
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "authorization code has expired, please let user authorize again");
                        }
                        try
                        {
                            auth = op.Get(code.AuthId);
                        }
                        catch
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "user has not authorized the app");
                        }
                        if (auth.RedirectUri != request.RedirectUri)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                        }
                        if (auth.ClientID != code.ClientId)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                        }
                        if (auth.UserID != code.UserId)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid authorization code");
                        }
                        break;

                    }
                case GrantType.RefreshToken:
                    {
                        RefreshToken token;
                        if (!TokenFactory.ValidateToken<RefreshToken>(request.RefreshToken, out token))
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                        }
                        try
                        {
                            auth = op.Get(token.AuthId);
                        }
                        catch
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "user has not authorized the app");
                        }
                        if (auth.RedirectUri != request.RedirectUri)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid redirect uri");
                        }
                        if (auth.ClientID != token.ClientId)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                        }
                        if (auth.UserID != token.UserId)
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid refresh token");
                        }
                        break;
                    }
                case GrantType.Password:
                    {
                        if (client.TrustLevel < (int)ApiClientTrustLevel.Owner)
                        {
                            throw new OAuthException(OAuthError.UnsupportedResponseType, "client trust level does not support password grant type");
                        }
                        SqlUserOperator opUser = new SqlUserOperator(ConfigurationManager.ConnectionStrings["Sql"].ConnectionString);
                        SqlUserEntity userLogin = null;
                        if (!opUser.TryGet(request.LoginEmail, out userLogin))
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid login credential");
                        }
                        if (!userLogin.LoginPassword.SequenceEqual(request.LoginPassword.ToSha512()))
                        {
                            throw new OAuthException(OAuthError.UnauthorizedRequest, "invalid login credential");
                        }
                        IEnumerable<SqlApiAuthRelation> result = op.QueryByUser(userLogin.ID).Where(x => x.ClientID == request.ClientID);
                        if (result.FirstOrDefault() == null)
                        {
                            auth = new SqlApiAuthRelation()
                            {
                                ClientID = request.ClientID,
                                RedirectUri = null,
                                Scopes = Int64.MaxValue,
                                UserID = userLogin.ID,
                                Requested = true
                            };
                            op.Insert(auth);
                        }
                        else
                        {
                            auth = result.First();
                        }
                        break;
                    }
                default:
                    {
                        throw new OAuthException(OAuthError.UnsupportedResponseType, "grant type:" + request.GrantType);
                        //break;
                    }
            }

            auth = op.Get(auth.ID);
            auth.Requested = true;
            op.Update(auth);
            UserEntity user = uop.Get(auth.UserID);
            TimeSpan expires = (request.RedirectUri == null ? new TimeSpan(14, 0, 0, 0) : new TimeSpan(1, 0, 0));
            return new AccessTokenResponse()
            {
                AccessToken = TokenFactory.IssueAccessToken(auth.UserID, request.ClientID, expires, (OAuthScopes)auth.Scopes, user.UserGroup),
                ExpiresIn = (int)expires.TotalSeconds,
                RefreshToken = ((OAuthScopes)auth.Scopes).HasFlag(OAuthScopes.OfflineAccess) ? TokenFactory.IssueRefreshToken(auth.ID, auth.UserID, auth.ClientID) : null
            };
        }
    }
}
