﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;

namespace Bahen.Common.Tests
{
    [TestClass]
    public class BitStreamTests
    {
        [TestMethod]
        public void BitStreamReadBasics()
        {
            Bahen.Common.BitStream stream = new Bahen.Common.BitStream(new byte[] { 116, 32, 8, 99, 100, 232, 4, 7 });
            byte[] buffer = new byte[3];
            Assert.AreEqual(12, stream.Read(buffer, 2, 12));
            Assert.AreEqual(208, (int)buffer[0]);
            Assert.AreEqual(128, (int)buffer[1]);

            stream.Seek(0, System.IO.SeekOrigin.Begin);
            buffer = new byte[1];
            stream.Read(buffer, 0, 6);
            Assert.AreEqual(116, buffer[0]);
            stream.Read(buffer, 0, 6);
            Assert.AreEqual(8, buffer[0]);

        }

        //[TestMethod]
        //public void BitStreamWriteBasics()
        //{
        //    Bahen.Common.BitStream stream = new Bahen.Common.BitStream(new byte[] { 116, 32, 8, 99, 100, 232, 4, 7});
        //    stream.Seek(2, System.IO.SeekOrigin.Begin);
        //    byte[] buffer = new byte[3] { 18, 225, 9 };
        //    stream.Write(buffer, 1, 9);
        //}
    }

    [TestClass]
    public class Base62Tests
    {
        [TestMethod]
        public void EncodeBasics()
        {
            string s = Bahen.Common.BaseConvert.ToBase62(new byte[] { 116, 32, 8, 99, 100, 232, 4, 7 });
            Assert.AreEqual("T208OsJe107", s);
        }

        [TestMethod]
        public void DecodeBasics()
        {
            byte[] result = Bahen.Common.BaseConvert.FromBase62("T208OsJe107");
            Assert.IsTrue(result.SequenceEqual(new byte[] { 116, 32, 8, 99, 100, 232, 4, 7 }));
            Assert.AreEqual(2, "02".FromBase62()[0]);
        }

        [TestMethod]
        public void Advanced()
        {
            string full = "eyJ0eXAiOiJKV0UiLCJ6aXAiOiJCQVNFNjIiLCJlbmMiOiJBRVMyNTZDQkMtSFMyNTYiLCJpdiI6ImE4RVpaTlZNT0UrQzNNenpkTzE0U0E9PSJ9.6PMZM9D6NA+s6k7ghienB8OrcHetiQ2y3Oir8aQ9OEdk7Lnh3q4z0ypfcTn3bT9he/3zMP/DjMYF4CMcrqg28A==.NPKHS1CnV66TefphncmmQtZfKcGcHcpQlgLfVR7YIBs=";
            byte[] x = Encoding.UTF8.GetBytes(full);
            string y = x.ToBase62();
            byte[] z = y.FromBase62();
            Assert.IsTrue(x.SequenceEqual(z));
        }

        [TestMethod]
        public void Advanced2()
        {
            //    byte[] x = new byte[] { 60 };
            //    string x1 = x.ToBase62();
            //    byte[] x2 = x1.FromBase62();
            string x = "PNbACbfOIMbFQKbuJ6f1QKn3Id1aQKasIMnmL69LD3DMUbPpMJ1MS4vLTpHQRbbvJKPeLKrhHJbGKqevBaPhHr5JTcvNPrHtTbWmK3HMTqPGQNfvOdXYQtfiDZ8pTLX8DMGvHKfJGcCnSb5jUd9sHK8oC6bhD5DwC6TBQMvhLK9fJ3LGMMauPt5GOKyyKnAGK8nKJqybp8hepgdh22tChEeAAcsdBUpDZKmbxqpEwMZg9T";
            x.FromBase62();
        }

        [TestMethod]
        public void Multiple()
        {
            for (int i = 0; i < 1000; i++)
            {
                StringBuilder sb = new StringBuilder();
                int length = new Random().Next(1000) + 5;
                for (int j = 0; j < length; j++)
                {
                    sb.Append((char)new Random().Next(128));
                }
                Assert.IsTrue(Encoding.UTF8.GetBytes(sb.ToString()).SequenceEqual(Encoding.UTF8.GetBytes(sb.ToString()).ToBase62().FromBase62()));
            }
        }

        [TestMethod]
        public void Length()
        {
            byte[] x = new byte[32];
            int lengthx = x.ToBase62().Length;
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = Byte.MaxValue;
            }
            int lengthy = x.ToBase62().Length;
            Assert.AreEqual(lengthx, lengthy);
        }

        [TestMethod]
        public void Hex()
        {
            var f = (Int64.MaxValue - DateTime.Now.Ticks).ToHexString() + Guid.NewGuid().ToString("N");
        }
    }
}
