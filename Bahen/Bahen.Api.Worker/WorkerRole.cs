using Microsoft.Owin.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace Bahen.Api.Worker
{
    public class WorkerRole : RoleEntryPoint
    {
        private IDisposable _webAppHttps = null;
        private IDisposable _webAppHttp = null;

        public override void Run()
        {
            // This is a sample worker implementation. Replace with your logic.
            Trace.TraceInformation("Bahen.Api.Worker entry point called", "Information");

            while (true)
            {
                Thread.Sleep(10000);
                Trace.TraceInformation("Working", "Information");
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            var httpsEndpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["Https"];
            string httpsBaseUri = string.Format("{0}://{1}",
                httpsEndpoint.Protocol, httpsEndpoint.IPEndpoint);

            var httpEndpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["Http"];
            string httpBaseUri = string.Format("{0}://{1}",
                httpEndpoint.Protocol, httpEndpoint.IPEndpoint);

            Trace.TraceInformation(String.Format("Starting OWIN at {0}", httpsBaseUri),
                "Information");
            Trace.TraceInformation(String.Format("Starting OWIN at {0}", httpBaseUri),
                "Information");

            _webAppHttps = WebApp.Start<Startup>(new StartOptions(httpsBaseUri));
            _webAppHttp = WebApp.Start<Startup>(new StartOptions(httpBaseUri));


            return base.OnStart();
        }
    }
}
