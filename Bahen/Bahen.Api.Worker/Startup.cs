﻿using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Bahen.Api.Worker
{
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //var endpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["Https"];
            //string baseUri = string.Format("{0}://{1}",
            //    endpoint.Protocol, endpoint.IPEndpoint);

            //HttpSelfHostConfiguration config = new HttpSelfHostConfiguration(baseUri);
            HttpConfiguration config = new HttpConfiguration();

            // Setup camel case
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter()
            {
                CamelCaseText = true
            });

            // Setup ignoring null value properties
            jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            
            config.Routes.MapHttpRoute(
                "Default",
                "{controller}/{uid}",
                new { uid = RouteParameter.Optional });

            //AttributeRoutingConfig.RegisterRoutes(config);
            //config.Routes.Cast<HttpRoute>().ToArray().LogTo(Console.Out);

            app.UseWebApi(config);
        }
    }
}
