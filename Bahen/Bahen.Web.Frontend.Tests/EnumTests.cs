﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Common;
using Bahen.DataModels;

namespace Bahen.Common.Frontend.Tests
{
    [TestClass]
    public class EnumTests
    {
        [TestMethod]
        public void EnumConvertTests()
        {
            OAuthScopes scopes = OAuthScopes.Email | OAuthScopes.FollowingsEvents | OAuthScopes.UserCycles;
            string s = scopes.ToDescriptionString();
            Assert.AreEqual("email followings_events user_circles", s);

            OAuthScopes back = EnumConvert<OAuthScopes>.ToEnum(s);
            Assert.AreEqual(scopes, back);
        }
    }
}
