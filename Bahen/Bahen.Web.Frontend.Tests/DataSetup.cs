﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.DataOperationLayer;
using System.Configuration;
using Bahen.DataModels;
using Bahen.DataModels;
using Bahen.Common;
using Bahen.DataOperationLayer;
using System.Diagnostics;
using System.Collections.Generic;
using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Win32;
using System.Linq;
using System.Threading;

namespace Bahen.Common.Frontend.Tests
{
    [TestClass]
    public class DataSetup
    {
        [TestMethod]
        public void SetupAuth()
        {
            // Add super admin
            UserEntity developer = new UserEntity()
            {
                FirstName = "Eventub",
                PreferredName = "Eventub",
                LoginEmail = "admin@eventub.com",
                LoginPassword = "123456".ToSha512(),
                UserGroup = UserRoleLevel.SuperAdmin
            };
            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            userOp.Insert(developer);

            // Add website client account
            SqlApiClientOperator clientOp = new SqlApiClientOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString);
            SqlApiClientEntity client = new SqlApiClientEntity()
            {
                Name = "Eventub",
                TrustLevel = (int)ApiClientTrustLevel.Owner,
                DeveloperID = developer.ID,
                ClientSecret = Guid.Empty
            };
            clientOp.Insert(client);

            // Set configuration
            Trace.WriteLine(client.ID.ToString());
            Trace.WriteLine(client.ClientSecret.ToString("N"));
        }

        [TestMethod]
        public void Setup()
        {
            StartAzure();
            CleanTable();
            DropSql();
            CreateSql();
            SetupAuth();
            GenerateTestData();
        }

        public void StartSql()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("net start mssqlserver", "");
            Process startProcess = Process.Start(startInfo);
            startProcess.WaitForExit();
        }

        public void StartAzure()
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows Azure Emulator\");
            object value = key.GetValue("InstallPath");
            string rootPath = value.ToString();
            key.Close();

            ProcessStartInfo startInfo = new ProcessStartInfo(string.Format(@"{0}\Emulator\csrun", rootPath), "/devstore:start /devfabric:start");
            Process startProcess = Process.Start(startInfo);
            startProcess.WaitForExit();
        }

        //[TestMethod]
        //public void atesttest()
        //{
        //    Trace.WriteLine(((Int64.MaxValue - DateTime.Now.Ticks).ToHexString()).Length);
        //    //TableLikeOperator likeOp = new TableLikeOperator(
        //    //    ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
        //    //for (int i = 0; i < 10; i++)
        //    //{
        //    //    TableLikeEntity likeEntity = new TableLikeEntity(i.ToString(), EntityTypeCode.Event, 03);
        //    //    likeOp.Insert(likeEntity);
        //    //}
        //}


        [TestMethod]
        public void DropSql()
        {
            string slnDir = Directory.CreateDirectory(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            string sqlFileDir = string.Format(@"{0}\Bahen.DataModels\DropTables.sql", slnDir);
            FileInfo file = new FileInfo(sqlFileDir);
            string script = file.OpenText().ReadToEnd();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand(script, connection);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                }

            }
        }

        [TestMethod]
        public void CreateSql()
        {
            string slnDir = Directory.CreateDirectory(Environment.CurrentDirectory).Parent.Parent.Parent.FullName;
            string sqlFileDir = string.Format(@"{0}\Bahen.DataModels\CreateTables.sql", slnDir);
            FileInfo file = new FileInfo(sqlFileDir);
            string script = file.OpenText().ReadToEnd();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(script, connection);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (SqlException)
                {
                }
            }
        }
        
        [TestMethod]
        public void CleanTable()
        {
            //DSInit /ForceCreate
            //C:\Windows\System32\cmd.exe /E:ON /V:ON /K "C:\Program Files\Microsoft SDKs\Windows Azure\.NET SDK\2012-10\\bin\setenv.cmd"
            //C:\Program Files\Microsoft SDKs\Windows Azure\Emulator\devstore

            RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows Azure Emulator\");
            object value = key.GetValue("InstallPath");
            string rootPath = value.ToString();
            key.Close();

            ProcessStartInfo shutdownInfo = new ProcessStartInfo(string.Format(@"{0}\Emulator\csrun", rootPath), "/devstore:shutdown");
            Process shutdownProcess = Process.Start(shutdownInfo);
            shutdownProcess.WaitForExit();

            Thread.Sleep(3000);

            ProcessStartInfo resetInfo = new ProcessStartInfo(string.Format(@"{0}\Emulator\devstore\DSInit", rootPath), "/ForceCreate");
            Process resetProcess = Process.Start(resetInfo);
            resetProcess.WaitForExit();

            ProcessStartInfo startInfo = new ProcessStartInfo(string.Format(@"{0}\Emulator\csrun", rootPath), "/devstore:start");
            Process startProcess = Process.Start(startInfo);
            startProcess.WaitForExit();

            
        }

        [TestMethod]
        public void GenerateTestData()
        {

            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            var userList = new List<UserEntity>();
            var firstNames = new string[] {"Steven", "Bob", "John", "Sarah", "Linda", "Smith", "Mike", "Peter", "Selena", "Gary"};
            var preferredNames = new string[] { "Steve", "King", "O'John", "Sarah", "Linda", "Smith", "Mikey", "Pete", "Sele", "Gary" };
            var lastNames = new string[] {"Zhao", "Qian", "Sun", "Li", "Zhou", "Wu", "Zheng", "Wang", "He", "Ren", "Gates", "Jobs", "Kamehameha o 'Iolani"};
            var middleNames = new string[] {"A", "W", "Vango", "DaMao", "BB", "Brian"};
            var streets = new string[] {"Yonge St", "Lawrence Ave", "Steeles Ave", "University St", "Don Mills Rd"};
            var postalCodes = new string[] {"M3W 3N2", "M2W 3P8"};


            int userNum = 1000;
            DateTime start = DateTime.Now;
            for (int i = 0; i < userNum; i++)
            {
                string firstName = firstNames[i % firstNames.Length];
                UserEntity testUser = new UserEntity()
                {
                    FirstName = firstName,
                    PreferredName = preferredNames[i % preferredNames.Length],
                    LoginEmail = String.Format("{0}_{1}@eventub.com", firstName, i),
                    LoginPassword = "123456".ToSha512(),
                    UserGroup = UserRoleLevel.User
                };
                if (i % 2 == 0)
                {
                    testUser.LastName = lastNames[(i / 2) % lastNames.Length];
                }
                if (i % 3 == 0)
                {
                    testUser.MiddleName = middleNames[(i / 3) % middleNames.Length];
                }
                if (i % 4 == 0)
                {
                    testUser.Birthday = new DateTime(100 / (userNum - 1) * i + 1900, 11 / (userNum - 1) * i + 1, 27 / (userNum - 1) * i + 1);
                }
                if (i % 5 == 0)
                {
                    Address address = new Address()
                    {
                        Street = i + streets[(i / 5) % streets.Length],
                        City = "Toronto",
                        Province = "Ontario",
                        Country = "Canada",
                        PostalCode = postalCodes[(i / 5) % postalCodes.Length],
                        Type = UserContactAddressType.Home
                    };
                    testUser.Address.Add(address);
                }
                if (i % 6 == 0)
                {
                    PhoneNumber phoneNumber = new PhoneNumber()
                    {
                        Value = string.Format("416-{0}-{1}", 899 / userNum * i + 100, 8999 / userNum * i + 1000),
                        Type = UserContactPhoneType.Mobile
                    };
                    testUser.PhoneNumbers.Add(phoneNumber);
                }
                userOp.Insert(testUser);
                userList.Add(testUser);
            }

            DateTime end = DateTime.Now;
            Trace.WriteLine("User: " + (end - start).TotalSeconds);

            start = DateTime.Now;
            EventOperator eventOp = new EventOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            var eventList = new List<EventEntity>();
            for (int i = 0; i < 20; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    var eventEntity = new EventEntity()
                    {
                        Name = String.Format("Test Event {0}, {1}", i, j),
                        PublisherID = userList[i].UserID,
                        StartTime = i == j ? new DateTime(2013, 6, 6, i, 0, 0) : new DateTime(2013, 6, 6, Math.Min(i, j), 0, 0),
                        EndTime = i == j ? new DateTime(2013, 6, 6, i, 30, 0) : new DateTime(2013, 6, 6, Math.Max(i, j), 0, 0)
                    };
                    eventOp.Insert(eventEntity);
                    eventList.Add(eventEntity);
                }
            }
            end = DateTime.Now;
            Trace.WriteLine("event: " + (end - start).TotalSeconds);

            // Insert Likes

            start = DateTime.Now;
            TableLikeOperator likeOp = new TableLikeOperator(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            int k = 0;
            int userListCount = userList.Count;
            var eventIDList = eventList.Take(eventList.Count/2).Select(x => x.ID);
            foreach (var id in eventIDList)
            {
                for (int j = 0; j < k; j++)
                {
                    TableLikeEntity likeEntity = new TableLikeEntity(id.ToString(), EntityTypeCode.Event, userList[j].UserID);
                    likeOp.Insert(likeEntity);
                }
                if (k < userListCount)
                {
                    k++;
                }
            }
            DateTime start2 = DateTime.Now;
            //likeOp.SaveChanges();


            
            end = DateTime.Now;
            Trace.WriteLine("savechange: " + (end - start2).TotalSeconds);
            Trace.WriteLine("like: " + (end - start).TotalSeconds);


            // Insert Comment
            TableCommentOperator commentOp = new TableCommentOperator(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            k = 0;
            foreach (var id in eventIDList)
            {
                for (int j = 0; j < k; j++)
                {
                    TableCommentEntity commentEntity = new TableCommentEntity(id.ToString(), EntityTypeCode.Event, userList[j].UserID);
                    commentEntity.Content = userList[j].PreferredName + userList[j].UserID + " has commented";
                    commentOp.Insert(commentEntity);
                }
                if (k < userListCount)
                {
                    k++;
                }
            }
        }
    }
}
