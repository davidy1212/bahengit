﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.DataOperationLayer;
using System.Configuration;
using Bahen.DataModels;
using Bahen.Common;
using System.Collections.Generic;
using Bahen.DataModels;
using Bahen.DataOperationLayer;

namespace Bahen.Common.Frontend.Tests
{
    [TestClass]
    public class PaginationTests
    {
        [TestMethod]
        public void InvitedUserPaginationCreateUsers()
        {
            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);

            List<long> IDs = new List<long>();
            for (int i = 0; i < 200; i++)
            {
                UserEntity testUser = new UserEntity()
                    {
                        FirstName = "Mengye_" + i,
                        LastName = "Ren_" + i,
                        PreferredName = "Mengye Ren " + i,
                        LoginEmail = "renmengye_" + i + "@gmail.com",
                        LoginPassword = "123456".ToSha512()
                    };
                userOp.Insert(testUser);
                IDs.Add(testUser.UserID);
            }

            // Add Attendance
            EventOperator evOp = new EventOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            foreach (long id in IDs)
            {
                evOp.AddAttendance(1, id, AttendanceStatus.Going);
            }

            Console.WriteLine("done");

        }

        [TestMethod]
        public void InsertFriendsTest()
        {
            long userId = 2;

            UserOperator userOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);

            List<long> IDs = new List<long>();
            for (int i = 0; i < 200; i++)
            {
                UserEntity testUser = new UserEntity()
                {
                    FirstName = "Mengye_" + i,
                    LastName = "Ren_" + i,
                    PreferredName = "Mengye Ren " + i,
                    LoginEmail = "renmengye_" + i + "@gmail.com",
                    LoginPassword = "123456".ToSha512()
                };
                userOp.Insert(testUser);
                IDs.Add(testUser.UserID);
            }

            // Insert subscription
            foreach (long id in IDs)
            {
                userOp.AddSubscription(id, userId);
            }
        }

        [TestMethod]
        public void InsertNewsfeedTest()
        {
            TableNewsfeedOperator op = new TableNewsfeedOperator(
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            for (int i = 0; i < 2000; i++)
            {
                op.Insert(
                    new TableNewsfeedEntity(2, 2, true, DateTime.Now.AddDays(-i))
                    {
                        Content = "haha"
                    },
                    false);
            }
            op.SaveChanges();
        }

        [TestMethod]
        public void InsertEventsTest()
        {
            UserOperator uOp = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);


            UserEntity publisher = new UserEntity()
            {
                LoginEmail = "publisher@eventub.com",
                LoginPassword = "123456".ToSha512(),
                PreferredName = "Mengye Ren Publisher"
            };
            uOp.Insert(publisher);

            UserEntity user = new UserEntity()
            {
                 LoginEmail = "renmengye@gmail.com",
                 LoginPassword = "123456".ToSha512(),
                 PreferredName = "Mengye Ren"
            };
            uOp.Insert(user);

            EventOperator evOp = new EventOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);

            List<long> evList = new List<long>();
            for (int i = 0; i < 200; i++)
            {
                EventEntity ev = new EventEntity()
                {
                    Name = "Test Event " + i,
                    StartTime = DateTime.Now.AddDays(i),
                    EndTime = DateTime.Now.AddDays(i).AddHours(i % 10),
                    PublisherID = publisher.ID
                };
                evOp.Insert(ev);
                evList.Add(ev.ID);
                if (i % 3 == 0)
                {
                    evOp.AddAttendance(ev.ID, user.ID, AttendanceStatus.Going);
                }
                else if (i % 3 == 1)
                {
                    evOp.AddAttendance(ev.ID, user.ID, AttendanceStatus.Interested);
                }
                else if (i % 3 == 2)
                {
                    evOp.AddAttendance(ev.ID, user.ID, AttendanceStatus.Invited);
                }
            }

        }
    }
}
