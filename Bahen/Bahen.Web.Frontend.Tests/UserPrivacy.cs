﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.DataModels;
using Bahen.Common;
using Bahen.DataModels;
using Bahen.DataOperationLayer;
using System.Configuration;

namespace Bahen.Common.Frontend.Tests
{
    [TestClass]
    public class UserPrivacy
    {
        [TestMethod]
        public void Basics()
        {
            UserEntity user = new UserEntity()
            {
                LoginEmail = "123@123.com",
                LoginPassword = "123456".ToSha512(),
                FirstName = "oh",
                LastName = "oh",
                PreferredName = "oh oh",
                SubscriptionPolicy = new PrivacyPolicy()
            };

            UserOperator op = new UserOperator(
                ConfigurationManager.ConnectionStrings["SqlTest"].ConnectionString,
                ConfigurationManager.ConnectionStrings["TableTest"].ConnectionString);
            op.Insert(user);
        }
    }
}
