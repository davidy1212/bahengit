﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bahen.Web.Api.Controllers.Utilities;
using Bahen.DataModels;

namespace Bahen.Common.Frontend.Tests
{
    [TestClass]
    public class TokenTest
    {
        [TestMethod]
        public void TokenBasics()
        {
            string a_token = OAuthTokenFactory.IssueAccessToken(
                1, 3, new TimeSpan(1, 0, 0), OAuthScopes.CreateEvent | OAuthScopes.Email, UserRoleLevel.Admin);
            string r_token = OAuthTokenFactory.IssueRefreshToken(
                1, 3, 5);
            string c_token = OAuthTokenFactory.IssuAuthCode(
                1, 3, 5, new TimeSpan(0, 10, 0));

            OAuthAccessToken token2;
            Assert.IsTrue(OAuthTokenFactory.ValidateToken(a_token, out token2));
        }
        [TestMethod]
        public void AnotherToken()
        {
            OAuthAccessToken token2;
            Assert.IsTrue(OAuthTokenFactory.ValidateToken("PNbACbfOIMbFQKbuJ6f1QKn3Id1aQKasIMnmL69LD3DMUbPpMJ1MS4vLTpHQRbbvJKPeLKrhHJbGKqevBaPhHr5JTcvNPrHtTbWmK3HMTqPGQNfvOdXYQtfiDZ8pTLX8DMGvHKfJGcCnSb5jUd9sHK8oC6bhD5DwC6TBQMvhLK9fJ3LGMMauPt5GOKyyKnAGK8nKJqybp8hepgdh22tChEeAAcsdBUpDZKmbxqpEwMZg9T", out token2));
        }
    }
}
