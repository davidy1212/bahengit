﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace Bahen.Common
{
    public static class SecureHash {

        private static SHA512 sha512;
        
        static SecureHash()
        {
            sha512 = new SHA512Managed();
        }

        /// <summary>
        /// Compute a byte hash array for a string (password purpose) using SHA512 algorithm
        /// </summary>
        /// <param name="password">Password</param>
        /// <returns>Byte array form of the hash code</returns>
        public static byte[] ToSha512(this string password)
        {
            return sha512.ComputeHash(password.Select<char, byte>(x => Convert.ToByte(x)).ToArray<byte>());
        }
    }
}
