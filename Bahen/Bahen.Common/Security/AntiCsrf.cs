﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bahen.Common
{
    public static class AntiCsrf
    {
        public static string GetCsrfToken(this HttpResponse response)
        {
            string token = Guid.NewGuid().ToString("N");
            HttpCookie cookie = new HttpCookie("csrftoken_" + new Random().Next(int.MaxValue), token)
            {
                //Secure = true,
                HttpOnly = true,
                Expires = DateTime.Now.AddMinutes(10.0)
            };
            response.Cookies.Add(cookie);
            return cookie.Value;
        }

        public static string GetCsrfHtml(this HttpResponse response)
        {
            string token = GetCsrfToken(response);
            return String.Format("<input type=\"hidden\" name=\"csrftoken\" value=\"{0}\"/>", token);
        }

        public static bool ValidateCsrfToken(HttpRequest request)
        {
            string id = request.Params.Get("csrftoken");
            if (String.IsNullOrEmpty(id))
            {
                id = request.Headers["X-CSRFTOKEN"];
            }
            IEnumerable<string> keys=request.Cookies.Keys.Cast<string>().Where(x => x.StartsWith("csrftoken"));
            foreach (string key in keys)
            {
                HttpCookie cookie = request.Cookies.Get(key);
                if (cookie != null)
                {
                    if (cookie.Value == id)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
