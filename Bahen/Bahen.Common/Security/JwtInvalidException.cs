﻿using System;

namespace Bahen.Common
{
    [Serializable]
    public class JwtInvalidException : Exception
    {
        public JwtInvalidException() { }
        public JwtInvalidException(string message) : base(message) { }
        public JwtInvalidException(string message, Exception inner) : base(message, inner) { }
        protected JwtInvalidException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
