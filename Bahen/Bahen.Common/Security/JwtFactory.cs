﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Bahen.Common
{
    public static class JwtFactory
    {
        private static byte[] key = Guid.Parse("717D6CC2-15DC-465F-AF03-7D5CDF3230CA").ToByteArray();

        public static string Encode(string payload)
        {
            byte[] iv = Guid.NewGuid().ToByteArray();
            return Encode(payload, iv);
        }

        public static string Encode(string payload, byte[] iv)
        {
            string head = "{\"ver\":\"1.0\",\"iv\":\"" + Convert.ToBase64String(iv) + "\"}";
            string head64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(head));
            string payload64 = Convert.ToBase64String(EncryptStringToBytes_Aes(payload, key, iv));

            HMACSHA1 sha = new HMACSHA1(Encoding.UTF8.GetBytes(payload));
            string sig = Convert.ToBase64String(sha.ComputeHash(Encoding.UTF8.GetBytes(head64 + payload64)));
            string full = String.Join(".", head64, payload64, sig);

            return Encoding.UTF8.GetBytes(full).ToBase62();
        }

        public static string Decode(string jwt)
        {
            string full = Encoding.UTF8.GetString(jwt.FromBase62());
            string[] part = full.Split('.');
            if (part.Length != 3)
            {
                throw new JwtInvalidException("corrupted token");
            }
            string head = Encoding.UTF8.GetString(Convert.FromBase64String(part[0]));
            JwtHeader header;
            try
            {
                header = JsonConvert.DeserializeObject<JwtHeader>(head);
            }
            catch
            {
                throw new JwtInvalidException("corrupted token");
            }
            string payload = DecryptStringFromBytes_Aes(Convert.FromBase64String(part[1]), key, Convert.FromBase64String(header.iv));

            string check = Convert.ToBase64String(new HMACSHA1(Encoding.UTF8.GetBytes(payload)).ComputeHash(Encoding.UTF8.GetBytes(part[0] + part[1])));
            if (check != part[2])
            {
                throw new JwtInvalidException("signature does not match");
            }
            return payload;
        }

        public static bool ValidateToken<T>(string token, out T tokenOut) where T : class
        {
            try
            {
                tokenOut = JsonConvert.DeserializeObject<T>(JwtFactory.Decode(token));
                return true;
            }
            catch
            {
                tokenOut = null;
                return false;
            }
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesManaged object 
            // with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.Mode = CipherMode.CBC;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }

        private static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an AesManaged object 
            // with the specified key and IV. 
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;
                aesAlg.Mode = CipherMode.CBC;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream 
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

    }
}
