﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Filters;

namespace Bahen.Common
{
    public class AntiCsrfAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!AntiCsrf.ValidateCsrfToken(HttpContext.Current.Request))
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                actionContext.Response.Content = new StringContent("{\"message\":\"invalid synchronization token\"}", Encoding.UTF8, "application/json");
            }
        }
    }
}