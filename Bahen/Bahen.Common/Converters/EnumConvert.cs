﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Bahen.Common
{
    public static class EnumConvert
    {
        public static string ToDescriptionString(this Enum value)
        {
            FieldInfo[] fi = value.ToString().Split(',').Select(
                x => value.GetType().GetField(x.Trim())).ToArray();

            string[] attributes =
                fi.Select(x => (DescriptionAttribute)x.GetCustomAttribute(typeof(DescriptionAttribute), false) == null ? x.Name :
                    ((DescriptionAttribute)x.GetCustomAttribute(typeof(DescriptionAttribute), false)).Description).ToArray();

            return String.Join(",", attributes);
        }
    }

    public static class EnumConvert<T>
    {
        private static Dictionary<string, T> Dict { get; set; }
        static EnumConvert()
        {
            Dict = new Dictionary<string, T>();
            foreach (T value in Enum.GetValues(typeof(T)))
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    Dict.Add(attributes[0].Description, value);
                }
                else
                {
                    Dict.Add(value.ToString(), value);
                }
            }
        }

        public static T ToEnum(string scopes)
        {
            string[] parts = scopes.Split(',');
            //T result = 0;
            ulong result = 0;
            foreach (string part in parts)
            {
                T scope;
                if (Dict.TryGetValue(part, out scope))
                {
                    result = result | Convert.ToUInt64(scope);
                }
                else
                {
                    throw new FormatException("Format in scopes parameter is not parsable.");
                }
            }
            return (T)Enum.ToObject(typeof(T), result);
        }
    }
}
