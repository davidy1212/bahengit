﻿using System;
using System.Collections.Generic;

namespace Bahen.Common
{
    public static class ByteConvert
    {
        public static byte[] GetBytes(ICollection<long> data)
        {
            byte[] result = new byte[data.Count * 8];
            int offset = 0;
            foreach (long value in data)
            {
                Array.Copy(BitConverter.GetBytes(value), 0, result, offset, 8);
                offset += 8;
            }
            return result;
        }

        public static ICollection<long> GetInt64(byte[] data)
        {
            long[] result = new long[data.Length / 8];
            //byte[] buffer = new byte[8];
            for (int i = 0, j = 0; j < data.Length; i++, j += 8)
            {
                //Array.Copy(data, i * 8, buffer, 0, 8);
                result[i] = BitConverter.ToInt64(data, j);
            }
            return result;
        }
    }
}
