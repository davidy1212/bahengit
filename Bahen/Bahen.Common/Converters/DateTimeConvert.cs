﻿using System;

namespace Bahen.Common
{
    public static class DateTimeConvert
    {
        public static long ToUnixTime(this DateTime dt)
        {
            return (long)(dt - new DateTime(1970, 1, 1)).TotalMilliseconds;
        }

        public static DateTime FromUnixTime(this long dt)
        {
            return new DateTime(1970, 1, 1) + new TimeSpan(dt * 10000);
        }
    }
}
