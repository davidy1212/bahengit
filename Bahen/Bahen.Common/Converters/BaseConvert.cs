﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Bahen.Common
{
    /// <summary>
    /// Static class that can generate hash codes
    /// </summary>
    public static class BaseConvert
    {
        private static string Base62CodingSpace = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        
        /// <summary>
        /// Return the hex string of a given long int
        /// </summary>
        /// <param name="a">Long type integer</param>
        /// <returns>Hex value in string form</returns>
        public static string ToHexString(this long a)
        {
            //return BitConverter.GetBytes(a).Aggregate<byte, StringBuilder>(new StringBuilder(), (sb, v) => sb.Append(v.ToString("X"))).ToString();

            string result = "";
            foreach (var item in BitConverter.GetBytes(a).Reverse())
            {
                result = result + item.ToString("X2");
            }
            return result.ToLowerInvariant();
        }

        public static string ToHexShort(this long a)
        {
            var bytes = BitConverter.GetBytes(a).Reverse();

            string result = "";
            foreach (var item in bytes.SkipWhile(x => x == (byte)0).ToArray())
            {
                result = result + item.ToString("X2");
            }
            return result.ToLowerInvariant();
        }

        public static string ToHexString(this byte[] a)
        {
            string hex = BitConverter.ToString(a);
            hex = hex.Replace("-", "");
            return hex.ToLowerInvariant();
        }

        /// <summary>
        /// Convert a Int64 to Base64
        /// </summary>
        /// <param name="a">Int64 value</param>
        /// <returns>Base64 string</returns>
        public static byte[] GetBytes(this long a)
        {
            var bytes = BitConverter.GetBytes(a).Reverse();
            return bytes.SkipWhile(x => x == (byte)0).ToArray();
        }

        public static long ToInt64(this byte[] a)
        {
            byte[] source = new byte[8];
            Array.Copy(a.Reverse().ToArray(), 0, source, 0, a.Length);
            return BitConverter.ToInt64(source, 0);
        }

        /// <summary>
        /// Convert a Guid to Base64 string
        /// </summary>
        /// <param name="a">Guid value</param>
        /// <returns>Base64 string</returns>
        public static string ToBase64(Guid a)
        {
            return Convert.ToBase64String(a.ToByteArray(), 0, 16).Replace('/', '-');
        }

        /// <summary>
        /// Convert a Base64 string to Int64
        /// </summary>
        /// <param name="base64">Base64 string</param>
        /// <returns>Int64 value</returns>
        public static long FromBase64ToInt64(string base64)
        {
            byte[] source = new byte[8];
            Array.Copy(Convert.FromBase64String(base64).ToArray(), 0, source, 0, Convert.FromBase64String(base64).ToArray().Length);
            return BitConverter.ToInt64(source, 0);
        }

        /// <summary>
        /// Convert a Base64 string to Guid
        /// </summary>
        /// <param name="base64">Base64 string</param>
        /// <returns>Guid value</returns>
        public static Guid FromBase64ToGuid(string base64)
        {
            byte[] source = new byte[16];
            Array.Copy(Convert.FromBase64String(base64).ToArray(), 0, source, 0, 16);
            return new Guid(source);
        }

        /// <summary>
        /// Convert a Base62 string to byte array
        /// </summary>
        /// <param name="base62">Base62 string</param>
        /// <returns>Byte array</returns>
        public static byte[] FromBase62(this string base62)
        {
            // Character count
            int count = 0;

            // Set up the BitStream
            BitStream stream = new BitStream(base62.Length * 6 / 8);

            foreach (char c in base62)
            {
                // Look up coding table
                int index = Base62CodingSpace.IndexOf(c);

                // If end is reached
                if (count == base62.Length - 1)
                {
                    // Check if the ending is good
                    int mod = (int)(stream.Position % 8);
                    stream.Write(new byte[] { (byte)(index << (mod)) }, 0, 8 - mod);
                }
                else
                {
                    // If 60 or 61 then only write 5 bits to the stream, otherwise 6 bits.
                    if (index == 60)
                    {
                        stream.Write(new byte[] { 0xf0 }, 0, 5);
                    }
                    else if (index == 61)
                    {
                        stream.Write(new byte[] { 0xf8 }, 0, 5);
                    }
                    else
                    {
                        stream.Write(new byte[] { (byte)index }, 2, 6);
                    }
                }
                count++;
            }

            // Dump out the bytes
            byte[] result = new byte[stream.Position / 8];
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(result, 0, result.Length * 8);
            return result;
        }

        /// <summary>
        /// Convert a byte array
        /// </summary>
        /// <param name="original">Byte array</param>
        /// <returns>Base62 string</returns>
        public static string ToBase62(this byte[] original)
        {
            StringBuilder sb = new StringBuilder();
            BitStream stream = new BitStream(original);         // Set up the BitStream
            byte[] read = new byte[1];                          // Only read 6-bit at a time
            while (true)
            {
                read[0] = 0;
                int length = stream.Read(read, 0, 6);           // Try to read 6 bits
                if (length == 6)                                // Not reaching the end
                {
                    if ((int)(read[0] >> 3) == 0x1f)            // First 5-bit is 11111
                    {
                        sb.Append(Base62CodingSpace[61]);
                        stream.Seek(-1, SeekOrigin.Current);    // Leave the 6th bit to next group
                    }
                    else if ((int)(read[0] >> 3) == 0x1e)       // First 5-bit is 11110
                    {
                        sb.Append(Base62CodingSpace[60]);
                        stream.Seek(-1, SeekOrigin.Current);
                    }
                    else                                        // Encode 6-bit
                    {
                        sb.Append(Base62CodingSpace[(int)(read[0] >> 2)]);
                    }
                }
                else
                {
                    // Padding 0s to make the last bits to 6 bit
                    sb.Append(Base62CodingSpace[(int)(read[0] >> (int)(8 - length))]);
                    break;
                }
            }
            return sb.ToString();
        }
    }
}
