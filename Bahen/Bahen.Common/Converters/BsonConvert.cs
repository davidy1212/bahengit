﻿using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.IO;

namespace Bahen.Common
{
    public static class BsonConvert<T>
    {
        /// <summary>
        /// Read T in Bson format byte array
        /// </summary>
        /// <param name="value">T Bson byte array</param>
        /// <returns>A deserialized T</returns>
        public static T Deserialize(byte[] value)
        {
            MemoryStream ms = new MemoryStream(value);
            BsonReader reader = new BsonReader(ms);
            JsonSerializer serializer = new JsonSerializer();
            return serializer.Deserialize<T>(reader);
        }
        
        /// <summary>
        /// Serialize T into Bson byte array
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Serialized Bson byte array</returns>
        public static byte[] Serialize(T value)
        {
            MemoryStream ms = new MemoryStream();
            JsonSerializer serializer = new JsonSerializer();
            BsonWriter writer = new BsonWriter(ms);
            serializer.Serialize(writer, value);

            return ms.ToArray();
        }
    }
}
