﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Bahen.Common
{
    /// <summary>
    /// Static utility class that provide sql functions for non-entity-framework operations
    /// </summary>
    public static class SqlUtilities
    {
        public static SqlCommand GetCommand(SqlConnection connection, string command)
        {
            SqlCommand sqlCommand = new SqlCommand(command, connection);
            return sqlCommand;
        }

        public static SqlParameter AddParameter(SqlCommand command, string param, SqlDbType type, object value)
        {
            SqlParameter sqlParam = new SqlParameter("@" + param, type);
            sqlParam.Value = value;
            command.Parameters.Add(sqlParam);
            return null;
        }

        public static SqlConnection GetConnection(string connectionString)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
            return connection;
        }
    }
}
