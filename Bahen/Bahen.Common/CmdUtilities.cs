﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Common
{
    public static class CmdUtilities
    {
        public static void RunCommand(string filePath, string args)
        {
            var start = new ProcessStartInfo()
            {
                Arguments = args,
                FileName = filePath
            };
            var proc = new Process()
            {
                StartInfo = start
            };
            proc.Start();
            proc.WaitForExit();
        }
    }
}
