﻿using System.Text.RegularExpressions;

namespace Bahen.Common
{
    public class RichTextValidator
    {
        public string Filter(string original)
        {
            string acceptable = "b|strong|i|em|u|a|p|sup|sub|div|img|span|font|br|ul|ol|li";
            string stringPattern = @"</?(?(?=" + acceptable + @")notag|[a-zA-Z0-9]+)(?:\s[a-zA-Z0-9\-]+=?(?:(["",']?).*?\1?)?)*\s*/?>";
            return Regex.Replace(original, stringPattern, "sausage");
        }
    }
}
