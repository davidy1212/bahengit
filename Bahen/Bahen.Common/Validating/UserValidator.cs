﻿using System.Text.RegularExpressions;

namespace Bahen.Common
{
    /// <summary>
    /// Validate properties of a User
    /// </summary>
    public static class UserValidators
    {
        public static bool UserNameValidator(string name)
        {
            if (name.Length > 0 && name.Length < 30)
            {
                string regPattern = @"^[a-z0-9_\-\./^&*()+=#!~]{3,30}$";
                RegexOptions regOptions = RegexOptions.IgnoreCase;
                Regex passRegex = new Regex(regPattern, regOptions);

                return passRegex.IsMatch(name);
            }
            else
            {
                return false;
            }
        }

        public static bool UserPasswordValidator(string pass1, string pass2)
        {
            if (pass1 != pass2)
            {
                return false;
            }
            else
            {
                string regPattern = @"^[a-z0-9_-]{6,20}$";
                RegexOptions regOptions = RegexOptions.IgnoreCase;
                Regex passRegex = new Regex(regPattern, regOptions);

                return passRegex.IsMatch(pass1);
            }
        }

        public static bool UserEmailValidator(string loginEmail)
        {
            if (loginEmail.Length > 100)
            {
                return false;
            }
            else
            {
                string regPattern = @"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$";
                RegexOptions regOptions = RegexOptions.IgnoreCase;
                Regex emailRegex = new Regex(regPattern, regOptions);

                return emailRegex.IsMatch(loginEmail);
            }
        }
    }
}
