use Griddy;

create unique index Users_ID on Users (UserID)
create unique index Events_ID on Events (EventID)
create index Events_StartTime on Events (PublisherID StartTime desc)
create unique index Cycles_ID on Cycles (CycleID)
create index Attendances_User on Attendances (UserID)
create index Attendances_Event on Attendances (EventID)
create index CycleEvents_Cycle on CycleEvents (CycleID)
create index CycleEvents_Event on CycleEvents (EventID)
create index CycleUsers_Cycle on CycleUsers (CycleID)
create index CycleUsers_User on CycleUsers (UserID)
create index SubscriptionsHost on Subscriptions (HostID)
create index SubscriptionsSubscriber on Subscriptions (SubscriberID)