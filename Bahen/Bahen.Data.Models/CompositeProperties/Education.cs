﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// An education data structure used in runtime.
    /// Will be serialized into Json string and saved into nosql table.
    /// </summary>
    public struct Education
    {
        /// <summary>
        /// Cycle of the education place
        /// </summary>
        public long CycleID { get; set; }

        /// <summary>
        /// Display name of the education place (user customizable)
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Type of education place (e.g. high school, university, etc.)
        /// </summary>
        public UserEducationType Type { get; set; }

        /// <summary>
        /// Program of study (only application)
        /// </summary>
        public string Program { get; set; }

        /// <summary>
        /// Graduation year (e.g. Class of 2011)
        /// </summary>
        public int GraduationYear { get; set; }

        /// <summary>
        /// City of the education place
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Province of the education place
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Country of the education place
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Start date of the education
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// End date of the education
        /// </summary>
        public DateTime DateTo { get; set; }

        ///// <summary>
        ///// Visibility of the education infomation
        ///// </summary>
        //public UserProfileVisibilityLevel PrivacyLevel { get; set; }
    }
}
