﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public class RecurringExceptions
    {
        public ICollection<DateTime> Added { get; set; }
        public ICollection<DateTime> Removed { get; set; }
    }
}
