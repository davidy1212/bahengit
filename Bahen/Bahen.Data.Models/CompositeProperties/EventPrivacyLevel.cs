﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represents the privacy level of the visibility and attendance restriction of an event
    /// </summary>
    public class EventPrivacyLevel
    {
        public EventPrivacyLevel()
        {
            this.CustomAllowedCycleIDs = new List<long>();
            this.CustomAllowedUserIDs = new List<long>();
            this.CustomBlockedCycleIDs = new List<long>();
            this.CustomBlockedUserIDs = new List<long>();
        }

        /// <summary>
        /// Allow everyone to access
        /// </summary>
        public bool AllowEveryone { get; set; }

        /// <summary>
        /// Allow all subscribers to the publishers to access
        /// </summary>
        public bool AllowSubscribers { get; set; }

        /// <summary>
        /// Allow all the hosts that the publisher subscribes to
        /// </summary>
        public bool AllowSubscribedHost { get; set; }

        /// <summary>
        /// Allow all the members of the cycles that the publisher joins
        /// </summary>
        public bool AllowCycleMembers { get; set; }

        /// <summary>
        /// Allow only the publisher to access
        /// </summary>
        public bool AllowOnlyPublisher { get; set; }

        /// <summary>
        /// Special allowed cycle IDs (override AllowCycleMembers)
        /// </summary>
        public ICollection<long> CustomAllowedCycleIDs { get; set; }

        /// <summary>
        /// Special blocked cycle IDs (override AllowCycleMembers)
        /// </summary>
        public ICollection<long> CustomBlockedCycleIDs { get; set; }

        /// <summary>
        /// Special allowed user IDs (override bool properties)
        /// </summary>
        public ICollection<long> CustomAllowedUserIDs { get; set; }

        /// <summary>
        /// Special blocked user IDs (override bool properties)
        /// </summary>
        public ICollection<long> CustomBlockedUserIDs { get; set; }
    }
}
