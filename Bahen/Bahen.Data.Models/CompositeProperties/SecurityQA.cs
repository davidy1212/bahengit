﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public struct SecurityQA
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
