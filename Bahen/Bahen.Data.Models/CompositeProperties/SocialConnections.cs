﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public struct SocialConnections
    {
        public string FacebookID { get; set; }
        public string TwitterID { get; set; }
        public string WeiboID { get; set; }
        public string RenrenID { get; set; }
    }
}
