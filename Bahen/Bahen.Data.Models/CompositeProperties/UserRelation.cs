﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models.CompositePropoties
{
    /// <summary>
    /// Describes the relation between two users
    /// </summary>
    public class UserRelation
    {
        /// <summary>
        /// Construct new user relation object
        /// </summary>
        public UserRelation()
        {
            this.CommonHostIDs = new List<long>();
            this.CommonCycleIDs = new List<long>();
        }

        /// <summary>
        /// The first user is the host and the second user is the first's subscriber
        /// </summary>
        public bool IsHostSubsriber { get; set; }

        /// <summary>
        /// The first user is the subsriber of the second user
        /// </summary>
        public bool IsSubscriberHost { get; set; }

        /// <summary>
        /// Both users subscribe to each other
        /// </summary>
        public bool IsMutuallySubscribed
        {
            get
            {
                return IsHostSubsriber && IsSubscriberHost;
            }
        }

        /// <summary>
        /// The first user and the second user are the same user
        /// </summary>
        public bool IsSelf { get; set; }

        /// <summary>
        /// The common hosts subscribed by both first and second user
        /// </summary>
        public ICollection<long> CommonHostIDs { get; set; }

        /// <summary>
        /// The common cycles joined by both first and second user
        /// </summary>
        public ICollection<long> CommonCycleIDs { get; set; }

        /// <summary>
        /// There is no explicit relation between the first and second user
        /// </summary>
        public bool IsNone
        {
            get
            {
                return
                    !this.IsHostSubsriber &&
                    !this.IsSubscriberHost &&
                    !this.IsSelf &&
                    this.CommonHostIDs.Count == 0 &&
                    this.CommonCycleIDs.Count == 0;
            }
        }
    }
}
