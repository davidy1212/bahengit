﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// A workplace data structure used in runtime.
    /// Will be serialized into Json string and saved into nosql table.
    /// </summary>
    public struct WorkPlace
    {
        /// <summary>
        /// Cycle of the work place
        /// </summary>
        public long CycleID { get; set; }

        /// <summary>
        /// Display name of the work place (user customizable)
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Job title
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Province name
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Country name
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Start date of this job
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// End date of this job
        /// </summary>
        public DateTime DateTo { get; set; }

        ///// <summary>
        ///// Visibility of this work information to other users
        ///// </summary>
        //public UserProfileVisibilityLevel PrivacyLevel { get; set; }
    }
}
