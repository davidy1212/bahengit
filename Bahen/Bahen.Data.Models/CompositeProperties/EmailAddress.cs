﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Bahen.Data.Models
{
    /// <summary>
    /// An email address data structure used in runtime.
    /// Will be serialized into Json string and saved into nosql table.
    /// </summary>
    public struct EmailAddress
    {
        private UserContactEmailType type;
        private string typeString;

        /// <summary>
        /// Value of the email address
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Type of the email address (e.g. Home, work, fax, etc.)
        /// </summary>
        public UserContactEmailType Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
                if (value != UserContactEmailType.Custom)
                {
                    this.typeString = value.ToString();
                }
            }
        }

        /// <summary>
        /// Type of the email address in string (for Custom type cases)
        /// </summary>
        public string TypeString
        {
            get
            {
                return this.typeString;
            }
            set
            {
                this.typeString = value;
                UserContactEmailType myType;
                if (Enum.TryParse<UserContactEmailType>(value, true, out myType))
                {
                    this.type = myType;
                }
                else
                {
                    this.type = UserContactEmailType.Custom;
                }
            }
        }

        ///// <summary>
        ///// Visibility of the email address
        ///// </summary>
        //public UserProfileVisibilityLevel PrivacyLevel { get; set; }

        /// <summary>
        /// Whether the email address has been confirmed
        /// </summary>
        public bool IsConfirmed { get; set; }

        /// <summary>
        /// Whether the email address is used to reset password
        /// </summary>
        public bool IsSecurityEmail { get; set; }
    }
}
