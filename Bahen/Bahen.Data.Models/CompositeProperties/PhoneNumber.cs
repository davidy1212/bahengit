﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Bahen.Data.Models
{
    /// <summary>
    /// A phone number data structure used in runtime.
    /// Will be serialized into Json string and saved into nosql table.
    /// </summary>
    public struct PhoneNumber
    {
        private UserContactPhoneType type;
        private string typeString;

        /// <summary>
        /// Value of the phone number (e.g. 444-444-4444)
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Type of the phone number (e.g. Home, work, fax, etc.)
        /// </summary>
        public UserContactPhoneType Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
                if (value != UserContactPhoneType.Custom)
                {
                    this.typeString = value.ToString();
                }
            }
        }

        /// <summary>
        /// Type of the phone number in string (for Custom type cases)
        /// </summary>
        public string TypeString
        {
            get
            {
                return this.typeString;
            }
            set
            {
                this.typeString = value;
                UserContactPhoneType myType;
                if (Enum.TryParse<UserContactPhoneType>(value, true, out myType))
                {
                    this.type = myType;
                }
                else
                {
                    this.type = UserContactPhoneType.Custom;
                }
            }
        }

        ///// <summary>
        ///// Visibility of the phone number
        ///// </summary>
        //public UserProfileVisibilityLevel PrivacyLevel { get; set; }

        /// <summary>
        /// Try to read a phone number struct serialized in Json format
        /// </summary>
        /// <param name="value">A phone number struct serialized in Json format</param>
        /// <param name="result">Deserialized phone number struct</param>
        /// <returns>Whether the read was successful, if failed then result is an empty struct</returns>
        public static bool TryReadJson(string value, out PhoneNumber result)
        {
            try
            {
                result = ReadJson(value);
            }
            catch (JsonReaderException ex)
            {
                Trace.WriteLine(ex.ToString());
                result = new PhoneNumber();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Try to read a collection of phone number structs serialized in Json format
        /// </summary>
        /// <param name="value">A collection of phone number structs serialized in Json format</param>
        /// <param name="results">A collection of phone number structs</param>
        /// <returns>Whether the read was successful, if failed then results is an empty collection</returns>
        public static bool TryReadJsonBatch(string value, out ICollection<PhoneNumber> results)
        {
            try
            {
                results = ReadJsonBatch(value);
            }
            catch (JsonReaderException ex)
            {
                Trace.WriteLine(ex.ToString());
                results = new List<PhoneNumber>();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Read a phone number struct in Json format
        /// </summary>
        /// <param name="value">A phone number struct Json string</param>
        /// <returns>A deserialized phone number struct</returns>
        public static PhoneNumber ReadJson(string value)
        {
            return JsonConvert.DeserializeObject<PhoneNumber>(value);
        }

        /// <summary>
        /// Read a collection of phone number structs in Json format
        /// </summary>
        /// <param name="value">A collection of phone number structs in Json format</param>
        /// <returns>A deserialized collection of phone number structs</returns>
        public static ICollection<PhoneNumber> ReadJsonBatch(string value)
        {
            return JsonConvert.DeserializeObject<ICollection<PhoneNumber>>(value);
        }

        /// <summary>
        /// Serialize the phone number struct into Json string
        /// </summary>
        /// <returns>Serialized Json string</returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Serialize a collection of phone number structs into Json string
        /// </summary>
        /// <param name="values">A collection of phone number structs</param>
        /// <returns>Serialized Json string</returns>
        public static string ToStringBatch(IEnumerable<PhoneNumber> values)
        {
            return JsonConvert.SerializeObject(values);
        }
    }
}
