﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bahen.Common;

namespace Bahen.Data.Models
{
    /// <summary>
    /// General privacy definition for both allowed and blocked collections
    /// </summary>
    public class PrivacyGroup
    {
        public IEnumerable<long> Users { get; set; }
        public IEnumerable<long> Cycles { get; set; }
        public Relation Relations { get; set; }
        public int Priority { get; set; }

        public PrivacyGroup()
        {
            this.Users = new List<long>();
            this.Cycles = new List<long>();
            this.Relations = Relation.None;
            this.Priority = 0;
        }

        public PrivacyGroup(int priority)
        {
            this.Users = new List<long>();
            this.Cycles = new List<long>();
            this.Relations = Relation.None;
            this.Priority = priority;
        }

        public virtual byte[] Serialize()
        {
            List<byte> result = new List<byte>();

            result.AddRange(BitConverter.GetBytes(this.Users.Count()));
            foreach (long user in this.Users)
            {
                result.AddRange(BitConverter.GetBytes(user));
            }

            result.AddRange(BitConverter.GetBytes(this.Cycles.Count()));
            foreach (long cycle in this.Cycles)
            {
                result.AddRange(BitConverter.GetBytes(cycle));
            }

            result.AddRange(BitConverter.GetBytes((int)this.Relations));
            return result.ToArray();
        }

        protected static List<long> DeserializePartLong(byte[] data)
        {
            if (data == null) return new List<long>();
            int size = BitConverter.ToInt32(data, 0);
            List<long> result = new List<long>();
            for (int i = sizeof(int); i < size * sizeof(long) + sizeof(int); i = i + sizeof(long))
            {
                result.Add(BitConverter.ToInt64(data, i));
            }
            return result;
        }

        public static PrivacyGroup Deserialize(byte[] data, out byte[] remain)
        {
            if (data == null)
            {
                remain = null;
                return new PrivacyGroup();
            }
            List<long> users = DeserializePartLong(data);
            byte[] data1 = data.Skip(users.Count * sizeof(long) + sizeof(int)).ToArray();
            List<long> cycles = DeserializePartLong(data1);
            byte[] data2 = data1.Skip(cycles.Count * sizeof(long) + sizeof(int)).ToArray();
            Relation relations = (Relation)BitConverter.ToInt32(data2, 0);
            PrivacyGroup definition = new PrivacyGroup()
            {
                Users = users,
                Cycles = cycles,
                Relations = relations
            };
            remain = data2.Skip(sizeof(int)).ToArray();
            return definition;
        }
    }

    /// <summary>
    /// A policy class that defines in general the permission of performing operations on website
    /// </summary>
    public class PrivacyPolicy
    {
        /// <summary>
        /// Block collection (highest priority)
        /// </summary>
        public PrivacyGroup Blocked { get; set; }

        /// <summary>
        /// Collection that are allowed upon request (medium priority)
        /// </summary>
        public PrivacyGroup UponRequest { get; set; }

        /// <summary>
        /// Allowed collection (lowest priority)
        /// </summary>
        public PrivacyGroup Allowed { get; set; }

        public PrivacyPolicy() : this(true) { }

        public PrivacyPolicy(bool initPublic)
        {
            this.Blocked = new PrivacyGroup(10);
            this.UponRequest = new PrivacyGroup(5);
            this.Allowed = new PrivacyGroup(0);
            this.Allowed.Relations = Relation.Public;
        }

        public byte[] Serialize()
        {
            return Blocked.Serialize().Concat(UponRequest.Serialize()).Concat(Allowed.Serialize()).ToArray();
        }

        public static PrivacyPolicy Deserialize(byte[] data)
        {
            byte[] data1, data2, data3;
            PrivacyPolicy result = new PrivacyPolicy();
            result.Blocked = PrivacyGroup.Deserialize(data, out data1);
            result.Blocked.Priority = 10;
            result.UponRequest = PrivacyGroup.Deserialize(data1, out data2);
            result.UponRequest.Priority = 5;
            result.Allowed = PrivacyGroup.Deserialize(data2, out data3);
            result.Allowed.Priority = 0;
            return result;
        }
    }

    [Flags]
    public enum Relation
    {
        None = 0,
        Public = 1,
        Followers = 2,
        Invited = 4,
        Member = 8,
        Administrator = 16
    }
}
