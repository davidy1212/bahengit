﻿using Bahen.Data.Models;
using Bahen.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// An address data structure used in runtime.
    /// Will be serialized into Json string and saved in nosql table.
    /// </summary>
    public class Address
    {
        private UserContactAddressType type;
        private string typeString;

        /// <summary>
        /// Venue name of the place
        /// </summary>
        public string Venue { get; set; }

        /// <summary>
        /// Street name plus the room number
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// City/town name
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Province/state name
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Country name
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Postal code of the address
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Type of the address (e.g. Home, work, fax, etc.)
        /// </summary>
        public UserContactAddressType Type
        {
            get
            {
                return this.type;
            }
            set
            {
                this.type = value;
                if (value != UserContactAddressType.Custom)
                {
                    this.typeString = value.ToString();
                }
            }
        }

        /// <summary>
        /// Type of the address in string (for Custom type cases)
        /// </summary>
        public string TypeString
        {
            get
            {
                return this.typeString;
            }
            set
            {
                this.typeString = value;
                UserContactAddressType myType;
                if (Enum.TryParse<UserContactAddressType>(value, true, out myType))
                {
                    this.type = myType;
                }
                else
                {
                    this.type = UserContactAddressType.Custom;
                }
            }
        }

        ///// <summary>
        ///// Visibility of the address
        ///// </summary>
        //public UserProfileVisibilityLevel PrivacyLevel { get; set; }

        /// <summary>
        /// Return the address string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(this.Venue);
            builder.Append(" ");
            builder.Append(this.Street);
            builder.Append(" ");
            builder.Append(this.City);

            return builder.ToString();
        }
    }
}
