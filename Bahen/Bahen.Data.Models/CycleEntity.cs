﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Search.Indexing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Contains all the information of a cycle
    /// </summary>
    public class CycleEntity : IFullEntity<SqlCycleEntity, TableCycleEntity>, IIndexable
    {
        /// <summary>
        /// Construct a new cycle full entity by initializing two parts of the entity
        /// </summary>
        public CycleEntity()
        {
            this.SqlEntity = new SqlCycleEntity();
            this.TableEntity = new TableCycleEntity();
        }

        /// <summary>
        /// Assemble a cycle full entity by passing the sql and table entity of the cycle (has to be same ID)
        /// </summary>
        /// <param name="sqlEntity">SqlCycleEntity instance</param>
        /// <param name="tableEntity">TableCycleEntity instance</param>
        /// <exception cref="ArgumentException">IDs of the sql and table entities do not match</exception>
        public CycleEntity(SqlCycleEntity sqlEntity, TableCycleEntity tableEntity)
        {
            if (sqlEntity.ID != tableEntity.ID)
            {
                throw new ArgumentException(
                    String.Format("Cannot assemble SqlCycleEntity ID: {0} and TableCycleEntity ID: {1} because of different IDs.",
                    sqlEntity.ID, tableEntity.ID)
                    );
            }
            this.SqlEntity = sqlEntity;
            this.TableEntity = tableEntity;
        }

        /// <summary>
        /// Sql part of the entity
        /// </summary>
        public SqlCycleEntity SqlEntity { get; private set; }

        /// <summary>
        /// Table part of the entity
        /// </summary>
        public TableCycleEntity TableEntity { get; private set; }

        /// <summary>
        /// Cycle ID
        /// </summary>
        public long CycleID
        {
            get
            {
                this.TableEntity.CycleID = this.SqlEntity.ID;
                return this.SqlEntity.ID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int TypeCode
        {
            get
            {
                return (int)EntityTypeCode.Cycle;
            }
        }

        /// <summary>
        /// Cycle ID
        /// </summary>
        public long ID
        {
            get
            {
                return this.CycleID;
            }
        }

        public string UniversalID
        {
            get
            {
                return EntityTypeCode.Cycle.ToString() + this.ID.ToString();
            }
        }

        /// <summary>
        /// Cycle name
        /// </summary>
        public string Name
        {
            get
            {
                this.TableEntity.Name = this.SqlEntity.Name;
                return this.SqlEntity.Name;
            }
            set
            {
                if (value == null) throw new ArgumentException("Cycle name cannot be set to null");
                this.TableEntity.Name = value;
                this.SqlEntity.Name = value;
            }
        }

        /// <summary>
        /// Cycle page url
        /// </summary>
        public string Alias
        {
            get
            {
                this.TableEntity.Alias = this.SqlEntity.Alias;
                return this.SqlEntity.Alias;
            }
            set
            {
                if (value == null) throw new ArgumentException("Cycle page url cannot be set to null");
                this.TableEntity.Alias = value;
            }
        }

        /// <summary>
        /// Cycle picture url
        /// </summary>
        public string Picture
        {
            get
            {
                this.TableEntity.Picture = this.SqlEntity.Picture;
                return this.SqlEntity.Picture;
            }
            set
            {
                this.TableEntity.Picture = value;
                this.SqlEntity.Picture = value;
            }
        }

        /// <summary>
        /// A short description to the cycle
        /// </summary>
        public string DescriptionAbstract
        {
            get
            {
                return this.TableEntity.DescriptionAbstract;
            }
            set
            {
                this.TableEntity.DescriptionAbstract = value;
            }
        }

        /// <summary>
        /// A long description in rich text stored in blob
        /// </summary>
        public string DescriptionBlobUrl
        {
            get
            {
                return this.TableEntity.DescriptionBlobUrl;
            }
            set
            {
                this.TableEntity.DescriptionBlobUrl = value;
            }
        }

        private ICollection<CycleCategory> categories;

        /// <summary>
        /// Categories of the cycle
        /// </summary>
        public ICollection<CycleCategory> Categories
        {
            get
            {
                if (this.categories == null)
                {
                    try
                    {
                        this.categories = JsonConvert.DeserializeObject<ICollection<CycleCategory>>(this.TableEntity.Categories);
                    }
                    catch
                    {
                        this.categories = new List<CycleCategory>();
                    }
                }
                return this.categories;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Categories cannot be assigned to null");
                }
                this.categories = value;
                this.TableEntity.Categories = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Keywords that describes the cycle
        /// </summary>
        public ICollection<string> SearchKeywords
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<ICollection<string>>(this.TableEntity.SearchKeywords);
                }
                catch
                {
                    return new List<string>();
                }
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Keywords cannot be assigned to null");
                }
                this.TableEntity.SearchKeywords = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Country or region of the cycle
        /// </summary>
        public CountryRegion Region
        {
            get
            {
                return (CountryRegion)this.TableEntity.Region;
            }
            set
            {
                this.TableEntity.Region = (int)value;
            }
        }

        ///// <summary>
        ///// Ways to restrict people from joining the cycle
        ///// </summary>
        //public MembershipRestrictionLevel MembershipRestrictionLevel
        //{
        //    get
        //    {
        //        return (MembershipRestrictionLevel)this.TableEntity.MembershipRestrictionLevel;
        //    }
        //    set
        //    {
        //        this.TableEntity.MembershipRestrictionLevel = (int)value;
        //    }
        //}

        public string Type
        {
            get { return "cycle"; }
        }

        public IEnumerable<IndexString> IndexStrings
        {
            get
            {
                List<IndexString> result = new List<IndexString>();
                result.Add(new IndexString("id", this.ID.ToString()));
                result.Add(new IndexString("name", this.Name));
                if (String.IsNullOrEmpty(this.DescriptionAbstract.ToString())) { result.Add(new IndexString("description", this.DescriptionAbstract.ToString())); }
                return result;
            }
        }
    }
}
