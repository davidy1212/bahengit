﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public interface IGuidEntity
    {
        Guid ID { get; }
    }
}
