﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public class TableEntityCount : TableServiceEntity
    {
        public TableEntityCount() { }

        public TableEntityCount(String targetString, EntityTypeCode targetTypeCode)
        {
            this.PartitionKey = ComputePartitionKey(targetString, targetTypeCode);
            this.RowKey = targetTypeCode + targetString;
        }

        public static string ComputePartitionKey(String targetString, EntityTypeCode targetTypeCode)
        {
            return TableUserOpinionEntity.ComputePartitionKey(targetString, targetTypeCode);
        }

        public static string ComputeRowKey(String targetString, EntityTypeCode targetTypeCode)
        {
            return targetTypeCode + targetString;
        }

        public int Count { get; set; }
    }
}
