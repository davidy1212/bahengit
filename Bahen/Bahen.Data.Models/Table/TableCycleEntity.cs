﻿using Bahen.Common;
using Microsoft.WindowsAzure.Storage.Table.DataServices;

using System;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for cycle object in Azure Table Datastorage (NoSql)
    /// </summary>
    public class TableCycleEntity : TableServiceEntity, IInt64Id
    {
        private long cycleID;

        /// <summary>
        /// Cycle ID
        /// </summary>
        public long CycleID
        {
            get
            {
                return this.cycleID;
            }
            set
            {
                this.cycleID = value;

                // Use the first 48 bit of the id as partition key
                string hex = BaseConvert.ToHexString(value);
                this.PartitionKey = hex.Substring(0, 12);

                // Use the last 16 bit of the id as row key (each partition contains 65535=2^16 cycles)
                this.RowKey = hex.Substring(12, 4);
            }
        }

        /// <summary>
        /// Cycle ID
        /// </summary>
        public long ID
        {
            get
            {
                return this.CycleID;
            }
        }

        /// <summary>
        /// Cycle name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Cycle page url
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Cycle picture url
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Cycle short description
        /// </summary>
        public string DescriptionAbstract { get; set; }

        /// <summary>
        /// Cycle rich text description
        /// </summary>
        public string DescriptionBlobUrl { get; set; }

        /// <summary>
        /// Last update time
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Cycle categories
        /// </summary>
        public string Categories { get; set; }

        /// <summary>
        /// List of search keywords in Json
        /// </summary>
        public string SearchKeywords { get; set; }

        /// <summary>
        /// Region of the cycle (refer to CountryRegion enum)
        /// </summary>
        public int Region { get; set; }

        /// <summary>
        /// Restriction method from poeple joining the cycle (refer to MembershipRestrictionLevel)
        /// </summary>
        public int MembershipRestrictionLevel { get; set; }

        /// <summary>
        /// Compute the partition key of cycle entity based on Cycle ID
        /// </summary>
        /// <param name="ID">Cycle ID</param>
        /// <returns>Partition key</returns>
        public static string ComputePartitionKey(long ID)
        {
            return BaseConvert.ToHexString(ID).Substring(0, 12);
        }

        /// <summary>
        /// Compute the row key of cycle entity based on Cycle ID
        /// </summary>
        /// <param name="ID">Cycle ID</param>
        /// <returns>Row key</returns>
        public static string ComputeRowKey(long ID)
        {
            return BaseConvert.ToHexString(ID).Substring(12, 4);
        }
    }
}
