﻿using Bahen.Common;
using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for event object in Azure Table Datastorage (NoSql)
    /// </summary>
    public class TableEventEntity : TableServiceEntity, IInt64Id
    {
        #region Identification
        /// <summary>
        /// Event ID
        /// </summary>
        public long ID
        {
            get { return this.EventID; }
        }

        /// <summary>
        /// Event ID
        /// </summary>
        public long EventID { get; set; }

        /// <summary>
        /// Event host id (publisher id)
        /// </summary>
        public long PublisherID { get; set; }

        /// <summary>
        /// Rowkey of the entity
        /// </summary>
        public override string RowKey
        {
            get
            {
                base.RowKey = ComputeRowKey(this.EventID);
                return base.RowKey;
            }
            set
            {
                base.RowKey = value;
            }
        }

        /// <summary>
        /// Partition key of the entity
        /// </summary>
        public override string PartitionKey
        {
            get
            {
                base.PartitionKey = ComputePartitionKey(this.PublisherID);
                return base.PartitionKey;
            }
            set
            {
                base.PartitionKey = value;
            }
        }

        /// <summary>
        /// Time when this entity is last updated
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Time when this entity is created
        /// </summary>
        public DateTime TimeCreated { get; set; }

        /// <summary>
        /// Name of the event
        /// </summary>
        public string Name { get; set; }
        #endregion

        #region Location

        /// <summary>
        /// Address Json of the event.  Refer to enum Address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Latitude of the place
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitude of the place
        /// </summary>
        public double Longitude { get; set; }
        #endregion

        #region Time
        /// <summary>
        /// Start time of the event (only precise to Day if IsDayEvent is true)
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// End time of the event (only precise to Day if IsDayEvent is true)
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Whether the event will repeat
        /// </summary>
        public bool IsRecurring
        {
            get
            {
                return (EventRecurringType)this.RecurringFrequency != EventRecurringType.Once;
            }
        }

        /// <summary>
        /// Whether the event will last for the whole day
        /// </summary>
        public bool IsDayEvent { get; set; }

        /// <summary>
        /// Frequency of the recurring event.  
        /// Refer to Enum RecurringType for more specification.
        /// TODO how to store the first Saturday every month? first Saturday of October every year?
        /// </summary>
        public int RecurringFrequency { get; set; }

        private int recurringTimes;

        /// <summary>
        /// Number of recurring event scheduled.  
        /// 0 represents this recurring event has no end date.  
        /// If sets a negative number will throw exception.
        /// </summary>
        public int RecurringTimes
        {
            get
            {
                return recurringTimes;
            }
            set
            {
                if (value >= 0)
                {
                    this.recurringTimes = value;
                }
                else
                {
                    throw new ArgumentException("RecurringTimes property cannot be negative!");
                }
            }
        }

        /// <summary>
        /// Stores the RecurringExceptions struct in JSON format
        /// </summary>
        public string RecurringExceptions { get; set; }

        #endregion

        #region Miscellaneous
        /// <summary>
        /// Maximum number of attendants
        /// </summary>
        public int MaximumAttendants { get; set; }
        /// <summary>
        /// Whether this event is a slightly modified version of a header event
        /// </summary>
        public bool HasHeaderEvent { get; set; }

        /// <summary>
        /// Reference event id of the event's header event.  
        /// Always check HasHeaderEvent property before accessing this property.
        /// </summary>
        public long HeaderEvent { get; set; }

        /// <summary>
        /// Who can see this event
        /// </summary>
        public byte[] Visibility { get; set; }

        /// <summary>
        /// Who can attend this event
        /// </summary>
        public byte[] Attendability { get; set; }
        
        /// <summary>
        /// Whether the event host needs Griddy to handle the ticketing
        /// </summary>
        public bool RequiresTicket { get; set; }

        /// <summary>
        /// A short description that will be displayed on list view
        /// </summary>
        public string DescriptionAbstract { get; set; }

        /// <summary>
        /// Id to the description in blob storage
        /// </summary>
        public string DescriptionBlobId { get; set; }

        /// <summary>
        /// Url link to the logo picture storage in azure blob.  
        /// Not responsible for checking whether the link is valid.
        /// </summary>
        public string Picture { get; set; }

        /// <summary>
        /// Page url
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Store the categories of the event in Json string
        /// </summary>
        public string Categories { get; set; }

        /// <summary>
        /// Search Keywords, in JSON?
        /// </summary>
        public string SearchKeywords { get; set; }

        /// <summary>
        /// Url link for external webpage of the event
        /// </summary>
        public string ExternalLink { get; set; }

        #endregion

        /// <summary>
        /// Compute the partition key of event entity based on publisher User ID
        /// </summary>
        /// <param name="PublisherID">Publisher User ID</param>
        /// <returns>Partition key</returns>
        public static string ComputePartitionKey(long PublisherID)
        {
            return BaseConvert.ToHexString(PublisherID).Substring(0, 14);
        }

        /// <summary>
        /// Compute the row key of event entity based on Event ID
        /// </summary>
        /// <param name="EventID">Event ID</param>
        /// <returns>Row key</returns>
        public static string ComputeRowKey(long EventID)
        {
            return BaseConvert.ToHexString(EventID);
        }
    }
}
