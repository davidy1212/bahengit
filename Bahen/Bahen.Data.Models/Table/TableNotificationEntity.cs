﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represent data model for a notification to user
    /// </summary>
    public class TableNotificationEntity : TableMessageEntity
    {
        /// <summary>
        /// Thumbnail of the notification
        /// </summary>
        public string PictureUrl { get; set; }

        /// <summary>
        /// Whether this notificaiton has been read
        /// </summary>
        public bool Read { get; set; }

        /// <summary>
        /// Url link if this notification is clicked
        /// </summary>
        public string Url { get; set; }
    }
}
