﻿using Bahen.Common;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represent data model for a message with sender and recipient
    /// </summary>
    public class TableMessageEntity : TableServiceEntity, ITimeEntity
    {
        /// <summary>
        /// Construct an empty message
        /// </summary>
        public TableMessageEntity()
        {
        }

        /// <summary>
        /// Construct a new message entity (only use if need to send a new message)
        /// </summary>
        /// <param name="recipientId">User ID of the recipient</param>
        /// <param name="timestamp">Time when the original message was sent</param>
        public TableMessageEntity(long recipientId, DateTime timestamp)
            : this()
        {
            this.RecipientID = recipientId;
            this.ID = Guid.NewGuid();
            this.TimeSent = timestamp;
            this.PartitionKey = ComputePartitionKey(this.RecipientID);
            this.RowKey = ComputeRowKey(this.TimeSent, this.ID);
        }

        /// <summary>
        /// Timestamp of the message
        /// </summary>
        public DateTime TimeSent { get; set; }

        /// <summary>
        /// ITimeEntity implementation
        /// </summary>
        public DateTime Time
        {
            get
            {
                return TimeSent;
            }
        }

        /// <summary>
        /// Message ID
        /// </summary>
        public virtual Guid ID { get; set; }
        
        /// <summary>
        /// Sender of the message
        /// </summary>
        public long SenderID { get; set; }

        /// <summary>
        /// Receiver of the message
        /// </summary>
        public virtual long RecipientID { get; set; }

        /// <summary>
        /// Content of the message
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Compute the partition key of the entity
        /// </summary>
        /// <param name="recipientID">Receiver of the message</param>
        /// <returns>Partition key of the entity</returns>
        public static string ComputePartitionKey(long recipientID)
        {
            return BaseConvert.ToHexString(recipientID);
        }

        /// <summary>
        /// Compute the row key of the entity
        /// </summary>
        /// <param name="id">ID of the message</param>
        /// <returns>Row key of the entity</returns>
        public static string ComputeRowKey(DateTime timeSent, Guid id)
        {
            return (Int64.MaxValue - timeSent.Ticks).ToHexString() + id.ToString("N");
        }

        /// <summary>
        /// Compute the row key of the entity
        /// </summary>
        /// <param name="id">ID of the message</param>
        /// <returns>Row key of the entity</returns>
        public static string ComputeRowKey(DateTime timeSent, string id)
        {
            return timeSent.Ticks.ToHexString() + id;
        }
    }
}
