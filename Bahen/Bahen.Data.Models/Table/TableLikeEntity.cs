﻿using Bahen.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public class TableLikeEntity : TableUserOpinionEntity
    {
        public TableLikeEntity()
            : base()
        {
        }

        public TableLikeEntity(String targetString, EntityTypeCode targetTypeCode, long userID)
            : base(targetString, targetTypeCode, userID)
        {
        }
        //public TableLikeEntity(long targetID, long userID)
        //    : base(targetID, userID)
        //{
        //}
        //public TableLikeEntity(Guid targetGuid, long userID)
        //    : base(targetGuid, userID)
        //{
        //}

        public override int TypeCode
        {
            get
            {
                return (int)EntityTypeCode.Like;
            }
        }
    }
}
