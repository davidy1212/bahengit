﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represent a data model for keeping tracks of sent email links
    /// </summary>
    public class TableEmailEntity : TableMessageEntity
    {
        /// <summary>
        /// Receiver email
        /// </summary>
        public string ReceiverEmail { get; set; }

        /// <summary>
        /// Url to redirect
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// The action of the email
        /// </summary>
        public int EmailAction { get; set; }

        /// <summary>
        /// Time that the email link in not valid any more
        /// </summary>
        public DateTime ValidUntil
        {
            get
            {
                return this.TimeSent.AddDays(10);
            }
        }
    }
}
