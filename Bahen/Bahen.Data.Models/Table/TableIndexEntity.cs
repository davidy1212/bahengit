﻿using Bahen.Search.Indexing;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represents data model for searching index serialized in table
    /// </summary>
    public class TableIndexEntity : TableServiceEntity
    {
        /// <summary>
        /// Type of the index
        /// </summary>
        public string Type
        {
            get
            {
                return this.PartitionKey;
            }
            set
            {
                this.PartitionKey = value;
            }
        }

        private string prefix;
        /// <summary>
        /// Prefix of the index
        /// </summary>
        public string Prefix
        {
            get
            {
                return this.prefix;
            }
            set
            {
                this.prefix = value;
                this.RowKey = ComputeRowKey(value, this.root, this.indexNumber);
            }
        }

        private string root;

        /// <summary>
        /// Key of the index
        /// </summary>
        public string Root
        {
            get
            {
                return this.root;
            }
            set
            {
                this.root = value;
                this.RowKey = ComputeRowKey(this.prefix, value, this.indexNumber);
            }
        }

        /// <summary>
        /// Full key of the index
        /// </summary>
        public string Key
        {
            get
            {
                return new IndexString(this.Type, this.Prefix, this.Root).ToString();
            }
        }

        private int indexNumber;
        /// <summary>
        /// The order of this index entity with the same key
        /// </summary>
        public int IndexNumber
        {
            get
            {
                return this.indexNumber;
            }
            set
            {
                this.indexNumber = value;
                this.RowKey = ComputeRowKey(this.prefix, this.root, value);
            }
        }

        private byte[] value;

        /// <summary>
        /// Collection of IDs serialized into binary.
        /// Maximum of 1024 IDs.
        /// More IDs will result in an increment of IndexNumber
        /// </summary>
        public byte[] Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (value.Length > 8192)
                {
                    throw new ArgumentException("Maximum of 1024 IDs can be inserted in to the entity");
                }
                this.value = value;
            }
        }

        public static string ComputeRowKey(string prefix, string key, int indexNumber)
        {
            return String.Format("{0}/{1}_{2}", prefix, key, indexNumber);
        }
    }
}
