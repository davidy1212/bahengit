﻿using Bahen.Common;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represents the datamodel of album info data entries
    /// </summary>
    public class TableAlbumEntity : TableServiceEntity
    {
        /// <summary>
        /// Album ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Owner of the event (could be event or user)
        /// </summary>
        public long OwnerID { get; set; }

        /// <summary>
        /// Time when the album is created
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// Name of the album
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short description of the album
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Could be event album, profile picture album, custom album
        /// </summary>
        public string AlbumPrefix { get; set; }

        /// <summary>
        /// Partition key of the entity
        /// </summary>
        public override string PartitionKey
        {
            get
            {
                base.PartitionKey = ComputePartitionKey(this.AlbumPrefix, this.OwnerID);
                return base.PartitionKey;
            }
            set
            {
                base.PartitionKey = value;
            }
        }

        /// <summary>
        /// Row key of the entity
        /// </summary>
        public override string RowKey
        {
            get
            {
                base.RowKey = ComputeRowKey(this.OwnerID, this.ID);
                return base.RowKey;
            }
            set
            {
                base.RowKey = value;
            }
        }

        /// <summary>
        /// Compute the partition key of the entity
        /// </summary>
        /// <param name="prefix">Prefix of the album</param>
        /// <param name="ownerID">Owner of the album</param>
        /// <returns>Partition key of the entity</returns>
        public static string ComputePartitionKey(string prefix, long ownerID)
        {
            return prefix + "_" + BaseConvert.ToHexString(ownerID).Substring(0, 14);
        }

        /// <summary>
        /// Compute the row key of the entity
        /// </summary>
        /// <param name="ownerID">Owner of the album</param>
        /// <param name="albumID">ID of the album</param>
        /// <returns>Row key of the entity</returns>
        public static string ComputeRowKey(long ownerID, Guid albumID)
        {
            return BaseConvert.ToHexString(ownerID).Substring(14) + "_" + albumID.ToString("N");
        }
    }
}
