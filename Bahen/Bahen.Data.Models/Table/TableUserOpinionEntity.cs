﻿using Bahen.Common;
using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public abstract class TableUserOpinionEntity : TableServiceEntity, ITimeEntity, IStringId
    {
        public TableUserOpinionEntity() { }

        public TableUserOpinionEntity(String targetString, EntityTypeCode targetTypeCode, long userID)
        {
            this.UserID = userID;
            this.TargetString = targetString;
            this.PartitionKey = ComputePartitionKey(targetString, targetTypeCode);
            this.TargetTypeCode = (int)targetTypeCode;
            this.ActionTime = DateTime.UtcNow;
            this.RowKey = (Int64.MaxValue - this.ActionTime.Ticks).ToHexString() + Guid.NewGuid().ToString("N");
            //this.RowKey = DateTime.UtcNow.Ticks.ToString() + userID.ToString();
            
        }

        public static string ComputePartitionKey(string targetString, EntityTypeCode typeCode)
        {
            switch (GetIdType(typeCode))
            {
                case IdType.Long:
                    long targetID = Convert.ToInt64(targetString);
                    return (int)typeCode + "_" + BaseConvert.ToHexString(targetID).Substring(0, 11);  // max: 99999 in a partition
                case IdType.Guid:
                    Guid targetGuid = Guid.Parse(targetString);
                    return (int)typeCode + "_" + BaseConvert.ToHexString(targetGuid.ToByteArray()).Substring(0, 27);  // max: 99999 in a partition
                case IdType.String:
                    if (targetString.Length > 5)
                    {
                        return (int)typeCode + '_' + targetString.Substring(0, targetString.Length - 5);
                    }
                    else
                    {
                        return (int)typeCode + '_' + targetString.Substring(0, 1);
                    }
                default:
                    throw new Exception("Undefined IdType");
            }
        }

        public string TargetString { get; set; }


        public static IdType GetIdType(EntityTypeCode typeCode)
        {
            switch (typeCode)
            {
                case EntityTypeCode.NewsFeed:
                    return IdType.Guid;
                case EntityTypeCode.Comment:
                    return IdType.String;
                case EntityTypeCode.Event:
                    return IdType.Long;
                case EntityTypeCode.Cycle:
                    return IdType.Long;
                case EntityTypeCode.User:
                    return IdType.Long;
                case EntityTypeCode.Like:
                    return IdType.String;
                default:
                    throw new Exception("Undefined TypeCode");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public abstract int TypeCode { get; }


        /// <summary>
        /// 
        /// </summary>
        public int TargetTypeCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long UserID { get; set; }


        public DateTime ActionTime { get; set; }

        /// <summary>
        /// ITimeEntity implementation
        /// </summary>
        public DateTime Time
        {
            get
            {
                return ActionTime;
            }
        }

        public string StringId
        {
            get
            {
                return this.RowKey;
            }
            // MUST INCLUDE AN EMPTY SET FOR ANY STRING FIELD OR IT TAKES 3 SECS TO INSERT 1 ITEM!!!!!!!!!
            set
            {
            }
        }
    }
}
