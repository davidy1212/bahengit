﻿using Bahen.Common;
using Bahen.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for a news feed for a single recipient stored in azure table storage
    /// </summary>
    public class TableNewsfeedEntity : TableMessageEntity
    {
        /// <summary>
        /// Construct empty newsfeed entity
        /// </summary>
        public TableNewsfeedEntity()
        {
        }

        /// <summary>
        /// Construct an original newsfeed entity with time sent to be UtcNow
        /// </summary>
        /// <param name="recipientId">Recipient ID</param>
        public TableNewsfeedEntity(long sendId)
            : this(sendId, sendId, true, DateTime.UtcNow)
        {
        }

        /// <summary>
        /// Construct a newsfeed entity with specified recipient ID,
        /// whether the feed is original, and the time when the original feed was send
        /// </summary>
        /// <param name="recipientId">Recipient ID</param>
        /// <param name="senderId">Sender ID</param>
        /// <param name="isOriginal">Whether the feed is original (sent to the sender)</param>
        /// <param name="timeSent">Time when the original feed is sent</param>
        public TableNewsfeedEntity(long senderId, long recipientId, bool isOriginal, DateTime timeSent)
            : base(recipientId, timeSent)
        {
            this.IsOriginal = isOriginal;
            this.SenderID = senderId;
            if (isOriginal)
            {
                this.OriginalNewsfeedRowKey = TableMessageEntity.ComputeRowKey(timeSent, this.ID);
            }
        }

        /// <summary>
        /// Message ID
        /// </summary>
        public override Guid ID { get; set; }

        /// <summary>
        /// Type of the reference
        /// </summary>
        public int TypeCode
        {
            get
            {
                return (int)EntityTypeCode.NewsFeed;
            }
        }

        /// <summary>
        /// Reference of the newsfeed content (long type).
        /// </summary>
        public long ReferenceLongID { get; set; }

        /// <summary>
        /// Reference of the newsfeed content (Table partition key).
        /// </summary>
        public string ReferencePartitioinKey { get; set; }

        /// <summary>
        /// Reference of the newsfeed content (Table row key).
        /// </summary>
        public string ReferenceRowKey { get; set; }

        /// <summary>
        /// Reference of the original newsfeed ID
        /// </summary>
        public string OriginalNewsfeedRowKey { get; set; }

        /// <summary>
        /// If the newsfeed is original
        /// </summary>
        public bool IsOriginal { get; set; }
        
        /// <summary>
        /// Type of the newsfeed reference.
        /// </summary>
        public string ReferenceType { get; set; }
    }
}
