﻿using Bahen.Data.Models;
using Microsoft.WindowsAzure.Storage.Table.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public class TableLikeCount : TableEntityCount
    {
        public TableLikeCount() : base() { }

        public TableLikeCount(String targetString, EntityTypeCode targetTypeCode)
            : base(targetString, targetTypeCode)
        {
        }
    }
}
