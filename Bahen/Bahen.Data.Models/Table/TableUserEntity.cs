﻿using Bahen.Common;
using Microsoft.WindowsAzure.Storage.Table.DataServices;

using System;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for user object in Azure Table Datastorage (NoSql)
    /// </summary>
    public class TableUserEntity : TableServiceEntity, IInt64Id
    {
        private long userID;

        /// <summary>
        /// User ID
        /// </summary>
        public long UserID
        {
            get
            {
                return this.userID;
            }
            set
            {
                this.userID = value;

                // Use the first 48 bit of the id as partition key
                string hex = BaseConvert.ToHexString(value);
                this.PartitionKey = hex.Substring(0, 12);

                // Use the last 16 bit of the id as row key (each partition contains 65535=2^16 users)
                this.RowKey = hex.Substring(12, 4);
            }
        }

        /// <summary>
        /// User ID
        /// </summary>
        public long ID
        {
            get { return this.UserID; }
        }

        #region Security
        /// <summary>
        /// Email that user registered with.  
        /// Needs to be confirmed.
        /// Cannot be changed after registration.
        /// </summary>
        public string LoginEmail { get; set; }

        /// <summary>
        /// Hashed password using SHA512 algorithm
        /// </summary>
        public byte[] LoginPassword { get; set; }

        /// <summary>
        /// Activated the account by clicking the link sent to the login email.
        /// </summary>
        public bool LoginEmailConfirmed { get; set; }

        /// <summary>
        /// Whether the user is created automatically (under invitation of other users)
        /// </summary>
        public bool UnderInvitation { get; set; }
        #endregion

        #region Identification
        /// <summary>
        /// First name/Given name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Preferred name
        /// </summary>
        public string PreferredName { get; set; }

        /// <summary>
        /// Birthday visibility to other users
        /// </summary>
        public int BirthdayPrivacyLevel { get; set; }

        /// <summary>
        /// Birthday
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// Male or female
        /// </summary>
        public int Gender { get; set; }
        #endregion

        #region Contact
        public string Addresses { get; set; }
        // Consider save all the numbers in a single string
        // number,type,privacy|number,type,privacy
        // string,string,int|string,string,int
        public string PhoneNumbers { get; set; }

        // Consider save all the emails in a single string
        // address,type,privacy|address,type,privacy
        // string,string,int|string,string,int
        public string EmailAddresses { get; set; }

        /// <summary>
        /// Store IM addresses
        /// </summary>
        public string InstantMessages { get; set; }
        #endregion

        #region SocialNetworks Address
        /// <summary>
        /// Store Address to facebook page. Particular useful for publishers
        /// </summary>
        public string FacebookAddress { get; set; }

        /// <summary>
        /// Twitter Addresses
        /// </summary>
        public string TwitterAddress { get; set; }

        /// <summary>
        /// LinkedinAddress
        /// </summary>
        public string LinkedinAddress { get; set; }

        /// <summary>
        /// Google+ Address
        /// </summary>
        public string GooglePlusAddress { get; set; }

        #endregion

        #region Policies
        /// <summary>
        /// Profile visibility
        /// </summary>
        public byte[] ProfileVisibilityPolicy { get; set; }

        /// <summary>
        /// Restrict certain users to subscribe
        /// </summary>
        public byte[] SubscriptionPolicy { get; set; }

        public byte[] EmailVisibilityPolicy { get; set; }
        #endregion

        #region Miscellaneous

        /// <summary>
        /// User education history
        /// </summary>
        public string Educations { get; set; }

        /// <summary>
        /// User work place history
        /// </summary>
        public string WorkPlaces { get; set; }

        /// <summary>
        /// Url to the profile
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Url to the profile picture
        /// </summary>
        public string ProfilePicture { get; set; }

        /// <summary>
        /// CultureInfo of the user.  (Language/Calendar etc.)
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// User ability group.  (Publisher/Administrator etc.)
        /// </summary>
        public int UserGroup { get; set; }

        /// <summary>
        /// Social
        /// </summary>
        public string SocialNetworkConnections { get; set; }
        public string Description { get; set; }
        public DateTime TimeStamp { get; set; }
        #endregion

        #region Partition/Row Keys
        /// <summary>
        /// Compute the partition key of user entity based on User ID
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <returns>Partion key</returns>
        public static string ComputePartitionKey(long ID)
        {
            return BaseConvert.ToHexString(ID).Substring(0, 12);
        }

        /// <summary>
        /// Compute the row key of user entity based on User ID
        /// </summary>
        /// <param name="ID">User ID</param>
        /// <returns>Row key</returns>
        public static string ComputeRowKey(long ID)
        {
            return BaseConvert.ToHexString(ID).Substring(12, 4);
        }
        #endregion
    }
}
