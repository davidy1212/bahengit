﻿using Bahen.Common;
using Bahen.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public class TableCommentEntity : TableUserOpinionEntity
    {
        public TableCommentEntity()
            : base()
        {
        }

        public TableCommentEntity(String targetString, EntityTypeCode targetTypeCode, long userID)
            : base(targetString, targetTypeCode, userID)
        {
        }

        //public TableCommentEntity(long targetID, long userID)
        //    : base(targetID, userID)
        //{
        //}
        //public TableCommentEntity(Guid targetGuid, long userID)
        //    : base(targetGuid, userID)
        //{
        //}

        public string Content { get; set; }

        public override int TypeCode
        {
            get
            {
                return (int)EntityTypeCode.Comment;
            }
        }
    }
}
