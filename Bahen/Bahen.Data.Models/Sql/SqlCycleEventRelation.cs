using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for cycle event relation object in sql data storage
    /// </summary>
    public class SqlCycleEventRelation : IInt64Id
    {
        public long ID { get; private set; }
        public long CycleID { get; set; }
        public long EventID { get; set; }
        public byte[] Timestamp { get; set; }

        [JsonIgnore]
        public virtual SqlCycleEntity Cycle { get; private set; }

        [JsonIgnore]
        public virtual SqlEventEntity Event { get; private set; }
        //public long ID
        //{
        //    get
        //    {
        //        return this.ID;
        //    }
        //}
    }
}
