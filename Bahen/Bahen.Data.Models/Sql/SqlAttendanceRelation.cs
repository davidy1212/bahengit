using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for attendance relation object in sql data storage
    /// </summary>
    public class SqlAttendanceRelation : IInt64Id
    {
        public long ID { get; set; }
        public long UserID { get; set; }
        public int AttendanceStatus { get; set; }
        public long EventID { get; set; }
        public byte[] Timestamp { get; set; }

        [JsonIgnore]
        public virtual SqlEventEntity Event { get; private set; }

        [JsonIgnore]
        public virtual SqlUserEntity User { get; set; }
        //public long ID
        //{
        //    get
        //    {
        //        return this.ID;
        //    }
        //}
    }
}
