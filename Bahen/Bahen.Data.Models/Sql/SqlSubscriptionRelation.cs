using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for subscription relation object in sql data storage
    /// </summary>
    public class SqlSubscriptionRelation : IInt64Id
    {
        public long ID { get; private set; }
        public long HostID { get; set; }
        public long SubscriberID { get; set; }
        public byte[] Timestamp { get; set; }
        public virtual SqlUserEntity Host { get; private set; }
        public virtual SqlUserEntity Subscriber { get; private set; }
    }
}
