using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    public class SqlCookieEntity
    {
        public System.Guid CookieID { get; set; }
        public long UserID { get; set; }
        public string UserAgent { get; set; }
        public System.DateTime TimeLastLogin { get; set; }
        public byte[] Timestamp { get; set; }
        public System.Guid ID
        {
            get
            {
                return this.CookieID;
            }
        }
        //public virtual SqlUserEntity User { get; set; }
    }
}
