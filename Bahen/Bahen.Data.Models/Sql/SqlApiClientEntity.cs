using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    public partial class SqlApiClientEntity : IInt64Id
    {
        public SqlApiClientEntity()
        {
            this.ApiAuths = new List<SqlApiAuthRelation>();
        }

        public long ClientID { get; set; }
        public long DeveloperID { get; set; }
        public System.Guid ClientSecret { get; set; }
        public string Name { get; set; }
        public string PictureUrl { get; set; }
        public int TrustLevel { get; set; }
        public byte[] Timestamp { get; set; }
        public virtual ICollection<SqlApiAuthRelation> ApiAuths { get; set; }
        public virtual SqlUserEntity Developer { get; set; }

        public long ID
        {
            get { return this.ClientID; }
        }
    }
}
