using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for event object in sql data storage
    /// </summary>
    public class SqlEventEntity : IInt64Id
    {
        public SqlEventEntity()
        {
            this.Attendances = new List<SqlAttendanceRelation>();
            this.CycleEvents = new List<SqlCycleEventRelation>();
        }

        public long ID { get; private set; }
        public long PublisherID { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Alias { get; set; }
        public string Location { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public byte[] Timestamp { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlAttendanceRelation> Attendances { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlCycleEventRelation> CycleEvents { get; private set; }

        [JsonIgnore]
        public virtual SqlUserEntity Publisher { get; private set; }
    }
}
