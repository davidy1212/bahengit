using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for cycle object in sql data storage
    /// </summary>
    public class SqlCycleEntity : IInt64Id
    {
        public SqlCycleEntity()
        {
            this.CycleEvents = new List<SqlCycleEventRelation>();
            this.CycleUsers = new List<SqlCycleUserRelation>();
        }

        public long ID { get; private set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Alias { get; set; }
        public byte[] Timestamp { get; set; }

        [JsonIgnore]
        public virtual ICollection<SqlCycleEventRelation> CycleEvents { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlCycleUserRelation> CycleUsers { get; private set; }
        //public long ID
        //{
        //    get
        //    {
        //        return this.ID;
        //    }
        //}
    }
}
