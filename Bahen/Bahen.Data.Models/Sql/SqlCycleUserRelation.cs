using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for cycle user object in sql data storage
    /// </summary>
    public class SqlCycleUserRelation : IInt64Id
    {
        public long ID { get; private set; }
        public long CycleID { get; set; }
        public long UserID { get; set; }
        public int MembershipType { get; set; }
        public byte[] Timestamp { get; set; }

        [JsonIgnore]
        public virtual SqlCycleEntity Cycle { get; private set; }

        [JsonIgnore]
        public virtual SqlUserEntity User { get; private set; }
    }
}
