using System;
using System.Collections.Generic;

namespace Bahen.Data.Models
{
    public partial class SqlApiAuthRelation : IInt64Id
    {
        public long ID { get; set; }
        public long UserID { get; set; }
        public long ClientID { get; set; }
        public long Scopes { get; set; }
        public bool Requested { get; set; }
        public string RedirectUri { get; set; }
        public virtual SqlApiClientEntity ApiClient { get; set; }
        public virtual SqlUserEntity User { get; set; }
    }
}
