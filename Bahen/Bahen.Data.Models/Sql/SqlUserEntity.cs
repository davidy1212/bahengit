using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Datamodel for cycle object in sql data storage (NoSql)
    /// </summary>
    public class SqlUserEntity : IInt64Id
    {
        public SqlUserEntity()
        {
            this.Attendances = new List<SqlAttendanceRelation>();
            this.CycleUsers = new List<SqlCycleUserRelation>();
            this.Events = new List<SqlEventEntity>();
            this.HostSubscriptions = new List<SqlSubscriptionRelation>();
            this.SubscribersSubscriptions = new List<SqlSubscriptionRelation>();
        }

        public long ID { get; set; }
        public string Alias { get; set; }
        public string LoginEmail { get; set; }
        public byte[] LoginPassword { get; set; }
        public string PreferredName { get; set; }
        public string Picture { get; set; }
        public byte[] Timestamp { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlAttendanceRelation> Attendances { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlCycleUserRelation> CycleUsers { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlEventEntity> Events { get; private set; }

        [JsonIgnore]
        // The users that subscribed this user
        public virtual ICollection<SqlSubscriptionRelation> HostSubscriptions { get; private set; }

        [JsonIgnore]
        // Users that this user subscribed
        public virtual ICollection<SqlSubscriptionRelation> SubscribersSubscriptions { get; private set; }

        [JsonIgnore]
        public virtual ICollection<SqlCookieEntity> Cookies { get; private set; }

        public virtual ICollection<SqlApiAuthRelation> ApiAuths { get; private set; }

        public virtual ICollection<SqlApiClientEntity> DevelopedClients { get; private set; }
    }
}
