using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class EventMap : EntityTypeConfiguration<SqlEventEntity>
    {
        public EventMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Alias)
                .IsRequired()
                .HasMaxLength(50);
            
            this.Property(t => t.Picture);

            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Events");
            this.Property(t => t.ID).HasColumnName("EventID");
            this.Property(t => t.PublisherID).HasColumnName("PublisherID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Picture).HasColumnName("Picture");
            this.Property(t => t.Alias).HasColumnName("Alias");
            this.Property(t => t.StartTime).HasColumnName("StartTime");
            this.Property(t => t.EndTime).HasColumnName("EndTime");
            this.Property(t => t.Location).HasColumnName("Location");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            this.HasRequired(t => t.Publisher)
                .WithMany(t => t.Events)
                .HasForeignKey(d => d.PublisherID);

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
