using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class SubscriptionMap : EntityTypeConfiguration<SqlSubscriptionRelation>
    {
        public SubscriptionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Subscriptions");
            this.Property(t => t.ID).HasColumnName("SubscriptionID");
            this.Property(t => t.HostID).HasColumnName("HostID");
            this.Property(t => t.SubscriberID).HasColumnName("SubscriberID");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            this.HasRequired(t => t.Host)
                .WithMany(t => t.HostSubscriptions)
                .HasForeignKey(d => d.HostID);
            this.HasRequired(t => t.Subscriber)
                .WithMany(t => t.SubscribersSubscriptions)
                .HasForeignKey(d => d.SubscriberID);

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
