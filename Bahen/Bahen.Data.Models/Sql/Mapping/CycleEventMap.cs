using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class CycleEventMap : EntityTypeConfiguration<SqlCycleEventRelation>
    {
        public CycleEventMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("CycleEvents");
            this.Property(t => t.ID).HasColumnName("CycleEventID");
            this.Property(t => t.CycleID).HasColumnName("CycleID");
            this.Property(t => t.EventID).HasColumnName("EventID");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            this.HasRequired(t => t.Cycle)
                .WithMany(t => t.CycleEvents)
                .HasForeignKey(d => d.CycleID);
            this.HasRequired(t => t.Event)
                .WithMany(t => t.CycleEvents)
                .HasForeignKey(d => d.EventID);

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
