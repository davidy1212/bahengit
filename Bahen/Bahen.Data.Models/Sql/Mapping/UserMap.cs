using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class UserMap : EntityTypeConfiguration<SqlUserEntity>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Alias)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LoginEmail)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.LoginPassword)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(64);

            this.Property(t => t.PreferredName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Picture);

            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Users");
            this.Property(t => t.ID).HasColumnName("UserID");
            this.Property(t => t.LoginEmail).HasColumnName("LoginEmail");
            this.Property(t => t.LoginPassword).HasColumnName("LoginPassword");
            this.Property(t => t.PreferredName).HasColumnName("PreferredName");
            this.Property(t => t.Alias).HasColumnName("Alias");
            this.Property(t => t.Picture).HasColumnName("Picture");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");
            
            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
