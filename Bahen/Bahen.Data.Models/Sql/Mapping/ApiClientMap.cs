using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class ApiClientMap : EntityTypeConfiguration<SqlApiClientEntity>
    {
        public ApiClientMap()
        {
            // Primary Key
            this.HasKey(t => t.ClientID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PictureUrl)
                .HasMaxLength(200);

            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            this.Property(t => t.TrustLevel)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ApiClients");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.ClientSecret).HasColumnName("ClientSecret");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.PictureUrl).HasColumnName("PictureUrl");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");
            this.Property(t => t.TrustLevel).HasColumnName("TrustLevel");

            // Relationships
            this.HasRequired(t => t.Developer)
                .WithMany(t => t.DevelopedClients)
                .HasForeignKey(d => d.DeveloperID);
        }
    }
}
