using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class CookieMap : EntityTypeConfiguration<SqlCookieEntity>
    {
        public CookieMap()
        {
            // Primary Key
            this.HasKey(t => t.CookieID);

            // Properties
            this.Property(t => t.UserID)
                .IsRequired();
                //.IsFixedLength()
                //.HasMaxLength(64);

            this.Property(t => t.UserAgent)
                .IsRequired();

            this.Property(t => t.TimeLastLogin)
                .IsRequired();
                //.IsFixedLength()
                //.HasMaxLength(8)
                //.IsRowVersion();
            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Cookies");
            this.Property(t => t.CookieID).HasColumnName("CookieID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.UserAgent).HasColumnName("UserAgent");
            this.Property(t => t.TimeLastLogin).HasColumnName("TimeLastLogin");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            /*this.HasRequired(t => t.User)
                .WithMany(t => t.Cookies)
                .HasForeignKey(d => d.UserID);*/

        }
    }
}
