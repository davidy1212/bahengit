using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class CycleUserMap : EntityTypeConfiguration<SqlCycleUserRelation>
    {
        public CycleUserMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("CycleUsers");
            this.Property(t => t.ID).HasColumnName("CycleUserID");
            this.Property(t => t.CycleID).HasColumnName("CycleID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.MembershipType).HasColumnName("MembershipType");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            this.HasRequired(t => t.Cycle)
                .WithMany(t => t.CycleUsers)
                .HasForeignKey(d => d.CycleID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.CycleUsers)
                .HasForeignKey(d => d.UserID);

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
