using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class CycleMap : EntityTypeConfiguration<SqlCycleEntity>
    {
        public CycleMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Alias)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Picture);

            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Cycles");
            this.Property(t => t.ID).HasColumnName("CycleID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Picture).HasColumnName("Picture");
            this.Property(t => t.Alias).HasColumnName("Alias");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
