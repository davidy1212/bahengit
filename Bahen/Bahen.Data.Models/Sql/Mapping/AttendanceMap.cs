using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class AttendanceMap : EntityTypeConfiguration<SqlAttendanceRelation>
    {
        public AttendanceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Timestamp)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(8)
                .IsRowVersion();

            // Table & Column Mappings
            this.ToTable("Attendances");
            this.Property(t => t.ID).HasColumnName("AttendanceID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.AttendanceStatus).HasColumnName("AttendanceStatus");
            this.Property(t => t.EventID).HasColumnName("EventID");
            this.Property(t => t.Timestamp).HasColumnName("Timestamp");

            // Relationships
            this.HasRequired(t => t.Event)
                .WithMany(t => t.Attendances)
                .HasForeignKey(d => d.EventID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.Attendances)
                .HasForeignKey(d => d.UserID);

            // Ignore
            //this.Ignore<long>(t => t.ID);
        }
    }
}
