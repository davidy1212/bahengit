using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bahen.Data.Models
{
    public class ApiAuthMap : EntityTypeConfiguration<SqlApiAuthRelation>
    {
        public ApiAuthMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.RedirectUri)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("ApiAuth");
            this.Property(t => t.ID).HasColumnName("AuthID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ClientID).HasColumnName("ClientID");
            this.Property(t => t.Requested).HasColumnName("Requested");
            this.Property(t => t.Scopes).HasColumnName("Scopes");
            
            // Relationships
            this.HasRequired(t => t.ApiClient)
                .WithMany(t => t.ApiAuths)
                .HasForeignKey(d => d.ClientID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.ApiAuths)
                .HasForeignKey(d => d.UserID);
        }
    }
}
