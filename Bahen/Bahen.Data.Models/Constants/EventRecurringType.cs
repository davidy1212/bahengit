﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Mapping recurring types to numeric value.  
    /// Note: taking the sum of RecurringType and the sum of Weekdays will be more efficient.
    /// For negative number, treat as Once.  
    /// For positive number > 512, treat as Yearly.
    /// </summary>
    [Flags]
    public enum EventRecurringType
    {
        Once = -1,

        // 1-128 represent cetain weekdays recurring weekly
        Weekly = 0,

        Monday = 1,

        Tuesday = 2,

        Wednesday = 4,

        Thursday = 8,

        Friday=16,

        Saturday=32,

        Sunday = 64,

        // Daily is equivalent to recurring weekly on all weekdays ( 0 + 1 + 2 + 4 + 8 + 16 + 32 + 64 )
        Daily = 127,

        // 128-255 represent certain weekdays recurring biweekly
        BiWeekly = 128,

        // 256 represents recurring monthly (by taking the same day number directly)
        Monthly = 256,

        // 512 represents recurring yearly (by taking the same day number and month number directly)
        Yearly = 512
    }
}
