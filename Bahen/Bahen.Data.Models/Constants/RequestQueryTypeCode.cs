﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public enum RequestQueryTypeCode
    {
        NotCovered=0,

        LikeLikeAsyncID,
        LikeLikeAsyncGuid,
        LikeLikeSyncID,
        LikeLikeSyncGuid,
        LikeGetLikedUserAsyncID,
        LikeGetLikedUserAsyncGuid,
        LikeGetLikedUserSyncID,
        LikeGetLikedUserSyncGuid,

        CommentPostAsynID,
        CommentPostAsynGuid,
        CommentPostSyncID,
        CommentPostSyncGuid,
        CommentDeleteAsynID,
        CommentDeleteAsynGuid,
        CommentDeleteSyncID,
        CommentDeleteSyncGuid
    }
}
