﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.DataModels.Constants
{
    public enum AttendanceRestrictionLevel
    {
        OpenToAll = 0,
        SpecifiedCyclesOnly = 1,
        SubscribeesOnly = 2,    // People who subscribe you
        SubscribersOnly = 4,    // People who you subscribe
        ByInviteOnly = 8,
        Private = 16
    }
}
