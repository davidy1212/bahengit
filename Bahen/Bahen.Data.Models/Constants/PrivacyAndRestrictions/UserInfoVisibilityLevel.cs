﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.DataModels.Constants
{
    public enum UserInfoVisibilityLevel
    {
        Private = 0,
        Friends = 1,
        SubscribedPublishers = 2,
        Subscribers = 4,
        Cycles = 8,
        Public = 16
    }
}
