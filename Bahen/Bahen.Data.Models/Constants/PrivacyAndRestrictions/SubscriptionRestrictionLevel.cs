﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.DataModels.Constants
{
    public enum SubscriptionRestrictionLevel
    {
        Private,
        ByApproval,
        Subscribee,
        OpenToAll
    }
}
