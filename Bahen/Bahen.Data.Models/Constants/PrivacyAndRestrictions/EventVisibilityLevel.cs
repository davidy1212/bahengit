﻿
namespace Bahen.DataModels.Constants
{
    /// <summary>
    /// Types of privacy level of an event
    /// </summary>
    public enum EventVisibilityLevel
    {
        OpenToAll = 0,
        SpecifiedCyclesOnly = 1,
        SubscribeesOnly = 2,    // People who subscribe you
        SubscribersOnly = 4,    // People who you subscribe
        ByInviteOnly = 8,
        Private = 16
    }
}
