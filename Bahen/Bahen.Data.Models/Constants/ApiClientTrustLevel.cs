﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public enum ApiClientTrustLevel
    {
        Test=0,
        Public=10,
        Trusted=100,
        Owner=1000
    }
}
