﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public enum AttendanceStatus
    {
        None = 0,
        Interested = 1,
        Noreply = 2,
        Declined = 4,
        Maybe = 8,
        Going = 16,
        Organizer = 32,
        Copublisher = 64,
        Publisher = 128
    }
}
