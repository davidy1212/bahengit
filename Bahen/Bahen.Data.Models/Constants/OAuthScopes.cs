﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bahen.Data.Models
{
    [Flags]
    public enum OAuthScopes : long
    {
        [Description("email")]
        Email = 0x1 << 0,

        [Description("user_about_me")]
        UserAboutMe = 0x1 << 1,

        [Description("followings_about_me")]
        FollowingsAboutMe = 0x1 << 2,

        [Description("user_checkins")]
        UserCheckins = 0x1 << 3,

        [Description("followings_checkins")]
        FollowingsCheckins = 0x1 << 4,

        [Description("user_events")]
        UserEvents = 0x1 << 5,

        [Description("followings_events")]
        FollowingsEvents = 0x1 << 6,

        [Description("user_circles")]
        UserCycles = 0x1 << 7,

        [Description("followings_circles")]
        FollowingsCycles = 0x1 << 8,

        [Description("user_likes")]
        UserLikes = 0x1 << 9,

        [Description("followings_likes")]
        FollowingsLikes = 0x1 << 10,

        [Description("user_photos")]
        UserPhotos = 0x1 << 11,

        [Description("followings_photos")]
        FollowingsPhotos = 0x1 << 12,

        [Description("user_work_history")]
        UserWorkHistory = 0x1 << 13,

        [Description("followings_work_history")]
        FollowingsWorkHistory = 0x1 << 14,

        [Description("read_followers")]
        ReadFollowers = 0x1 << 15,

        [Description("read_followings")]
        ReadFollowings = 0x1 << 16,

        [Description("read_feeds")]
        ReadFeeds = 0x1 << 17,

        [Description("create_event")]
        CreateEvent = 0x1 << 18,

        [Description("manage_followings")]
        ManageFollowings = 0x1 << 20,

        [Description("manage_notifications")]
        ManageNotifications = 0x1 << 21,

        [Description("rsvp_event")]
        RsvpEvent = 0x1 << 22,

        [Description("offline_access")]
        OfflineAccess = 0x1 << 23,

        [Description("owner")]
        Owner = 0x1 << 62
    }
}