﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public enum EntityTypeCode
    {
        NewsFeed=0,
        Comment=1,
        Event=2,
        Cycle=3,
        User=4,
        Like=5
    }

    public enum IdType
    {
        Long,
        Guid,
        String
    }
}
