﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public enum UserEducationType
    {
        Elementary,
        Middle,
        High,
        University
    }
}
