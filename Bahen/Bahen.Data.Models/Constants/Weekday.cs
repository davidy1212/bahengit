﻿
namespace Bahen.Data.Models
{
    /// <summary>
    /// Mapping weekdays to numeric value (power of 2, so that the sum of a collection of weekdays are unique)
    /// </summary>
    public enum WeekDay
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Saturday = 32,
        Sunday = 64
    }
}
