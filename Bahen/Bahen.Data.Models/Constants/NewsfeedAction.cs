﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    public enum NewsfeedAction
    {
        Register,
        SubscribeUser,
        CreateEvent,
        AttendEvent,
        CreateCycle,
        JoinCycle,
        CommentEvent,
        CommentCycle,
        LikeEvent,
        LikeCycle,
        LikeComment,
        LeaveEvent,
        LeaveCycle,
        DeactivateUser
    }
}
