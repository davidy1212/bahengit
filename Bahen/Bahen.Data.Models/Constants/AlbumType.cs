﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Album types
    /// </summary>
    public enum AlbumType
    {
        ProfilePicture,
        EventPicture,
        CyclePicture,
        UserCustom
    }
}
