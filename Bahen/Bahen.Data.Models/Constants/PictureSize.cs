﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Profile picture sizes
    /// </summary>
    public enum PictureSize
    {
        /// <summary>
        /// Small thumbnail size for display on list
        /// </summary>
        Thumb,

        /// <summary>
        /// Medium picture display on personal profile
        /// </summary>
        Medium,

        /// <summary>
        /// Large picture for generic pictures
        /// </summary>
        Large
    }
}
