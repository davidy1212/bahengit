﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Search.Indexing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Contains all the information of an event
    /// </summary>
    public class EventEntity : IFullEntity<SqlEventEntity, TableEventEntity>, IIndexable
    {
        /// <summary>
        /// Construct a new event full entity by initializing two parts of the entity
        /// </summary>
        public EventEntity()
        {
            this.SqlEntity = new SqlEventEntity();
            this.TableEntity = new TableEventEntity();
        }

        /// <summary>
        /// Assemble an event full entity by passing the sql and table entity of the event (has to be same ID)
        /// </summary>
        /// <param name="sqlEntity">SqlEventEntity instance</param>
        /// <param name="tableEntity">TableEventEntity instance</param>
        /// <exception cref="ArgumentException">IDs of the sql and table entities do not match</exception>
        public EventEntity(SqlEventEntity sqlEntity, TableEventEntity tableEntity)
        {
            if (sqlEntity.ID != tableEntity.ID)
            {
                throw new ArgumentException(
                    String.Format("Cannot assemble SqlEventEntity ID: {0} and TableEventEntity ID: {1} because of different IDs.",
                    sqlEntity.ID, tableEntity.ID)
                    );
            }
            this.SqlEntity = sqlEntity;
            this.TableEntity = tableEntity;
        }

        public EntityTypeCode TypeCode
        {
            get
            {
                return EntityTypeCode.Event;
            }
        }

        /// <summary>
        /// Sql part of the entity
        /// </summary>
        public SqlEventEntity SqlEntity { get; private set; }

        /// <summary>
        /// Table part of the entity
        /// </summary>
        public TableEventEntity TableEntity { get; private set; }

        #region Identification
        /// <summary>
        /// Event ID
        /// </summary>
        public long EventID
        {
            get
            {
                this.TableEntity.EventID = this.SqlEntity.ID;
                return this.SqlEntity.ID;
            }
        }

        /// <summary>
        /// Event ID
        /// </summary>
        public long ID
        {
            get { return this.EventID; }
        }

        /// <summary>
        /// Event name
        /// </summary>
        public string Name
        {
            get
            {
                this.TableEntity.Name = this.SqlEntity.Name;
                return this.SqlEntity.Name;
            }
            set
            {
                if (value == null) throw new ArgumentException("Event name cannot be set to null");
                this.SqlEntity.Name = value;
                this.TableEntity.Name = value;
            }
        }

        /// <summary>
        /// Publisher User ID
        /// </summary>
        public long PublisherID
        {
            get
            {
                this.TableEntity.PublisherID = this.SqlEntity.ID;
                return this.SqlEntity.PublisherID;
            }
            set
            {
                this.TableEntity.PublisherID = value;
                this.SqlEntity.PublisherID = value;
            }
        }

        /// <summary>
        /// Event picture url
        /// </summary>
        public string Picture
        {
            get
            {
                this.TableEntity.Picture = this.SqlEntity.Picture;
                return this.SqlEntity.Picture;
            }
            set
            {
                this.TableEntity.Picture = value;
                this.SqlEntity.Picture = value;
            }
        }

        /// <summary>
        /// Page url
        /// </summary>
        public string Alias
        {
            get
            {
                this.TableEntity.Alias = this.SqlEntity.Alias;
                return this.SqlEntity.Alias;
            }
            set
            {
                if (value == null) throw new ArgumentException("Cycle page url cannot be set to null");
                this.TableEntity.Alias = value;
                this.SqlEntity.Alias = value;
            }
        }

        /// <summary>
        /// Time when the event is published
        /// </summary>
        public DateTime TimePublished
        {
            get
            {
                return this.TableEntity.TimeCreated;
            }
            set
            {
                this.TableEntity.TimeCreated = value;
            }
        }
        #endregion

        #region Description
        private Address address;

        /// <summary>
        /// Address of the event
        /// </summary>
        public Address Address
        {
            get
            {
                try
                {
                    this.address = JsonConvert.DeserializeObject<Address>(this.TableEntity.Address);
                }
                catch
                {
                    this.address = new Address();
                }
                return this.address;
            }
            set
            {
                this.address = value;
                this.TableEntity.Address = JsonConvert.SerializeObject(value);
                this.SqlEntity.Location = value.Venue;
            }
        }

        /// <summary>
        /// Geo coordinates
        /// </summary>
        public GeoCoordinate Coordinate
        {
            get
            {
                return new GeoCoordinate(this.TableEntity.Longitude, this.TableEntity.Latitude);
            }
            set
            {
                this.TableEntity.Longitude = value.Longitude;
                this.TableEntity.Latitude = value.Latitude;
            }
        }

        /// <summary>
        /// Start time of the event (UTC)
        /// </summary>
        public DateTime StartTime
        {
            get
            {
                this.TableEntity.StartTime = this.SqlEntity.StartTime;
                return this.SqlEntity.StartTime;
            }
            set
            {
                this.TableEntity.StartTime = value;
                this.SqlEntity.StartTime = value;
            }
        }

        /// <summary>
        /// End time of the event (UTC)
        /// </summary>
        public DateTime EndTime
        {
            get
            {
                this.TableEntity.EndTime = this.SqlEntity.EndTime;
                return this.SqlEntity.EndTime;
            }
            set
            {
                this.TableEntity.EndTime = value;
                this.SqlEntity.EndTime = value;
            }
        }

        /// <summary>
        /// Whether the event is recurring or single
        /// </summary>
        public bool IsRecurring
        {
            get
            {
                return this.TableEntity.IsRecurring;
            }
        }

        /// <summary>
        /// Whether the event last for entire day
        /// </summary>
        public bool IsDayEvent
        {
            get
            {
                return this.TableEntity.IsDayEvent;
            }
            set
            {
                this.TableEntity.IsDayEvent = value;
            }
        }

        /// <summary>
        /// Frequency of the event recurring
        /// </summary>
        public EventRecurringType RecurringFrequency
        {
            get
            {
                return (EventRecurringType)this.TableEntity.RecurringFrequency;
            }
            set
            {
                this.TableEntity.RecurringFrequency = (int)value;
            }
        }

        /// <summary>
        /// How many times of recurrence
        /// </summary>
        public int RecurringTimes
        {
            get
            {
                return this.TableEntity.RecurringTimes;
            }
            set
            {
                this.TableEntity.RecurringTimes = value;
            }
        }

        /// <summary>
        /// On which days there will be an recurring exception (single event added/removed in sequence)
        /// </summary>
        public RecurringExceptions RecurringExceptions
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<RecurringExceptions>(this.TableEntity.RecurringExceptions);
                }
                catch
                {
                    return new RecurringExceptions();
                }
            }
            set
            {
                this.TableEntity.RecurringExceptions = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Who can see this event
        /// </summary>
        public PrivacyPolicy Visibility
        {
            get
            {
                return PrivacyPolicy.Deserialize(this.TableEntity.Visibility);
            }
            set
            {
                this.TableEntity.Visibility = value.Serialize();
            }
        }

        /// <summary>
        /// Who can attend this event
        /// </summary>
        public PrivacyPolicy Attendability
        {
            get
            {
                return PrivacyPolicy.Deserialize(this.TableEntity.Attendability);
            }
            set
            {
                this.TableEntity.Attendability = value.Serialize();
            }
        }

        /// <summary>
        /// A short description that will be displayed on list view
        /// </summary>
        public string DescriptionAbstract
        {
            get
            {
                return this.TableEntity.DescriptionAbstract;
            }
            set
            {
                this.TableEntity.DescriptionAbstract = value;
            }
        }

        /// <summary>
        /// Event description url stored in blob
        /// </summary>
        public string DescriptionBlobId
        {
            get
            {
                return this.TableEntity.DescriptionBlobId;
            }
            set
            {
                this.TableEntity.DescriptionBlobId = value;
            }
        }

        private ICollection<EventCategory> categories;

        /// <summary>
        /// Categories of the event in Json
        /// </summary>
        public ICollection<EventCategory> Categories
        {
            get
            {
                if (this.categories == null)
                {
                    try
                    {
                        this.categories = JsonConvert.DeserializeObject<ICollection<EventCategory>>(this.TableEntity.Categories);
                    }
                    catch
                    {
                        this.categories = new List<EventCategory>();
                    }
                }
                return this.categories;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Categories cannot be assigned to null");
                }
                this.categories = value;
                this.TableEntity.Categories = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// A list of keywords that describes the event
        /// </summary>
        public ICollection<string> SearchKeywords
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<ICollection<string>>(this.TableEntity.SearchKeywords);
                }
                catch
                {
                    return new List<string>();
                }
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Keywords cannot be assigned to null");
                }
                this.TableEntity.SearchKeywords = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// Maximum number of attendants
        /// </summary>
        public int MaximumAttendants
        {
            get
            {
                return this.TableEntity.MaximumAttendants;
            }
            set
            {
                this.TableEntity.MaximumAttendants = value;
            }
        }
        #endregion

        public string Type
        {
            get { return "event"; }
        }

        public IEnumerable<IndexString> IndexStrings
        {
            get
            {
                List<IndexString> result = new List<IndexString>();
                //result.Add(new IndexString("id", this.ID.ToString()));
                //result.Add(new IndexString("name", this.Name));
                //if (!String.IsNullOrEmpty(this.Address.ToString())) { result.Add(new IndexString("address", this.Address.ToString())); }
                //if (!String.IsNullOrEmpty(this.DescriptionAbstract)) { result.Add(new IndexString("description", this.DescriptionAbstract.ToString())); }
                return result;
            }
        }
    }
}
