﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// General interface for any data object
    /// </summary>
    public interface IInt64Id
    {
        long ID { get; }
    }
}
