﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Interface for any full scale entity object that contains sql and table data storage sub entities
    /// </summary>
    /// <typeparam name="S">Sql data entity type</typeparam>
    /// <typeparam name="T">Table data entity type</typeparam>
    public interface IFullEntity<S, T> : IInt64Id
    {
        /// <summary>
        /// Sql part of the entity
        /// </summary>
        S SqlEntity { get; }

        /// <summary>
        /// Table part of the entity
        /// </summary>
        T TableEntity { get; }
    }
}
