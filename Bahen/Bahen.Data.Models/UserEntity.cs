﻿using Bahen.Common;
using Bahen.Data.Models;
using Bahen.Search.Indexing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Contains all the information of a user
    /// </summary>
    public class UserEntity : IFullEntity<SqlUserEntity, TableUserEntity>, IIndexable
    {
        /// <summary>
        /// Construct a new user full entity by initializing two parts of the entity
        /// </summary>
        public UserEntity()
        {
            this.SqlEntity = new SqlUserEntity();
            this.TableEntity = new TableUserEntity();
        }

        /// <summary>
        /// Assemble a user full entity by passing the sql and table entity of the user (has to be same ID)
        /// </summary>
        /// <param name="sqlEntity">SqlUserEntity instance</param>
        /// <param name="tableEntity">TableUserEntity instance</param>
        /// <exception cref="ArgumentException">IDs of the sql and table entities do not match</exception>
        public UserEntity(SqlUserEntity sqlEntity, TableUserEntity tableEntity)
        {
            if (sqlEntity.ID != tableEntity.ID)
            {
                throw new ArgumentException(
                    String.Format("Cannot assemble SqlUserEntity ID: {0} and TableUserEntity ID: {1} because of different IDs.",
                    sqlEntity.ID, tableEntity.ID)
                    );
            }
            this.SqlEntity = sqlEntity;
            this.TableEntity = tableEntity;
        }

        /// <summary>
        /// Sql part of the entity
        /// </summary>
        public SqlUserEntity SqlEntity { get; private set; }

        /// <summary>
        /// Table part of the entity
        /// </summary>
        public TableUserEntity TableEntity { get; private set; }

        /// <summary>
        /// The primary key of the user
        /// </summary>
        public long UserID
        {
            get
            {
                this.TableEntity.UserID = this.SqlEntity.ID;
                return this.SqlEntity.ID;
            }
        }

        /// <summary>
        /// The primary key of the user, same as UserID
        /// </summary>
        public long ID
        {
            get
            {
                return this.UserID;
            }
        }

        public int TypeCode
        {
            get
            {
                return (int)EntityTypeCode.User;
            }
        }

        public string UniversalID
        {
            get
            {
                return EntityTypeCode.User.ToString() + this.ID.ToString();
            }
        }

        public Gender Gender
        {
            get
            {
                return (Gender)this.TableEntity.Gender;
            }
            set
            {
                this.TableEntity.Gender = (int)value;
            }
        }

        #region Security
        /// <summary>
        /// The login email of the user (unique key)
        /// </summary>
        public string LoginEmail
        {
            get
            {
                this.TableEntity.LoginEmail = this.SqlEntity.LoginEmail;
                return this.SqlEntity.LoginEmail;
            }
            set
            {
                if (value == null) throw new ArgumentException("User login email cannot be set to null");
                this.SqlEntity.LoginEmail = value;
                this.TableEntity.LoginEmail = value;
            }
        }

        /// <summary>
        /// The login password of the user (hashed by SHA512 algorithm)
        /// </summary>
        public byte[] LoginPassword
        {
            get
            {
                this.TableEntity.LoginPassword = this.SqlEntity.LoginPassword;
                return this.SqlEntity.LoginPassword;
            }
            set
            {
                if (value == null) throw new ArgumentException("User login password cannot be set to null");
                this.SqlEntity.LoginPassword = value;
                this.TableEntity.LoginPassword = value;
            }
        }

        /// <summary>
        /// Whether the user has confirmed the login email
        /// </summary>
        public bool LoginEmailConfirmed
        {
            get
            {
                return this.TableEntity.LoginEmailConfirmed;
            }
            set
            {
                this.TableEntity.LoginEmailConfirmed = value;
            }
        }

        /// <summary>
        /// Whether the user is created automatically (under invitation of other users)
        /// </summary>
        public bool UnderInvitation
        {
            get
            {
                return this.TableEntity.UnderInvitation;
            }
            set
            {
                this.TableEntity.UnderInvitation = value;
            }
        }

        #endregion

        #region Identification
        /// <summary>
        /// The given name or first name of the user
        /// </summary>
        public string FirstName
        {
            get
            {
                return this.TableEntity.FirstName;
            }
            set
            {
                this.TableEntity.FirstName = value;
            }
        }

        /// <summary>
        /// The middle name of the user
        /// </summary>
        public string MiddleName
        {
            get
            {
                return this.TableEntity.MiddleName;
            }
            set
            {
                this.TableEntity.MiddleName = value;
            }
        }

        /// <summary>
        /// Last name of the user
        /// </summary>
        public string LastName
        {
            get
            {
                return this.TableEntity.LastName;
            }
            set
            {
                this.TableEntity.LastName = value;
            }
        }

        /// <summary>
        /// Preferred name of the user (display name)
        /// </summary>
        public string PreferredName
        {
            get
            {
                return this.SqlEntity.PreferredName;
            }
            set
            {
                this.SqlEntity.PreferredName = value;
                this.TableEntity.PreferredName = value;
            }
        }

        ///// <summary>
        ///// The visibility of birthday
        ///// </summary>
        //public UserProfileVisibilityLevel BirthdayPrivacyLevel
        //{
        //    get
        //    {
        //        return (UserProfileVisibilityLevel)this.TableEntity.BirthdayPrivacyLevel;
        //    }
        //    set
        //    {
        //        this.TableEntity.BirthdayPrivacyLevel = (int)value;
        //    }
        //}

        /// <summary>
        /// The birthday of the user
        /// </summary>
        public DateTime Birthday
        {
            get
            {
                return this.TableEntity.Birthday;
            }
            set
            {
                this.TableEntity.Birthday = value;
            }
        }
        #endregion

        #region Contact
        private ICollection<Address> address;

        /// <summary>
        /// The addresses of the user
        /// </summary>
        public ICollection<Address> Address
        {
            get
            {
                if (this.address == null)
                {
                    try
                    {
                        this.address = JsonConvert.DeserializeObject<ICollection<Address>>(this.TableEntity.Addresses);
                    }
                    catch
                    {
                        this.address = new List<Address>();
                    }
                }
                return this.address;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Addresses cannot be assigned to null");
                }
                this.address = value;
                this.TableEntity.Addresses = JsonConvert.SerializeObject(value);
            }
        }

        private ICollection<PhoneNumber> phoneNumbers;

        /// <summary>
        /// The phone numbers of the user
        /// </summary>
        public ICollection<PhoneNumber> PhoneNumbers
        {
            get
            {
                if (this.phoneNumbers == null)
                {
                    try
                    {
                        this.phoneNumbers = JsonConvert.DeserializeObject<ICollection<PhoneNumber>>(this.TableEntity.PhoneNumbers);
                    }
                    catch
                    {
                        this.phoneNumbers = new List<PhoneNumber>();
                    }
                }
                return this.phoneNumbers;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Phone numbers cannot be assigned to null");
                }
                this.phoneNumbers = value;
                this.TableEntity.PhoneNumbers = JsonConvert.SerializeObject(value);
            }
        }

        private ICollection<EmailAddress> emailAddresses;

        /// <summary>
        /// The email addresses of the user
        /// </summary>
        public ICollection<EmailAddress> EmailAddresses
        {
            get
            {
                if (this.emailAddresses == null)
                {
                    try
                    {
                        this.emailAddresses = JsonConvert.DeserializeObject<ICollection<EmailAddress>>(this.TableEntity.EmailAddresses);
                    }
                    catch
                    {
                        this.emailAddresses = new List<EmailAddress>();
                    }
                }
                return this.emailAddresses;
            }
            set
            {
                if (value == null)
                {
                    throw new NullReferenceException("Email addresses cannot be assigned to null");
                }
                this.emailAddresses = value;
                this.TableEntity.EmailAddresses = JsonConvert.SerializeObject(value);
            }
        }
        #endregion

        #region Policies

        /// <summary>
        /// The visibility of the profile of the user
        /// </summary>
        public PrivacyPolicy ProfileVisibilityPolicy
        {
            get
            {
                try
                {
                    return PrivacyPolicy.Deserialize(this.TableEntity.ProfileVisibilityPolicy);
                }
                catch
                {
                    PrivacyPolicy p = new PrivacyPolicy(true);
                    this.TableEntity.ProfileVisibilityPolicy = p.Serialize();
                    return p;
                }
            }
            set
            {
                this.TableEntity.ProfileVisibilityPolicy = value.Serialize();
            }
        }

        /// <summary>
        /// Subscription policy
        /// </summary>
        public PrivacyPolicy SubscriptionPolicy
        {
            get
            {
                try
                {
                    return PrivacyPolicy.Deserialize(this.TableEntity.SubscriptionPolicy);
                }
                catch
                {
                    PrivacyPolicy p = new PrivacyPolicy(true);
                    this.TableEntity.SubscriptionPolicy = p.Serialize();
                    return p;
                }
            }
            set
            {
                this.TableEntity.SubscriptionPolicy = value.Serialize();
            }
        }

        /// <summary>
        /// Can see the email addresses.
        /// For now, all emails are protected at the same level.
        /// For future, maybe different levels for each email address
        /// </summary>
        public PrivacyPolicy EmailVisibilityPolicy
        {
            get
            {
                try
                {
                    return PrivacyPolicy.Deserialize(this.TableEntity.EmailVisibilityPolicy);
                }
                catch
                {
                    PrivacyPolicy p = new PrivacyPolicy(true);
                    this.TableEntity.EmailVisibilityPolicy = p.Serialize();
                    return p;
                }
            }
            set
            {
                this.TableEntity.EmailVisibilityPolicy = value.Serialize();
            }
        }
        #endregion

        #region Miscellaneous
        private ICollection<Education> educations;

        /// <summary>
        /// The education history of the user
        /// </summary>
        public ICollection<Education> Educations
        {
            get
            {
                if (this.educations == null)
                {
                    try
                    {
                        this.educations = JsonConvert.DeserializeObject<ICollection<Education>>(this.TableEntity.Educations);
                    }
                    catch
                    {
                        this.educations = new List<Education>();
                    }
                }
                return this.educations;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Educations cannot be assigned to null");
                }
                this.educations = value;
                this.TableEntity.Educations = JsonConvert.SerializeObject(value);
            }
        }

        private ICollection<WorkPlace> workPlaces;

        /// <summary>
        /// The workplace history of the user
        /// </summary>
        public ICollection<WorkPlace> WorkPlaces
        {
            get
            {
                if (this.workPlaces == null)
                {
                    try
                    {
                        this.workPlaces = JsonConvert.DeserializeObject<ICollection<WorkPlace>>(this.TableEntity.WorkPlaces);
                    }
                    catch
                    {
                        this.workPlaces = new List<WorkPlace>();
                    }
                }
                return this.workPlaces;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Work places cannot be assigned to null");
                }
                this.workPlaces = value;
                this.TableEntity.WorkPlaces = JsonConvert.SerializeObject(value);
            }
        }

        /// <summary>
        /// The url of the profile
        /// </summary>
        public string Alias
        {
            get
            {
                Guid guid;
                if (Guid.TryParse(this.SqlEntity.Alias, out guid))
                {
                    return null;
                }
                return this.SqlEntity.Alias;
            }
            set
            {
                this.SqlEntity.Alias = value;
                this.TableEntity.Alias = value;
            }
        }

        /// <summary>
        /// The url of the profile picture
        /// </summary>
        public string ProfilePicture
        {
            get
            {
                return this.SqlEntity.Picture;
            }
            set
            {
                this.SqlEntity.Picture = value;
                this.TableEntity.ProfilePicture = value;
            }
        }

        private CultureInfo culture;

        /// <summary>
        /// The display language, calendar, etc., culture info of the user
        /// </summary>
        public CultureInfo Culture
        {
            get
            {
                if (this.culture == null)
                {
                    if (this.TableEntity.Culture != null)
                    {
                        this.culture = new CultureInfo(this.TableEntity.Culture);
                    }
                    else
                    {
                        this.culture = CultureInfo.CurrentCulture;
                    }
                }
                return this.culture;
            }
            set
            {
                this.culture = value;
                this.TableEntity.Culture = value.ToString();
            }
        }

        /// <summary>
        /// The user access group of the user
        /// </summary>
        public UserRoleLevel UserGroup
        {
            get
            {
                return (UserRoleLevel)this.TableEntity.UserGroup;
            }
            set
            {
                this.TableEntity.UserGroup = (int)value;
            }
        }
        #endregion

        /// <summary>
        /// Cast the full user entity into simplified IEntity
        /// </summary>
        /// <typeparam name="X">Type of the simplified IEntity</typeparam>
        /// <returns>The casted entity</returns>
        /// <exception cref="InvalidCastException">Cannot perform the cast</exception>
        public X Cast<X>() where X : class, IInt64Id, new()
        {
            // If dirrectly are sql or table types then return them as property
            if (typeof(X) == typeof(SqlUserEntity))
            {
                return this.SqlEntity as X;
            }
            else if (typeof(X) == typeof(TableUserEntity))
            {
                return this.TableEntity as X;
            }
            else
            {
                X result = new X();
                PropertyInfo[] properties = typeof(X).GetProperties();
                for (int i = 0; i < properties.Length; i++)
                {
                    PropertyInfo property = properties[i];
                    PropertyInfo matched = this.GetType().GetProperty(property.Name);

                    // Cannot find the property with the same name in the full entity
                    if (matched == null)
                    {
                        throw new InvalidCastException(String.Format("Cannot cast {0} into {1} because the property {2} in {1} was not found in {0}.", this.GetType().Name, typeof(X).Name, property.Name));
                    }

                    // Found the entity with the same name but the type does not match (trying to be picky here)
                    if (matched.GetType() != property.GetType())
                    {
                        throw new InvalidCastException(String.Format("Cannot cast {0} into {1} because property {2} in {1} has type {3} does not match type {4} in {0}.", this.GetType().Name, typeof(X).Name, property.Name, property.GetType().Name, matched.GetType().Name));
                    }
                    //MethodInfo setMethod = property.GetSetMethod();
                    property.SetValue(result, matched.GetGetMethod().Invoke(this, null), null);
                }
                return result;
            }
        }

        #region IIndexable Implementation
        public string Type
        {
            get { return "user"; }
        }

        /// <summary>
        /// Get all the indexable keyword-values of the entity
        /// </summary>
        public IEnumerable<IndexString> IndexStrings
        {
            get
            {
                List<IndexString> result = new List<IndexString>();
                result.Add(new IndexString("id", this.ID.ToString()));
                result.Add(new IndexString("email", this.LoginEmail, false));
                result.Add(new IndexString("name", this.FirstName));
                result.Add(new IndexString("name", this.PreferredName));
                if (String.IsNullOrEmpty(this.LastName)) { result.Add(new IndexString("name", this.LastName)); }
                if (String.IsNullOrEmpty(this.MiddleName)) { result.Add(new IndexString("name", this.MiddleName)); }
                return result;
            }
        }
        #endregion
    }
}
