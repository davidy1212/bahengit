﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    /// <summary>
    /// Represent a datamodel for a message generated after a data operation
    /// </summary>
    [Serializable]
    public class DataOperationMessage : ISerializable
    {
        /// <summary>
        /// Construct a new DataOperationMessage
        /// </summary>
        /// <param name="operation">Type of data operation</param>
        /// <param name="entity">ID of the entity to operate on</param>
        public DataOperationMessage(IInt64Id entity,DataOperation operation)
        {
            this.Operation = operation;
            this.EntityID = entity.ID;
            this.EntityType = entity.GetType().Name;
            this.Timestamp = DateTime.Now;
        }

        /// <summary>
        /// Type of the data entity
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// ID of the entity to operate on
        /// </summary>
        public long EntityID { get; set; }

        /// <summary>
        /// Type of data operation
        /// </summary>
        public DataOperation Operation { get; set; }

        /// <summary>
        /// Timestamp of the data operation
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// ISerializable implementation
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Operation", this.Operation, typeof(DataOperation));
            info.AddValue("EntityType", this.EntityType, typeof(string));
            info.AddValue("EntityID", this.EntityID, typeof(long));
            info.AddValue("Timestamp", this.Timestamp, typeof(DateTime));
        }

        /// <summary>
        /// Sepecial constructor used to deserialize values
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public DataOperationMessage(SerializationInfo info, StreamingContext context)
        {
            this.Operation = (DataOperation)info.GetValue("Operation", typeof(DataOperation));
            this.EntityType = (string)info.GetValue("EntityType", typeof(string));
            this.EntityID = (long)info.GetValue("EntityID", typeof(long));
            this.Timestamp = (DateTime)info.GetValue("Timestamp", typeof(DateTime));
        }
    }
}
