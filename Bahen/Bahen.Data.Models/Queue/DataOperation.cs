﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bahen.Data.Models
{
    public enum DataOperation
    {
        Insert,
        Update,
        Remove
    }
}
