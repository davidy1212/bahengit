use Griddy;

create table Users
(
UserID bigint not null primary key identity,
Alias nvarchar(50) not null unique,
LoginEmail nvarchar(100) not null unique,
LoginPassword binary(64) not null,
PreferredName nvarchar(100) not null,
Picture nvarchar(300),
Timestamp timestamp not null,

)

create table Subscriptions
(

SubscriptionID bigint not null primary key identity,
HostID bigint not null foreign key references Users(UserID),
SubscriberID bigint not null foreign key references Users(UserID),
Timestamp timestamp not null

)

create table Events
(

EventID bigint not null primary key identity,
PublisherID bigint not null foreign key references Users(UserID),
Alias nvarchar(50) not null unique,
Name nvarchar(200) not null,
Picture nvarchar(300),
StartTime datetime2 not null,
EndTime datetime2 not null,
Location nvarchar(50),
Timestamp timestamp not null

)

create table Attendances
(

AttendanceID bigint not null primary key identity,
UserID bigint not null foreign key references Users(UserID),
AttendanceStatus int not null,
EventID bigint not null foreign key references Events(EventID),
Timestamp timestamp not null

)

create table Cycles
(

CycleID bigint not null primary key identity,
Name nvarchar(50) not null,
Picture nvarchar(300),
Alias nvarchar(50) not null unique,
Timestamp timestamp not null

)

create table CycleEvents
(

CycleEventID bigint not null primary key identity,
CycleID bigint not null foreign key references Cycles(CycleID),
EventID bigint not null foreign key references Events(EventID),
Timestamp timestamp not null

)

create table CycleUsers
(

CycleUserID bigint not null primary key identity,
CycleID bigint not null foreign key references Cycles(CycleID),
UserID bigint not null foreign key references Users(UserID),
MembershipType int not null,
Timestamp timestamp not null

)

create table Cookies
(

CookieID uniqueidentifier not null primary key,
UserID bigint not null foreign key references Users(UserID),
UserAgent nvarchar(1000) not null,
TimeLastLogin datetime not null,
Timestamp timestamp not null

)

create table ApiClients
(

ClientID bigint not null primary key identity,
DeveloperID bigint not null foreign key references Users(UserID),
ClientSecret uniqueidentifier not null,
Name nvarchar(50) not null,
PictureUrl nvarchar(200),
TrustLevel int not null,
Timestamp timestamp not null

)

create table ApiAuth
(

AuthID bigint not null primary key identity,
UserID bigint not null foreign key references Users(UserID),
ClientID bigint not null foreign key references ApiClients(ClientID),
Scopes bigint not null,
RedirectUri nvarchar(200),
Requested bit not null,
Timestamp timestamp not null

)

--create table EmailConfirmations
--(

--ConfirmationID uniqueidentifier not null primary key,
--UserID bigint not null foreign key references Users(UserID),
--ConfirmationKey binary(64) not null,
--ConfirmationType int not null,
--EmailAddress nvarchar(100) not null,
--ExpirationDate datetime2 not null,
--TimeCreated timestamp not null,
--HasConfirmed bit not null

--)
