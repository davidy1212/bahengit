USE Griddy;

delete from Cookies where UserID!=0
delete from Attendances where AttendanceID>=0
delete from CycleEvents where CycleEventID>=0
delete from CycleUsers where CycleUserID>=0
delete from Subscriptions where SubscriptionID>=0
delete from Cycles where CycleID>=0
delete from Events where EventID>=0
delete from Users where UserID>=0