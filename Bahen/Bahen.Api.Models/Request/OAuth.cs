﻿using Newtonsoft.Json;

namespace Bahen.Api.Models
{
    public class OAuthAccessTokenRequest
    {
        public string Code { get; set; }

        [JsonProperty("Client_id")]
        public long ClientID { get; set; }

        [JsonProperty("Client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        [JsonProperty("Grant_type")]
        public string GrantType { get; set; }

        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("Email")]
        public string LoginEmail { get; set; }

        [JsonProperty("Password")]
        public string LoginPassword { get; set; }
    }
    public class OAuthAuthorizeRequest
    {
        [JsonProperty("Client_id")]
        public long ClientId { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        public string Scope { get; set; }

        [JsonProperty("Response_type")]
        public string ResponseType { get; set; }
    }
    public class OAuthRefreshTokenRequest
    {
        [JsonProperty("Refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("Redirect_uri")]
        public string RedirectUri { get; set; }

        [JsonProperty("Client_id")]
        public long ClientID { get; set; }

        [JsonProperty("Client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("Grant_type")]
        public string GrantType { get; set; }
    }
    public class OAuthRegisterRequest
    {
        public string Name { get; set; }
    }
}