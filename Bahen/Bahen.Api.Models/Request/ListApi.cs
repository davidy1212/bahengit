﻿using Bahen.Data.Operation;
using System;

namespace Bahen.Api.Models
{
    public class ListApiOptions : PagingOptions
    {
        /// <summary>
        /// Whether output summary in response
        /// </summary>
        public bool Summary { get; set; }

        /// <summary>
        /// Original uri of request (for getting pagination url)
        /// </summary>
        public Uri RequestUri { get; set; }
    }
}