﻿using System;
using System.Collections.Generic;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;

namespace Bahen.Api.Models
{
    /// <summary>
    /// Filter of the response properties
    /// </summary>
    public class FieldsFilter
    {
        private HashSet<string> Set { get; set; }

        public FieldsFilter()
        {
            this.Set = new HashSet<string>();
        }

        public FieldsFilter(string filter)
            : this()
        {
            if (filter != null)
            {
                this.Set.UnionWith(filter.ToLowerInvariant().Split(','));
            }
        }

        public bool Contains(string fields)
        {
            return this.Set.Contains(fields.ToLowerInvariant());
        }
    }
}