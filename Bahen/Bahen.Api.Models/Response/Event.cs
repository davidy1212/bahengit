﻿using Bahen.Data.Models;
using Newtonsoft.Json;
using System;

namespace Bahen.Api.Models
{
    public class EventBrief : IInt64Id
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; }
        public AttendanceStatus RsvpStatus { get; set; }

        [JsonIgnore]
        public long AttendanceID { get; set; }
    }

    public class EventDetail :  IInt64Id
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Location { get; set; }
        public Attendant Publisher { get; set; }
        public Attendant[] CoPublishers { get; set; }
        public string Picture { get; set; }
        public string Alias { get; set; }
        public Address Address { get; set; }
        public GeoCoordinate Coordinate { get; set; }
        public bool IsDayEvent { get; set; }
        public bool IsRecurring { get; set; }
        public EventRecurringType? RecurringType { get; set; }
        public int? RecurringTimes { get; set; }
        public RecurringExceptions RecurringExceptions { get; set; }
        public string BriefDescription { get; set; }
        public string FullDescription { get; set; }
        public EventCategory[] Categories { get; set; }
        public string[] Keywords { get; set; }
        public PrivacyPolicy Visibility { get; set; }
        public PrivacyPolicy Attendability { get; set; }
    }
}