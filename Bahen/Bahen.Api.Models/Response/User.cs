﻿using Bahen.Data.Models;
using Newtonsoft.Json;

namespace Bahen.Api.Models
{
    /// <summary>
    /// User output data model (detail)
    /// </summary>
    public class UserDetail : IInt64Id
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public string Gender { get; set; }
        public string Link { get; set; }
    }

    public class Friend : IInt64Id
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public long SubscriptionID { get; set; }
    }

    /// <summary>
    /// Simple output data type representing an attendant to the event
    /// </summary>
    public class Attendant : IInt64Id
    {
        public string Name { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public long ID { get; set; }

        public AttendanceStatus RsvpStatus { get; set; }

        [JsonIgnore]
        public long AttendanceID { get; set; }
    }

    /// <summary>
    /// Full summary structure counting each category in the response list
    /// </summary>
    public class AttendanceListSummary : CountSummary
    {
        public int NoreplyCount { get; set; }
        public int MaybeCount { get; set; }
        public int InterestedCount { get; set; }
        public int DeclinedCount { get; set; }
        public int AttendingCount { get; set; }
    }
}