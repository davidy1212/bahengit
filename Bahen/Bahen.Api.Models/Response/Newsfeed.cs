﻿using Bahen.Data.Models;
using Newtonsoft.Json;

namespace Bahen.Api.Models
{
    public class Newsfeed : IStringId
    {
        [JsonProperty("Id")]
        public string StringId { get; set; }
        public string Content { get; set; }
        public NewsfeedSender From { get; set; }
        [JsonProperty("Time")]
        public long TimeSent { get; set; }
    }

    public class NewsfeedSender
    {
        [JsonProperty("Id")]
        public long ID { get; set; }
        public string Name { get; set; }
    }
}