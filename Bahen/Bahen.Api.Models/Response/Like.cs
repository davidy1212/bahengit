﻿using Bahen.Data.Models;
using Newtonsoft.Json;

namespace Bahen.Api.Models
{
    /// <summary>
    /// Simple output data type representing an user who likes an entity
    /// </summary>
    public class Like : IStringId
    {
        [JsonProperty(PropertyName = "Name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public long UserID { get; set; }

        //[JsonProperty(PropertyName = "Pic")]
        //public string ProfilePic { get; set; }

        [JsonIgnore]
        public string StringId { get; set; }
    }
}