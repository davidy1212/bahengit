﻿using Bahen.Common;
using Newtonsoft.Json;
using System;

namespace Bahen.Api.Models
{
    public class AccessToken
    {
        [JsonProperty("exp"), JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTime Expires { get; set; }

        [JsonProperty("uid")]
        public long UserId { get; set; }

        [JsonProperty("cid")]
        public long ClientId { get; set; }

        [JsonProperty("sco")]
        public long Scope { get; set; }

        [JsonProperty("rol")]
        public long Role { get; set; }

        [JsonProperty("typ")]
        public string Type { get; set; }
    }
}