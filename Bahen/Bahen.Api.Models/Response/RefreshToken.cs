﻿using Newtonsoft.Json;

namespace Bahen.Api.Models
{
    public class RefreshToken
    {
        [JsonProperty("aid")]
        public long AuthId{get;set;}

        [JsonProperty("cid")]
        public long ClientId { get; set; }

        [JsonProperty("uid")]
        public long UserId { get; set; }

        [JsonProperty("typ")]
        public string Type { get; set; }
    }
}