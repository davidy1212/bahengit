using Bahen.Data.Models;
using Newtonsoft.Json;
using System;

namespace Bahen.Api.Models
{
    /// <summary>
    /// Simple output data type representing an user who comments an entity
    /// </summary>
    public class Comment : IStringId
    {
        [JsonProperty(PropertyName = "Name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public long UserID { get; set; }

        //[JsonProperty(PropertyName = "Pic")]
        //public string ProfilePic { get; set; }

        [JsonProperty(PropertyName = "Content")]
        public String Content { get; set; }

        [JsonProperty(PropertyName = "SId")]
        public string StringId { get; set; }
    }
}