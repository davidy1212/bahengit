﻿
namespace Bahen.Api.Models
{
    public class PureTextMessageResponse
    {
        public PureTextMessageResponse()
        {
        }

        public PureTextMessageResponse(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}