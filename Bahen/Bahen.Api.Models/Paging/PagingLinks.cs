﻿using Bahen.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Bahen.Api.Models
{
    /// <summary>
    /// Options when generating paging links
    /// </summary>
    public class PagingLinks
    {
        [JsonIgnore]
        public Uri OriginalUri { get; set; }
        [JsonIgnore]
        public string AppendingParameters { get; set; }
        [JsonIgnore]
        public string PreviousPageCursor { get; set; }
        [JsonIgnore]
        public string NextPageCursor { get; set; }
        [JsonIgnore]
        private NameValueCollection Query { get; set; }

        /// <summary>
        /// Construct paging links options with a root url
        /// and parameter names for previous page and next page
        /// and appending parameters
        /// </summary>
        /// <param name="root">Root url</param>
        /// <param name="append">Appending parameters to the root url</param>
        /// <param name="prevParam">Parameter name for previous page</param>
        /// <param name="nextParam">Parameter name for next page</param>
        public PagingLinks(
            Uri originalUri,
            string prevCursor = null,
            string nextCursor = null)
        {
            this.OriginalUri = originalUri;
            this.Query = HttpUtility.ParseQueryString(originalUri.Query);
            this.PreviousPageCursor = prevCursor;
            this.NextPageCursor = nextCursor;
        }

        /// <summary>
        /// Get paging links based on the list result.
        /// If list result contains element, then both previous and next page links will be stamped
        /// based on the first and last element ID, otherwise null pointer will be returned.
        /// The link generated does not guarantee there are elements in the previous/next page.
        /// If user tries to redirect to the paging links, he/she might retrieve empty result,
        /// Meaning the end or the beginning of the data.
        /// </summary>
        /// <param name="originalUri">Original Uri</param>
        /// <param name="data">List result</param>
        /// <param name="keySelector">Select paging cursor</param>
        /// <returns></returns>
        public static PagingLinks GetInt64PagingLinks<T>(
            Uri originalUri,
            IList<T> data,
            Func<T, long> keySelector)
        {
            if (data.FirstOrDefault() == null)
            {
                return null;
            }
            else
            {
                return new PagingLinks(
                    originalUri,
                    HttpServerUtility.UrlTokenEncode(keySelector(data.First()).GetBytes()),
                    HttpServerUtility.UrlTokenEncode(keySelector(data.Last()).GetBytes()));
            }
        }

        public string Previous
        {
            get
            {
                if (this.PreviousPageCursor != null)
                {
                    return this.ResetQuery("before", this.PreviousPageCursor);
                }
                else
                {
                    return null;
                }
            }
        }

        public string Next
        {
            get
            {
                if (this.NextPageCursor != null)
                {
                    return this.ResetQuery("after", this.NextPageCursor);
                }
                else
                {
                    return null;
                }
            }
        }

        private string ResetQuery(string param, string cursor)
        {
            this.Query.Remove("after");
            this.Query.Remove("before");
            this.Query.Remove("offset");
            this.Query.Add(param, cursor);
            return String.Format("{0}?{1}", this.OriginalUri.AbsoluteUri.Split('?')[0], this.Query.ToString());
        }
    }
}